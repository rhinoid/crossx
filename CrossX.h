#ifndef _H_CROSSX
#define _H_CROSSX

/////////////////////////////////////////////////////
// CROSSX.H
//
////
//
// Global defines for the CrossX porting platform
//
/////////////////////////////////////////////////////



//EMULATION DEFINES
//Make sure one of these defines is defined in order to properly emulate the device on your PC

//#define CROSSX_SYMBIAN60
//#define CROSSX_SYMBIANUIQ
#define CROSSX_POCKETPC

//Choose one of the next defines to select if the screen should be rotated or not
#define CROSSX_NOROTATION
//#define CROSSX_ROTATION90
//#define CROSSX_ROTATION180
//#define CROSSX_ROTATION270



/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// END OF CONFIGURATION SPACE... DON'T EDIT FOLLOWING LINES
/////////////////////////////////////////////////////////////////////////////////////////////////////////////////









/////////////////////////////////
// KEY DEFINITIONS

enum CROSSX_KEY
{
	CROSSX_UNKNOWN = -1,
	CROSSX_KEY_FIRE=1,
	CROSSX_KEY_LEFTMENU,
	CROSSX_KEY_RIGHTMENU,
	CROSSX_KEY_UP,
	CROSSX_KEY_DOWN,
	CROSSX_KEY_LEFT,
	CROSSX_KEY_RIGHT,
	CROSSX_KEY_NUM0 = 0x30,
	CROSSX_KEY_NUM1,
	CROSSX_KEY_NUM2,
	CROSSX_KEY_NUM3,
	CROSSX_KEY_NUM4,
	CROSSX_KEY_NUM5,
	CROSSX_KEY_NUM6,
	CROSSX_KEY_NUM7,
	CROSSX_KEY_NUM8,
	CROSSX_KEY_NUM9,
};



/////////////////////////////////
// all includes needed for CrossX
#include "2d/surface16.h"
#include "cgame.h"

#include "TestGame.h"			 // include here your own game derived class
#define CROSSX_GAMECLASS TestGame    // and define it



#endif
