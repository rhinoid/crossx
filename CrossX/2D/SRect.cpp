#include "2d/SRect.h"

SRect::SRect(int x, int y, int x2, int y2)
{
	SetRect(x, y, x2, y2);
}

SRect::SRect()
{
	SetRectEmpty();
}

SRect::~SRect()
{
}

int SRect::UnionRect(SRect *a, SRect *b)
{
	if(a->Width()==0 || a->Height()==0)
	{
		if(b->Width()==0 || b->Height()==0)
		{
			SetRectEmpty();
			return 0;
		}
		m_Left = b->m_Left;
		m_Right = b->m_Right;
		m_Top = b->m_Top;
		m_Bottom = b->m_Bottom;
		return 1;
	}
	if(b->Width()==0 || b->Height()==0)
	{
		m_Left = a->m_Left;
		m_Right = a->m_Right;
		m_Top = a->m_Top;
		m_Bottom = a->m_Bottom;
		return 1;
	}

	m_Left = (b->m_Left < a->m_Left) ? b->m_Left : a->m_Left;
	m_Top = (b->m_Top < a->m_Top) ? b->m_Top : a->m_Top;
	m_Right = (b->m_Right > a->m_Right) ? b->m_Right : a->m_Right;
	m_Bottom = (b->m_Bottom > a->m_Bottom) ? b->m_Bottom : a->m_Bottom;
	return 1;
}

int SRect::IntersectRect(SRect *a, SRect *b)
{
	if(a->m_Right < b->m_Left)
	{
		SetRectEmpty();
		return 0;
	}
	if(a->m_Bottom < b->m_Top)
	{
		SetRectEmpty();
		return 0;
	}
	if(a->m_Left > b->m_Right)
	{
		SetRectEmpty();
		return 0;
	}
	if(a->m_Top > b->m_Bottom)
	{
		SetRectEmpty();
		return 0;
	}

	m_Left = (a->m_Left < b->m_Left) ? b->m_Left : a->m_Left;
	m_Right = (a->m_Right > b->m_Right) ? b->m_Right : a->m_Right;
	m_Top = (a->m_Top < b->m_Top) ? b->m_Top : a->m_Top;
	m_Bottom = (a->m_Bottom > b->m_Bottom) ? b->m_Bottom : a->m_Bottom;
	return 1;
}

void SRect::SetRectEmpty()
{
	m_Left = m_Right = m_Top = m_Bottom = 0;
}

void SRect::SetRect(int x, int y, int x2, int y2)
{
	m_Left = x;
	m_Top = y;
	m_Right = x2;
	m_Bottom = y2;
}

int SRect::Width()
{
	return m_Right-m_Left;
}

int SRect::Height()
{
	return m_Bottom-m_Top;
}