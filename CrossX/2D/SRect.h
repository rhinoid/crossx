#ifndef _H_SRECT
#define _H_SRECT

class SRect
{
public:
	SRect(int x, int y, int x2, int y2);
	SRect();
	~SRect();

	int Width();
	int Height();
	void SetRectEmpty();
	void SetRect(int x, int y, int x2, int y2);
	int UnionRect(SRect *a, SRect *b);
	int IntersectRect(SRect *a, SRect *b);

	int	m_Left;
	int	m_Right;
	int	m_Top;
	int	m_Bottom;
};

#endif
