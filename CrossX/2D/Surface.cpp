#include "surface.h"
#include "utils/debug.h"
#include "2d/Surface16.h"
#include "2d/Surface32.h"


CSurface *CSurface::Factory(SURFACEMODE SurfaceMode)
{
	if(SurfaceMode == MODE_16BIT)
	{
		return new CSurface16();
	}
	else
	{
		return new CSurface32();
	}
	return 0;
}

CSurface::CSurface()
{
	m_ColorKey = 0xff00ff;
}


CSurface::~CSurface()
{
}


int			CSurface::GetWidth()
{
	return m_Width;
}


int			CSurface::GetHeight()
{
	return m_Height;
}


SURFACEMODE	CSurface::GetMode()
{
	return m_Mode;
}

bool CSurface::ClipCoords(int &x, int &y, int &w, int &h)
{
	//trivial reject
	if(x+w<m_ClipRect.m_Left) return false;	//out of bounds
	if(x>=m_ClipRect.m_Right) return false;	//out of bounds
	if(y+h<m_ClipRect.m_Top) return false;	//out of bounds
	if(y>=m_ClipRect.m_Bottom) return false;	//out of bounds

	//clipping
	if(x<m_ClipRect.m_Left)
	{
		w -= (m_ClipRect.m_Left-x);
		x = m_ClipRect.m_Left;
	}
	if((x+w)>=m_ClipRect.m_Right)
	{
		w -= ((x+w)-m_ClipRect.m_Right);
	}

	if(y<m_ClipRect.m_Top)
	{
		h -= (m_ClipRect.m_Top-y);
		y = m_ClipRect.m_Top;
	}
	if((y+h)>=m_ClipRect.m_Bottom)
	{
		h -= ((y+h)-m_ClipRect.m_Bottom);
	}

	return true;
}

bool CSurface::ClipCoords2(int &sx, int &sy, int &x, int &y, int &w, int &h)
{
	//trivial reject
	if(x+w<m_ClipRect.m_Left) return false;	//out of bounds
	if(x>=m_ClipRect.m_Right) return false;	//out of bounds
	if(y+h<m_ClipRect.m_Top) return false;	//out of bounds
	if(y>=m_ClipRect.m_Bottom) return false;	//out of bounds

	//clipping
	if(x<m_ClipRect.m_Left)
	{
		w -= (m_ClipRect.m_Left-x);
		sx += (m_ClipRect.m_Left-x);
		x = m_ClipRect.m_Left;
	}
	if((x+w)>=m_ClipRect.m_Right)
	{
		w -= ((x+w)-m_ClipRect.m_Right);
	}

	if(y<m_ClipRect.m_Top)
	{
		h -= (m_ClipRect.m_Top-y);
		sy += (m_ClipRect.m_Top-y);
		y = m_ClipRect.m_Top;
	}
	if((y+h)>=m_ClipRect.m_Bottom)
	{
		h -= ((y+h)-m_ClipRect.m_Bottom);
	}

	return true;
}

void    CSurface::RemoveClipping()
{
	m_ClipRect.SetRect(0, 0, m_Width, m_Height);
}

SRect CSurface::GetClipping()
{
	return m_ClipRect;
}

void    CSurface::SetClipping(SRect ClipRect)
{
	// accept new clipping rect
	m_ClipRect = ClipRect;

	//Clip clippingrect against surface boundary
	if(m_ClipRect.m_Left   < 0) m_ClipRect.m_Left = 0;
	if(m_ClipRect.m_Top    < 0) m_ClipRect.m_Top  = 0;
	if(m_ClipRect.m_Right  > m_Width)  m_ClipRect.m_Right  = m_Width;
	if(m_ClipRect.m_Bottom > m_Height) m_ClipRect.m_Bottom = m_Height;
}

CROSSX_COLOR32		CSurface::GetColorKey(void)
{
	return m_ColorKey;
}

void CSurface::SetColorKey(CROSSX_COLOR32 Color)
{
	m_ColorKey = Color;
}


void 	CSurface::ClrColorKey(void)
{
	m_ColorKey = -1;
}
