#ifndef _H_SURFACE
#define _H_SURFACE

#include "utils/types.h"
#include "utils/xstring.h"
#include "2d/SRect.h"

typedef int CROSSX_COLOR32;    // Representation of a 32 bit rgb color
typedef int CROSSX_COLOR16;    // Representation of a 16 bit rgb color


#define RGB565_BLUE  (0x001f)
#define RGB565_GREEN (0x07e0)
#define RGB565_RED   (0xf800)

#define RGB565_BLUESHIFT  (0)
#define RGB565_GREENSHIFT (5)
#define RGB565_REDSHIFT   (11)

#define RGB444_BLUE  (0x000f)
#define RGB444_GREEN (0x00f0)
#define RGB444_RED   (0x0f00)

#define RGB444_BLUESHIFT  (0)
#define RGB444_GREENSHIFT (4)
#define RGB444_REDSHIFT   (8)

#define RGB888_BLUE  (0x000000ff)
#define RGB888_GREEN (0x0000ff00)
#define RGB888_RED   (0x00ff0000)
#define RGB888_ALPHA (0xff000000)

#define RGB888_BLUESHIFT  (0)
#define RGB888_GREENSHIFT (8)
#define RGB888_REDSHIFT   (16)
#define RGB888_ALPHASHIFT (24)


enum SURFACEMODE
{
	MODE_16BIT = 0,
	MODE_32BIT
};

class CSurface   // base class for 16 and 32 bit versions
{
public:
	static CSurface *CSurface::Factory(SURFACEMODE SurfaceMode);

	// Constructors/Destructor
public:
	CSurface();
	virtual            ~CSurface() = 0;

	virtual void		Destroy()=0;
	virtual bool		Create(int Width, int Height)=0;
	virtual	bool        Create(void *ExternalBuffer, int Width, int Height)=0;
	virtual int			Load(XString Filename)=0;

	// Properties
	virtual void		*GetBuffer()=0;
	virtual void		SetBuffer(void *ExternalBuffer)=0;   //only works with external surfaces

	int					GetWidth();
	int					GetHeight();
	SURFACEMODE			GetMode();
	void				SetColorKey(CROSSX_COLOR32 color);
	void				ClrColorKey(void);
	CROSSX_COLOR32		GetColorKey(void);

	// Operations
	virtual void		Clear(int x, int y, int w, int h, int Color)=0;
	virtual void		Clear(int x, int y, int w, int h, int Color, int Transparency)=0;
	virtual void		Blit(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency)=0;
	virtual void		BlitNoAlpha(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency)=0;
	virtual void		BlitColor(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency, int Color)=0;	// if a bit is not transparent, it is colored into the supplied color (used for blitting charactersets in any color)
	virtual void		BlitColorKey(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency)=0;
	virtual void		PlotPix(int x, int y, int Color, int Transparency)=0;

	virtual void		ClearGradient(int x, int y, int w, int h, int ColorTL, int ColorTR, int ColorBL, int ColorBR, int Transparency)=0;

	// Clipping
	void				RemoveClipping();
	SRect				GetClipping();
	void				SetClipping(SRect ClipRect);

	// Utility
	virtual void		Enlarge()=0;

protected:
	bool				ClipCoords(int &x, int &y, int &w, int &h);
	bool				ClipCoords2(int &sx, int &sy, int &x, int &y, int &w, int &h);
	SURFACEMODE m_Mode;

	int			m_Width;
	int			m_Height;
	int			m_ExternalBuffer;		// flag to indicate if the next buffer has been allocated by us or not

	CROSSX_COLOR32 m_ColorKey;

	SRect		m_ClipRect;		// Current Clipping Rect


};

#endif
