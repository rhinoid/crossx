#include "2d/surface16.h"
#include "2d/surface32.h"
#include "utils/debug.h"
#include "utils/ltga.h"
#include "utils/clib.h"
#include "utils/globals.h"
#include <string.h>

#ifndef __SYMBIAN32__
#include "2d/dib_win32.h"
#endif

CROSSX_COLOR16 RGB565_MIX(CROSSX_COLOR16 a, CROSSX_COLOR16 b, int Transparency)
{
	int ar,ag,ab;
	ab = a&RGB565_BLUE;
	ag = (a&RGB565_GREEN)>>RGB565_GREENSHIFT;
	ar = (a&RGB565_RED)>>RGB565_REDSHIFT;

	int br,bg,bb;
	bb = b&RGB565_BLUE;
	bg = (b&RGB565_GREEN)>>RGB565_GREENSHIFT;
	br = (b&RGB565_RED)>>RGB565_REDSHIFT;

	bb = (ab*(Transparency>>3)) + (bb * (31-(Transparency>>3)));
	bg = (ag*(Transparency>>2)) + (bg * (63-(Transparency>>2)));
	br = (ar*(Transparency>>3)) + (br * (31-(Transparency>>3)));

	bb >>=5;
	bg >>=6;
	br >>=5;

	bb &=31;
	bg &=63;
	br &=31;

	return (br<<RGB565_REDSHIFT)+(bg<<RGB565_GREENSHIFT)+(bb);
}

CROSSX_COLOR16 RGB565_MIX2(int ar, int ag, int ab, CROSSX_COLOR16 b, int Transparency)
{
	int br,bg,bb;
	bb = b&RGB565_BLUE;
	bg = (b&RGB565_GREEN)>>RGB565_GREENSHIFT;
	br = (b&RGB565_RED)>>RGB565_REDSHIFT;

	bb = (ab*(Transparency>>3)) + (bb * (31-(Transparency>>3)));
	bg = (ag*(Transparency>>2)) + (bg * (63-(Transparency>>2)));
	br = (ar*(Transparency>>3)) + (br * (31-(Transparency>>3)));

	bb >>=5;
	bg >>=6;
	br >>=5;

	bb &=31;
	bg &=63;
	br &=31;

	return (br<<RGB565_REDSHIFT)+(bg<<RGB565_GREENSHIFT)+(bb);
}

CROSSX_COLOR16 RGB888TORGB565(CROSSX_COLOR32 Color)
{
	int r,g,b;
	b = Color&0xff;
	g = (Color&0xff00)>>8;
	r = (Color&0xff0000)>>16;

	/*
	r>>=3;
	g>>=2;
	b>>=3;

	return (r<<RGB565_REDSHIFT) + (g<<RGB565_GREENSHIFT) + b;
	*/
	r>>=4;
	g>>=4;
	b>>=4;

	return (r<<RGB444_REDSHIFT) + (g<<RGB444_GREENSHIFT) + b;
}

CSurface16::CSurface16()
{
	m_Buffer = 0;
	m_ExternalBuffer = 0;
}


CSurface16::~CSurface16()
{
	Destroy();
}

void		CSurface16::Destroy()
{
	if(	m_ExternalBuffer==0 )
	{
		if(m_Buffer)
		{
			delete []m_Buffer;
			m_Buffer = 0;
		}
	}
}

bool		CSurface16::Create(int Width, int Height)
{
	Destroy();

	m_Mode = MODE_16BIT;
	m_Width = Width;
	m_Height = Height;
	m_ExternalBuffer = 0;
	RemoveClipping();

	m_Buffer = new uint16[m_Width*m_Height];

	return (m_Buffer) ? true : false;
}

bool        CSurface16::Create(void *ExternalBuffer, int Width, int Height)
{
	Destroy();

	m_Mode = MODE_16BIT;
	m_Width = Width;
	m_Height = Height;
	m_ExternalBuffer = 1;

	m_Buffer = (uint16 *)ExternalBuffer;

	RemoveClipping();

	return true;
}

void       *CSurface16::GetBuffer()
{
	return m_Buffer;
}

void       CSurface16::SetBuffer(void *ExternalBuffer)
{
	if(m_ExternalBuffer)
	{
		m_Buffer = (uint16 *)ExternalBuffer;
	}
}


void 	CSurface16::Clear(int x, int y, int w, int h, CROSSX_COLOR32 Color)
{
	ASSERT(m_Buffer); // no buffer attached to surface

	if(ClipCoords(x, y, w, h) == false) return;

	Color = RGB888TORGB565(Color);
// Clearing
	uint16 *Dest;
	int		cx,cy;

	Dest = m_Buffer;
	Dest += x;
	Dest += (y*m_Width);
	for(cy=0; cy<h; cy++)
	{
		for(cx=0; cx<w; cx++)
		{
			*Dest++ = Color;	//clear bitmap
		}
		Dest += (m_Width-w);
	}
}


void 	CSurface16::Clear(int x, int y, int w, int h, CROSSX_COLOR32 Color, int Transparency)
{
	ASSERT(m_Buffer); // no buffer attached to surface

	if(ClipCoords(x, y, w, h) == false) return;

	Color = RGB888TORGB565(Color);  //convert to 16bit format
// Clearing
	int cx,cy;
	uint16 *Dest;
	Dest = m_Buffer;
	Dest += x;
	Dest += (y*m_Width);
	for(cy=0; cy<h; cy++)
	{
		int sr,sg,sb;
		sb = Color&RGB565_BLUE;
		sg = (Color&RGB565_GREEN)>>RGB565_GREENSHIFT;
		sr = (Color&RGB565_RED)>>RGB565_REDSHIFT;
		for(cx=0; cx<w; cx++)
		{
			*Dest++ = RGB565_MIX2(sr, sg, sb, *Dest, Transparency);
		}
		Dest += (m_Width-w);
	}
}

void	CSurface16::ClearGradient(int x, int y, int w, int h, int ColorTL, int ColorTR, int ColorBL, int ColorBR, int Transparency)
{
	ASSERT(m_Buffer); // no buffer attached to surface

	if(ClipCoords(x, y, w, h) == false) return;

	uint16 *Dest;
	unsigned int DestWidth;
	int tx,ty;
	uint16 *BufPnt;
	int	TLR,TLG,TLB, TRR, TRG, TRB;
	int	BLR,BLG,BLB, BRR, BRG, BRB;
	int	TLRDelta, TLGDelta, TLBDelta;
	int	TRRDelta, TRGDelta, TRBDelta;

	Dest = m_Buffer;
	DestWidth = m_Width;

	BufPnt = Dest + (y*DestWidth);

	TLR = (ColorTL&(0xff0000))>>8;
	TLG = (ColorTL&(0x00ff00));
	TLB = (ColorTL&(0x0000ff))<<8;

	TRR = (ColorTR&(0xff0000))>>8;
	TRG = (ColorTR&(0x00ff00));
	TRB = (ColorTR&(0x0000ff))<<8;

	BLR = (ColorBL&(0xff0000))>>8;
	BLG = (ColorBL&(0x00ff00));
	BLB = (ColorBL&(0x0000ff))<<8;

	BRR = (ColorBR&(0xff0000))>>8;
	BRG = (ColorBR&(0x00ff00));
	BRB = (ColorBR&(0x0000ff))<<8;

	int DeltaY, DeltaX;
	DeltaY = h;
	DeltaX = w;
	if(DeltaX == 0) DeltaX = 1;
	if(DeltaY == 0) DeltaY = 1;
	TLRDelta = (BLR-TLR)/DeltaY;
	TLGDelta = (BLG-TLG)/DeltaY;
	TLBDelta = (BLB-TLB)/DeltaY;

	TRRDelta = (BRR-TRR)/DeltaY;
	TRGDelta = (BRG-TRG)/DeltaY;
	TRBDelta = (BRB-TRB)/DeltaY;

	for(ty=y; ty<(y+h); ty++)
	{
		int CurR,CurG,CurB;
		int	XDeltR, XDeltG, XDeltB;
		uint16 *BufPnt2;

		XDeltR = (TRR-TLR)/DeltaX;
		XDeltG = (TRG-TLG)/DeltaX;
		XDeltB = (TRB-TLB)/DeltaX;

		CurR = TLR;
		CurG = TLG;
		CurB = TLB;

		BufPnt2 = BufPnt+x;

		if(Transparency==255)
		{
			for(tx=x; tx<(x+w); tx++)
			{
				unsigned int color;

				color = ((CurR>>8)&0xff)<<16;
				color += ((CurG>>8)&0xff)<<8;
				color += ((CurB>>8)&0xff);
				*BufPnt2++ = RGB888TORGB565(color);

				CurR += XDeltR;
				CurG += XDeltG;
				CurB += XDeltB;
			}
		}
		else
		{
			for(tx=x; tx<(x+w); tx++)
			{
				unsigned int color;

				color = ((CurR>>8)&0xff)<<16;
				color += ((CurG>>8)&0xff)<<8;
				color += ((CurB>>8)&0xff);

				color = RGB888TORGB565(color);
				*BufPnt2++ = RGB565_MIX(color, *BufPnt2, Transparency);

				CurR += XDeltR;
				CurG += XDeltG;
				CurB += XDeltB;
			}
		}
		BufPnt += DestWidth;

		TLR += TLRDelta;
		TLG += TLGDelta;
		TLB += TLBDelta;

		TRR += TRRDelta;
		TRG += TRGDelta;
		TRB += TRBDelta;
	}
}

void	CSurface16::Blit(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency)
{
	ASSERT(m_Buffer); // no buffer attached to surface
	ASSERT(m_Mode == Source->GetMode());   // surface must be the same bitdepth

	if(Transparency!=255) // blitten met transparency? dan speciale functie gebruiken
	{
		BlitTransp(Source, sx, sy, dx, dy, w, h, Transparency);
		return;
	}

	if(ClipCoords(dx, dy, w, h) == false) return;

	if(Source->GetColorKey() != -1)
	{
		BlitColorKey(Source, sx, sy, dx, dy, w, h, Transparency);
		return;
	}

// blitting met rekening houden van alpha... (in 16 bit versie zit dit niet (zit niet in het sourceplaatje))
	int		x,y;
	uint16 *Src;
	uint16 *Dest;
	int		SourceWidth;

	Src   = (uint16 *)Source->GetBuffer();
	Dest  = m_Buffer;
	SourceWidth = Source->GetWidth();

	Src  += (sx) + (sy*SourceWidth);
	Dest += (dx) + (dy*m_Width);

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			*Dest++ = *Src++;
		}
		Src += SourceWidth - w;
		Dest += m_Width - w;
	}
}

void	CSurface16::BlitColorKey(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency)
{
	ASSERT(m_Buffer); // no buffer attached to surface
	ASSERT(m_Mode == Source->GetMode());   // surface must be the same bitdepth

	if(Transparency!=255) // blitten met transparency? dan speciale functie gebruiken
	{
		BlitTranspColorKey(Source, sx, sy, dx, dy, w, h, Transparency);
		return;
	}

	if(ClipCoords(dx, dy, w, h) == false) return;

// blitting met rekening houen van alpha... (in 16 bit versie zit dit niet (zit niet in het sourceplaatje))
	int		x,y;
	uint16 *Src;
	uint16 *Dest;
	int		SourceWidth;

	Src   = (uint16 *)Source->GetBuffer();
	Dest  = m_Buffer;
	SourceWidth = Source->GetWidth();

	Src  += (sx) + (sy*SourceWidth);
	Dest += (dx) + (dy*m_Width);

	uint16 ColorKey = RGB888TORGB565(Source->GetColorKey());

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			uint16 Color;
			Color = *Src++;
			if(Color==ColorKey)
			{
				Dest++;
			}
			else
			{
				*Dest++ = Color;
			}
		}
		Src += SourceWidth - w;
		Dest += m_Width - w;
	}
}

void	CSurface16::BlitNoAlpha(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency)
{
	Blit(Source, sx, sy, dx, dy, w, h, Transparency); // in 16bit we don't have alpha anyway
}



void    CSurface16::BlitColor(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency, int Color)
{
	ASSERT(m_Buffer); // no buffer attached to surface
	ASSERT(m_Mode == Source->GetMode());   // surface must be the same bitdepth

	if(m_ColorKey != -1)
	{
		BlitColorColorKey(Source, sx, sy, dx, dy, w, h, Transparency, Color);
		return;
	}

	if(ClipCoords(dx, dy, w, h) == false) return;

	Color = RGB888TORGB565(Color);
// blitting
	int		x,y;
	uint16 *Src;
	uint16 *Dest;
	int		SourceWidth;


	Src   = (uint16 *)Source->GetBuffer();
	Dest  = m_Buffer;
	SourceWidth = Source->GetWidth();

	Src  += (sx) + (sy*SourceWidth);
	Dest += (dx) + (dy*m_Width);

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			int sr,sg,sb;
			CROSSX_COLOR16 scolor;
			scolor = *Src;
			if(scolor == 0xffff) //only the white color in the charset gets replaced
			{
				sb = Color&RGB565_BLUE;
				sg = (Color&RGB565_GREEN)>>RGB565_GREENSHIFT;
				sr = (Color&RGB565_RED)>>RGB565_REDSHIFT;
			}
			else
			{
				sb = scolor&RGB565_BLUE;
				sg = (scolor&RGB565_GREEN)>>RGB565_GREENSHIFT;
				sr = (scolor&RGB565_RED)>>RGB565_REDSHIFT;
			}

			if(Transparency!=0)
			{
				*Dest++ = RGB565_MIX2(sr, sg, sb, *Dest, Transparency);
				Src++;
			}
			else //completely transparent
			{
				Dest++;
				Src++;
			}
		}
		Src += SourceWidth - w;
		Dest += m_Width - w;
	}
}

void    CSurface16::BlitColorColorKey(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency, int Color)
{
	ASSERT(m_Buffer); // no buffer attached to surface
	ASSERT(m_Mode == Source->GetMode());   // surface must be the same bitdepth

	if(ClipCoords(dx, dy, w, h) == false) return;

	Color = RGB888TORGB565(Color);
// blitting
	int		x,y;
	uint16 *Src;
	uint16 *Dest;
	int		SourceWidth;


	Src   = (uint16 *)Source->GetBuffer();
	Dest  = m_Buffer;
	SourceWidth = Source->GetWidth();

	Src  += (sx) + (sy*SourceWidth);
	Dest += (dx) + (dy*m_Width);

	uint16 ColrKey = RGB888TORGB565(Source->GetColorKey());

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			int sr,sg,sb;
			CROSSX_COLOR16 scolor;
			scolor = *Src;
			if(scolor == 0xffff) //only the white color in the charset gets replaced
			{
				sb = Color&RGB565_BLUE;
				sg = (Color&RGB565_GREEN)>>RGB565_GREENSHIFT;
				sr = (Color&RGB565_RED)>>RGB565_REDSHIFT;
			}
			else
			{
				sb = scolor&RGB565_BLUE;
				sg = (scolor&RGB565_GREEN)>>RGB565_GREENSHIFT;
				sr = (scolor&RGB565_RED)>>RGB565_REDSHIFT;
			}

			if(ColrKey == scolor)
			{
				Dest++;
				Src++;
			}
			else
			{
				if(Transparency!=0)
				{
					*Dest++ = RGB565_MIX2(sr, sg, sb, *Dest, Transparency);
					Src++;
				}
				else //completely transparent
				{
					Dest++;
					Src++;
				}
			}
		}
		Src += SourceWidth - w;
		Dest += m_Width - w;
	}
}

void	CSurface16::BlitTransp(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency)
{
	ASSERT(m_Buffer); // no buffer attached to surface
	ASSERT(m_Mode == Source->GetMode());   // surface must be the same bitdepth

	if(ClipCoords(dx, dy, w, h) == false) return;

// blitting
	int x,y;
	uint16 *Src;
	uint16 *Dest;
	int		SourceWidth;

	Src   = (uint16 *)Source->GetBuffer();
	Dest  = m_Buffer;
	SourceWidth = Source->GetWidth();

	Src  += (sx) + (sy*SourceWidth);
	Dest += (dx) + (dy*m_Width);

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			*Dest++ = RGB565_MIX(*Src, *Dest, Transparency);
			Src++;
		}
		Src += SourceWidth - w;
		Dest += m_Width - w;
	}
}

void	CSurface16::BlitTranspColorKey(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency)
{
	ASSERT(m_Buffer); // no buffer attached to surface
	ASSERT(m_Mode == Source->GetMode());   // surface must be the same bitdepth

	if(ClipCoords(dx, dy, w, h) == false) return;

// blitting
	int x,y;
	uint16 *Src;
	uint16 *Dest;
	int		SourceWidth;

	Src   = (uint16 *)Source->GetBuffer();
	Dest  = m_Buffer;
	SourceWidth = Source->GetWidth();

	Src  += (sx) + (sy*SourceWidth);
	Dest += (dx) + (dy*m_Width);

	uint16 ColKey = RGB888TORGB565(Source->GetColorKey());
	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			uint16 Color;
			Color = *Src++;
			if(Color == ColKey)
			{
				Dest++;
			}
			else
			{
				*Dest++ = RGB565_MIX(Color, *Dest, Transparency);
			}
		}
		Src += SourceWidth - w;
		Dest += m_Width - w;
	}
}

void    CSurface16::PlotPix(int x, int y, int Color, int Transparency)
{
	uint16 *Dest;

	if(x<m_ClipRect.m_Left) return;	//out of bounds
	if(x>=m_ClipRect.m_Right) return;	//out of bounds
	if(y<m_ClipRect.m_Top) return;	//out of bounds
	if(y>=m_ClipRect.m_Bottom) return;//out of bounds

	Color = RGB888TORGB565(Color);

	Dest  = m_Buffer;
	Dest += (x) + (y*m_Width);

	*Dest++ = RGB565_MIX(Color, *Dest, Transparency);
}

void	CSurface16::BlitConvert(CSurface32 *Source, int sx, int sy, int dx, int dy, int w, int h)
{
	ASSERT(m_Buffer); // no buffer attached to surface
	ASSERT(m_Mode != Source->GetMode());   // surface must be the opposite bitdepth (thats why its called the convert blit)

	if(ClipCoords(dx, dy, w, h) == false) return;

	int		x,y;
	uint32 *Src;
	uint16 *Dest;
	int		SourceWidth;

	Src   = (uint32 *)Source->GetBuffer();
	Dest  = m_Buffer;
	SourceWidth = Source->GetWidth();

	Src  += (sx) + (sy*SourceWidth);
	Dest += (dx) + (dy*m_Width);

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			*Dest++ = RGB888TORGB565(*Src++);
		}
		Src += SourceWidth - w;
		Dest += m_Width - w;
	}
}

void CSurface16::Enlarge()
{
	uint16 *LargeBuf;

	LargeBuf = new uint16[m_Width * m_Height * 4 * 4];

	int x,y;
	for(y=0; y<m_Height; y++)
	{
		int CurColor,RightColor, BottomColor;

		int CurR, CurG, CurB;
		int RightR, RightG, RightB;
		int BottomR, BottomG, BottomB;

		for(x=0; x<m_Width; x++)
		{
			CurColor = m_Buffer[y*m_Width + x];
			LargeBuf[y*m_Width*4 + x*2] = CurColor;

			if(x < (m_Width-1))
			{
				RightColor = m_Buffer[y*m_Width + x+1];
			}
			else
			{
				RightColor = m_Buffer[y*m_Width + x];
			}
			if(y < (m_Height-1))
			{
				BottomColor = m_Buffer[(y+1)*m_Width + x];
			}
			else
			{
				BottomColor = m_Buffer[y*m_Width + x];
			}

			CurB = CurColor&RGB565_BLUE;
			CurG = (CurColor&RGB565_GREEN)>>RGB565_GREENSHIFT;
			CurR = (CurColor&RGB565_RED)>>RGB565_REDSHIFT;
			RightB = RightColor&RGB565_BLUE;
			RightG = (RightColor&RGB565_GREEN)>>RGB565_GREENSHIFT;
			RightR = (RightColor&RGB565_RED)>>RGB565_REDSHIFT;
			BottomB = BottomColor&RGB565_BLUE;
			BottomG = (BottomColor&RGB565_GREEN)>>RGB565_GREENSHIFT;
			BottomR = (BottomColor&RGB565_RED)>>RGB565_REDSHIFT;

			int NewR, NewG, NewB;
			NewR = (CurR+RightR)/2;
			NewG = (CurG+RightG)/2;
			NewB = (CurB+RightB)/2;
			LargeBuf[y*m_Width*4 + x*2 + 1] = (NewR<<RGB565_REDSHIFT) + (NewG<<RGB565_GREENSHIFT) + (NewB);

			NewR = (CurR+BottomR)/2;
			NewG = (CurG+BottomG)/2;
			NewB = (CurB+BottomB)/2;
			LargeBuf[y*m_Width*4 + x*2 + m_Width*2] = (NewR<<RGB565_REDSHIFT) + (NewG<<RGB565_GREENSHIFT) + (NewB);

			NewR = (RightR+BottomR)/2;
			NewG = (RightG+BottomG)/2;
			NewB = (RightB+BottomB)/2;
			LargeBuf[y*m_Width*4 + x*2 + m_Width*2 + 1] = (NewR<<RGB565_REDSHIFT) + (NewG<<RGB565_GREENSHIFT) + (NewB);
		}
	}

	delete [] m_Buffer;
	m_Buffer = LargeBuf;
	m_Height *= 2;
	m_Width *= 2;
}

int CSurface16::Load(XString Filename)
{
	LTGA TgaLoader;

	Filename = MakeFullPath(Filename);

	// is the file an .exe file? in that case we extract the icon from the app and make a surface out of it
	int len;
	len = Filename.length();
	if((Filename.at(len-1) == 'e' || Filename.at(len-1) == 'E') && (Filename.at(len-2) == 'x' || Filename.at(len-2) == 'X') && (Filename.at(len-3) == 'e' || Filename.at(len-3) == 'E'))
	{
		return LoadAppIcon(Filename);
	}

	if(TgaLoader.LoadFromFile(Filename.c_str()))
	{
		int pd;
		if((pd = TgaLoader.GetPixelDepth())>=24)
		{
			byte *Source;
			uint16 *Dest;
			int x,y;

			Destroy();  // remove previous buffer

			m_Width = TgaLoader.GetImageWidth();
			m_Height = TgaLoader.GetImageHeight();

			m_Buffer = new uint16[m_Width*m_Height];

			Source = TgaLoader.GetPixels();
			Source += (((m_Height-1)*(pd>>3))*m_Width);
			Dest = m_Buffer;

			for(y=0; y<m_Height; y++)
			{
				for(x=0; x<m_Width; x++)
				{
					int r,g,b,a;

					r = *Source++;
					g = *Source++;
					b = *Source++;

					if(pd>24)
					{
						a = *Source++;
					}

					//convert to RGB565
					r >>= 3;
					g >>= 2;
					b >>= 3;
					*Dest++ = (r<<RGB565_REDSHIFT) + (g<<RGB565_GREENSHIFT) + b;
				}
				Source -= m_Width*(pd>>3);
				Source -= m_Width*(pd>>3);
			}

			RemoveClipping();
		}

		return 1;
	}

	return 0;
}

bool CSurface16::LoadAppIcon(XString Name)
{

#ifndef __SYMBIAN32__
	HICON	hIconHandle[5] ;   // Icons from Pocket Word
	HICON	hResultHandle ;    // Actual ICOn we want to put on the button

	DIB *IconDIB;

	int IconWidth,IconHeight;
	IconWidth = GetSystemMetrics(SM_CXICON);
	IconHeight = GetSystemMetrics(SM_CYICON);

	// Get the ICONs from the file
#if defined WIN32 && defined _WIN32_WCE
	WCHAR WidePath[512];
	mbstowcs(WidePath, Name.c_str(), 510);
	hResultHandle = (HICON) ExtractIconEx(WidePath, 0,hIconHandle, 0, 1) ;
#endif

#if defined WIN32 && !defined _WIN32_WCE
	hResultHandle = (HICON) ExtractIconEx(Name.c_str(), 0,hIconHandle, 0, 1) ;
#endif

	if(hResultHandle!=0)
	{
		HWND hwnd;
		hwnd = GetDesktopWindow();
		// create a DIB
		IconDIB = new DIB();
		IconDIB->Create(hwnd, IconWidth*2, IconHeight*2, 0, DIBMODE_32BIT); //load icon on a texture which is twcie as big to make sure during texturing we don't get out of bounds due to rounding errors

		DrawIcon(IconDIB->m_DibHdc, (IconDIB->m_DibWidth - IconWidth)/2, (IconDIB->m_DibHeight - IconHeight)/2, hIconHandle[0]);

		DestroyIcon(hIconHandle[0]) ;

		// now copy the contents of the dib to a texture
		m_Width = IconWidth*2;
		m_Height = IconHeight*2;
		m_Buffer = (uint16 *)malloc(m_Width * m_Height * sizeof(uint16));
		int x,y;
		unsigned int *Src = (unsigned int *)IconDIB->m_DibSrcBits;
		uint16 *Dst = (uint16 *)m_Buffer;
		for(y=0; y<m_Height; y++)
		{
			for(x=0; x<m_Width; x++)
			{
				*Dst++ = RGB888TORGB565(*Src++);
			}
		}

		RemoveClipping();
		delete IconDIB;

		return true;
	}
#endif
	return false;
}
