#ifndef _H_SURFACE16
#define _H_SURFACE16

// baseclass
#include "2d/Surface.h"

class CSurface32;

class CSurface16 : public CSurface
{
public:

	// Constructors/Destructor
	CSurface16();
	~CSurface16();

	void		Destroy();
	bool		Create(int Width, int Height);
	bool        Create(void *ExternalBuffer, int Width, int Height);
	int			Load(XString Filename);

	// Properties
	void       *GetBuffer();
	void        SetBuffer(void *ExternalBuffer);   //only works with external surfaces


	// Operations
	void		Clear(int x, int y, int w, int h, int Color);
	void		Clear(int x, int y, int w, int h, int Color, int Transparency);
	void		Blit(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency);
	void		BlitNoAlpha(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency);
	void		BlitColor(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency, int Color);	// if a bit is not transparent, it is colored into the supplied color (used for blitting charactersets in any color)
	void		BlitColorKey(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency);
	void		PlotPix(int x, int y, int Color, int Transparency);

	void		ClearGradient(int x, int y, int w, int h, int ColorTL, int ColorTR, int ColorBL, int ColorBR, int Transparency);


	void		BlitConvert(CSurface32 *Source, int sx, int sy, int dx, int dy, int w, int h); // convert from 565 to 888

	// Utility
	void		Enlarge();

private:
	void		BlitTransp(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency);
	void		BlitTranspColorKey(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency);
	void		BlitColorColorKey(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency, int Color);	// if a bit is not transparent, it is colored into the supplied color (used for blitting charactersets in any color)

	bool		LoadAppIcon(XString Name);

	uint16     *m_Buffer;
};

#endif
