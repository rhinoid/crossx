#include "2d/surface32.h"
#include "2d/surface16.h"
#include "utils/debug.h"
#include "utils/ltga.h"
#include "utils/clib.h"
#include "utils/globals.h"

#ifndef __SYMBIAN32__
#include "2d/dib_win32.h"
#endif

#include <string.h>


CROSSX_COLOR16 RGB888_MIX(CROSSX_COLOR32 a, CROSSX_COLOR32 b, int Transparency)
{
	int Alpha;
	int ar,ag,ab;
	Alpha = (a&RGB888_ALPHA)>>RGB888_ALPHASHIFT;
	ab = a&RGB888_BLUE;
	ag = (a&RGB888_GREEN)>>RGB888_GREENSHIFT;
	ar = (a&RGB888_RED)>>RGB888_REDSHIFT;

	int br,bg,bb;
	bb = b&RGB888_BLUE;
	bg = (b&RGB888_GREEN)>>RGB888_GREENSHIFT;
	br = (b&RGB888_RED)>>RGB888_REDSHIFT;

	Transparency *= Alpha;
	Transparency >>= 8;

	bb = (ab*Transparency) + (bb * (255-Transparency));
	bg = (ag*Transparency) + (bg * (255-Transparency));
	br = (ar*Transparency) + (br * (255-Transparency));

	bb >>=8;
	bg >>=8;
	br >>=8;

	bb &=255;
	bg &=255;
	br &=255;

	return (br<<RGB888_REDSHIFT)+(bg<<RGB888_GREENSHIFT)+(bb);
}

CROSSX_COLOR16 RGB888_MIXNOALPHA(CROSSX_COLOR32 a, CROSSX_COLOR32 b, int Transparency)
{
	int ar,ag,ab;
	ab = a&RGB888_BLUE;
	ag = (a&RGB888_GREEN)>>RGB888_GREENSHIFT;
	ar = (a&RGB888_RED)>>RGB888_REDSHIFT;

	int br,bg,bb;
	bb = b&RGB888_BLUE;
	bg = (b&RGB888_GREEN)>>RGB888_GREENSHIFT;
	br = (b&RGB888_RED)>>RGB888_REDSHIFT;

	bb = (ab*Transparency) + (bb * (255-Transparency));
	bg = (ag*Transparency) + (bg * (255-Transparency));
	br = (ar*Transparency) + (br * (255-Transparency));

	bb >>=8;
	bg >>=8;
	br >>=8;

	bb &=255;
	bg &=255;
	br &=255;

	return (br<<RGB888_REDSHIFT)+(bg<<RGB888_GREENSHIFT)+(bb);
}

CROSSX_COLOR32 RGB888_MIX2(int ar, int ag, int ab, int aa, CROSSX_COLOR32 b, int Transparency)
{
	int br,bg,bb;
	bb = b&RGB888_BLUE;
	bg = (b&RGB888_GREEN)>>RGB888_GREENSHIFT;
	br = (b&RGB888_RED)>>RGB888_REDSHIFT;

	Transparency *= aa;
	Transparency >>= 8;

	bb = (ab*Transparency) + (bb * (255-Transparency));
	bg = (ag*Transparency) + (bg * (255-Transparency));
	br = (ar*Transparency) + (br * (255-Transparency));


	bb >>=8;
	bg >>=8;
	br >>=8;

	bb &=255;
	bg &=255;
	br &=255;

	return (br<<RGB888_REDSHIFT)+(bg<<RGB888_GREENSHIFT)+(bb);
}

CROSSX_COLOR32 RGB565TORGB888(CROSSX_COLOR16 Color)
{
	int r,g,b;
	b = Color&RGB565_BLUE;
	g = (Color&RGB565_GREEN)>>RGB565_GREENSHIFT;
	r = (Color&RGB565_RED)>>RGB565_REDSHIFT;

	r<<=3;
	g<<=2;
	b<<=3;

	return (r<<RGB888_REDSHIFT) + (g<<RGB888_GREENSHIFT) + b;
}

CSurface32::CSurface32()
{
	m_Buffer = 0;
	m_ExternalBuffer = 0;
}


CSurface32::~CSurface32()
{
	Destroy();
}

void		CSurface32::Destroy()
{
	if(	m_ExternalBuffer==0 )
	{
		if(m_Buffer)
		{
			delete []m_Buffer;
			m_Buffer = 0;
		}
	}
}

bool		CSurface32::Create(int Width, int Height)
{
	Destroy();

	m_Mode = MODE_32BIT;
	m_Width = Width;
	m_Height = Height;
	m_ExternalBuffer = 0;
	RemoveClipping();

	m_Buffer = new uint32[m_Width*m_Height];

	return (m_Buffer) ? true : false;
}

bool        CSurface32::Create(void *ExternalBuffer, int Width, int Height)
{
	Destroy();

	m_Mode = MODE_32BIT;
	m_Width = Width;
	m_Height = Height;
	m_ExternalBuffer = 1;

	m_Buffer = (uint32 *)ExternalBuffer;

	RemoveClipping();

	return true;
}


void       *CSurface32::GetBuffer()
{
	return m_Buffer;
}


void       CSurface32::SetBuffer(void *ExternalBuffer)
{
	if(m_ExternalBuffer)
	{
		m_Buffer = (uint32 *)ExternalBuffer;
	}
}


void 	CSurface32::Clear(int x, int y, int w, int h, CROSSX_COLOR32 Color)
{
	ASSERT(m_Buffer); // no buffer attached to surface

	if(ClipCoords(x, y, w, h) == false) return;

// Clearing
	uint32 *Dest;
	int		cx,cy;

	Dest = m_Buffer;
	Dest += x;
	Dest += (y*m_Width);
	for(cy=0; cy<h; cy++)
	{
		for(cx=0; cx<w; cx++)
		{
			*Dest++ = Color;	//clear bitmap
		}
		Dest += (m_Width-w);
	}
}


void 	CSurface32::Clear(int x, int y, int w, int h, CROSSX_COLOR32 Color, int Transparency)
{
	ASSERT(m_Buffer); // no buffer attached to surface

	if(ClipCoords(x, y, w, h) == false) return;

// Clearing
	int cx,cy;
	uint32 *Dest;
	Dest = m_Buffer;
	Dest += x;
	Dest += (y*m_Width);
	for(cy=0; cy<h; cy++)
	{
		int sr,sg,sb,sa;
		sb = Color&RGB888_BLUE;
		sg = (Color&RGB888_GREEN)>>RGB888_GREENSHIFT;
		sr = (Color&RGB888_RED)>>RGB888_REDSHIFT;
		sa = (Color&RGB888_ALPHA)>>RGB888_ALPHASHIFT;
		for(cx=0; cx<w; cx++)
		{
			*Dest++ = RGB888_MIX2(sr, sg, sb, sa, *Dest, Transparency);
		}
		Dest += (m_Width-w);
	}
}

void	CSurface32::ClearGradient(int x, int y, int w, int h, int ColorTL, int ColorTR, int ColorBL, int ColorBR, int Transparency)
{
	ASSERT(m_Buffer); // no buffer attached to surface

	if(ClipCoords(x, y, w, h) == false) return;

	uint32 *Dest;
	unsigned int DestWidth;
	int tx,ty;
	uint32 *BufPnt;
	int	TLR,TLG,TLB, TRR, TRG, TRB;
	int	BLR,BLG,BLB, BRR, BRG, BRB;
	int	TLRDelta, TLGDelta, TLBDelta;
	int	TRRDelta, TRGDelta, TRBDelta;

	Dest = m_Buffer;
	DestWidth = m_Width;

	BufPnt = Dest + (y*DestWidth);

	TLR = (ColorTL&(0xff0000))>>8;
	TLG = (ColorTL&(0x00ff00));
	TLB = (ColorTL&(0x0000ff))<<8;

	TRR = (ColorTR&(0xff0000))>>8;
	TRG = (ColorTR&(0x00ff00));
	TRB = (ColorTR&(0x0000ff))<<8;

	BLR = (ColorBL&(0xff0000))>>8;
	BLG = (ColorBL&(0x00ff00));
	BLB = (ColorBL&(0x0000ff))<<8;

	BRR = (ColorBR&(0xff0000))>>8;
	BRG = (ColorBR&(0x00ff00));
	BRB = (ColorBR&(0x0000ff))<<8;

	int DeltaY, DeltaX;
	DeltaY = h;
	DeltaX = w;
	if(DeltaX == 0) DeltaX = 1;
	if(DeltaY == 0) DeltaY = 1;
	TLRDelta = (BLR-TLR)/DeltaY;
	TLGDelta = (BLG-TLG)/DeltaY;
	TLBDelta = (BLB-TLB)/DeltaY;

	TRRDelta = (BRR-TRR)/DeltaY;
	TRGDelta = (BRG-TRG)/DeltaY;
	TRBDelta = (BRB-TRB)/DeltaY;

	for(ty=y; ty<(y+h); ty++)
	{
		int CurR,CurG,CurB;
		int	XDeltR, XDeltG, XDeltB;
		uint32 *BufPnt2;

		XDeltR = (TRR-TLR)/DeltaX;
		XDeltG = (TRG-TLG)/DeltaX;
		XDeltB = (TRB-TLB)/DeltaX;

		CurR = TLR;
		CurG = TLG;
		CurB = TLB;

		BufPnt2 = BufPnt+x;
		if(Transparency==255)
		{
			for(tx=x; tx<(x+w); tx++)
			{
				unsigned int color;

				color = ((CurR>>8)&0xff)<<16;
				color += ((CurG>>8)&0xff)<<8;
				color += ((CurB>>8)&0xff);
				*BufPnt2++ = color;

				CurR += XDeltR;
				CurG += XDeltG;
				CurB += XDeltB;
			}
		}
		else
		{
			for(tx=x; tx<(x+w); tx++)
			{
				unsigned int color;

				color = ((CurR>>8)&0xff)<<16;
				color += ((CurG>>8)&0xff)<<8;
				color += ((CurB>>8)&0xff);
				*BufPnt2++ = RGB888_MIX(color, *BufPnt2, Transparency);

				CurR += XDeltR;
				CurG += XDeltG;
				CurB += XDeltB;
			}
		}
		BufPnt += DestWidth;

		TLR += TLRDelta;
		TLG += TLGDelta;
		TLB += TLBDelta;

		TRR += TRRDelta;
		TRG += TRGDelta;
		TRB += TRBDelta;
	}
}

void	CSurface32::Blit(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency)
{
	ASSERT(m_Buffer); // no buffer attached to surface
	ASSERT(m_Mode == Source->GetMode());   // surface must be the same bitdepth

	if(Transparency!=255) // blitten met transparency? dan speciale functie gebruiken
	{
		BlitTransp(Source, sx, sy, dx, dy, w, h, Transparency);
		return;
	}

	if(ClipCoords2(sx,sy, dx, dy, w, h) == false) return;

	if(Source->GetColorKey() != -1)
	{
		BlitColorKey(Source, sx, sy, dx, dy, w, h, Transparency);
		return;
	}

// blitting met rekening houden van alpha...
	int		x,y;
	uint32 *Src;
	uint32 *Dest;
	int		SourceWidth;

	Src   = (uint32 *)Source->GetBuffer();
	Dest  = m_Buffer;
	SourceWidth = Source->GetWidth();

	Src  += (sx) + (sy*SourceWidth);
	Dest += (dx) + (dy*m_Width);

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			*Dest++ = RGB888_MIX(*Src++, *Dest, 255);
		}
		Src += SourceWidth - w;
		Dest += m_Width - w;
	}
}

void CSurface32::BlitColorKey(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency)
{
	int		x,y;
	uint32 *Src;
	uint32 *Dest;
	int		SourceWidth;

	Src   = (uint32 *)Source->GetBuffer();
	Dest  = m_Buffer;
	SourceWidth = Source->GetWidth();

	Src  += (sx) + (sy*SourceWidth);
	Dest += (dx) + (dy*m_Width);

	uint32 ColorKey = Source->GetColorKey();

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			uint32 Color;
			Color = *Src++;
			if((Color&0xffffff)==ColorKey)
			{
				Dest++;
			}
			else
			{
				*Dest++ = RGB888_MIX(Color, *Dest, 255);
			}
		}
		Src += SourceWidth - w;
		Dest += m_Width - w;
	}
}

void	CSurface32::BlitNoAlpha(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency)
{
	ASSERT(m_Buffer); // no buffer attached to surface
	ASSERT(m_Mode == Source->GetMode());   // surface must be the same bitdepth

	if(Transparency!=255) // blitten met transparency? dan speciale functie gebruiken
	{
		BlitTranspNoAlpha(Source, sx, sy, dx, dy, w, h, Transparency);
		return;
	}

	if(ClipCoords2(sx, sy, dx, dy, w, h) == false) return;

// blitting zonder rekening houden met alpha...
	int		x,y;
	uint32 *Src;
	uint32 *Dest;
	int		SourceWidth;

	Src   = (uint32 *)Source->GetBuffer();
	Dest  = m_Buffer;
	SourceWidth = Source->GetWidth();

	Src  += (sx) + (sy*SourceWidth);
	Dest += (dx) + (dy*m_Width);

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			*Dest++ = *Src++;
		}
		Src += SourceWidth - w;
		Dest += m_Width - w;
	}
}


void    CSurface32::BlitColor(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency, int Color)
{
	ASSERT(m_Buffer); // no buffer attached to surface
	ASSERT(m_Mode == Source->GetMode());   // surface must be the same bitdepth

	if(ClipCoords2(sx, sy, dx, dy, w, h) == false) return;

// blitting
	int		x,y;
	uint32 *Src;
	uint32 *Dest;
	int		SourceWidth;


	Src   = (uint32 *)Source->GetBuffer();
	Dest  = m_Buffer;
	SourceWidth = Source->GetWidth();

	Src  += (sx) + (sy*SourceWidth);
	Dest += (dx) + (dy*m_Width);

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			int sr,sg,sb,sa;
			CROSSX_COLOR32 scolor;
			scolor = *Src;
			if((scolor & 0xffffff) == 0xffffff) //only the white color in the charset gets replaced
			{
				sb = Color&RGB888_BLUE;
				sg = (Color&RGB888_GREEN)>>RGB888_GREENSHIFT;
				sr = (Color&RGB888_RED)>>RGB888_REDSHIFT;
				sa = (scolor&RGB888_ALPHA)>>RGB888_ALPHASHIFT;
			}
			else
			{
				sb = scolor&RGB888_BLUE;
				sg = (scolor&RGB888_GREEN)>>RGB888_GREENSHIFT;
				sr = (scolor&RGB888_RED)>>RGB888_REDSHIFT;
				sa = (scolor&RGB888_ALPHA)>>RGB888_ALPHASHIFT;
			}

			*Dest++ = RGB888_MIX2(sr, sg, sb, sa, *Dest, Transparency);
			Src++;
		}
		Src += SourceWidth - w;
		Dest += m_Width - w;
	}
}

void	CSurface32::BlitTransp(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency)
{
	ASSERT(m_Buffer); // no buffer attached to surface
	ASSERT(m_Mode == Source->GetMode());   // surface must be the same bitdepth

	if(ClipCoords2(sx, sy, dx, dy, w, h) == false) return;

// blitting
	int x,y;
	uint32 *Src;
	uint32 *Dest;
	int		SourceWidth;

	Src   = (uint32 *)Source->GetBuffer();
	Dest  = m_Buffer;
	SourceWidth = Source->GetWidth();

	Src  += (sx) + (sy*SourceWidth);
	Dest += (dx) + (dy*m_Width);

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			*Dest++ = RGB888_MIX(*Src, *Dest, Transparency);
			Src++;
		}
		Src += SourceWidth - w;
		Dest += m_Width - w;
	}
}

void	CSurface32::BlitTranspNoAlpha(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency)
{
	ASSERT(m_Buffer); // no buffer attached to surface
	ASSERT(m_Mode == Source->GetMode());   // surface must be the same bitdepth

	if(ClipCoords2(sx, sy, dx, dy, w, h) == false) return;

// blitting
	int x,y;
	uint32 *Src;
	uint32 *Dest;
	int		SourceWidth;

	Src   = (uint32 *)Source->GetBuffer();
	Dest  = m_Buffer;
	SourceWidth = Source->GetWidth();

	Src  += (sx) + (sy*SourceWidth);
	Dest += (dx) + (dy*m_Width);

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			*Dest++ = RGB888_MIXNOALPHA(*Src, *Dest, Transparency);
			Src++;
		}
		Src += SourceWidth - w;
		Dest += m_Width - w;
	}
}

void    CSurface32::PlotPix(int x, int y, int Color, int Transparency)
{
	uint32 *Dest;

	if(x<m_ClipRect.m_Left) return;	//out of bounds
	if(x>=m_ClipRect.m_Right) return;	//out of bounds
	if(y<m_ClipRect.m_Top) return;	//out of bounds
	if(y>=m_ClipRect.m_Bottom) return;//out of bounds

	Dest  = m_Buffer;
	Dest += (x) + (y*m_Width);

	*Dest++ = RGB888_MIX(Color, *Dest, Transparency);
}

void	CSurface32::BlitConvert(CSurface16 *Source, int sx, int sy, int dx, int dy, int w, int h)
{
	ASSERT(m_Buffer); // no buffer attached to surface
	ASSERT(m_Mode == Source->GetMode());   // surface must be the same bitdepth

	if(ClipCoords2(sx, sy, dx, dy, w, h) == false) return;

	int		x,y;
	uint16 *Src;
	uint32 *Dest;
	int		SourceWidth;

	Src   = (uint16 *)Source->GetBuffer();
	Dest  = m_Buffer;
	SourceWidth = Source->GetWidth();

	Src  += (sx) + (sy*SourceWidth);
	Dest += (dx) + (dy*m_Width);

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			*Dest++ = RGB565TORGB888(*Src++);
		}
		Src += SourceWidth - w;
		Dest += m_Width - w;
	}
}

void CSurface32::Enlarge()
{
	uint32 *LargeBuf;

	LargeBuf = new uint32[m_Width * m_Height * 4 * 4];

	int x,y;
	for(y=0; y<m_Height; y++)
	{
		int CurColor,RightColor, BottomColor;

		int CurR, CurG, CurB;
		int RightR, RightG, RightB;
		int BottomR, BottomG, BottomB;

		for(x=0; x<m_Width; x++)
		{
			CurColor = m_Buffer[y*m_Width + x];
			LargeBuf[y*m_Width*4 + x*2] = CurColor;

			if(x < (m_Width-1))
			{
				RightColor = m_Buffer[y*m_Width + x+1];
			}
			else
			{
				RightColor = m_Buffer[y*m_Width + x];
			}
			if(y < (m_Height-1))
			{
				BottomColor = m_Buffer[(y+1)*m_Width + x];
			}
			else
			{
				BottomColor = m_Buffer[y*m_Width + x];
			}

			CurB = CurColor&RGB888_BLUE;
			CurG = (CurColor&RGB888_GREEN)>>RGB888_GREENSHIFT;
			CurR = (CurColor&RGB888_RED)>>RGB888_REDSHIFT;
			RightB = RightColor&RGB888_BLUE;
			RightG = (RightColor&RGB888_GREEN)>>RGB888_GREENSHIFT;
			RightR = (RightColor&RGB888_RED)>>RGB888_REDSHIFT;
			BottomB = BottomColor&RGB888_BLUE;
			BottomG = (BottomColor&RGB888_GREEN)>>RGB888_GREENSHIFT;
			BottomR = (BottomColor&RGB888_RED)>>RGB888_REDSHIFT;

			int NewR, NewG, NewB;
			NewR = (CurR+RightR)/2;
			NewG = (CurG+RightG)/2;
			NewB = (CurB+RightB)/2;
			LargeBuf[y*m_Width*4 + x*2 + 1] = (NewR<<RGB888_REDSHIFT) + (NewG<<RGB888_GREENSHIFT) + (NewB);

			NewR = (CurR+BottomR)/2;
			NewG = (CurG+BottomG)/2;
			NewB = (CurB+BottomB)/2;
			LargeBuf[y*m_Width*4 + x*2 + m_Width*2] = (NewR<<RGB888_REDSHIFT) + (NewG<<RGB888_GREENSHIFT) + (NewB);

			NewR = (RightR+BottomR)/2;
			NewG = (RightG+BottomG)/2;
			NewB = (RightB+BottomB)/2;
			LargeBuf[y*m_Width*4 + x*2 + m_Width*2 + 1] = (NewR<<RGB888_REDSHIFT) + (NewG<<RGB888_GREENSHIFT) + (NewB);
		}
	}

	delete [] m_Buffer;
	m_Buffer = LargeBuf;
	m_Height *= 2;
	m_Width *= 2;
}

int CSurface32::Load(XString Filename)
{
	LTGA TgaLoader;

	XString FullName;
	FullName = MakeFullPath(Filename);

	// is the file an .exe file? in that case we extract the icon from the app and make a surface out of it
	int len;
	len = FullName.length();
	if((FullName[len-1] == 'e' || FullName[len-1] == 'E') && (FullName[len-2] == 'x' || FullName[len-2] == 'X') && (FullName[len-3] == 'e' || FullName[len-3] == 'E'))
	{
		return LoadAppIcon(FullName);
	}

	if(TgaLoader.LoadFromFile(FullName.c_str()))
	{
		int pd;
		if((pd = TgaLoader.GetPixelDepth())>=24)
		{
			byte *Source;
			uint32 *Dest;
			int x,y;

			Destroy();  // remove previous buffer

			m_Width = TgaLoader.GetImageWidth();
			m_Height = TgaLoader.GetImageHeight();

			m_Buffer = new uint32[m_Width*m_Height];

			Source = TgaLoader.GetPixels();
			Source += (((m_Height-1)*(pd>>3))*m_Width);
			Dest = m_Buffer;

			for(y=0; y<m_Height; y++)
			{
				for(x=0; x<m_Width; x++)
				{
					int r,g,b,a=0;

					r = *Source++;
					g = *Source++;
					b = *Source++;

					if(pd>24)
					{
						a = *Source++;
					}

					//convert to RGBA8888
					*Dest++ = (a<<RGB888_ALPHASHIFT)+ (r<<RGB888_REDSHIFT) + (g<<RGB888_GREENSHIFT) + b;
				}
				Source -= m_Width*(pd>>3);
				Source -= m_Width*(pd>>3);
			}

			RemoveClipping();
		}

		return 1;
	}

	return 0;
}

bool CSurface32::LoadAppIcon(XString Name)
{

#ifndef __SYMBIAN32__
	HICON	hIconHandle[5] ;   // Icons from Pocket Word
	HICON	hResultHandle ;    // Actual ICOn we want to put on the button

	DIB *IconDIB;

	int IconWidth,IconHeight;
	IconWidth = GetSystemMetrics(SM_CXICON);
	IconHeight = GetSystemMetrics(SM_CYICON);

	// Get the ICONs from the file
#if defined WIN32 && defined _WIN32_WCE
	WCHAR WidePath[512];
	mbstowcs(WidePath, Name.c_str(), 510);
	hResultHandle = (HICON) ExtractIconEx(WidePath, 0,hIconHandle, 0, 1) ;
#endif

#if defined WIN32 && !defined _WIN32_WCE
	const char *t = Name.c_str();
	hResultHandle = (HICON) ExtractIconEx(Name.c_str(), 0,hIconHandle, 0, 1) ;
#endif

	if(hResultHandle!=0)
	{
		HWND hwnd;
		hwnd = GetDesktopWindow();
		// create a DIB
		IconDIB = new DIB();
		IconDIB->Create(hwnd, IconWidth*2, IconHeight*2, 0, DIBMODE_32BIT); //load icon on a texture which is twcie as big to make sure during texturing we don't get out of bounds due to rounding errors

		if(hIconHandle[0] != 0)
		{
			DrawIcon(IconDIB->m_DibHdc, (IconDIB->m_DibWidth - IconWidth)/2, (IconDIB->m_DibHeight - IconHeight)/2, hIconHandle[0]);
			DestroyIcon(hIconHandle[0]) ;
		}

		// now copy the contents of the dib to a texture
		m_Width = IconWidth*2;
		m_Height = IconHeight*2;
		m_Buffer = (unsigned int *)malloc(m_Width * m_Height * sizeof(unsigned int));
		int x,y;
		unsigned int *Src = (unsigned int *)IconDIB->m_DibSrcBits;
		unsigned int *Dst = (unsigned int *)m_Buffer;
		for(y=0; y<m_Height; y++)
		{
			for(x=0; x<m_Width; x++)
			{
				*Dst++ = *Src++;
			}
		}

		RemoveClipping();
		delete IconDIB;
		return true;
	}
#endif
	return false;
}
