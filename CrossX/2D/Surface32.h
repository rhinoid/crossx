#ifndef _H_SURFACE32
#define _H_SURFACE32

// baseclass
#include "2d/Surface.h"

class CSurface16;

class CSurface32 : public CSurface
{
public:
	// Constructors/Destructor
	CSurface32();
	~CSurface32();

	void		Destroy();
	bool		Create(int Width, int Height);
	bool        Create(void *ExternalBuffer, int Width, int Height);
	int			Load(XString Filename);

	// basics
	void       *GetBuffer();
	void        SetBuffer(void *ExternalBuffer);   //only works with external surfaces

	// Operations
	void		Clear(int x, int y, int w, int h, int Color);
	void		Clear(int x, int y, int w, int h, int Color, int Transparency);
	void		Blit(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency);
	void		BlitNoAlpha(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency);
	void		BlitColor(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency, int Color);	// if a bit is not transparent, it is colored into the supplied color (used for blitting charactersets in any color)
	void		BlitColorKey(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency);
	void		PlotPix(int x, int y, int Color, int Transparency);

	void		ClearGradient(int x, int y, int w, int h, int ColorTL, int ColorTR, int ColorBL, int ColorBR, int Transparency);

	void		BlitConvert(CSurface16 *Source, int sx, int sy, int dx, int dy, int w, int h); // convert from 888 to 565

	// Utility
	void		Enlarge();

private:
	void		BlitTransp(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency);
	void		BlitTranspNoAlpha(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency);

	bool		LoadAppIcon(XString Name);

	uint32     *m_Buffer;

};

#endif
