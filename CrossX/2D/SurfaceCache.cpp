#include "2d/SurfaceCache.h"
#include "utils/clib.h"
#include "utils/debug.h"
#include "utils/globals.h"

/////////////////////////////////////////
// CSurfaceCache Methods
/////////////////////////////////////////

CSurfaceCache *CSurfaceCache::GetSurfaceCache() 
{ 
	if(GLOBALS->m_SurfaceCache == 0)
	{
		GLOBALS->m_SurfaceCache = new CSurfaceCache();
	}
	return GLOBALS->m_SurfaceCache;
};

CSurfaceCache::CSurfaceCache()
{
	m_List = 0;
}

CSurfaceCache::~CSurfaceCache()
{
	RemoveAll();
}

void	CSurfaceCache::Flush(void)
{
	CacheNode *CurNode;
	CacheNode **CurNodePtr;

	CurNode = m_List;
	CurNodePtr = &m_List;
	while(CurNode)
	{
		CacheNode *NextNode;

		NextNode = CurNode->m_Next;

		if(CurNode->m_UseCount==0)
		{
			delete CurNode->m_Surface;
			delete CurNode;
		}
		else
		{
			*CurNodePtr = CurNode;
			CurNodePtr = &CurNode->m_Next;
		}
		CurNode = NextNode;
	}
	*CurNodePtr = CurNode;
}

void	CSurfaceCache::RemoveAll(void)
{
	CacheNode *CurNode;

	CurNode = m_List;
	while(CurNode)
	{
		CacheNode *NextNode;

		NextNode = CurNode->m_Next;

		delete CurNode->m_Surface;
		delete CurNode;

		CurNode = NextNode;
	}
}

CSurface *CSurfaceCache::LoadSurface(XString Filename, SURFACEMODE Mode)
{
	CacheNode *CurNode;
	CacheNode *PrevNode;

	CurNode = m_List;
	PrevNode = m_List;
	while(CurNode)
	{
		CacheNode *NextNode;

		NextNode = CurNode->m_Next;

		if(CurNode->m_Filename == Filename)
		{
			//check if mode is the same
			if(CurNode->m_Surface->GetMode() == Mode)
			{
				CurNode->m_UseCount++;
				return CurNode->m_Surface;
			}
		}

		PrevNode = CurNode;
		CurNode = NextNode;
	}

	// Create a new surface

	CacheNode *Node;
	CSurface *Surface;
	Surface    = CSurface::Factory(Mode);
	Surface->Load(Filename);
	Node = new CacheNode();
	Node->m_Filename = Filename;
	Node->m_Surface = Surface;
	Node->m_UseCount = 1;
	Node->m_Next = 0;


	//attach to the end of the list
	if(PrevNode==0)	//allereerste surface
	{
		m_List = Node;
	}
	else
	{
		PrevNode->m_Next = Node;
	}
	return Surface;
}

void	CSurfaceCache::RemoveSurface(XString Filename)
{
	CacheNode *CurNode;

	CurNode = m_List;
	while(CurNode)
	{
		CacheNode *NextNode;

		NextNode = CurNode->m_Next;

		if(CurNode->m_Filename == Filename)
		{
			CurNode->m_UseCount--;
			ASSERT(CurNode->m_UseCount>=0);	//more often removed than made!
			return;
		}

		CurNode = NextNode;
	}
	ASSERT(0);	// surface being removed doesn't exist
}

void	CSurfaceCache::RemoveSurface(CSurface *Surface)
{
	CacheNode *CurNode;

	CurNode = m_List;
	while(CurNode)
	{
		CacheNode *NextNode;

		NextNode = CurNode->m_Next;

		if(CurNode->m_Surface==Surface)
		{
			CurNode->m_UseCount--;
			ASSERT(CurNode->m_UseCount>=0);	//more often removed than made!
			return;
		}

		CurNode = NextNode;
	}
	ASSERT(0);	// surface being removed doesn't exist
}
