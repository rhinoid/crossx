#ifndef _H_SURFACECACHE
#define _H_SURFACECACHE

#include "2d/surface.h"
#include "utils/XString.h"

///////////////////////////////////////////////////////////

class CSpotMaster;

struct CacheNode
{
	CacheNode  *m_Next;
	int		    m_UseCount;
	XString     m_Filename;
	CSurface   *m_Surface;
};


class CSurfaceCache
{
public:
	static CSurfaceCache *GetSurfaceCache();
	~CSurfaceCache();

private:
	CSurfaceCache();

public:
	void	Flush(void);		//flushes all not used surfaces
	void	RemoveAll(void);	//removes all surfaces (used or not) and resets everything

	CSurface *LoadSurface(XString Filename, SURFACEMODE Mode);
	void	RemoveSurface(XString Filename);
	void	RemoveSurface(CSurface *Surface);
private:
	CSpotMaster *m_Parent;

	CacheNode *m_List;
};


#endif
