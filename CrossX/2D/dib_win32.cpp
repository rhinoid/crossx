// CustomItem.cpp : Defines the entry point for the DLL application.
//

#include "windows.h"
#include "dib_win32.h"

/////DIB stuff
typedef struct tagBITMAPINFORHINO {
    BITMAPINFOHEADER    bmiHeader;
    RGBQUAD             bmiColors[3];
} BITMAPINFORHINO;


DIB::DIB()
{
}

DIB::~DIB()
{
}


void DIB::Create(HWND hwnd, int Width, int Height, void *Pixels, DIBMODE DIBMode)
{
	m_DibWidth = Width;
	m_DibHeight = Height;

    // fill in the BITMAPINFO structure
    BITMAPINFORHINO bmi;
    ZeroMemory(&bmi, sizeof(BITMAPINFORHINO));
    bmi.bmiHeader.biSize            =   sizeof(BITMAPINFOHEADER);
    bmi.bmiHeader.biWidth           =   m_DibWidth;
    bmi.bmiHeader.biHeight          =   -m_DibHeight;
    bmi.bmiHeader.biPlanes          =   1;

	if(DIBMode == DIBMODE_16BIT)
	{
		bmi.bmiHeader.biBitCount        =   16;
		bmi.bmiHeader.biCompression     =   BI_BITFIELDS;
		bmi.bmiHeader.biClrUsed         =   3;
		bmi.bmiHeader.biSizeImage       =   2 * m_DibWidth * m_DibHeight;
		bmi.bmiHeader.biClrImportant    = 0;

		int *maskcolors;
		maskcolors = (int *) bmi.bmiColors;
		*maskcolors++ = 0xf800;
		*maskcolors++ = 0x07E0;
		*maskcolors++ = 0x001F;
	}
	else
	{
		bmi.bmiHeader.biBitCount        =   32;
		bmi.bmiHeader.biCompression     =   BI_RGB;
		bmi.bmiHeader.biClrUsed         =   0;
		bmi.bmiHeader.biSizeImage       =   4 * m_DibWidth * m_DibHeight;
		bmi.bmiHeader.biClrImportant    = 0;
	}
//    bmi.bmiHeader.biCompression     =   BI_RGB;
    bmi.bmiHeader.biXPelsPerMeter   =   72;
    bmi.bmiHeader.biYPelsPerMeter   =   72;

    // Get hdc of screen for CreateDIBSection and create the DIB
    HDC hdcScreen = GetDC(hwnd);
    m_DibHbm = CreateDIBSection(
                hdcScreen, (BITMAPINFO *)&bmi, DIB_RGB_COLORS, 
                (void**)&m_DibSrcBits, NULL, 0);

    // Create and select the bitmap into a DC
    m_DibHdc = CreateCompatibleDC(hdcScreen);
    m_DibHbmOld = (HBITMAP)SelectObject(m_DibHdc, m_DibHbm);

    // Copy the backbuffer into the DIB
	if(DIBMode == DIBMODE_16BIT)
	{
		if(Pixels)
		{
			memcpy(m_DibSrcBits, Pixels, m_DibHeight*m_DibHeight*2);
		}
		else
		{
			memset(m_DibSrcBits, 0, m_DibHeight*m_DibHeight*2);
		}
	}
	else
	{
		if(Pixels)
		{
			memcpy(m_DibSrcBits, Pixels, m_DibHeight*m_DibHeight*4);
		}
		else
		{
			memset(m_DibSrcBits, 0, m_DibHeight*m_DibHeight*4);
		}
	}

    // all GDI functions must be flushed before we can use the DIB
#ifndef _WIN32_WCE
    GdiFlush();
#endif
    ReleaseDC(NULL, hdcScreen);
}	


void DIB::Blit(HDC DestDC)
{
	//blit the image to the screen
    BitBlt(DestDC, 0, 0, m_DibWidth, m_DibHeight, m_DibHdc, 0, 0, SRCCOPY);
}


void DIB::Destroy()
{
    SelectObject(m_DibHdc, m_DibHbmOld);          
    DeleteObject(m_DibHbm);
    DeleteDC(m_DibHdc);
}

