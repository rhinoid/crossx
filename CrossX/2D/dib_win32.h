#ifndef _H_DIB
#define _H_DIB

#include <windows.h>

enum DIBMODE
{
	DIBMODE_16BIT = 0,
	DIBMODE_32BIT
};


class DIB
{
public:
	DIB();
	~DIB();

	void			Create(HWND hwnd, int Width, int Height, void *Pixels, DIBMODE DIBMode);
	void			Blit(HDC DestDC);
	void			Destroy();


	DIBMODE         m_DIBMode;         // either a 16 or 32bit dib

	HDC             m_DibHdc;          // HDC of the DIB
	HBITMAP         m_DibHbm;          // HBITMAP of the DIB   
	HBITMAP         m_DibHbmOld;       // old HBITMAP from the hdc
	unsigned short *m_DibSrcBits;     // pointer to DIB pixel array
	int				m_DibWidth;
	int				m_DibHeight;
};

#endif
