#include "spot.h"
#include "spotsurface_mfc.h"

#define USEPACKER


// a is 0..256
// ================================================================================================
// 'a' is base-256, not 255, s alpha is ignored
// ================================================================================================
static inline void PixelLerp(unsigned int a, unsigned int &d, const unsigned int s)
{
	unsigned int dstrb = d      & 0xFF00FF;
	unsigned int dstag = d >> 8 & 0xFF00FF;

	unsigned int srcrb = s      & 0xFF00FF;
	unsigned int srcag = s >> 8 & 0xFF00FF;

	unsigned int drb = srcrb - dstrb;
	unsigned int dag = srcag - dstag;

	drb *= a;  
	dag *= a;  
	drb >>= 8;
	dag >>= 8;

	const unsigned int rb  = (drb + dstrb)      & 0x00FF00FF;
	const unsigned int ag  = (dag + dstag) << 8 & 0xFF00FF00;

	d = rb | ag;
}



#define rbmask 0xFF00FF
#define agmask 0xFF00FF00

typedef unsigned int uint32;

inline uint32 SpotFastBilerp32(uint32 a, uint32 b, uint32 c, uint32 d, uint32 xp, uint32 yp)
{
#define arb (a & rbmask)
#define brb (b & rbmask)
#define crb (c & rbmask)
#define drb (d & rbmask)

#define aag (a & agmask)
#define bag (b & agmask)
#define cag (c & agmask)
#define dag (d & agmask)

	uint32 agd1 = bag - aag;
	uint32 agd2 = dag - cag;
	uint32 rbd1 = brb - arb;
	agd1 >>= 8;
	uint32 rbd2 = drb - crb;
	agd2 >>= 8;

	rbd1 *= xp;  b = arb;
	agd1 *= xp;  rbd1 >>= 8;
	rbd2 *= xp;  d = crb;
	agd2 *= xp;  rbd2 >>= 8;

	b += rbd1;
	a += agd1;

	d += rbd2;
	c += agd2;

	b &= rbmask;
	a &= agmask;
	c &= agmask;
	d &= rbmask;

	// ---- d is now rb for the bottom
	//      c is     ag for the bottom
	//      b is     rb for the top
	//      a is     ag for the top

	c -= a;  // agd
	d -= b;  // rbd
	c >>= 8;

	d *= yp;
	c *= yp;
	d >>= 8;

	a += c;
	b += d;

	a &= agmask;
	b &= rbmask;

	return a | b;

#undef aag
#undef bag
#undef cag
#undef dag

#undef arb
#undef brb
#undef crb
#undef drb
}



#ifdef _WIN32_WCE

#define LOG(x)  { \
	  FILE *log; \
	  log = fopen("\\Program Files\\Syntrax\\log.txt","a"); \
	  if(log) \
{ \
		  fwrite((x), strlen(x), 1, log); \
		  fclose(log); \
		  fflush(log); \
} \
} 
#endif

#ifdef __SYMBIAN32__

#define LOG(x)  { \
	  FILE *log; \
	  log = fopen("c:\\system\\apps\\syntrax\\log.txt","a"); \
	  if(log) \
{ \
		  fwrite((x), strlen(x), 1, log); \
		  fclose(log); \
		  fflush(log); \
} \
} 
#endif


#ifdef _WIN32_WCE
#define assert(x) if(x==0) MessageBox(0, _T("oeps"), _T("oeps"), MB_ICONEXCLAMATION)
//#define assert(x)
#else
#include "assert.h"
#endif


#ifdef __SYMBIAN32__
typedef unsigned char BYTE;
#endif


////////////////////////////////////
// Implementation of small wrapper class to handle DibSections

CDIB::CDIB()
{
    m_pSrcBits = NULL;
    m_iWidth = 0;
    m_iHeight = 0;
    m_iSWidth = 0;
}

CDIB::~CDIB()
{
    Release();
}


bool CDIB::Create(int iSrcX, int iSrcY,
                  int iWidth, int iHeight)
{
    // Release the old DIB
    Release();

    m_iWidth = iWidth;
    m_iHeight = iHeight;
    
    // Each line of the DIB is always quad aligned.
    m_iSWidth = (((iWidth*sizeof(int)) + 3) & ~3);

	m_pSrcBits = (int *)malloc(m_iWidth*m_iHeight*4);
    return true;
}

bool CDIB::Create(int *Data, int iWidth, int iHeight)
{
    // Release the old DIB
    Release();

    m_iWidth = iWidth;
    m_iHeight = iHeight;
    
    // Each line of the DIB is always quad aligned.
    m_iSWidth = (((iWidth*sizeof(int)) + 3) & ~3);

	m_pSrcBits = (int *)malloc(m_iWidth*m_iHeight*4);

    int *src,*dest;
	dest = m_pSrcBits;
	src = Data;
	if(src)
	{
		for(int y=0; y<iHeight; y++)
		{
			for(int x=0; x<iWidth; x++)
			{
				*dest++ = *src++;
			}
		}
	}
	
	return true;
}

void CDIB::Release()
{
	if(m_pSrcBits) free(m_pSrcBits);
    m_pSrcBits = NULL;
    m_iWidth = 0;
    m_iHeight = 0;
}


////////////////////////////////////
// Surface implementation under MFC

CSurfaceMfc::CSurfaceMfc()
{
#ifdef __SYMBIAN32__
	m_FullPath = 0;
#endif
	m_Valid    = false;
	m_Width    = 0;
	m_Height   = 0;
	m_ColorKey = -1;
	m_ContextSurface = 0;
}


CSurfaceMfc::~CSurfaceMfc()
{
}


int *CSurfaceMfc::GetPixels()
{
	return m_Bitmap.m_pSrcBits;
}


void	CSurfaceMfc::SetContext(CRootSurface *RootSurface)
{
	m_ContextSurface = (CSurfaceMfcRoot *)RootSurface;
}


int		CSurfaceMfc::Create(int Width, int Height)
{
	int rc;
	assert(m_ContextSurface);	// only use surfaces created through the root surface

	if(m_ContextSurface)
	{
//		rc = m_Bitmap.CreateCompatibleBitmap(m_ContextSurface->m_WinDC, Width, Height);
		rc = m_Bitmap.Create(0, Width, Height);
		assert(rc);
		m_Width = Width;
		m_Height = Height;
		RemoveClipping();
	}

	return rc ? 1: 0;
}



#if defined WIN32 && !defined _WIN32_WCE
char *CSurfaceMfc::FullPath( const char *a_File )
{
	strcpy(m_Path, "assets\\");
	strcat(m_Path, a_File);
	return m_Path;
}
#endif

#if defined _WIN32_WCE
//-------------------------------------------------------------------------
/*!	Converts the given filename to its fullpath using unicode
*///-----------------------------------------------------------------------

char *CSurfaceMfc::FullPath( const char *a_File )
{
	mbstowcs(s_WTemp, a_File, 512);
	GetModuleFileNameW( 0, s_WFileName, _MAX_PATH );
	int pos = wcslen( s_WFileName );
	if (pos) while (--pos) if (s_WFileName[pos] == '\\') break;
	wcscpy( s_WFileName + pos + 1, s_WTemp );

	wcstombs(s_Cchar, s_WFileName, 512);
	return s_Cchar;
}
#endif

#ifdef __SYMBIAN32__

	#include <coecntrl.h>
	#include <coecobs.h>
	#include <eiklabel.h>
	#include <eikspane.h>
	#include <eikapp.h>
	#include <eikdoc.h>
	#include <e32std.h>
	char* CSurfaceMfc::FullPath( const char* a_File )
	{
		if (!m_FullPath)
		{
			m_FullPath = new char[512];
			m_LocalPath = new char[512];
			TFileName appname = CEikonEnv::Static()->EikAppUi()->Application()->AppFullName();
			int pos = 0;
			const unsigned short* p = appname.Ptr();
			while (*p) m_LocalPath[pos++] = *p++;
			m_LocalPath[pos] = 0;
			char* tp = strstr( m_LocalPath, ".app" );
			while (*tp != '\\') *tp-- = 0;
		}
		strcpy( m_FullPath, m_LocalPath );
		strcat( m_FullPath, a_File );
		return m_FullPath;
	}

#endif


int CSurfaceMfc::GetWidth()
{
	return m_Width;
}

int CSurfaceMfc::GetHeight()
{
	return m_Height;
}

void CSurfaceMfc::Enlarge()
{
	int *LargeBuf;

	LargeBuf = (int *)malloc(m_Bitmap.m_iWidth * m_Bitmap.m_iHeight * 4 * 4);

	int x,y;
	for(y=0; y<m_Bitmap.m_iHeight; y++)
	{
		int CurColor,RightColor, BottomColor;

		int CurR, CurG, CurB;
		int RightR, RightG, RightB;
		int BottomR, BottomG, BottomB;

		for(x=0; x<m_Bitmap.m_iWidth; x++)
		{
			CurColor = m_Bitmap.m_pSrcBits[y*m_Bitmap.m_iWidth + x];
			LargeBuf[y*m_Bitmap.m_iWidth*4 + x*2] = CurColor;

			if(x < (m_Bitmap.m_iWidth-1))
			{
				RightColor = m_Bitmap.m_pSrcBits[y*m_Bitmap.m_iWidth + x+1];
			}
			else
			{
				RightColor = m_Bitmap.m_pSrcBits[y*m_Bitmap.m_iWidth + x];
			}
			if(y < (m_Bitmap.m_iHeight-1))
			{
				BottomColor = m_Bitmap.m_pSrcBits[(y+1)*m_Bitmap.m_iWidth + x];
			}
			else
			{
				BottomColor = m_Bitmap.m_pSrcBits[y*m_Bitmap.m_iWidth + x];
			}
			CurR = (CurColor&0xff0000)>>16;
			CurG = (CurColor&0xff00)>>8;
			CurB = (CurColor&0xff);
			RightR = (RightColor&0xff0000)>>16;
			RightG = (RightColor&0xff00)>>8;
			RightB = (RightColor&0xff);
			BottomR = (BottomColor&0xff0000)>>16;
			BottomG = (BottomColor&0xff00)>>8;
			BottomB = (BottomColor&0xff);

			int NewR, NewG, NewB;
			NewR = (CurR+RightR)/2;
			NewG = (CurG+RightG)/2;
			NewB = (CurB+RightB)/2;
			LargeBuf[y*m_Bitmap.m_iWidth*4 + x*2 + 1] = (NewR<<16) + (NewG<<8) + (NewB);

			NewR = (CurR+BottomR)/2;
			NewG = (CurG+BottomG)/2;
			NewB = (CurB+BottomB)/2;
			LargeBuf[y*m_Bitmap.m_iWidth*4 + x*2 + m_Bitmap.m_iWidth*2] = (NewR<<16) + (NewG<<8) + (NewB);

			NewR = (RightR+BottomR)/2;
			NewG = (RightG+BottomG)/2;
			NewB = (RightB+BottomB)/2;
			LargeBuf[y*m_Bitmap.m_iWidth*4 + x*2 + m_Bitmap.m_iWidth*2 + 1] = (NewR<<16) + (NewG<<8) + (NewB);
		}
	}

	free(m_Bitmap.m_pSrcBits);
	m_Bitmap.m_pSrcBits = LargeBuf;
	m_Bitmap.m_iHeight *= 2;
	m_Bitmap.m_iWidth *= 2;
	m_Bitmap.m_iSWidth *= 2;
	m_Width *= 2;
	m_Height *= 2;
}

int		CSurfaceMfc::Load(const char *FileName)
{
	int rc;

	// load targa file
	BYTE* tgabuff = new BYTE[20];
	bool OK = true;

	
#ifdef USEPACKER
	{
		char filename[128];
		sprintf(filename, "%s.dat", FullPath(FileName));
//		LOG("--");
//		LOG(filename);
		Packer *MyPacker;
		MyPacker = new Packer;
		MyPacker->DeCompress(filename, tgabuff, 20);
		delete MyPacker;
	}
#else
	FILE* tga = fopen( FullPath(FileName), "rb" );
	if (!tga)
	{
		delete [] tgabuff;
		return 0; 
	}
	fread( tgabuff, 1, 20, tga );
	fclose( tga );
#endif
	
	int TgaIDLen;
	int TgaCMapType;
	int TgaImgType;
	int TgaCMapOrig;
	int TgaCMapLen;
	int TgaCMapSize;
	int TgaXPos;
	int TgaYPos;
	int TgaWidth;
	int TgaHeight;
	int TgaPixSize;
	TgaIDLen		= *tgabuff;
	TgaCMapType	= *(tgabuff + 1);
	TgaImgType	= *(tgabuff + 2);
	TgaCMapOrig	= *(tgabuff + 3) + 256 * *(tgabuff + 4);
	TgaCMapLen	= *(tgabuff + 5) + 256 * *(tgabuff + 6);
	TgaCMapSize	= *(tgabuff + 7);
	TgaXPos		= *(tgabuff + 8) + 256 * *(tgabuff + 9);
	TgaYPos		= *(tgabuff + 10) + 256 * *(tgabuff + 11);
	TgaWidth		= *(tgabuff + 12) + 256 * *(tgabuff + 13);
	TgaHeight		= *(tgabuff + 14) + 256 * *(tgabuff + 15);
	TgaPixSize	= *(tgabuff + 16);
	delete [] tgabuff;

	int w,h;
	w = TgaWidth;
	h = TgaHeight;
	int *dest;
	int size = w * 4 * h + 20;
	tgabuff = new BYTE[size];
	dest = new int[w*h];  // hier komt uitgepakte plaatje

#ifdef USEPACKER
	{
		char filename[128];
		sprintf(filename, "%s.dat", FullPath(FileName));
		Packer *MyPacker;
		MyPacker = new Packer;
		MyPacker->DeCompress(filename, tgabuff, size);
		delete MyPacker;
	}
#else
	tga = fopen( FullPath(FileName), "rb" );
	if (!tga)
	{
		delete [] tgabuff;
		delete [] dest;
		return 0;
	}
	int read = fread( tgabuff, 1, size, tga );
	fclose( tga );
#endif	

	
	if (TgaImgType == 1)
	{
		// Palettized image
		unsigned short* pal = new unsigned short[256];
		for ( int i = 0; i < 256; i++ )
		{
			int b = *(tgabuff + 18 + i * 3);
			int g = *(tgabuff + 18 + i * 3 + 1);
			int r = *(tgabuff + 18 + i * 3 + 2);
			pal[i] = (unsigned short)(((r >> 3) << 11) + ((g >> 2) << 5) + (b >> 3));
		}
		unsigned char* src = tgabuff + 18 + 768 + (h - 1) * w;
		unsigned short* dst = (unsigned short*)dest;
		for ( int y = 0; y < h; y++ )
		{
			for ( int x = 0; x < w; x++ )
			{
				int idx = *(src + x);
				*(dst + x) = pal[idx];
			}
			dst += w;
			src -= w;
		}
	}
	else
	{
		// Store the data at the specified target address
		unsigned char* src = (tgabuff + 18) + (((h - 1) * w)*4);
		unsigned int* dst = (unsigned int*)dest;
		for ( int i = 0; i < h; i++ )
		{
			for ( int x = 0; x < w; x++ )
			{
				int r,g,b,a,rgba;
				b= *src++;
				g= *src++;
				r= *src++;
				a= *src++;
				rgba = (a<<24)+(r<<16)+(g<<8)+b;
				*(dst + x) = rgba; //*(src + x);
			}
			dst += w;
			src -= (w*8);
		}
	}

	//Create dibsection
	rc = m_Bitmap.Create(dest, w, h);
	m_Width = w;
	m_Height = h;
	

	// Release temporary storage

	delete [] tgabuff;	//tga loader
	delete [] dest;		//tga loader

	return 1;
}


void	CSurfaceMfc::SetColorKey(int Color)
{
	assert(m_ContextSurface);	// only use surfaces created through the root surface
	m_ColorKey = Color;
}


void 	CSurfaceMfc::ClrColorKey(void)
{
	assert(m_ContextSurface);	// only use surfaces created through the root surface
	m_ColorKey = -1;
}


void 	CSurfaceMfc::Clear(int x, int y, int w, int h, int Color)
{

	assert(m_ContextSurface);	// only use surfaces created through the root surface

	//trivial reject
	if(x+w<m_ClipRect.left) return;	//out of bounds
	if(x>=m_ClipRect.right) return;	//out of bounds
	if(y+h<m_ClipRect.top) return;	//out of bounds
	if(y>=m_ClipRect.bottom) return;	//out of bounds

	//clipping
	if(x<m_ClipRect.left)
	{
		w -= (m_ClipRect.left-x);
		x = m_ClipRect.left;
	}
	if((x+w)>=m_ClipRect.right)
	{
		w -= ((x+w)-m_ClipRect.right);
	}

	if(y<m_ClipRect.top)
	{
		h -= (m_ClipRect.top-y);
		y = m_ClipRect.top;
	}
	if((y+h)>=m_ClipRect.bottom)
	{
		h -= ((y+h)-m_ClipRect.bottom);
	}


// Clearing


	int cx,cy;
	int *Dest;
	Dest = m_Bitmap.m_pSrcBits;
	Dest += x;
	Dest += (y*m_Width);
	for(cy=0; cy<h; cy++)
	{
		for(cx=0; cx<w; cx++)
		{
			*Dest++ = Color;	//clear bitmap
		}
		Dest += (m_Width-w);
	}
//	CompatDC.CreateCompatibleDC(m_ContextSurface->m_WinDC);
//	OldBitmap  = CompatDC.SelectObject(&m_Bitmap);
//	
//	CompatDC.FillSolidRect(x, y, w, h, Color);
//
//	CompatDC.SelectObject(OldBitmap);
}


void 	CSurfaceMfc::Clear(int x, int y, int w, int h, int Color, int Transparency)
{

	assert(m_ContextSurface);	// only use surfaces created through the root surface

	//trivial reject
	if(x+w<m_ClipRect.left) return;	//out of bounds
	if(x>=m_ClipRect.right) return;	//out of bounds
	if(y+h<m_ClipRect.top) return;	//out of bounds
	if(y>=m_ClipRect.bottom) return;	//out of bounds

	//clipping
	if(x<m_ClipRect.left)
	{
		w -= (m_ClipRect.left-x);
		x = m_ClipRect.left;
	}
	if((x+w)>=m_ClipRect.right)
	{
		w -= ((x+w)-m_ClipRect.right);
	}

	if(y<m_ClipRect.top)
	{
		h -= (m_ClipRect.top-y);
		y = m_ClipRect.top;
	}
	if((y+h)>=m_ClipRect.bottom)
	{
		h -= ((y+h)-m_ClipRect.bottom);
	}


// Clearing


	int cx,cy;
	int *Dest;
	Dest = m_Bitmap.m_pSrcBits;
	Dest += x;
	Dest += (y*m_Width);
	for(cy=0; cy<h; cy++)
	{
		int sr,sg,sb;
		sb = Color&0x000000ff;
		sg = (Color&0x0000ff00)>>8;
		sr = (Color&0x00ff0000)>>16;
		for(cx=0; cx<w; cx++)
		{
			int dr,dg,db;
			db = *Dest&0x000000ff;
			dg = (*Dest&0x0000ff00)>>8;
			dr = (*Dest&0x00ff0000)>>16;

			db = (sb*Transparency) + (db * (255-Transparency));
			dg = (sg*Transparency) + (dg * (255-Transparency));
			dr = (sr*Transparency) + (dr * (255-Transparency));

			db >>=8;
			dg >>=8;
			dr >>=8;

			db &=255;
			dg &=255;
			dr &=255;

			*Dest++ = (db)+(dg<<8)+(dr<<16);
		}
		Dest += (m_Width-w);
	}
//	CompatDC.CreateCompatibleDC(m_ContextSurface->m_WinDC);
//	OldBitmap  = CompatDC.SelectObject(&m_Bitmap);
//	
//	CompatDC.FillSolidRect(x, y, w, h, Color);
//
//	CompatDC.SelectObject(OldBitmap);
}


//@@@@ Optimization:   Make 2 DC's static and use these constantly for blits

void	CSurfaceMfc::Blit(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transparency)
{
	CSurfaceMfc *SourceMfc;
	int x,y;
	int *Src;
	int *Dest;

	//trivial reject
	if(dx+w<m_ClipRect.left) return;	//out of bounds
	if(dx>=m_ClipRect.right) return;	//out of bounds
	if(dy+h<m_ClipRect.top) return;	//out of bounds
	if(dy>=m_ClipRect.bottom) return;	//out of bounds

	//clipping
	if(dx<m_ClipRect.left)
	{
		sx += (m_ClipRect.left-dx);
		w -= (m_ClipRect.left-dx);
		dx = m_ClipRect.left;
	}
	if((dx+w)>=m_ClipRect.right)
	{
		w -= ((dx+w)-m_ClipRect.right);
	}

	if(dy<m_ClipRect.top)
	{
		sy += (m_ClipRect.top-dy);
		h -= (m_ClipRect.top-dy);
		dy = m_ClipRect.top;
	}
	if((dy+h)>=m_ClipRect.bottom)
	{
		h -= ((dy+h)-m_ClipRect.bottom);
	}

	if(Transparency!=255)
	{
		BlitTransp(Source, sx, sy, dx, dy, w, h, Transparency);
		return;
	}

// blitting

	SourceMfc = (CSurfaceMfc *)Source;

	Src   = SourceMfc->m_Bitmap.m_pSrcBits;
	Dest  = m_Bitmap.m_pSrcBits;

	Src  += (sx) + (sy*SourceMfc->m_Width);
	Dest += (dx) + (dy*m_Width);

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			int alpha;
			int sr,sg,sb,dr,dg,db;
			sb = *Src&0x000000ff;
			sg = (*Src&0x0000ff00)>>8;
			sr = (*Src&0x00ff0000)>>16;
			alpha = (*Src&0xff000000)>>24;
			db = *Dest&0x000000ff;
			dg = (*Dest&0x0000ff00)>>8;
			dr = (*Dest&0x00ff0000)>>16;

			db = (sb*alpha) + (db * (255-alpha));
			dg = (sg*alpha) + (dg * (255-alpha));
			dr = (sr*alpha) + (dr * (255-alpha));

			db >>=8;
			dg >>=8;
			dr >>=8;

			db &=255;
			dg &=255;
			dr &=255;

			*Dest++ = (db)+(dg<<8)+(dr<<16);
			Src++;
		}
		Src += SourceMfc->m_Width - w;
		Dest += m_Width - w;
	}

#if 0
	CSurfaceMfc *SourceMfc;
	CDC		 CompatDCSrc,   CompatDCDest;
	CBitmap *OldBitmapSrc, *OldBitmapDest;

	assert(m_ContextSurface);	// only use surfaces created through the root surface
	assert(Source);

	SourceMfc = (CSurfaceMfc *)Source;
	CompatDCSrc.CreateCompatibleDC(m_ContextSurface->m_WinDC);
	CompatDCDest.CreateCompatibleDC(m_ContextSurface->m_WinDC);
	OldBitmapSrc  = CompatDCSrc.SelectObject(&SourceMfc->m_Bitmap);
	OldBitmapDest = CompatDCDest.SelectObject(&m_Bitmap);

	CompatDCDest.BitBlt(dx, dy, w, h, &CompatDCSrc, sx, sy, SRCCOPY);

	CompatDCSrc.SelectObject(OldBitmapSrc);
	CompatDCDest.SelectObject(OldBitmapDest);
#endif
}

void    CSurfaceMfc::RemoveClipping()
{
	m_ClipRect.SetRect(0, 0, m_Width, m_Height);
}

void    CSurfaceMfc::SetClipping(SRect ClipRect)
{
	// accept new clipping rect
	m_ClipRect = ClipRect;

	//Clip clippingrect against surface boundary
	if(m_ClipRect.left   < 0) m_ClipRect.left = 0;
	if(m_ClipRect.top    < 0) m_ClipRect.top  = 0;
	if(m_ClipRect.right  > m_Width)  m_ClipRect.right  = m_Width;
	if(m_ClipRect.bottom > m_Height) m_ClipRect.bottom = m_Height;
}

void    CSurfaceMfc::BlitColor(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transp, int Color)
{
	CSurfaceMfc *SourceMfc;
	int x,y;
	int *Src;
	int *Dest;

	//trivial reject
	if(dx+w<m_ClipRect.left) return;	//out of bounds
	if(dx>=m_ClipRect.right) return;	//out of bounds
	if(dy+h<m_ClipRect.top) return;	//out of bounds
	if(dy>=m_ClipRect.bottom) return;	//out of bounds

	//clipping
	if(dx<m_ClipRect.left)
	{
		sx += (m_ClipRect.left-dx);
		w -= (m_ClipRect.left-dx);
		dx = m_ClipRect.left;
	}
	if((dx+w)>=m_ClipRect.right)
	{
		w -= ((dx+w)-m_ClipRect.right);
	}

	if(dy<m_ClipRect.top)
	{
		sy += (m_ClipRect.top-dy);
		h -= (m_ClipRect.top-dy);
		dy = m_ClipRect.top;
	}
	if((dy+h)>=m_ClipRect.bottom)
	{
		h -= ((dy+h)-m_ClipRect.bottom);
	}

// blitting

	SourceMfc = (CSurfaceMfc *)Source;

	Src   = SourceMfc->m_Bitmap.m_pSrcBits;
	Dest  = m_Bitmap.m_pSrcBits;

	Src  += (sx) + (sy*SourceMfc->m_Width);
	Dest += (dx) + (dy*m_Width);

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			int alpha;
			int sr,sg,sb,dr,dg,db;
			int scolor;
			scolor = *Src;
			if((scolor&0xffffff) == 0xffffff) //only the white color in the charset gets replaced
			{
				sb = Color&0x000000ff;
				sg = (Color&0x0000ff00)>>8;
				sr = (Color&0x00ff0000)>>16;
			}
			else
			{
				sb = scolor&0x000000ff;
				sg = (scolor&0x0000ff00)>>8;
				sr = (scolor&0x00ff0000)>>16;
			}
			alpha = (*Src&0xff000000)>>24;
			db = *Dest&0x000000ff;
			dg = (*Dest&0x0000ff00)>>8;
			dr = (*Dest&0x00ff0000)>>16;

			alpha *= Transp;
			alpha >>= 8;
			if(alpha)
			{
				db = (sb*alpha) + (db * (255-alpha));
				dg = (sg*alpha) + (dg * (255-alpha));
				dr = (sr*alpha) + (dr * (255-alpha));

				db >>= 8;
				dg >>= 8;
				dr >>= 8;

				db &= 255;
				dg &= 255;
				dr &= 255;

				*Dest++ = (db)+(dg<<8)+(dr<<16);
				Src++;
			}
			else //completely transparent
			{
				Dest++;
				Src++;
			}
		}
		Src += SourceMfc->m_Width - w;
		Dest += m_Width - w;
	}
}

void	CSurfaceMfc::BlitTransp(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transp)
{
	CSurfaceMfc *SourceMfc;
	int x,y;
	int *Src;
	int *Dest;

	//trivial reject
	if(dx+w<m_ClipRect.left) return;	//out of bounds
	if(dx>=m_ClipRect.right) return;	//out of bounds
	if(dy+h<m_ClipRect.top) return;	//out of bounds
	if(dy>=m_ClipRect.bottom) return;	//out of bounds

	//clipping
	if(dx<m_ClipRect.left)
	{
		sx += (m_ClipRect.left-dx);
		w -= (m_ClipRect.left-dx);
		dx = m_ClipRect.left;
	}
	if((dx+w)>=m_ClipRect.right)
	{
		w -= ((dx+w)-m_ClipRect.right);
	}

	if(dy<m_ClipRect.top)
	{
		sy += (m_ClipRect.top-dy);
		h -= (m_ClipRect.top-dy);
		dy = m_ClipRect.top;
	}
	if((dy+h)>=m_ClipRect.bottom)
	{
		h -= ((dy+h)-m_ClipRect.bottom);
	}

// blitting

	SourceMfc = (CSurfaceMfc *)Source;

	Src   = SourceMfc->m_Bitmap.m_pSrcBits;
	Dest  = m_Bitmap.m_pSrcBits;

	Src  += (sx) + (sy*SourceMfc->m_Width);
	Dest += (dx) + (dy*m_Width);

	for(y=0; y<h; y++)
	{
		for(x=0; x<w; x++)
		{
			int alpha;
			int sr,sg,sb,dr,dg,db;
			sb = *Src&0x000000ff;
			sg = (*Src&0x0000ff00)>>8;
			sr = (*Src&0x00ff0000)>>16;
			alpha = (*Src&0xff000000)>>24;
			db = *Dest&0x000000ff;
			dg = (*Dest&0x0000ff00)>>8;
			dr = (*Dest&0x00ff0000)>>16;

			alpha *= Transp;
			alpha >>= 8;

			db = (sb*alpha) + (db * (255-alpha));
			dg = (sg*alpha) + (dg * (255-alpha));
			dr = (sr*alpha) + (dr * (255-alpha));

			db >>=8;
			dg >>=8;
			dr >>=8;

			db &=255;
			dg &=255;
			dr &=255;

			*Dest++ = (db)+(dg<<8)+(dr<<16);
			Src++;
		}
		Src += SourceMfc->m_Width - w;
		Dest += m_Width - w;
	}
}



void    CSurfaceMfc::PlotPix(int x, int y, int Color, int Transparency)
{
	int *Dest;

	if(x<m_ClipRect.left) return;	//out of bounds
	if(x>=m_ClipRect.right) return;	//out of bounds
	if(y<m_ClipRect.top) return;	//out of bounds
	if(y>=m_ClipRect.bottom) return;//out of bounds

	Dest  = m_Bitmap.m_pSrcBits;
	Dest += (x) + (y*m_Width);

	int sr,sg,sb,dr,dg,db;
	sb = Color&0x000000ff;
	sg = (Color&0x0000ff00)>>8;
	sr = (Color&0x00ff0000)>>16;
	db = *Dest&0x000000ff;
	dg = (*Dest&0x0000ff00)>>8;
	dr = (*Dest&0x00ff0000)>>16;

	db = (sb*Transparency) + (db * (255-Transparency));
	dg = (sg*Transparency) + (dg * (255-Transparency));
	dr = (sr*Transparency) + (dr * (255-Transparency));

	db >>=8;
	dg >>=8;
	dr >>=8;

	db &=255;
	dg &=255;
	dr &=255;

	*Dest = (db)+(dg<<8)+(dr<<16);
}




///////////////////////////////////////
// RootSurface implementation under MFC

CSurfaceMfcRoot::CSurfaceMfcRoot()
{
}

CSurfaceMfcRoot::~CSurfaceMfcRoot()
{
}


int CSurfaceMfcRoot::Create(void)
{
	return 1;
}


CSurface       *CSurfaceMfcRoot::CreateSurface(void)
{
	CSurfaceMfc *NewSurface;

	NewSurface = new CSurfaceMfc();
	NewSurface->SetContext(this);

	return	NewSurface;
}

void	CSurfaceMfcRoot::SetBackBuffer(unsigned short *Buf, int Width, int Height)
{
	m_HardwareBuffer = Buf;
	m_Width = Width;
	m_Height = Height;
}


void	CSurfaceMfcRoot::Clear(int x, int y, int w, int h, int Color)
{

//	m_WinDC->FillSolidRect(x, y, w, h, Color);


}

int CSurfaceMfcRoot::GetWidth()
{
	return m_Width;
}

int CSurfaceMfcRoot::GetHeight()
{
	return m_Height;
}

void	CSurfaceMfcRoot::Blit(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Scaled)
{
	CSurfaceMfc *RealSurface;

	assert(Source);

	RealSurface = (CSurfaceMfc *) Source;

	int *SourceBytes, *SourceBytesY;
	SourceBytes = RealSurface->m_Bitmap.m_pSrcBits;


    if (m_HardwareBuffer)
    {
		unsigned short *Destbuf;
		Destbuf = (unsigned short *)m_HardwareBuffer;

		if(Scaled==0)
		{
			unsigned short PixelCol = 0;
			unsigned short * pusLine = Destbuf;
			if (pusLine == NULL) return; // NOT OK TO DRAW, return failure.

			SourceBytesY = SourceBytes;
			SourceBytesY += (sy*(RealSurface->m_Bitmap.m_iWidth))+(sx);
			pusLine += (dy*(m_Width))+(dx);
			for (unsigned int y = 0; y < h; y++) {
				unsigned short * pusDest = pusLine;
				int * pusSource = SourceBytesY;

				if((dy+y)>=m_Height) break; //end of draw since we reached the bottom of the screen
				if((dy+y)>=0) //don't draw stuff which falls outside of the screen
				{
					for (unsigned int x = 0; x < w; x++) {
						int sr,sg,sb;
						sb = *pusSource&0x000000ff;
						sg = (*pusSource&0x0000ff00)>>8;
						sr = (*pusSource&0x00ff0000)>>16;
						PixelCol = (unsigned short) (((sr & 0xf8)<< 8) | ((sg & 0xfc) << 3) | ((sb & 0xf8)>>3));
						pusSource++;
						if((dx+x)>=0 && (dx+x)<m_Width) //only draw stuff which falls in the screen
						{
							*pusDest++ = PixelCol;
						}
						else
						{
							pusDest++;
						}
					}
				}
				pusLine += m_Width;
				SourceBytesY += RealSurface->m_Bitmap.m_iWidth;
			}
		}
		else
		{
			int u,v; // current stepper door de source picture
			int du, dv; //current step size for u and v
			int x,y;
			int sourcewidth = RealSurface->m_Bitmap.m_iWidth;

			int w,h;
#if defined(PHAL_SYMBIAN_SERIE60)
			w = 176; //width
			h = 208; //height
#endif
#if defined(PHAL_SYMBIAN_UIQ)
			w = 208; //width
			h = 320; //height
#endif
#ifdef WIN32
			w = 208; //width
			h = 320; //height
#endif


#if defined(PHAL_SYMBIAN_SERIE60)
			du = (RealSurface->m_Width<<8)/w; //m_Width;
			dv = (RealSurface->m_Height<<8)/h;

			v=0;
			for(y=0; y<h; y++)
			{
				unsigned int *tempy;
				tempy = (unsigned int *) &SourceBytes[((v>>8)*sourcewidth)];
				u=0;
				for(x=0; x<w; x++)
				{
					unsigned int *temp;
					unsigned short PixelCol;
					unsigned int color1,color2,color3,color4;
					int sr,sg,sb;
					temp = tempy+(u>>8);
					if((u>>8)==w-1)
					{
						if((v>>8)==h-1)
						{
							color1 = *temp;
							color2 = *(temp);
							color3 = *(temp);
							color4 = *(temp);
						}
						else
						{
							color1 = *temp;
							color2 = *(temp);
							color3 = *(temp+sourcewidth);
							color4 = *(temp+sourcewidth);
						}
					}
					else
					{
						if((v>>8)==w-1)
						{
							color1 = *temp;
							color2 = *(temp+1);
							color3 = *(temp);
							color4 = *(temp+1);
						}
						else
						{
							color1 = *temp;
							color2 = *(temp+1);
							color3 = *(temp+sourcewidth);
							color4 = *(temp+sourcewidth+1);
						}
					}
					color1 = SpotFastBilerp32(color1, color2, color3, color4, u&255, v&255);
					sb = color1&0x000000ff;
					sg = (color1&0x0000ff00)>>8;
					sr = (color1&0x00ff0000)>>16;
					PixelCol = (unsigned short) (((sr & 0xf8)<< 8) | ((sg & 0xfc) << 3) | ((sb & 0xf8)>>3));
					*Destbuf++ = PixelCol;
					u+=du;
				}
				Destbuf += (m_Width-w);
				v+=dv;
			}
#endif


#if defined(PHAL_SYMBIAN_UIQ) || WIN32
			du = (RealSurface->m_Width<<8)/w; //m_Width;
			dv = (RealSurface->m_Height<<8)/h;

			v=0;
			for(y=0; y<h; y++)
			{
				unsigned int *tempy;
				tempy = (unsigned int *) &SourceBytes[((v>>8)*sourcewidth)];
				u=0;
				for(x=0; x<w; x++)
				{
					unsigned int *temp;
					unsigned short PixelCol;
					unsigned int color1,color2,color3,color4;
					int sr,sg,sb;
					temp = tempy+(u>>8);
					if((u>>8)==w-1)
					{
						color1 = *temp;
						color2 = *(temp);
					}
					else
					{
						color1 = *temp;
						color2 = *(temp+1);
					}
					
					PixelLerp((u&255), color1, color2);
					PixelCol = ((color1>>3)&0x1f)|((color1>>5)&0x07e0)|((color1>>8)&0xf800);
					*Destbuf++ = PixelCol;
					u+=du;
				}
				Destbuf += (m_Width-w);
				v+=dv;
			}
#endif
		}

	}
}




/**************************************************************
	LZARI.C -- A Data Compression Program
	(tab = 4 spaces)
***************************************************************
	4/7/1989 Haruhiko Okumura
	Use, distribute, and modify this program freely.
	Please send me your improved versions.
		PC-VAN		SCIENCE
		NIFTY-Serve	PAF01022
		CompuServe	74050,1022
**************************************************************/
//#include <stdio.h>
//#include <stdlib.h>
//#include <string.h>
//#include <ctype.h>

/********** Bit I/O **********/



Packer::Packer()
{
	InitMembers();
}

Packer::~Packer()
{
}

void Packer::InitMembers(void)
{

	inbuffer = 0;
	outbuffer = 0;
	infile = 0;
	outfile = 0;
	textsize = 0;
	codesize = 0;
	printcount = 0;
	buffer = 0;
	mask = 0;
	memset(text_buf, 0, sizeof(text_buf));
	match_position = 0;
	match_length = 0;
	memset(lson, 0, sizeof(lson));
	memset(rson, 0, sizeof(rson));
	memset(dad, 0, sizeof(dad));
	low = 0;
	high = 0;
	value = 0;
	shifts = 0;
	memset(char_to_sym, 0, sizeof(char_to_sym));
	memset(sym_to_char, 0, sizeof(sym_to_char));
	memset(sym_freq, 0, sizeof(sym_freq));
	memset(sym_cum, 0, sizeof(sym_cum));

	low = 0;
	high = PACKER_Q4;
	value = 0;
	shifts = 0;
	textsize = 0;
	codesize = 0;
	printcount = 0;
}

void Packer::Error(char *message)
{
//	printf("\n%s\n", message);
//	exit(EXIT_FAILURE);
}


/********** LZSS with multiple binary trees **********/


void Packer::InitTree(void)  /* Initialize trees */
{
	int  i;

	/* For i = 0 to N - 1, rson[i] and lson[i] will be the right and
	   left children of node i.  These nodes need not be initialized.
	   Also, dad[i] is the parent of node i.  These are initialized to
	   NIL (= N), which stands for 'not used.'
	   For i = 0 to 255, rson[N + i + 1] is the root of the tree
	   for strings that begin with character i.  These are initialized
	   to NIL.  Note there are 256 trees. */

	for (i = PACKER_N + 1; i <= PACKER_N + 256; i++) rson[i] = PACKER_NIL;	/* root */
	for (i = 0; i < PACKER_N; i++) dad[i] = PACKER_NIL;	/* node */
}

void Packer::InsertNode(int r)
	/* Inserts string of length F, text_buf[r..r+F-1], into one of the
	   trees (text_buf[r]'th tree) and returns the longest-match position
	   and length via the global variables match_position and match_length.
	   If match_length = F, then removes the old node in favor of the new
	   one, because the old one will be deleted sooner.
	   Note r plays double role, as tree node and position in buffer. */
{
	int  i, p, cmp, temp;
	unsigned char  *key;

	cmp = 1;  key = &text_buf[r];  p = PACKER_N + 1 + key[0];
	rson[r] = lson[r] = PACKER_NIL;  match_length = 0;
	for ( ; ; ) {
		if (cmp >= 0) {
			if (rson[p] != PACKER_NIL) p = rson[p];
			else {  rson[p] = r;  dad[r] = p;  return;  }
		} else {
			if (lson[p] != PACKER_NIL) p = lson[p];
			else {  lson[p] = r;  dad[r] = p;  return;  }
		}
		for (i = 1; i < PACKER_F; i++)
			if ((cmp = key[i] - text_buf[p + i]) != 0)  break;
		if (i > PACKER_THRESHOLD) {
			if (i > match_length) {
				match_position = (r - p) & (PACKER_N - 1);
				if ((match_length = i) >= PACKER_F) break;
			} else if (i == match_length) {
				if ((temp = (r - p) & (PACKER_N - 1)) < match_position)
					match_position = temp;
			}
		}
	}
	dad[r] = dad[p];  lson[r] = lson[p];  rson[r] = rson[p];
	dad[lson[p]] = r;  dad[rson[p]] = r;
	if (rson[dad[p]] == p) rson[dad[p]] = r;
	else                   lson[dad[p]] = r;
	dad[p] = PACKER_NIL;  /* remove p */
}

void Packer::DeleteNode(int p)  /* Delete node p from tree */
{
	int  q;
	
	if (dad[p] == PACKER_NIL) return;  /* not in tree */
	if (rson[p] == PACKER_NIL) q = lson[p];
	else if (lson[p] == PACKER_NIL) q = rson[p];
	else {
		q = lson[p];
		if (rson[q] != PACKER_NIL) {
			do {  q = rson[q];  } while (rson[q] != PACKER_NIL);
			rson[dad[q]] = lson[q];  dad[lson[q]] = dad[q];
			lson[q] = lson[p];  dad[lson[p]] = q;
		}
		rson[q] = rson[p];  dad[rson[p]] = q;
	}
	dad[q] = dad[p];
	if (rson[dad[p]] == p) rson[dad[p]] = q;
	else                   lson[dad[p]] = q;
	dad[p] = PACKER_NIL;
}

/********** Arithmetic Compression **********/

/*  If you are not familiar with arithmetic compression, you should read
		I. E. Witten, R. M. Neal, and J. G. Cleary,
			Communications of the ACM, Vol. 30, pp. 520-540 (1987),
	from which much have been borrowed.  */


void Packer::StartModel(void)  /* Initialize model */
{
	int ch, sym, i;
	
	sym_cum[PACKER_N_CHAR] = 0;
	for (sym = PACKER_N_CHAR; sym >= 1; sym--) {
		ch = sym - 1;
		char_to_sym[ch] = sym;  sym_to_char[sym] = ch;
		sym_freq[sym] = 1;
		sym_cum[sym - 1] = sym_cum[sym] + sym_freq[sym];
	}
	sym_freq[0] = 0;  /* sentinel (!= sym_freq[1]) */
	position_cum[PACKER_N] = 0;
	for (i = PACKER_N; i >= 1; i--)
		position_cum[i - 1] = position_cum[i] + 10000 / (i + 200);
			/* empirical distribution function (quite tentative) */
			/* Please devise a better mechanism! */
}

void Packer::UpdateModel(int sym)
{
	int i, c, ch_i, ch_sym;
	
	if (sym_cum[0] >= PACKER_MAX_CUM) {
		c = 0;
		for (i = PACKER_N_CHAR; i > 0; i--) {
			sym_cum[i] = c;
			c += (sym_freq[i] = (sym_freq[i] + 1) >> 1);
		}
		sym_cum[0] = c;
	}
	for (i = sym; sym_freq[i] == sym_freq[i - 1]; i--) ;
	if (i < sym) {
		ch_i = sym_to_char[i];    ch_sym = sym_to_char[sym];
		sym_to_char[i] = ch_sym;  sym_to_char[sym] = ch_i;
		char_to_sym[ch_i] = sym;  char_to_sym[ch_sym] = i;
	}
	sym_freq[i]++;
	while (--i >= 0) sym_cum[i]++;
}

void Packer::Output(int bit)  /* Output 1 bit, followed by its complements */
{
	PutBit(bit);
	for ( ; shifts > 0; shifts--) PutBit(! bit);
}

void Packer::EncodeChar(int ch)
{
	int  sym;
	unsigned long int  range;

	sym = char_to_sym[ch];
	range = high - low;
	high = low + (range * sym_cum[sym - 1]) / sym_cum[0];
	low +=       (range * sym_cum[sym    ]) / sym_cum[0];
	for ( ; ; ) {
		if (high <= PACKER_Q2) Output(0);
		else if (low >= PACKER_Q2) {
			Output(1);  low -= PACKER_Q2;  high -= PACKER_Q2;
		} else if (low >= PACKER_Q1 && high <= PACKER_Q3) {
			shifts++;  low -= PACKER_Q1;  high -= PACKER_Q1;
		} else break;
		low += low;  high += high;
	}
	UpdateModel(sym);
}

void Packer::EncodePosition(int position)
{
	unsigned long int  range;

	range = high - low;
	high = low + (range * position_cum[position    ]) / position_cum[0];
	low +=       (range * position_cum[position + 1]) / position_cum[0];
	for ( ; ; ) {
		if (high <= PACKER_Q2) Output(0);
		else if (low >= PACKER_Q2) {
			Output(1);  low -= PACKER_Q2;  high -= PACKER_Q2;
		} else if (low >= PACKER_Q1 && high <= PACKER_Q3) {
			shifts++;  low -= PACKER_Q1;  high -= PACKER_Q1;
		} else break;
		low += low;  high += high;
	}
}

void Packer::EncodeEnd(void)
{
	shifts++;
	if (low < PACKER_Q1) Output(0);  else Output(1);
	FlushBitBuffer();  /* flush bits remaining in buffer */
}

int Packer::BinarySearchSym(unsigned int x)
	/* 1      if x >= sym_cum[1],
	   N_CHAR if sym_cum[N_CHAR] > x,
	   i such that sym_cum[i - 1] > x >= sym_cum[i] otherwise */
{
	int i, j, k;
	
	i = 1;  j = PACKER_N_CHAR;
	while (i < j) {
		k = (i + j) / 2;
		if (sym_cum[k] > x) i = k + 1;  else j = k;
	}
	return i;
}

int Packer::BinarySearchPos(unsigned int x)
	/* 0 if x >= position_cum[1],
	   N - 1 if position_cum[N] > x,
	   i such that position_cum[i] > x >= position_cum[i + 1] otherwise */
{
	int i, j, k;
	
	i = 1;  j = PACKER_N;
	while (i < j) {
		k = (i + j) / 2;
		if (position_cum[k] > x) i = k + 1;  else j = k;
	}
	return i - 1;
}

void Packer::StartDecode(void)
{
	int i;

	for (i = 0; i < PACKER_M + 2; i++)
		value = 2 * value + GetBit();
}

int Packer::DecodeChar(void)
{
	int	 sym, ch;
	unsigned long int  range;
	
	range = high - low;
	sym = BinarySearchSym((unsigned int)
		(((value - low + 1) * sym_cum[0] - 1) / range));
	high = low + (range * sym_cum[sym - 1]) / sym_cum[0];
	low +=       (range * sym_cum[sym    ]) / sym_cum[0];
	for ( ; ; ) {
		if (low >= PACKER_Q2) {
			value -= PACKER_Q2;  low -= PACKER_Q2;  high -= PACKER_Q2;
		} else if (low >= PACKER_Q1 && high <= PACKER_Q3) {
			value -= PACKER_Q1;  low -= PACKER_Q1;  high -= PACKER_Q1;
		} else if (high > PACKER_Q2) break;
		low += low;  high += high;
		value = 2 * value + GetBit();
	}
	ch = sym_to_char[sym];
	UpdateModel(sym);
	return ch;
}

int Packer::DecodePosition(void)
{
	int position;
	unsigned long int  range;
	
	range = high - low;
	position = BinarySearchPos((unsigned int)
		(((value - low + 1) * position_cum[0] - 1) / range));
	high = low + (range * position_cum[position    ]) / position_cum[0];
	low +=       (range * position_cum[position + 1]) / position_cum[0];
	for ( ; ; ) {
		if (low >= PACKER_Q2) {
			value -= PACKER_Q2;  low -= PACKER_Q2;  high -= PACKER_Q2;
		} else if (low >= PACKER_Q1 && high <= PACKER_Q3) {
			value -= PACKER_Q1;  low -= PACKER_Q1;  high -= PACKER_Q1;
		} else if (high > PACKER_Q2) break;
		low += low;  high += high;
		value = 2 * value + GetBit();
	}
	return position;
}

/********** Encode and Decode **********/

void Packer::Encode(void)
{
	int  i, c, len, r, s, last_match_length;
	
buffer = 0;  mask = 128;

	fseek(infile, 0L, SEEK_END);
	textsize = ftell(infile);
	if (fwrite(&textsize, sizeof textsize, 1, outfile) < 1)
		Error("Write Error");  /* output size of text */
	codesize += sizeof textsize;
	if (textsize == 0) return;
	fseek( infile, 0L, SEEK_SET );
//	rewind(infile);	//since rewind is not supported on win32, we use fseek
	textsize = 0;
	StartModel();  InitTree();
	s = 0;  r = PACKER_N - PACKER_F;
	for (i = s; i < r; i++) text_buf[i] = ' ';
	for (len = 0; len < PACKER_F && (c = getc(infile)) != EOF; len++)
		text_buf[r + len] = c;
	textsize = len;
	for (i = 1; i <= PACKER_F; i++) InsertNode(r - i);
	InsertNode(r);
	do {
		if (match_length > len) match_length = len;
		if (match_length <= PACKER_THRESHOLD) {
			match_length = 1;  EncodeChar(text_buf[r]);
		} else {
			EncodeChar(255 - PACKER_THRESHOLD + match_length);
			EncodePosition(match_position - 1);
		}
		last_match_length = match_length;
		for (i = 0; i < last_match_length &&
				(c = getc(infile)) != EOF; i++) {
			DeleteNode(s);  text_buf[s] = c;
			if (s < PACKER_F - 1) text_buf[s + PACKER_N] = c;
			s = (s + 1) & (PACKER_N - 1);
			r = (r + 1) & (PACKER_N - 1);
			InsertNode(r);
		}
		if ((textsize += i) > printcount) {
			printf("%12ld\r", textsize);  printcount += 1024;
		}
		while (i++ < last_match_length) {
			DeleteNode(s);
			s = (s + 1) & (PACKER_N - 1);
			r = (r + 1) & (PACKER_N - 1);
			if (--len) InsertNode(r);
		}
	} while (len > 0);
	EncodeEnd();
	printf("In : %lu bytes\n", textsize);
	printf("Out: %lu bytes\n", codesize);
	printf("Out/In: %.3f\n", (double)codesize / textsize);
}



void Packer::PutBit(int bit)  /* Output one bit (bit = 0,1) */
{

	if (bit) buffer |= mask;
	if ((mask >>= 1) == 0) {
		if(outbuffer)
		{
			*outbuffer++ = buffer;
		}
		else
		{
			if (putc(buffer, outfile) == EOF) Error("Write Error");
		}
		buffer = 0;  mask = 128;  codesize++;
	}
}


void Packer::FlushBitBuffer(void)  /* Send remaining bits */
{
	int  i;
	
	for (i = 0; i < 7; i++) PutBit(0);
}

int Packer::GetBit(void)  /* Get one bit (0 or 1) */
{

	if ((mask >>= 1) == 0) {
		if(inbuffer)
		{
			buffer = *inbuffer++;
		}
		else
		{
			buffer = getc(infile);
		}
		mask = 128;
	}
	return ((buffer & mask) != 0);
}


void	Packer::DeCompress(void *SrcBuffer, void *DestBuffer, int BufferSize)
{
	int  i, j, k, r, c;
	unsigned long int  count;
	InitMembers();
	buffer = 0;
	mask = 0;

	inbuffer  = (unsigned char *) SrcBuffer;
	outbuffer = (unsigned char *) DestBuffer;

	textsize =  *((unsigned long int *)inbuffer);
	inbuffer+=4;
	if (textsize == 0) return;
	StartDecode();  StartModel();
	for (i = 0; i < PACKER_N - PACKER_F; i++) text_buf[i] = ' ';
	r = PACKER_N - PACKER_F;
	for (count = 0; count < textsize; ) {
		c = DecodeChar();
		if (c < 256) {
			/*putc(c, outfile);*/ *outbuffer++ = c;  text_buf[r++] = c;
			r &= (PACKER_N - 1);  count++;
		} else {
			i = (r - DecodePosition() - 1) & (PACKER_N - 1);
			j = c - 255 + PACKER_THRESHOLD;
			for (k = 0; k < j; k++) {
				c = text_buf[(i + k) & (PACKER_N - 1)];
				/*putc(c, outfile);*/ *outbuffer++ = c;  text_buf[r++] = c;
				r &= (PACKER_N - 1);  count++;
				if(count==BufferSize) break;
			}
		}
		if(count==BufferSize) break;
	}
}

void	Packer::DeCompress(char *SrcFileName, void *DestBuffer, int BufferSize)
{
	int  i, j, k, r, c;
	unsigned long int  count;
	InitMembers();
	buffer = 0;
	mask = 0;

	infile = fopen(SrcFileName, "rb");
	if(infile==0) return;


	inbuffer = 0;
	outbuffer = (unsigned char *) DestBuffer;

	if (fread(&textsize, sizeof textsize, 1, infile) < 1)
		Error("Read Error");  /* read size of text */
	if (textsize == 0) return;
	StartDecode();  StartModel();
	for (i = 0; i < PACKER_N - PACKER_F; i++) text_buf[i] = ' ';
	r = PACKER_N - PACKER_F;
	for (count = 0; count < textsize; ) {
		c = DecodeChar();
		if (c < 256) {
			/*putc(c, outfile);*/ *outbuffer++ = c;  text_buf[r++] = c;
			r &= (PACKER_N - 1);  count++;
		} else {
			i = (r - DecodePosition() - 1) & (PACKER_N - 1);
			j = c - 255 + PACKER_THRESHOLD;
			for (k = 0; k < j; k++) {
				c = text_buf[(i + k) & (PACKER_N - 1)];
				/*putc(c, outfile);*/ *outbuffer++ = c;  text_buf[r++] = c;
				r &= (PACKER_N - 1);  count++;
				if(count==BufferSize) break;
			}
		}
		if(count==BufferSize) break;
	}

	fclose(infile);
	infile = 0;
}


int	Packer::GetDecompressSize(void *SrcBuffer)
{
	InitMembers();
	inbuffer  = (unsigned char *) SrcBuffer;

	textsize =  *((unsigned long int *)inbuffer);
	return textsize;
}

int	Packer::GetDecompressSize(char *SrcFileName)
{
	InitMembers();
	infile = fopen(SrcFileName, "rb");
	if(infile==0) return -1;

	if (fread(&textsize, sizeof textsize, 1, infile) < 1)
		return -1;

	fclose(infile);
	return textsize;
}


int  Packer::Compress(void *SrcBuffer, char *DestFileName)
{
	int  i, c, len, r, s, last_match_length;

	InitMembers();
	buffer = 0;  mask = 128;

	outfile = fopen(DestFileName, "wb");
	if(outfile==0) return 0;
	infile = 0;


	inbuffer = (unsigned char *) SrcBuffer;
	outbuffer = 0;

	fseek(outfile, 0L, SEEK_END);
	textsize = ftell(outfile);
	if (fwrite(&textsize, sizeof textsize, 1, outfile) < 1)
	{
		fclose(outfile);
		return 0;
	}
	codesize += sizeof textsize;
	if (textsize == 0) return 0;
	textsize = 0;
	StartModel();  InitTree();
	s = 0;  r = PACKER_N - PACKER_F;
	for (i = s; i < r; i++) text_buf[i] = ' ';
	for (len = 0; len < PACKER_F && (/*c = getc(infile)*/ c = *inbuffer++) != EOF; len++)
		text_buf[r + len] = c;
	textsize = len;
	for (i = 1; i <= PACKER_F; i++) InsertNode(r - i);
	InsertNode(r);
	do {
		if (match_length > len) match_length = len;
		if (match_length <= PACKER_THRESHOLD) {
			match_length = 1;  EncodeChar(text_buf[r]);
		} else {
			EncodeChar(255 - PACKER_THRESHOLD + match_length);
			EncodePosition(match_position - 1);
		}
		last_match_length = match_length;
		for (i = 0; i < last_match_length &&
				(/*c = getc(infile)*/ c = *inbuffer++ ) != EOF; i++) {
			DeleteNode(s);  text_buf[s] = c;
			if (s < PACKER_F - 1) text_buf[s + PACKER_N] = c;
			s = (s + 1) & (PACKER_N - 1);
			r = (r + 1) & (PACKER_N - 1);
			InsertNode(r);
		}
		while (i++ < last_match_length) {
			DeleteNode(s);
			s = (s + 1) & (PACKER_N - 1);
			r = (r + 1) & (PACKER_N - 1);
			if (--len) InsertNode(r);
		}
	} while (len > 0);
	EncodeEnd();
	return codesize;
}

int  Packer::Compress(void *SrcBuffer, void *DestBuffer, int BufferSize)
{
	int  i, c, len, r, s, last_match_length;

	InitMembers();
	buffer = 0;  mask = 128;

	outfile = 0;
	infile = 0;


	inbuffer = (unsigned char *) SrcBuffer;
	outbuffer = (unsigned char *) DestBuffer;

	textsize = BufferSize;
	*((unsigned int *)outbuffer) = textsize;
	outbuffer += 4;

	codesize += sizeof textsize;
	if (textsize == 0) return 0;
	textsize = 0;
	StartModel();  InitTree();
	s = 0;  r = PACKER_N - PACKER_F;
	for (i = s; i < r; i++) text_buf[i] = ' ';
	for (len = 0; len < PACKER_F && (/*c = getc(infile)*/ c = *inbuffer++) != EOF; len++)
		text_buf[r + len] = c;
	textsize = len;
	for (i = 1; i <= PACKER_F; i++) InsertNode(r - i);
	InsertNode(r);
	do {
		if (match_length > len) match_length = len;
		if (match_length <= PACKER_THRESHOLD) {
			match_length = 1;  EncodeChar(text_buf[r]);
		} else {
			EncodeChar(255 - PACKER_THRESHOLD + match_length);
			EncodePosition(match_position - 1);
		}
		last_match_length = match_length;
		for (i = 0; i < last_match_length &&
				(/*c = getc(infile)*/ c = *inbuffer++ ) != EOF; i++) {
			DeleteNode(s);  text_buf[s] = c;
			if (s < PACKER_F - 1) text_buf[s + PACKER_N] = c;
			s = (s + 1) & (PACKER_N - 1);
			r = (r + 1) & (PACKER_N - 1);
			InsertNode(r);
		}
		while (i++ < last_match_length) {
			DeleteNode(s);
			s = (s + 1) & (PACKER_N - 1);
			r = (r + 1) & (PACKER_N - 1);
			if (--len) InsertNode(r);
		}
	} while (len > 0);
	EncodeEnd();
	return codesize;
}



