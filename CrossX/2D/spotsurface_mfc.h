#ifndef _SPOTSURFACEMFC_H
#define _SPOTSURFACEMFC_H


#include <PocketHAL/PocketHAL.h>
#include <PocketHAL/surface.h>
#include <stdlib.h>
#include <stdio.h>

using namespace PHAL;

class CSurfaceMfcRoot;
class CSurfaceMfc;


// small wrapper class to handle dibsections
class CDIB
{
    public:
    CDIB();
    ~CDIB();

    bool            Create(int iSrcX, int iSrcY, int iWidth, int iHeight);
	bool			Create(int *Data, int iWidth, int iHeight);

    void            Release();

    // Varibles
    int            *m_pSrcBits;     // pointer to DIB pixel array
    int             m_iWidth;       // Width of the DIB
    int             m_iHeight;      // Height of the DIB
    int             m_iSWidth;      // Storage Width (in bytes)
};



//////////////////////////////////////////////////////////////////
// MFC root surface... use this ONLY to init the spotmaster with

class CSurfaceMfcRoot : public CRootSurface
{
friend class CSurfaceMfc;
public:
	CSurfaceMfcRoot();
	~CSurfaceMfcRoot();

	int			    Create(void);				// creates the root surface
	CSurface       *CreateSurface(void);		// creates a new surface compatible with the root surface

	void	Clear(int x, int y, int w, int h, int Color);
	void	Blit(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Scaled);
	int		GetWidth();
	int		GetHeight();

	// extra internal functions speciic for this platform
	void	SetBackBuffer(unsigned short *Buf, int Width, int Height);

private:
	bool	m_Valid;		// the surface is valid (it has been created)

	int		m_Width;
	int		m_Height;
	unsigned short *m_HardwareBuffer;
};


/////////////////////////////////////////////////////////////////
// MFC surface... Don't use this to init the spotmaster with, use
// the CSurfaceMfcRoot class instead

class CSurfaceMfc : public CSurface
{
friend class CSurfaceMfcRoot;
public:
	CSurfaceMfc();
	~CSurfaceMfc();

	int		Create(int Width, int Height);
	int		Load(const char *FileName);
	void	SetColorKey(int Color);
	void	ClrColorKey(void);

	void    Enlarge();


	void	Blit(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transp);
	void    BlitColor(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transp, int Color);
	void	Clear(int x, int y, int w, int h, int Color);
	void	Clear(int x, int y, int w, int h, int Color, int Transparency);
	void	PlotPix(int x, int y, int Color, int Transparency);
	int    *GetPixels();

	void    RemoveClipping();
	void    SetClipping(SRect ClipRect);

	int		GetWidth();
	int		GetHeight();


private:
	void	BlitTransp(CSurface *Source, int sx, int sy, int dx, int dy, int w, int h, int Transp);
	void	SetContext(CRootSurface *RootSurface);	// is only called by the root surface

	bool	m_Valid;		// the surface is valid (it has been created)

	int		m_Width;		// Width of surface
	int		m_Height;		// Height of Surface

	SRect	m_ClipRect;		// Current Clipping Rect

	int		m_ColorKey;		// Colorkey used for blitting

	CSurfaceMfcRoot *m_ContextSurface;	// optional surface used as reference to generate this surface
	CDIB	m_Bitmap;		// actual surface


	char* FullPath( const char* a_File );
#ifdef WIN32
	char		m_Path[256];
#endif

#ifdef _WIN32_WCE
	unsigned short  s_WFileName[512];
	unsigned short  s_WTemp[512];
	char            s_Cchar[512];
#endif

#ifdef __SYMBIAN32__
	char *m_FullPath, *m_LocalPath;
#endif	

};

#define PACKER_N		 4096	/* size of ring buffer */
#define PACKER_F		   60	/* upper limit for match_length */
#define PACKER_THRESHOLD	2   /* encode string into position and length
						   if match_length is greater than this */
#define PACKER_NIL			PACKER_N	/* index for root of binary search trees */

#define PACKER_M   15
#define PACKER_Q1  (1UL << PACKER_M)
#define PACKER_Q2  (2 * PACKER_Q1)
#define PACKER_Q3  (3 * PACKER_Q1)
#define PACKER_Q4  (4 * PACKER_Q1)
#define PACKER_MAX_CUM (PACKER_Q1 - 1)

#define PACKER_N_CHAR  (256 - PACKER_THRESHOLD + PACKER_F)


class Packer
{
public:
	Packer();
	~Packer();

	int  Compress(void *SrcBuffer, void *DestBuffer, int BufferSize);	//returns compressed size (buffersize is size of original buffer)
	int  Compress(void *SrcBuffer, char *DestFileName);					//returns compressed size

	void DeCompress(void *SrcBuffer, void *DestBuffer, int BufferSize = -1);	 //decompress one buffer into the other. Specify -1 as size to not have a bufferlimit
	void DeCompress(char *SrcFileName, void *DestBuffer, int BufferSize = -1); // decompress a file to memory. Specify -1 as size to not have a bufferlimit

	int  GetDecompressSize(void *SrcBuffer);
	int  GetDecompressSize(char *SrcFileName);	//-1 is error

private:
	int  main(int argc, char *argv[]);
	void Decode(void);
	void Encode(void);
	int  DecodePosition(void);
	int  DecodeChar(void);
	void StartDecode(void);
	int  BinarySearchPos(unsigned int x);
	int  BinarySearchSym(unsigned int x);
	void EncodeEnd(void);
	void EncodePosition(int position);
	void EncodeChar(int ch);
	void Output(int bit);  /* Output 1 bit, followed by its complements */
	void UpdateModel(int sym);
	void StartModel(void);  /* Initialize model */
	void DeleteNode(int p);  /* Delete node p from tree */
	void InsertNode(int r);
	void InitTree(void);  /* Initialize trees */
	int  GetBit(void);  /* Get one bit (0 or 1) */
	void FlushBitBuffer(void);  /* Send remaining bits */
	void PutBit(int bit);  /* Output one bit (bit = 0,1) */
	void Error(char *message);

	void InitMembers(void);

	unsigned char  *inbuffer;
	unsigned char  *outbuffer;
	FILE  *infile;
	FILE  *outfile;
	unsigned long int	textsize;
	unsigned long int	codesize;
	unsigned long int	printcount;

	unsigned int		buffer;
	unsigned int		mask;



	unsigned char		text_buf[PACKER_N + PACKER_F - 1];	/* ring buffer of size N,
				with extra F-1 bytes to facilitate string comparison */
	int					match_position;
	int					match_length;  /* of longest match.  These are
				set by the InsertNode() procedure. */
	int					lson[PACKER_N + 1];
	int					rson[PACKER_N + 257];
	int					dad[PACKER_N + 1];  /* left & right children &
				parents -- These constitute binary search trees. */

	/*	Q1 (= 2 to the M) must be sufficiently large, but not so
		large as the unsigned long 4 * Q1 * (Q1 - 1) overflows.  */

		/* character code = 0, 1, ..., N_CHAR - 1 */

	unsigned long int	low;
	unsigned long int	high;
	unsigned long int	value;
	int					shifts;  /* counts for magnifying low and high around Q2 */
	int					char_to_sym[PACKER_N_CHAR];
	int					sym_to_char[PACKER_N_CHAR + 1];
	unsigned int		sym_freq[PACKER_N_CHAR + 1];  /* frequency for symbols */
	unsigned int		sym_cum[PACKER_N_CHAR + 1];   /* cumulative freq for symbols */
	unsigned int		position_cum[PACKER_N + 1];   /* cumulative freq for positions */
};




#endif