#include "engine.h"


Camera::Camera()
{
}

Camera::~Camera()
{
}

void Camera::Set(Vector3 Eye, Vector3 Loc, Vector3 Up)
{
	m_Location = Loc;
	m_Eye = Eye;
	m_Up = Up;
}

void Camera::SetLocation(Vector3 v)
{
	m_Location = v;
}

void Camera::SetEye(Vector3 v)
{
	m_Eye = v;
}

void Camera::SetUp(Vector3 v)
{
	m_Up = v;
}


Matrix4 Camera::CreateMatrix()
{
	Matrix4 result;
	result.Lookat(m_Eye, m_Location, m_Up);
	return result;
}


