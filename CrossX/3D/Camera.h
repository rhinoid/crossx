#ifndef _H_CAMERA
#define _H_CAMERA

class Camera
{
public:
	Camera();
	~Camera();

	void Set(Vector3 Loc, Vector3 Dir, Vector3 Up); 
	void SetLocation(Vector3 v);
	void SetEye(Vector3 v);
	void SetUp(Vector3 v);

	Matrix4 CreateMatrix();

public:
	Vector3 m_Location;
	Vector3 m_Eye;
	Vector3 m_Up;
};

#endif
