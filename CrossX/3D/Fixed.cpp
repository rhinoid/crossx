
#include "fixed.h"


// --------------------------------------------------------------------------
// Calculate the inverse of the square root of fixed point number
//
// Parameters:
//	a		-	the numbers whose inverse of square root should be 
//				calculated
// --------------------------------------------------------------------------
EGL_Fixed EGL_InvSqrt(EGL_Fixed a) {
    EGL_Fixed x /*= (a+EGL_ONE)>>1*//*a>>1*/;
    /* 1/(2sqrt(x)) - extra divide by 2 to scale to 16 bits */
    static const U16 __gl_rsq_tab[] = { /* domain 0.5 .. 1.0-1/16 */
		0xb504, 0xaaaa, 0xa1e8, 0x9a5f, 0x93cd, 0x8e00, 0x88d6, 0x8432,
    };
    I32 i, exp;
    if (a == EGL_ZERO) return 0x7fffffff;
    if (a == EGL_ONE) return a;

#ifdef EGL_USE_CLZ
	exp = _CountLeadingZeros(a);
#else
    x = a;
    exp = 31;
    if (x & 0xffff0000) { exp -= 16; x >>= 16; }
    if (x & 0xff00) { exp -= 8; x >>= 8; }
    if (x & 0xf0) { exp -= 4; x >>= 4; }
    if (x & 0xc) { exp -= 2; x >>= 2; }
    if (x & 0x2) { exp -= 1; }
#endif
    x = __gl_rsq_tab[(a>>(28-exp))&0x7]<<1;
//printf("t %f %x %f %d %d\n", __GL_F_2_FLOAT(a), a, __GL_F_2_FLOAT(x), exp, 28-exp);
    exp -= 16;
    if (exp <= 0)
		x >>= -exp>>1;
    else
		x <<= (exp>>1)+(exp&1);
    if (exp&1) x = EGL_Mul(x, __gl_rsq_tab[0]);
//printf("nx %f\n", __GL_F_2_FLOAT(x));

    /* newton-raphson */
    /* x = x/2*(3-(a*x)*x) */
    i = 0;
    do {
//printf("%f\n", __GL_F_2_FLOAT(x));
		x = EGL_Mul((x>>1),(EGL_ONE*3 - EGL_Mul(EGL_Mul(a,x),x)));
    } while(++i < 3);
//printf("rsqrt %f %f\n", __GL_F_2_FLOAT(a), __GL_F_2_FLOAT(x));
    return x;
}
