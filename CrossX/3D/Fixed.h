

#ifndef EGL_FIXED_H
#define EGL_FIXED_H 1

#ifdef ARM 
	extern "C" int Mul64_16(int a, int b); 
	extern "C" unsigned int FDIV(unsigned int a, unsigned int b); 
	extern "C" int FDIVS(int a, int b); 
	extern "C" unsigned int SQRT(unsigned int a); 
#endif 

#define assert(x)




typedef signed char 		I8;
typedef unsigned char 		U8;
typedef short 				I16;
typedef unsigned short 		U16;
typedef int 				I32;
typedef unsigned int 		U32;


#ifdef EGL_XSCALE
#define EGL_USE_CLZ
#endif

#if defined(__SYMBIAN32__) && !defined (__WINS__) 
typedef unsigned long long	U64;
typedef long long			I64;
#else
typedef unsigned __int64	U64;
typedef __int64				I64;
#endif


// --------------------------------------------------------------------------
// Constants
// --------------------------------------------------------------------------


#define EGL_PRECISION 16					// number of fractional bits
#define EGL_ONE		  (1 << EGL_PRECISION)	// representation of 1
#define EGL_ZERO	  0						// representation of 0
#define EGL_HALF	  0x08000				// S15.16 0.5 
#define EGL_PINF	  0x7fffffff			// +inf 
#define EGL_MINF	  0x80000000			// -inf 

#define EGL_2PI			EGL_FixedFromFloat(6.28318530717958647692f)
#define EGL_R2PI		EGL_FixedFromFloat(1.0f/6.28318530717958647692f)

typedef I32 EGL_Fixed;


// --------------------------------------------------------------------------
// Calculate product of two fixed point numbers
//
// Parameters:
//	a			-	first operand
//	b			-	second operand
// --------------------------------------------------------------------------
inline EGL_Fixed EGL_Mul(EGL_Fixed a, EGL_Fixed b) {

#if defined(__SYMBIAN32__) && defined (__WINS__) 
	TInt64 src64(src);
	TInt64 mult64(mult);
	src64 *= mult64;
	src64 >>= RASFIXSHIFT;
	return src64.Low();
#else
	return (EGL_Fixed) (((I64) a * (I64) b)  >> EGL_PRECISION);
#endif

}


// --------------------------------------------------------------------------
// Calculate the inverse of the square root of fixed point number
//
// Parameters:
//	value		-	the numbers whose inverse of square root should be 
//					calculated
// --------------------------------------------------------------------------
EGL_Fixed EGL_InvSqrt(EGL_Fixed value);


#endif //ndef EGL_FIXED_H
