#include "3d/engine.h"
#include "3d/object.h"

void	Object::CustomSave(XString FileName)
{


// on symbian we'll probably never want to write a file in custom format
#ifndef __SYMBIAN32__
	int i, j;
	FILE *fp;
	FileName = MakeFullPath(FileName);
	fp = fopen(FileName.c_str(), "wb");
	if(fp)
	{
		//write nr of normals
		WriteInt(fp, m_Mesh.m_Normals.Size());
		for(i=0; i<m_Mesh.m_Normals.Size(); i++)
		{
			Vertice V;
			Vector3 N;

			//write normals
			V = m_Mesh.m_Normals[i];
			N = V.Point;
			WriteInt(fp ,N.M[0].val);
			WriteInt(fp ,N.M[1].val);
			WriteInt(fp ,N.M[2].val);
		}

		//write nr of Vertices
		WriteInt(fp, m_Mesh.m_Vertices.Size());
		for(i=0; i<m_Mesh.m_Vertices.Size(); i++)
		{
			Vertice V;
			Vector3 N;

			//write normals
			V = m_Mesh.m_Vertices[i];
			N = V.Point;
			WriteInt(fp ,N.M[0].val);
			WriteInt(fp ,N.M[1].val);
			WriteInt(fp ,N.M[2].val);
		}

		//Write nr of triangle list
		WriteInt(fp, m_Mesh.m_TriangleLists.Size());
		for(i=0; i<m_Mesh.m_TriangleLists.Size(); i++)
		{
			TriangleList Tl;
			Vector3 N;

			WriteInt(fp ,Tl.m_RenderFlags); //write Renderflags
				
			//write material
			Tl = m_Mesh.m_TriangleLists[i];
			N = Tl.m_Material.m_Ambient; //write ambient amount
			WriteInt(fp ,N.M[0].val);
			WriteInt(fp ,N.M[1].val);
			WriteInt(fp ,N.M[2].val);

			N = Tl.m_Material.m_Diffuse; //write diffuse amount
			WriteInt(fp ,N.M[0].val);
			WriteInt(fp ,N.M[1].val);
			WriteInt(fp ,N.M[2].val);

			WriteInt(fp ,Tl.m_Material.m_Shading); //write Shading amount

			WriteInt(fp ,Tl.m_Material.m_Shininess.val); //write Shininess amount

			N = Tl.m_Material.m_Specular; //write specular amount
			WriteInt(fp ,N.M[0].val);
			WriteInt(fp ,N.M[1].val);
			WriteInt(fp ,N.M[2].val);

			WriteInt(fp ,Tl.m_Material.m_Transparency.val); //write Transparency amount

			Texture *Te;
			Te = Tl.m_Material.m_Texture; //write Texture name
			if(Te==0)
			{
				unsigned char Len;
				Len = 0;
				fwrite(&Len, 1, 1, fp); //direct 0 byte
			}
			else
			{
				unsigned char Len;
				XString TexName;
				TexName = Te->GetName();
				Len = TexName.length();

				fwrite(&Len, 1, 1, fp); //length of name
				fwrite(TexName.c_str(), 1, Len, fp); //textname
			}

			//write nr of Triangles
			WriteInt(fp, Tl.m_Triangles.Size());
			for(j=0; j<Tl.m_Triangles.Size(); j++)
			{
				Triangle Tr;

				Tr = Tl.m_Triangles[j];

				WriteInt(fp ,Tr.m_Flags);

				WriteInt(fp ,Tr.m_Color[0]);
				WriteInt(fp ,Tr.m_Color[1]);
				WriteInt(fp ,Tr.m_Color[2]);

				WriteInt(fp ,Tr.m_Normal);

				WriteInt(fp ,Tr.m_U[0]);
				WriteInt(fp ,Tr.m_U[1]);
				WriteInt(fp ,Tr.m_U[2]);

				WriteInt(fp ,Tr.m_V[0]);
				WriteInt(fp ,Tr.m_V[1]);
				WriteInt(fp ,Tr.m_V[2]);

				WriteInt(fp ,Tr.m_Vertices[0]);
				WriteInt(fp ,Tr.m_Vertices[1]);
				WriteInt(fp ,Tr.m_Vertices[2]);

				WriteInt(fp ,Tr.m_VNormals[0]);
				WriteInt(fp ,Tr.m_VNormals[1]);
				WriteInt(fp ,Tr.m_VNormals[2]);
			}
		}

		fclose(fp);
	}

#endif
}

void Object::WriteInt(FILE *fp, int Number)
{
	unsigned char Number1;
	unsigned char Number2;
	unsigned char Number3;
	unsigned char Number4;
	unsigned char Number5;
	Number1 = 0;
	Number2 = 0;
	Number3 = 0;
	Number4 = 0;
	Number5 = 0;
	if(Number<0)
	{
		Number = -Number;
		Number1 = 0x80;
	}

	if(Number<0x40)
	{
		Number1 |= Number;
		fwrite(&Number1 , 1, 1, fp);//1bit sign, 1bit extension bit, 6 bit number
	}
	else
	{
		Number1 |= 0x40;  //extension bit
		if(Number<0x2000)
		{
			Number1 |= Number&0x3f;
			Number2 = (Number>>6) & 0x7f;
			fwrite(&Number1 , 1, 1, fp);//1bit sign, 1bit extension bit, 6 bit number
			fwrite(&Number2 , 1, 1, fp);//1bit extension bit, 7bit number
		}
		else
		{
			Number2 |= 0x80;  //extension bit
			if(Number<0x100000)
			{
				Number1 |= Number&0x3f;
				Number2 |= (Number>>6) & 0x7f;
				Number3 = (Number>>13) & 0x7f;
				fwrite(&Number1 , 1, 1, fp);//1bit sign, 1bit extension bit, 6 bit number
				fwrite(&Number2 , 1, 1, fp);//1bit extension bit, 7bit number
				fwrite(&Number3 , 1, 1, fp);//1bit extension bit, 7bit number
			}
			else
			{
				Number3 |= 0x80;  //extension bit
				if(Number<0x8000000)
				{
					Number1 |= Number&0x3f;
					Number2 |= (Number>>6) & 0x7f;
					Number3 |= (Number>>13) & 0x7f;
					Number4 = (Number>>20) & 0x7f;
					fwrite(&Number1 , 1, 1, fp);//1bit sign, 1bit extension bit, 6 bit number
					fwrite(&Number2 , 1, 1, fp);//1bit extension bit, 7bit number
					fwrite(&Number3 , 1, 1, fp);//1bit extension bit, 7bit number
					fwrite(&Number4 , 1, 1, fp);//1bit extension bit, 7bit number
				}
				else
				{
					//worst case...store it as 5 bytes
					Number4 |= 0x80;  //extension bit
					Number1 |= Number&0x3f;
					Number2 |= (Number>>6) & 0x7f;
					Number3 |= (Number>>13) & 0x7f;
					Number4 |= (Number>>20) & 0x7f;
					Number5 = (Number>>27) & 0x7f;
					fwrite(&Number1 , 1, 1, fp);//1bit sign, 1bit extension bit, 6 bit number
					fwrite(&Number2 , 1, 1, fp);//1bit extension bit, 7bit number
					fwrite(&Number3 , 1, 1, fp);//1bit extension bit, 7bit number
					fwrite(&Number4 , 1, 1, fp);//1bit extension bit, 7bit number
					fwrite(&Number5 , 1, 1, fp);//1bit extension bit, 7bit number
				}
			}
		}
	}
}

int Object::ReadInt(FILE *fp)
{
	bool Negative;
	int Number;
	unsigned char Number1;
	unsigned char Number2;
	unsigned char Number3;
	unsigned char Number4;
	unsigned char Number5;
	Negative = false;

	fread(&Number1, 1, 1, fp);
	if(Number1&0x80)
	{
		Negative = true;
	}
	Number = Number1&0x3f;

	if(Number1&0x40)
	{
		fread(&Number2, 1, 1, fp);
		Number |= (Number2&0x7f)<<6;

		if(Number2&0x80)
		{
			fread(&Number3, 1, 1, fp);
			Number |= (Number3&0x7f)<<13;

			if(Number3&0x80)
			{
				fread(&Number4, 1, 1, fp);
				Number |= (Number4&0x7f)<<20;

				if(Number4&0x80)
				{
					fread(&Number5, 1, 1, fp);
					Number |= (Number5&0x7f)<<27;
				}
			}
		}
	}
	if(Negative)
	{
		Number = -Number;
	}
	return Number;
}
