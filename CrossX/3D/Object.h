#ifndef _H_OBJECT
#define _H_OBJECT

#include "stdio.h"
#include "string.h"

class Object
{
public:
	Object() {}
	virtual ~Object() {}

	virtual bool			Load(XString Objname, float Scale) = 0;
	virtual void			Render(Pipeline *pl) = 0;

	void					CustomSave(XString FileName); //saves the object in a custom 'r3d' format

	Mesh                   *GetMesh() { return &m_Mesh; }
protected:
	int						ReadInt(FILE *fp); //auxilary function for customsave
	void					WriteInt(FILE *fp, int Number); //auxilary function for customsave

	Mesh                   m_Mesh;
};

#endif
