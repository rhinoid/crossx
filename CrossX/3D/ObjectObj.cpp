#include "3d/engine.h"
#include "3d/objectobj.h"
#include "stdio.h"
#include "stdlib.h"
#include <string.h>
#include "utils/clib.h"



ObjectObj::ObjectObj()
{
}

ObjectObj::~ObjectObj()
{
}

struct ObjMaterial
{
public:
	ObjMaterial() : m_Ambient(0.0f), m_Diffuse(0.0f), m_Specular(0.0f){};
	char    m_Name[256];
	float	m_Ambient;
	float	m_Diffuse;
	float	m_Specular;
	char    m_Texture[256];
};


typedef StorageVector<Vector3> Vector3Vector;
typedef StorageVector<Material> MaterialVector;
typedef StorageVector<int> VertexNormalIdxVector; // Index lookup table of Vertex normalIdx and Actual Vertex Normal IDx.   These values differ because we skip double values 
typedef StorageVector<int> NormalIdxVector; // Index lookup table of normalIdx and Actual Normal IDx.   These values differ because we skip double values 

bool	ObjectObj::Load(XString Objname, float Scale)
{
	bool VertexNormalsFlag; //are vertex normals used?
	Material Empty;

	//temp storage
	Vector3Vector UVCoords;
	MaterialVector Materials;
	TriangleList CurTriangleList;

	VertexNormalIdxVector VertexNormalLookup;

	char RealFileName[256];
	sprintf(RealFileName, "%s/%s.obj",GLOBALS->m_FullPath, Objname);

	FILE* f = fopen( RealFileName, "r" );
	char buffer[256];
	char cmd[32];

	if(f==0) return false;
	// load data
	while (!feof( f ))
	{
		fgets( buffer, 250, f );
		sscanf( buffer, "%s", cmd );
		if (!strcmp( cmd, "v" ))
		{
			// vertex, add to list
			float x, y, z;
			sscanf( buffer, "%s %f %f %f", cmd, &x, &y, &z );
			x *= Scale, y *= Scale, z *= Scale;

			m_Mesh.m_Vertices.Add(Vector3(x, y, z));
		}
		else if (!strcmp( cmd, "g" ))
		{
			// object name, not used
//			char objname[128];
//			sscanf( buffer + 2, "%s", objname );
		}
		else if (!strcmp( cmd, "mtllib" ))
		{
			// material lib load command
			char libname[128];
			sscanf( buffer + 7, "%s", libname );

			// a parser in a parser... here we load the material list
			char RealLibFileName[256];
			sprintf(RealLibFileName, "%s/%s", GLOBALS->m_FullPath, libname);
			
			FILE* mf = fopen( RealLibFileName, "r" );
			if(mf)
			{
				int curmat = 0;
				char buffer[256], cmd[128];
				Material CurMat;
				while (!feof( mf ))
				{
					fgets( buffer, 250, mf );
					sscanf( buffer, "%s", cmd );
					if (!strcmp( cmd, "newmtl" ))
					{
						//store old material in list (if there was a prev material)
						if(CurMat.m_Name[0] != 0)
						{
							Materials.Add(CurMat);
						}

						//Init new material
						CurMat = Material();
						sscanf( buffer + strlen( cmd ), "%s", CurMat.m_Name );
					}
					if (!strcmp( cmd, "Ka" )) //ambient
					{
						float r, g, b;
						sscanf( buffer + 3, "%f %f %f", &r, &g, &b );
						CurMat.m_Ambient = Vector3(r, g, b);
					}
					if (!strcmp( cmd, "Kd" ))
					{
						float r, g, b;
						sscanf( buffer + 3, "%f %f %f", &r, &g, &b );
						CurMat.m_Diffuse = Vector3(r, g, b);
					}
					if (!strcmp( cmd, "Ks" ))
					{
						float r, g, b;
						sscanf( buffer + 3, "%f %f %f", &r, &g, &b );
						CurMat.m_Specular = Vector3(r, g, b);
					}
					if (!strcmp( cmd, "map_Kd" ))
					{
						char tname[128];
						sscanf( buffer + 7, "%s", tname );

						char RealTextFileName[256];
						sprintf(RealTextFileName, "%s",tname);


						CurMat.m_Texture = GLOBALS->m_TexManager->GetTexture(RealTextFileName);
						assert(CurMat.m_Texture); //texture could not be loaded?
					}
					if (!strcmp( cmd, "illum" ))
					{
						float illum;
						sscanf( buffer + 6, "%f", &illum );
						CurMat.m_Shininess = illum;

					}
					//'ns' phong specular component is ignored for now   
				}
				//store last read material in list
				if(CurMat.m_Name[0] != 0)
				{
					Materials.Add(CurMat);
				}
			}
			else
			{
				assert(0); // could not find material description file
			}
		}
		else if (!strcmp( cmd, "vt" ))
		{
			// texture coordinate
			float u, v;
			sscanf( buffer, "%s %f %f", cmd, &u, &v );
			UVCoords.Add(Vector3(u,v,0.0f));
		}
		else if (!strcmp( cmd, "vn" ))
		{
			// vertex normal
			float x, y, z;
			sscanf( buffer, "%s %f %f %f", cmd, &x, &y, &z );

//@@@ Add check here to see if normal is already in list somewhere
			Vector3 vn;
			vn = Vector3(x, y, z);
			int nc;
			bool found = false;
			for(nc=0; nc<m_Mesh.m_Normals.Size(); nc++)
			{
				if(m_Mesh.m_Normals[nc].Point == vn)
				{
					found = true;

					VertexNormalLookup.Add(nc); //add indirection from real idx to actual idx
					break;
				}
			}
			if(found==false)
			{
				int NewIdx = m_Mesh.m_Normals.Size();
				m_Mesh.m_Normals.Add(vn);
				VertexNormalLookup.Add(NewIdx); //add indirection from real idx to actual idx
			}
		}
		else if (!strcmp( cmd, "usemtl" ))
		{
			// set active material
			char matname[128];
			sscanf( buffer + 7, "%s", matname );

			//see if there was a previous trianglelist... if so add it to the list
			if(CurTriangleList.m_Triangles.Size()>0)
			{
				m_Mesh.m_TriangleLists.Add(CurTriangleList);
			}

			//new facelist will follow... create a new trianglelist, initialised with the found material
			CurTriangleList = TriangleList();
			VertexNormalsFlag = false;
			for(int i=0; i<Materials.Size(); i++)
			{
				Material mat;
				mat = Materials.Get(i);
				if(strcmp(mat.m_Name, matname)==0)
				{
					CurTriangleList.m_Material = mat;
					break;
				}
			}
		}
		else if (!strcmp( cmd, "f" ))
		{
			char vinfo[128];
			// face
			Triangle CurTriangle;
			char* start = buffer + 2;
			for ( int i = 0; i < 3; i++ )
			{
				while (*start == 32) start++;
				sscanf( start, "%s", vinfo );
				start = strstr( start, " " ) + 1;
				char* part2 = strstr( vinfo, "/" );
				char* part3 = strstr( part2 + 1, "/" );
				*part2++ = *part3++ = 0;
				sscanf( vinfo, "%i", &CurTriangle.m_Vertices[i] ); //vertice idx
				CurTriangle.m_Vertices[i]--; //obj-format used 1 based indices
				if (*part2) // uv idx?
				{
					int UVIdx;
					sscanf( part2, "%i", &UVIdx );
					Vector3 RealUV = UVCoords.Get(UVIdx-1); // -1 since indices are 1 based in obj-format

					// retrieve current texture to prescale the UV coords with the texture res
					if (CurTriangleList.m_Material.m_Texture)
					{
						CurTriangle.m_U[i] = RealUV.M[0].val;
						CurTriangle.m_V[i] = RealUV.M[1].val;
						CurTriangle.m_U[i] *= CurTriangleList.m_Material.m_Texture->GetWidth();
						CurTriangle.m_V[i] *= CurTriangleList.m_Material.m_Texture->GetHeight();
						if(Fixed8::SHIFT != RASFIXSHIFT)
						{
							CurTriangle.m_U[i] <<= RASFIXSHIFT;
							CurTriangle.m_U[i] >>= Fixed8::SHIFT;
							CurTriangle.m_V[i] <<= RASFIXSHIFT;
							CurTriangle.m_V[i] >>= Fixed8::SHIFT;
						}
					}
					else
					{
						assert(0);
						//lijkt me dat dit niet kan gebeuren... UV coords worden vermeld maar er is geen texture
					}
				}
				sscanf( part3, "%i", &CurTriangle.m_VNormals[i] ); // Vertexnormal idx
				CurTriangle.m_VNormals[i]--; //obj-format used 1 based indices

				//use the vertex normal lookuptable to obtain the actual indexes (a lot of them were double and have been removed)
				CurTriangle.m_VNormals[i] = VertexNormalLookup[CurTriangle.m_VNormals[i]];


				CurTriangle.m_Color[i] = CurTriangleList.m_Material.m_Diffuse.ToRGB();
			}

			//Precalc face normal
			Vector3 Vec1(m_Mesh.m_Vertices[CurTriangle.m_Vertices[0]].Point - m_Mesh.m_Vertices[CurTriangle.m_Vertices[1]].Point);
			Vector3 Vec2(m_Mesh.m_Vertices[CurTriangle.m_Vertices[0]].Point - m_Mesh.m_Vertices[CurTriangle.m_Vertices[2]].Point);
			Vertice FaceNormal;
			FaceNormal.Point = Vec1 ^ Vec2;
			FaceNormal.Point *= Fixed8::fromDouble(0.1f); //alvast een beetje terugschalen. anders precisie problemen
			FaceNormal.Point.Normalize();

//@@@ Add check here to see if normal is already in list somewhere
			int nc;
			bool found = false;
			for(nc=0; nc<m_Mesh.m_Normals.Size(); nc++)
			{
				if(m_Mesh.m_Normals[nc].Point == FaceNormal.Point)
				{
					found = true;
					CurTriangle.m_Normal = nc;
					break;
				}
			}
			if(found==false)
			{	
				CurTriangle.m_Normal = m_Mesh.m_Normals.Size();
				m_Mesh.m_Normals.Add(FaceNormal);
			}

			
			// set face flags
			CurTriangle.m_Flags |= TRIANGLEFLAGS_VERTEXLIGHTING; //vertex normals were used in this model

			// add face to list
			CurTriangleList.m_Triangles.Add(CurTriangle);
		}
	}

	//see if there was a previous trianglelist... if so add it to the list
	if(CurTriangleList.m_Triangles.Size()>0)
	{
		m_Mesh.m_TriangleLists.Add(CurTriangleList);
	}
	return true;
}

void	ObjectObj::Render(Pipeline *pl)
{
	pl->Render(&m_Mesh);
}
