#ifndef _H_OBJECTOBJ
#define _H_OBJECTOBJ

#include "object.h"

class ObjectObj : public Object
{
public:
	ObjectObj();
	~ObjectObj();

	bool			Load(XString Objname, float Scale);
	void			Render(Pipeline *pl);


private:
};

#endif
