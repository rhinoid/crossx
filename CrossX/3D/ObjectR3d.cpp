#include "3d/engine.h"
#include "stdio.h"
#include "stdlib.h"
#include <string.h>
#include "utils/clib.h"



ObjectR3d::ObjectR3d()
{
}

ObjectR3d::~ObjectR3d()
{
}

void ObjectR3d::SetTextureOverride(XString TexName)
{
	m_TextureOverride = TexName;
}

bool	ObjectR3d::Load(XString Objname, float Scale)
{
	return Load(Objname, Scale, -1, -1);
}

bool	ObjectR3d::Load(XString Objname, float Scale, int StencilColor, int SwapColor)
{
	int i, j;

	Objname = MakeFullPath(Objname);
	FILE* fp = fopen( Objname.c_str(), "rb" );

	if(fp==0) return false;

	//Read Normals
	int NormalSize;
	NormalSize = ReadInt(fp);
	for(i=0; i<NormalSize; i++)
	{
		Vertice V;
		V.Point.M[0].val = ReadInt(fp);
		V.Point.M[1].val = ReadInt(fp);
		V.Point.M[2].val = ReadInt(fp);
		m_Mesh.m_Normals.Add(V);
	}

	//Read Vertices
	int VerticesSize;
	VerticesSize = ReadInt(fp);

	for(i=0; i<VerticesSize; i++)
	{
		Vertice V;
		V.Point.M[0].val = ReadInt(fp);
		V.Point.M[1].val = ReadInt(fp);
		V.Point.M[2].val = ReadInt(fp);
		V.Point.M[0].val = (int)(Scale * V.Point.M[0].val);
		V.Point.M[1].val = (int)(Scale * V.Point.M[1].val);
		V.Point.M[2].val = (int)(Scale * V.Point.M[2].val);
		m_Mesh.m_Vertices.Add(V);
	}

	// Read Trianglelists
	int TriangleListsSize;
	TriangleListsSize = ReadInt(fp);

	for(i=0; i<TriangleListsSize; i++)
	{
		TriangleList Tl;
		Tl.m_RenderFlags = ReadInt(fp); //Read Renderflags

		// read material
		Tl.m_Material.m_Ambient.M[0].val = ReadInt(fp);
		Tl.m_Material.m_Ambient.M[1].val = ReadInt(fp);
		Tl.m_Material.m_Ambient.M[2].val = ReadInt(fp);

		Tl.m_Material.m_Diffuse.M[0].val = ReadInt(fp);
		Tl.m_Material.m_Diffuse.M[1].val = ReadInt(fp);
		Tl.m_Material.m_Diffuse.M[2].val = ReadInt(fp);

		Tl.m_Material.m_Shading = ReadInt(fp);

		Tl.m_Material.m_Shininess.val = ReadInt(fp);

		Tl.m_Material.m_Specular.M[0].val = ReadInt(fp);
		Tl.m_Material.m_Specular.M[1].val = ReadInt(fp);
		Tl.m_Material.m_Specular.M[2].val = ReadInt(fp);

		Tl.m_Material.m_Transparency.val = ReadInt(fp);

		unsigned char TextureNameLen;
		fread(&TextureNameLen, 1, 1, fp);
		if(TextureNameLen > 0)
		{
			char TexName[256];
			fread(TexName, 1, TextureNameLen, fp);
			TexName[TextureNameLen] = 0;
			if(m_TextureOverride.length()>0)
			{
				strcpy(TexName, m_TextureOverride.c_str());
			}
			Tl.m_Material.m_Texture = TexManager::GetTextureManager()->GetTexture(TexName);
		}

		// Read actual triangles
		int TrianglesSize;
		TrianglesSize = ReadInt(fp);

		for(j=0; j<TrianglesSize; j++)
		{
			Triangle Tr;
			Tr.m_Flags = ReadInt(fp);

			Tr.m_Color[0] = ReadInt(fp);
			Tr.m_Color[1] = ReadInt(fp);
			Tr.m_Color[2] = ReadInt(fp);
			if(Tr.m_Color[0] == StencilColor)
			{
				Tr.m_Color[0] = SwapColor;
			}
			if(Tr.m_Color[1] == StencilColor)
			{
				Tr.m_Color[1] = SwapColor;
			}
			if(Tr.m_Color[2] == StencilColor)
			{
				Tr.m_Color[2] = SwapColor;
			}

			Tr.m_Normal = ReadInt(fp);

			Tr.m_U[0] = ReadInt(fp);
			Tr.m_U[1] = ReadInt(fp);
			Tr.m_U[2] = ReadInt(fp);

			Tr.m_V[0] = ReadInt(fp);
			Tr.m_V[1] = ReadInt(fp);
			Tr.m_V[2] = ReadInt(fp);

			Tr.m_Vertices[0] = ReadInt(fp);
			Tr.m_Vertices[1] = ReadInt(fp);
			Tr.m_Vertices[2] = ReadInt(fp);

			Tr.m_VNormals[0] = ReadInt(fp);
			Tr.m_VNormals[1] = ReadInt(fp);
			Tr.m_VNormals[2] = ReadInt(fp);
			Tl.m_Triangles.Add(Tr);
		}
		m_Mesh.m_TriangleLists.Add(Tl);
	}

	return true;
}

void	ObjectR3d::Render(Pipeline *pl)
{
	pl->Render(&m_Mesh);
}
