#ifndef _H_OBJECTR3D
#define _H_OBJECTR3D

#include "3d/object.h"

class ObjectR3d : public Object
{
public:
	ObjectR3d();
	~ObjectR3d();

	bool			Load(XString Objname, float Scale);
	bool			Load(XString Objname, float Scale, int StencilColor, int SwapColor);
	void			Render(Pipeline *pl);

	void            SetTextureOverride(XString TexName);
private:
	XString         m_TextureOverride;
};

#endif
