#include "3d/engine.h"
#include <stdio.h>
#include <stdlib.h>

#ifndef __SYMBIAN32__
#include <windows.h>
#include <winbase.h>
#endif

#define FOV (400)



//#define USEPOW

Pipeline::Pipeline()
{
	m_DestSurface = 0;
	m_FrameNr=2;
	m_NumLights=0;
	m_Lights=0;		// Array of lights
	m_ZBuffer=0;
	m_ZBufferWidth = 0;
	m_ZBufferHeight = 0;
}

Pipeline::~Pipeline()
{
	delete [] m_ZBuffer;
}

void Pipeline::SetBuffer(CSurface *DestSurface)
{
	m_DestSurface = DestSurface;

	if(m_ZBufferWidth != m_DestSurface->GetWidth() || m_ZBufferHeight != m_DestSurface->GetHeight())
	{
		delete [] m_ZBuffer;
		m_ZBufferWidth = DestSurface->GetWidth();
		m_ZBufferHeight = DestSurface->GetHeight();
		m_ZBuffer = new int[m_ZBufferWidth*m_ZBufferHeight];

	}
}


void Pipeline::SetTexture(unsigned int *Buffer)
{
	m_Texture = Buffer;
}

void Pipeline::SetRenderFlags(int Flags)
{
	m_EngineFlags = Flags;
}

#ifdef ARM
	extern "C" int CLEARSCREEN(int Buffer, int Color);
	extern "C" int CLEARZBUFFER(int Buffer);
#endif
	

void	Pipeline::BeginFrame(int ClearZFlag)
{
	m_FrameNr++;

	if(ClearZFlag == 1)
	{
	if(m_ZBuffer)
	{
#ifdef ARM
		CLEARZBUFFER((int)m_ZBuffer);
#else
		int *zbufptr;
		zbufptr = m_ZBuffer;
		int TotalLength = m_ZBufferWidth*m_ZBufferHeight;
		for(int i=0; i<TotalLength ;i++)
		{
			*zbufptr++ = 0x7fffffff;
		}
#endif
	}
}
}

void	Pipeline::EndFrame()
{
}

void	Pipeline::Render(Mesh *mesh)
{
	int j;
	RenderContext rc;

	if(m_DestSurface==0) return;

	// fill out rendercontext with defaults (for the time being globals)
	rc.m_DestBuf = m_DestSurface->GetBuffer();
	rc.m_DestPitch = m_DestSurface->GetWidth();
	rc.m_ZBuffer = m_ZBuffer;

	SRect ClipRect;
	ClipRect = m_DestSurface->GetClipping();
	rc.m_ClipMinX = ClipRect.m_Left;
	rc.m_ClipMinY = ClipRect.m_Top;
	rc.m_ClipMaxX = ClipRect.m_Right-1;
	rc.m_ClipMaxY = ClipRect.m_Bottom-1;

	// Precalced values which are needed during loop	
	Vector3 CamDir(m_Cam->m_Location - m_Cam->m_Eye); //viewing angle (into the screen) (needed for culling)

	Matrix4 CombinedMatrix =  m_CameraMatrix * m_LocalMatrix;

	Matrix4 InvCombinedMatrix = CombinedMatrix;
	InvCombinedMatrix.Inverse();
	Vector3 CamDirInvTransformed = InvCombinedMatrix * CamDir;

	//Cycle trough all trianglelists in this mesh
	for(int TriangleListIter=0; TriangleListIter<mesh->m_TriangleLists.Size(); TriangleListIter++)
	{
		TriangleList &CurTriangleList = mesh->m_TriangleLists[TriangleListIter];

		//Cycle trough all triangles in the triangle list
		for(int TriangleIter=0; TriangleIter<CurTriangleList.m_Triangles.Size(); TriangleIter++)
		{
			Triangle &CurTriangle = CurTriangleList.m_Triangles.Get(TriangleIter);

			//first we do a quick cul (trivially rejecting more most polygons facing backwards)
			// but this method is not good enough and later has to be redone with a slightly more precise method

			MATHTYPE CamAngle;
			CamAngle = CamDirInvTransformed.smul(mesh->m_Normals[CurTriangle.m_Normal].Point);
			if(CamAngle < Fixed8::fromInt(0)) continue; // are we facing triangle? if not cull (not precise enough)


			// now lets transform only 1 coord since it is needed in lighting calculations etc
//@@@ ADD FRAMECNT HERE!
			mesh->m_Vertices[CurTriangle.m_Vertices[0]].TPoint = m_LocalMatrix * mesh->m_Vertices[CurTriangle.m_Vertices[0]].Point;
			mesh->m_Vertices[CurTriangle.m_Vertices[1]].TPoint = m_LocalMatrix * mesh->m_Vertices[CurTriangle.m_Vertices[1]].Point;
			mesh->m_Vertices[CurTriangle.m_Vertices[2]].TPoint = m_LocalMatrix * mesh->m_Vertices[CurTriangle.m_Vertices[2]].Point;


			// cull triangle (precise method)    Not necessary maybe
//			Vector3 CamToTri(m_Cam->m_Location - mesh->m_Vertices[CurTriangle.m_Vertices[0]].TPoint); //viewing angle (into the screen)
//			CamAngle = CamToTri.smul(mesh->m_Normals[CurTriangle.m_Normal].TPoint);
//			if(CamAngle < Fixed8::fromInt(0)) continue;// are we facing triangle? if not cull

			

			if(1)
			{
				rc.m_RenderFlags = 0; //reset renderflags

				// perform lighting
				if(1)//Headlight lighting (the lower the angle of our viewing position to the triangle normal, the more intense
				{
					if((m_EngineFlags&EF_LIGHTS))
					{
	#if 0
						CurTriangle.m_LColor[0] = CurTriangle.m_Color[0];
						CurTriangle.m_LColor[1] = CurTriangle.m_Color[1];
						CurTriangle.m_LColor[2] = CurTriangle.m_Color[2];
	#else

						if((CurTriangle.m_Flags & TRIANGLEFLAGS_VERTEXLIGHTING)==0 || (m_EngineFlags&EF_SMOOTHGROUP)==0) //Triangle flat lighting
						{

							// lighting for the entire triangle
							
							Vector3 finalColour;     // if we have some kind of emissive color..we can fill it in here!

							// calculate normal of transformed triangle
							Vector3 Vec1(mesh->m_Vertices[CurTriangle.m_Vertices[0]].TPoint - mesh->m_Vertices[CurTriangle.m_Vertices[1]].TPoint);
							Vector3 Vec2(mesh->m_Vertices[CurTriangle.m_Vertices[0]].TPoint - mesh->m_Vertices[CurTriangle.m_Vertices[2]].TPoint);
							CurTriangle.m_TNormal = Vec1 ^ Vec2;
							CurTriangle.m_TNormal *= Fixed8::fromDouble(0.1f); //alvast een beetje terugschalen. anders precisie problemen
							CurTriangle.m_TNormal.Normalize();

							for (int LightIdx=0; LightIdx<m_NumLights; LightIdx++) 
							{
								Fixed8 spotFactor;
								if (1)   //light / vector is not occluded)
								{
									if (m_Lights[LightIdx].m_Flag&LIGHT_SPOTLIGHT) 
									{
										Vector3 lightDir = m_Lights[LightIdx].m_Position - mesh->m_Vertices[CurTriangle.m_Vertices[0]].TPoint; // where is the light positioned in respect to the triangle?
										Fixed8 temp = lightDir * m_Lights[LightIdx].m_Direction;

										Fixed8 CosVal = DEGTORAD*m_Lights[LightIdx].m_CutOff;
										if (temp >= cos(Fixed8::toDouble(CosVal))) 
										{
											spotFactor = Fixed8::fromDouble(pow(Fixed8::toDouble(temp), Fixed8::toDouble(m_Lights[LightIdx].m_Exponent)));
										} 
										else 
										{
											spotFactor = Fixed8::fromInt(0);
										}
									} 
									else 
									{
										spotFactor = Fixed8::fromInt(1);
									}
									
									if (spotFactor > Fixed8::fromInt(0)) 
									{
										Vector3 lightDir = m_Lights[LightIdx].m_Position - mesh->m_Vertices[CurTriangle.m_Vertices[0]].TPoint; // where is the light positioned inr espect to the triangle?
										lightDir.Normalize();

										Vector3 t;
										//ambient
										t = Vector3::componentMult(CurTriangleList.m_Material.m_Ambient, m_Lights[LightIdx].m_Ambient);
										t.scale(spotFactor);
										finalColour += t;

										//diffuse
										Fixed8 diffuseFactor = MAX(Fixed8::fromInt(0), Vector3::smul(CurTriangle.m_TNormal, lightDir));
										t = Vector3::componentMult(CurTriangleList.m_Material.m_Diffuse, m_Lights[LightIdx].m_Diffuse);
										t.scale(diffuseFactor * spotFactor);
										finalColour += t;

										// specular
										Vector3 cameraDir = (m_Cam->m_Location - mesh->m_Vertices[CurTriangle.m_Vertices[0]].TPoint);
										cameraDir.Normalize();
										Vector3 halfway = lightDir + cameraDir;
										halfway.Normalize();
										Fixed8 temp = MAX(Fixed8::fromInt(0), Vector3::smul(CurTriangle.m_TNormal, halfway));
	#ifdef USEPOW
										Fixed8 specularFactor = Fixed8::fromDouble(pow(Fixed8::toDouble(temp), Fixed8::toDouble(CurTriangleList.m_Material.m_Shininess)));
	#else
										Fixed8 temp2 = (CurTriangleList.m_Material.m_Shininess - temp*CurTriangleList.m_Material.m_Shininess + temp);
										Fixed8 specularFactor;
										if(temp2!=0) specularFactor = temp / temp2;
	#endif
										t = Vector3::componentMult(CurTriangleList.m_Material.m_Specular, m_Lights[LightIdx].m_Specular);
										t.scale(specularFactor* spotFactor);
										finalColour += t;
									}
								}
							}
							CurTriangle.m_LColor[0] = finalColour.ToRGB();
							CurTriangle.m_LColor[1] = CurTriangle.m_LColor[0];
							CurTriangle.m_LColor[2] = CurTriangle.m_LColor[0];

						}
						if((m_EngineFlags&EF_SMOOTHGROUP))
						{
							if(CurTriangle.m_Flags & TRIANGLEFLAGS_VERTEXLIGHTING) //vertex lighting
							{
								for(int v=0; v<3; v++)
								{
									//transform Vertex normal to camera space (if not done so before)
									int vn; //current vertex nr of the triangle (0,1 or 2)
									vn = CurTriangle.m_VNormals[v];
									if(mesh->m_Normals[vn].m_FrameNr < m_FrameNr) // VertexNormal not yet transformed this frame?
									{
										mesh->m_Normals[vn].TPoint = m_LocalMatrix * mesh->m_Normals[vn].Point;
										mesh->m_Normals[vn].m_FrameNr = m_FrameNr;
									}
									Vector3 VertNorm = mesh->m_Normals[vn].TPoint;

									// lighting per vertex
									Vector3 finalColour;     // if we have some kind of emissive color..we can fill it in here!

									for (int LightIdx=0; LightIdx<m_NumLights; LightIdx++) 
									{
										Fixed8 spotFactor;
										if (1)   //light / vector is not occluded)
										{
											if (m_Lights[LightIdx].m_Flag&LIGHT_SPOTLIGHT) 
											{
												Vector3 lightDir = m_Lights[LightIdx].m_Position - mesh->m_Vertices[CurTriangle.m_Vertices[0]].TPoint; // where is the light positioned inr espect to the triangle?
												Fixed8 temp = lightDir * m_Lights[LightIdx].m_Direction;
												Fixed8 CosVal = DEGTORAD*m_Lights[LightIdx].m_CutOff;
												if (temp >= cos(Fixed8::toDouble(CosVal))) 
												{
													spotFactor = Fixed8::fromDouble(pow(Fixed8::toDouble(temp), Fixed8::toDouble(m_Lights[LightIdx].m_Exponent)));
												} 
												else 
												{
													spotFactor = Fixed8::fromInt(0);
												}
											} 
											else 
											{
												spotFactor = Fixed8::fromInt(1);
											}
											if (spotFactor > Fixed8::fromInt(0)) 
											{
												Vector3 lightDir = m_Lights[LightIdx].m_Position - mesh->m_Vertices[CurTriangle.m_Vertices[v]].TPoint; // where is the light positioned inr espect to the triangle?
												lightDir.Normalize();
												
												Vector3 t;
												//ambient
												t = Vector3::componentMult(CurTriangleList.m_Material.m_Ambient, m_Lights[LightIdx].m_Ambient);
												t.scale(spotFactor);
												finalColour += t;

												//diffuse
												Fixed8 diffuseFactor = MAX(Fixed8::fromInt(0), Vector3::smul(VertNorm, lightDir));
												t = Vector3::componentMult(CurTriangleList.m_Material.m_Diffuse, m_Lights[LightIdx].m_Diffuse);
												t.scale(diffuseFactor * spotFactor);
												finalColour += t;

												// specular
	#if 1
												Vector3 cameraDir = (m_Cam->m_Location - mesh->m_Vertices[CurTriangle.m_Vertices[v]].TPoint);
												cameraDir.Normalize();
												Vector3 halfway = cameraDir+lightDir;//+cameraDir;
												halfway.Normalize();
												Fixed8 temp = MAX(Fixed8::fromInt(0), Vector3::smul(VertNorm, halfway));

	#ifdef USEPOW
												Fixed8 specularFactor = Fixed8::fromDouble(pow(Fixed8::toDouble(temp), Fixed8::toDouble(CurTriangleList.m_Material.m_Shininess)));
	#else
												Fixed8 temp2 = (CurTriangleList.m_Material.m_Shininess - temp*CurTriangleList.m_Material.m_Shininess + temp);
												Fixed8 specularFactor;
												if(temp2!=0) specularFactor = temp / temp2;
	#endif

												t = Vector3::componentMult(CurTriangleList.m_Material.m_Specular, m_Lights[LightIdx].m_Specular);
												Fixed8 Scale = specularFactor% spotFactor;
												t.scale(Scale);
												finalColour += t;
	#endif
											
											}
										}
									}
									CurTriangle.m_LColor[v] = finalColour.ToRGB();
								}
							}
						}
	#endif
					}
					else
					{
						CurTriangle.m_LColor[0] = CurTriangle.m_Color[0];
						CurTriangle.m_LColor[1] = CurTriangle.m_Color[1];
						CurTriangle.m_LColor[2] = CurTriangle.m_Color[2];
					}

				} //LIGHTING


				//ENV MAPPING
				if((m_EngineFlags&EF_ENVMAPPING))
				{
					if(CurTriangleList.m_Material.m_Texture==0)
					{
					for(int v=0; v<3; v++)
					{
						//transform Vertex normal to camera space (if not done so before)
						int vn; //current vertex nr of the triangle (0,1 or 2)
						vn = CurTriangle.m_VNormals[v];
						if(mesh->m_Normals[vn].m_FrameNr < m_FrameNr) // VertexNormal not yet transformed this frame?
						{
							mesh->m_Normals[vn].TPoint = m_LocalMatrix * mesh->m_Normals[vn].Point;
							mesh->m_Normals[vn].m_FrameNr = m_FrameNr;
						}

						// get transformed vertex normal
						Vector3 VertNorm = mesh->m_Normals[CurTriangle.m_VNormals[v]].TPoint;

						CurTriangle.m_U[v] = VertNorm.M[0].val<<7;
						CurTriangle.m_V[v] = VertNorm.M[1].val<<7;
					}
					//select phong texture
					Texture *PhongTex = TexManager::GetTextureManager()->GetPhongTexture();
					rc.m_TextBuf = PhongTex->GetBuf();
					rc.m_TextPitch = PhongTex->GetWidth();
					rc.m_TextShift = PhongTex->GetWidthShift();
					rc.m_RenderFlags |= RENDERFLAG_TEXTUREMAPPING;
					if(m_EngineFlags&EF_BILINEAR)
					{
						rc.m_RenderFlags |= RENDERFLAG_BILERP;
					}
				}
				}

				//transform 3 vertices to camera space
				for(j=0; j<3; j++)
				{
					
					int vn; //current vertex nr of the triangle (0,1 or 2)
					vn = CurTriangle.m_Vertices[j];

					//Transform Vertice to WorldSpace
					if(mesh->m_Vertices[vn].m_FrameNr < m_FrameNr) // vertice not yet transformed this frame?
					{
						mesh->m_Vertices[vn].TCPoint = CombinedMatrix * mesh->m_Vertices[vn].Point;
						mesh->m_Vertices[vn].m_FrameNr = m_FrameNr;
					}
				}

				//perspective transform 3 vertices to screen
				for(j=0; j<3; j++)
				{
					int vn; //current vertex nr of the triangle (0,1 or 2)
					vn = CurTriangle.m_Vertices[j];

					// do perspective transform
					Fixed8 xd,yd;
					xd = ((mesh->m_Vertices[vn].TCPoint.M[0] % Fixed8::fromInt(FOV)).PreciseDivide(mesh->m_Vertices[vn].TCPoint.M[2]));
					yd = ((mesh->m_Vertices[vn].TCPoint.M[1] % Fixed8::fromInt(FOV)).PreciseDivide(mesh->m_Vertices[vn].TCPoint.M[2]));
					rc.x[j] = (FIXRAS)xd.val << (RASFIXSHIFT-Fixed8::SHIFT);
					rc.y[j] = (FIXRAS)yd.val << (RASFIXSHIFT-Fixed8::SHIFT);
	#ifdef USEPERSPCORRECT
					//in case of perspective correct we have to supply the z as 1/z and u and v as u/z and v/z
					//in this case we change the fixed point range.
					//the z gets 28bits of accuracy (since this can only vary from 0..1) and the u,v get 22 point (so we still get 256 bytes texture coordinates
					int zint = (int)(mesh->m_Vertices[vn].TCPoint.M[2]+Fixed8::fromDouble(0.5f));
					rc.z[j] = ((1<<28) / zint);
					rc.u[j] = (CurTriangle.m_U[j]<<(22-RASFIXSHIFT))/zint;   //u is already fixedpoint... so to make it 22 we shift it only 14
					rc.v[j] = (CurTriangle.m_V[j]<<(22-RASFIXSHIFT))/zint;
	#else
					rc.z[j] = (FIXRAS)mesh->m_Vertices[vn].TCPoint.M[2].val << (RASFIXSHIFT-Fixed8::SHIFT);

					rc.u[j] = CurTriangle.m_U[j];
					rc.v[j] = CurTriangle.m_V[j];
	#endif

					//put triangle in middle of screen
					rc.x[j] += (m_ViewRect.m_Left+((m_ViewRect.m_Right-m_ViewRect.m_Left)>>1)) << RASFIXSHIFT;
					rc.y[j] += (m_ViewRect.m_Top+((m_ViewRect.m_Bottom-m_ViewRect.m_Top)>>1)) << RASFIXSHIFT;
		//			rc.z[j] += 20*RASFIXONE;

					rc.c[j] = CurTriangle.m_LColor[j];
				}



				// very coarse clipping...has to be much more refined
				// is one of the points through the near clipping plane? Then reject
				if(rc.z[0]>RASFIXONE && rc.z[1]>RASFIXONE && rc.z[2]>RASFIXONE)
				{

					if((m_EngineFlags&EF_ENVMAPPING) != 0)
					{
							rc.m_RenderFlags |= RENDERFLAG_ZBUFFER|RENDERFLAG_FLAT;
					}
					else
					{
						// draw the triangle
						if(CurTriangle.m_Flags & TRIANGLEFLAGS_VERTEXLIGHTING)
						{
							rc.m_RenderFlags |= RENDERFLAG_ZBUFFER|RENDERFLAG_GOURAUD;
						}
						else
						{
							rc.m_RenderFlags |= RENDERFLAG_ZBUFFER|RENDERFLAG_FLAT;
						}
					}
					//choose correct rendering mode
					if(m_DestSurface->GetMode() == MODE_32BIT)
					{
						rc.m_RenderFlags |= RENDERFLAG_24BIT;
					}

						if(CurTriangleList.m_Material.m_Texture)
						{
							rc.m_TextBuf = CurTriangleList.m_Material.m_Texture->GetBuf();
							rc.m_TextPitch = CurTriangleList.m_Material.m_Texture->GetWidth();
							rc.m_TextShift = CurTriangleList.m_Material.m_Texture->GetWidthShift();
							rc.m_RenderFlags |= RENDERFLAG_TEXTUREMAPPING;
							if(m_EngineFlags&EF_BILINEAR)
							{
								rc.m_RenderFlags |= RENDERFLAG_BILERP;
							}
		#ifdef USEPERSPCORRECT
						rc.m_RenderFlags |= RENDERFLAG_PERSPCORRECT;
		#endif
						}

					Rasterize(&rc);
				} // ZCLIPPING
			} // CULL
		}
	}	
}


void	Pipeline::SetViewPort(SRect r)
{
	m_ViewRect = r;
}

void	Pipeline::SetCamera(Camera *cam)
{
	m_Cam = cam;
	m_CameraMatrix = cam->CreateMatrix();
}

void	Pipeline::SetLocalMatrix(Matrix4 m)
{
	m_LocalMatrix = m;
}

void Pipeline::AddLight(Light *l)
{
	// allocate memory for the mesh in the current object
	if(m_NumLights==0)
	{
		m_NumLights++;
		m_Lights = (Light *)malloc(m_NumLights*sizeof(Light));
	}
	else
	{
		m_NumLights++;
		m_Lights = (Light *)realloc(m_Lights, m_NumLights*sizeof(Light));
	}
	m_Lights[m_NumLights-1] = *l;
}

Light *Pipeline::GetLight(int i)
{
	if(i >= m_NumLights) return 0;
	return &m_Lights[i];
}

void Pipeline::RemLight()
{
	if(m_NumLights==0) return;


	m_NumLights--;
	if(m_NumLights==0)
	{
		free(m_Lights);
		m_Lights = 0;
	}
	else
	{
		m_Lights = (Light *)realloc(m_Lights, m_NumLights*sizeof(Light));
	}
}


void Mesh::Center()
{
	int i;
	Vector3 Min, Max;
	Min = Vector3(0x7fffffff, 0x7fffffff, 0x7fffffff); 
	Max = Vector3(-0x7fffffff, -0x7fffffff, -0x7fffffff);
	//first scan for max and min values
	for(i=0; i<m_Vertices.Size(); i++)
	{
		if(m_Vertices[i].Point.M[0] < Min.M[0]) Min.M[0] = m_Vertices[i].Point.M[0];
		if(m_Vertices[i].Point.M[1] < Min.M[1]) Min.M[1] = m_Vertices[i].Point.M[1];
		if(m_Vertices[i].Point.M[2] < Min.M[2]) Min.M[2] = m_Vertices[i].Point.M[2];
		if(m_Vertices[i].Point.M[0] > Max.M[0]) Max.M[0] = m_Vertices[i].Point.M[0];
		if(m_Vertices[i].Point.M[1] > Max.M[1]) Max.M[1] = m_Vertices[i].Point.M[1];
		if(m_Vertices[i].Point.M[2] > Max.M[2]) Max.M[2] = m_Vertices[i].Point.M[2];
	}

	//now adjust points to center
	Min += Max;
	Min /= Fixed8::fromInt(2);
	for(i=0; i<m_Vertices.Size(); i++)
	{
		m_Vertices[i].Point -= Min;
	}
}

