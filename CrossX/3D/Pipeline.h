#ifndef _H_PIPELINE
#define _H_PIPELINE


#include "Utils/StorageVector.h"
#include "2D/Surface.h"
#include "2D/SRect.h"

//------- 3D structs

struct Material
{
	Material() : m_Shading(0), m_Texture(0), m_Shininess(0), m_Transparency(0) { m_Name[0]=0; }
	char		m_Name[128];    //should be allocated
	short		m_Shading;      //see defines!
	Vector3		m_Ambient;		//ambient color
	Vector3		m_Diffuse;		//diffuse color
	Vector3		m_Specular;		//specular color
	Texture    *m_Texture;		//Texture map    (no need for copy constructor since all similar textures point to the same textureclass)
	Fixed8		m_Shininess;	//shininess
	Fixed8		m_Transparency;	//transparency
};

struct Vertice
{
	Vertice() : m_FrameNr(0) {};
	Vertice(float x, float y, float z) : Point(x, y, z), m_FrameNr(0)  { };
	Vertice(Vector3 v) : Point(v), m_FrameNr(0) { };
	Vector3 Point;		// actual coordinate
	Vector3 TPoint;		// Transformed worldspace coordinate
	Vector3 TCPoint;		// Transformed cameraspace coordinate
	int		m_FrameNr;  // The framenr so we know TCpoint is already transformed
};


#define TRIANGLEFLAGS_VERTEXLIGHTING (1)

struct Triangle
{
	Triangle() : m_Normal(0), m_Flags(0) {}
	int		m_Vertices[3]; // indexes into vertices of TriangleList
	int		m_Color[3];		// color of point
	int		m_LColor[3];	// color of point AFTER lighting calculations
	int     m_U[3];			// U (texture coord) (preshifted in rasterizers fixedpoint)
	int     m_V[3];			// V (texture coord) (preshifted in rasterizers fixedpoint)

	int     m_Normal;       // Index of Triangle Normal in NormalList (NOT USED YET!)
	Vector3 m_TNormal;   //transformed normal of triangle   (should be done precalced with indices)
	
	int     m_VNormals[3];	// Indices of Vertex Normals in Normallist

	int		m_Flags;		// Eventueel later Odd/Even Flags en Flat Flags(voor Culling(zie executebuffers helpfile van d3d))
};

typedef StorageVector<Triangle> TriangleVec;

class TriangleList
{
public:
	TriangleList() : m_RenderFlags(0) {}

	TriangleVec m_Triangles;		// Vector of Triangles

	// to every trianglelist belongs 1 material // to speed up the pipeline
	int			m_RenderFlags;      //see defines!
	Material    m_Material;
};

typedef StorageVector<TriangleList> TriangleListVec;
typedef StorageVector<Vertice> VerticeVec;


class Mesh 
{
public:
	Mesh() {};

	void Center(); // center the mesh

	VerticeVec m_Vertices;				//Vector of Vertices
	VerticeVec m_Normals;				//Vector of all Normals (Face and Vertex)
	TriangleListVec m_TriangleLists;	//Vector of TriangleLists
private:

};



#define LIGHT_SPOTLIGHT (1)

class Light 
{
public:
	Light() : m_Flag(0), m_CutOff(1.0f), m_Exponent(1.0f) {}

	Vector3 m_Colour;
	Vector3 m_Ambient;
	Vector3 m_Diffuse;
	Vector3 m_Specular;
	Vector3 m_Position;
	Vector3 m_Direction;
	int		m_Flag;		// is the light a directional or spotlight?
	float	m_CutOff, m_Exponent;
 };


//renderflags defines
#define EF_LIGHTS (1)
#define EF_SMOOTHGROUP (2)
#define EF_ENVMAPPING (4)
#define EF_BILINEAR (8)

class Pipeline
{
public:
	Pipeline();
	~Pipeline();

	void	BeginFrame(int ClearZFlag);
	void	Render(Mesh *mesh);
	void	EndFrame(void);


	void	SetBuffer(CSurface *DestSurface);
	void	SetTexture(unsigned int *Buffer);
	void	SetViewPort(SRect r);
	void	SetCamera(Camera *cam);
	void	SetLocalMatrix(Matrix4 m);
	void    SetRenderFlags(int Flags);

	void	AddLight(Light *l);
	Light  *GetLight(int i);
	void	RemLight();

private:
	SRect m_ViewRect;
	Matrix4 m_LocalMatrix;	// how is the object transformed into the world?
	Matrix4 m_CameraMatrix;	// how is the world transformed into cameraspace
	Camera	*m_Cam;

	unsigned int   *m_Texture;

	int    *m_ZBuffer;
	int		m_ZBufferWidth;
	int		m_ZBufferHeight;

	CSurface *m_DestSurface;

	int		m_EngineFlags; // om ff via de user interface features aan en uit te zetten
	int		m_NumLights;
	Light  *m_Lights;		// Array of lights

	int		m_FrameNr;	//current frame we are rendering
};

#endif

