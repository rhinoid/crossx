#ifdef __SYMBIAN32__
#include <libc/stdlib.h>
#include <libc/stdio.h>
#else
#include "stdio.h"
#include "malloc.h"
#include <windows.h>
#endif

#include "string.h"
#include "utils/clib.h"
#include "utils/ltga.h"
#include "utils/xstring.h"
#include "2d/surface32.h"

#include "engine.h"


TexManager *TexManager::GetTextureManager()
{
	if(GLOBALS->m_TexManager) return GLOBALS->m_TexManager;

	GLOBALS->m_TexManager = new TexManager();
	return GLOBALS->m_TexManager;
}


TexManager::TexManager()
{
	m_NumTextures = 0;
	m_Textures = 0;
}

TexManager::~TexManager()
{
}

int TexManager::Size()
{
	return m_NumTextures;
}


void TexManager::Clear()
{
	Texture *CurTexture;
	CurTexture = m_Textures;
	while(CurTexture)
	{
		Texture *Next;
		Next = CurTexture->m_Next;
		delete CurTexture;
		CurTexture = Next;
	}
	m_Textures = 0;
}

Texture *TexManager::GetTexture(XString Name)
{
	Texture *CurTexture;
	CurTexture = m_Textures;
	while(CurTexture)
	{
		if(Name.CompareNoCase(CurTexture->m_Name)==0)
		{
			return CurTexture;
		}
		CurTexture= CurTexture->m_Next;
	}

	// load textures
	Texture *NewTexture;
	NewTexture = new Texture( this );

	//load tga and fill out the struct
	if(NewTexture->Load(Name))
	{
		NewTexture->SetName(Name);
		Texture *last;
		last = m_Textures;
		NewTexture->m_Next = 0;
		if(last==0)
		{
			m_Textures = NewTexture;
		}
		else
		{
			while(last->m_Next)
			{
				last = last->m_Next;
			}
			last->m_Next = NewTexture;
		}
		m_NumTextures++;
		return NewTexture;
	}
	else
	{
		delete NewTexture;
		return 0;
	}
}


Texture *TexManager::GetPhongTexture()  //phong texture is first in the list (if available)
{
	return m_Textures;
}


void TexManager::RemoveTexture(Texture *Tex)
{
	Texture *CurTexture, *PrevTexture;

//	assert(m_Textures>0);
	if(m_Textures==0) return;

	PrevTexture = 0;
	CurTexture = m_Textures;
	while(CurTexture)
	{
		if(CurTexture==Tex)
		{
			if(PrevTexture==0)
			{
				m_Textures = CurTexture->m_Next;
				delete CurTexture;
			}
			else
			{
				PrevTexture->m_Next = CurTexture->m_Next;
				delete CurTexture;
			}
			return;
		}
		CurTexture= CurTexture->m_Next;
	}

	// should not happen. Deleting  texture which is not managed here
//	assert(0);
}

////////////////

Texture::~Texture()
{
}

Texture::Texture(TexManager *Owner)
{
	m_Owner = Owner;
	m_Buffer = 0;
	m_Surface = 0;
}


int		Texture::Load(XString Filename)
{
	int succes;
	CSurface32 *Surf;
	Surf = new CSurface32();
	succes = Surf->Load(Filename);

	if(succes)
	{
		m_Surface = Surf;
		return 1;
	}

	delete Surf;
	return 0;
}


int		Texture::GetWidth()
{
	if(m_Surface) return m_Surface->GetWidth();
	return 0;
}


int		Texture::GetWidthShift()
{
	int n=0;
	if(m_Surface == 0) return 0; 
	int w = m_Surface->GetWidth();
	while(w!=1)
	{
		n++;
		w>>=1;
	}
	return n;
}


int		Texture::GetHeight()
{
	if(m_Surface == 0) return 0; 
	return m_Surface->GetHeight();;
}

unsigned int    *Texture::GetBuf()
{
	if(m_Surface == 0) return 0; 
	return (unsigned int *)m_Surface->GetBuffer();
}

XString Texture::GetName()
{
	return m_Name;
}

void Texture::SetName(XString Name)
{
	m_Name = Name;
}
