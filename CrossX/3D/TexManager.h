#ifndef _H_TEXMANAGER
#define _H_TEXMANAGER

#include "utils/xstring.h"

class Texture;
class CSurface;

class TexManager
{
	friend class Texture;
public:
	~TexManager();
private:
	TexManager();    // use the gettexturemanger function

public:

	static  TexManager *GetTextureManager();

	void	Clear();
	int     Size();
	Texture *GetTexture(XString Name);
	Texture *GetPhongTexture(); //phong texture is first in the list (if available)
	void	RemoveTexture(Texture *);
private:
	Texture *m_Textures;
	int      m_NumTextures;
};


class Texture
{
	friend class TexManager;
public:
	~Texture();

	int				Load(XString Filename);

	int				GetWidth();
	int				GetWidthShift(); //which 2 power is the width of the texture?
	int				GetHeight();
	unsigned int   *GetBuf();
	XString 	    GetName();
	void            SetName(XString Name);
	CSurface       *GetSurface();

protected:
	Texture(TexManager *Owner);	//should only be created through the texture manager

private:
	//linked list management
	Texture        *m_Next;

	//members
	CSurface       *m_Surface;  //actual texture data
	XString         m_Name;
	TexManager     *m_Owner;
	unsigned int   *m_Buffer;

};

#endif
