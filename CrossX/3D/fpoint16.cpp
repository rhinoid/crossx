#include "fpoint16.h"


#define PI 3.14159265359f

int16 *Fixed16::GetSinTab()
{
	int i;
	if(GLOBALS->m_Fixed16SinTab) return GLOBALS->m_Fixed16SinTab;

	GLOBALS->m_Fixed16SinTab = new int16[4096];
	for ( i = 0; i < 4096; i++ )
	{
		GLOBALS->m_Fixed16SinTab[i]    = (int)((1<<15) * ::sin( (float)i * PI / 2048 ) );
	}
	return GLOBALS->m_Fixed16SinTab;
}
