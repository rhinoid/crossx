#ifndef _FPOINT16_H
#define _FPOINT16_H

#include "utils/types.h"
#include "utils/globals.h"

#ifdef __SYMBIAN32__
#include <libc\math.h>
#else
#include <math.h>
#endif

#ifdef ARM 
	extern "C" int Mul64_16(int a, int b); 
	extern "C" unsigned int FDIV(unsigned int a, unsigned int b); 
	extern "C" int FDIVS(int a, int b); 
	extern "C" unsigned int SQRT(unsigned int a); 
#endif 


#define OVERFLOWDETECT(a,b) {\
	int numbit1=0, numbit2=0;\
	int t = a;\
	while(t>0)\
	{\
		numbit1++;\
		t>>=1;\
	}\
	t = b;\
	while(t>0)\
	{\
		numbit2++;\
		t>>=1;\
	}\
	if((numbit1/2)+(numbit2/2) > 15)\
	{\
		int j=0,k=0;\
		j /= k; \
	}\
}



struct Fixed16 
{	
	enum { SHIFT = 16 };
	enum { SHIFTROOT = 8};    //wortel van Fixed factor(256))=16 (nodig om wortel te trekken)  (is toevallig de helft van de shift)

	int32 val;

	Fixed16() : val(0) {}
	Fixed16(const int32 &v) : val(v) { }
	Fixed16(const double d) {
		val = fromDouble(d).val;
	}

	static int16 *Fixed16::GetSinTab(); // Get the internal sine table



	//BIGMultiplication
	inline Fixed16 operator%(const Fixed16 &f) const {

#ifdef ARM
		return Mul64_16(val, f.val);
#else
		int64 big(static_cast<int64 >(val) * f.val);
		return Fixed16(static_cast<int32 >(big >> SHIFT));
#endif
	}
	inline Fixed16& operator%=(const Fixed16 &f) {
#ifdef ARM
		val = Mul64_16(val, f.val);
#else
		int64 big(static_cast<int64 >(val) * f.val);

		val = static_cast<int32>(big >> SHIFT);
#endif
		return *this;
	}

	//normal Multiplication
	inline Fixed16 operator*(const Fixed16 &f) const {
//for optimization very usefull: overflowdetection to see if 64bit multiplication(^) has to be used
//		OVERFLOWDETECT(val, f.val);
		return Fixed16(((val) * (f.val)) >> SHIFT);
	}

	inline Fixed16& operator*=(const Fixed16 &f) {
//for optimization very usefull: overflowdetection to see if 64bit multiplication(^) has to be used
//		OVERFLOWDETECT(val, f.val);
		val = ((val) * (f.val)) >> SHIFT;
		return *this;
	}


	//Division
	inline Fixed16 operator/(const Fixed16 &f) const {
		int64 big(static_cast<int64>(val) << SHIFT);
		return Fixed16(static_cast<int32>(big / f.val));
	}
	inline Fixed16& operator/=(const Fixed16 &f) {
		int64 Big = static_cast<int64 >(val) << SHIFT;
		val = static_cast<int32>(Big / f.val);
		return *this;
	}

	//Addition
	inline Fixed16 operator+(const Fixed16 &f) const {
		return Fixed16(val + f.val);
	}
	inline Fixed16& operator+=(const Fixed16 &f) {
		val += f.val;
		return *this;
	}

	//Subtraction
	inline Fixed16 operator-(const Fixed16 &f) const {
		return Fixed16(val - f.val);
	}
	inline Fixed16& operator-=(const Fixed16 &f) {
		val -= f.val;
		return *this;
	}

	//Assignment
	inline Fixed16& operator=(const double d) {
		val = fromDouble(d).val;
		return *this;
	}
	inline Fixed16& operator=(const Fixed16 &v) {
		val = v.val;
		return *this;
	}
	inline Fixed16& operator=(const int &v) {
		val = v;
		return *this;
	}

	//Comparison
	inline bool operator==(const Fixed16 &f) const {
		return f.val == val;
	}
	inline bool operator!=(const Fixed16 &f) const {
		return f.val != val;
	}
	inline bool operator<(const Fixed16 &f) const {
		return val < f.val;
	}
	inline bool operator<=(const Fixed16 &f) const {
		return val <= f.val;
	}
	inline bool operator>(const Fixed16 &f) const {
		return val >f.val;
	}
	inline bool operator>=(const Fixed16 &f) const {
		return val >=f.val;
	}

	//Unary
	inline Fixed16 operator -() const {
      Fixed16 ret(-val);
      return ret;
    }


	//functions
	Fixed16 sqrt()
	{
#ifdef ARM
		Fixed16 rv;
		rv.val = SQRT(val);
		rv.val <<=SHIFT;
		rv.val >>=SHIFTROOT;
		return rv;
#else
		Fixed16 rv;
		rv.val = (int)::sqrt(val);
		rv.val <<=SHIFT;
		rv.val >>=SHIFTROOT;
		return rv;
#endif
	}

	Fixed16 sqr()
	{
		int64 Big;
		Fixed16 rv;
		Big = static_cast<int64>(val*val);
		Big >>= SHIFT;
		rv.val = static_cast<int32>(Big);
		return rv;
	}

	inline static Fixed16 sin(int a)  //attention! the parameter a is not a standard sin parameter but has a range of 0-4095 for wavelength
	{
		int16 *SinTab = Fixed16::GetSinTab();
		return SinTab[a&4095];
	}

	inline static Fixed16 cos(int a) 
	{
		int16 *SinTab = Fixed16::GetSinTab();
		return SinTab[(a+1024)&4095];
	}



	//Conversions
	static inline Fixed16 fromDouble(const double d) {
		return static_cast<int32>(d * (static_cast<int32>(1) << SHIFT) + 0.5);
	}
	static inline double toDouble(const Fixed16 &f) {
		return static_cast<double>(f.val) / (static_cast<int32>(1) << SHIFT);
	}
	static inline Fixed16 fromInt(const int d) {
		return static_cast<int32>(d << SHIFT);
	}
	static inline int toInt(const Fixed16 &f) {
		return static_cast<int>(f.val >> SHIFT);
	}
};

#endif
