#include "fpoint8.h"


#define PI 3.14159265359f

int16 *Fixed8::GetSinTab()
{
	int i;
	if(GLOBALS->m_Fixed8SinTab) return GLOBALS->m_Fixed8SinTab;

	GLOBALS->m_Fixed8SinTab = new int16[4096];
	for ( i = 0; i < 4096; i++ )
	{
		GLOBALS->m_Fixed8SinTab[i]    = (int)((1<<8) * ::sin( (float)i * PI / 2048 ) );
	}
	return GLOBALS->m_Fixed8SinTab;
}

