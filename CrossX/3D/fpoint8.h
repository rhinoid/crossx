#ifndef _FPOINT8_H
#define _FPOINT8_H

#include "utils/types.h"
#include "3d/fixed.h"
#include "utils/globals.h"

#ifdef __SYMBIAN32__
#include <libc\math.h>
#else
#include <math.h>
#endif

#ifdef ARM 
	extern "C" int Mul64_8(int a, int b); 
	extern "C" unsigned int FDIV(unsigned int a, unsigned int b); 
	extern "C" int FDIVS(int a, int b); 
	extern "C" unsigned int SQRT(unsigned int a); 
#endif 




struct Fixed8 
{	
	enum { SHIFT = 8 };
	enum { SHIFTROOT = 4};    //wortel van Fixed factor(256))=16 (nodig om wortel te trekken)  (is toevallig de helft van de shift)

	int32 val;

	Fixed8() : val(0) {}
	Fixed8(const int32 &v) : val(v) { }
	Fixed8(const double d) {
		val = fromDouble(d).val;
	}

	static int16 *Fixed8::GetSinTab(); // Get the internal sine table


	//BIGMultiplication
	inline Fixed8 operator%(const Fixed8 &f) const {

#ifdef ARM
		return Mul64_8(val, f.val);
#else
		int64 big(static_cast<int64 >(val) * f.val);
		return Fixed8(static_cast<int32 >(big >> SHIFT));
#endif
	}
	inline Fixed8& operator%=(const Fixed8 &f) {
#ifdef ARM
		val = Mul64_8(val, f.val);
#else
		int64 big(static_cast<int64 >(val) * f.val);

		val = static_cast<int32>(big >> SHIFT);
#endif
		return *this;
	}


#if 1
	//normal Multiplication
	inline Fixed8 operator*(const Fixed8 &f) const {
		return Fixed8(((val) * (f.val)) >> SHIFT);
	}

	inline Fixed8& operator*=(const Fixed8 &f) {
		val = ((val) * (f.val)) >> SHIFT;
		return *this;
	}
#endif

	//Division
	inline Fixed8 PreciseDivide(const Fixed8 &f) const {
		int64 big(static_cast<int64>(val) << SHIFT);
		return Fixed8(static_cast<int32>(big / f.val));
	}

#if 0 //slow 64bit version
	//Division
	inline Fixed8 operator/(const Fixed8 &f) const {
		int64 big(static_cast<int64>(val) << SHIFT);
		return Fixed8(static_cast<int32>(big / f.val));
	}
	inline Fixed8& operator/=(const Fixed8 &f) {
		int64 Big = static_cast<int64 >(val) << SHIFT;
		val = static_cast<int32>(Big / f.val);
		return *this;
	}
#endif

#if 1
	//Division
	inline Fixed8 operator/(const Fixed8 &f) const {
#ifdef ARM
		int32 big((val) << SHIFT);
		return Fixed8(FDIVS(big, f.val));
#else
		int32 big((val) << SHIFT);
		return Fixed8(big / f.val);
#endif
	}

	inline Fixed8& operator/=(const Fixed8 &f) {
#ifdef ARM
		int32 Big((val) << SHIFT);
		val = (FDIVS(Big, f.val));
#else
		int32 Big((val) << SHIFT);
		val = (Big / f.val);
#endif
		return *this;
	}
#endif



	//Addition
	inline Fixed8 operator+(const Fixed8 &f) const {
		return Fixed8(val + f.val);
	}
	inline Fixed8& operator+=(const Fixed8 &f) {
		val += f.val;
		return *this;
	}

	//Subtraction
	inline Fixed8 operator-(const Fixed8 &f) const {
		return Fixed8(val - f.val);
	}
	inline Fixed8& operator-=(const Fixed8 &f) {
		val -= f.val;
		return *this;
	}

	//Assignment
	inline Fixed8& operator=(const double d) {
		val = fromDouble(d).val;
		return *this;
	}
	inline Fixed8& operator=(const Fixed8 &v) {
		val = v.val;
		return *this;
	}
	inline Fixed8& operator=(const int &v) {
		val = v;
		return *this;
	}

	//Comparison
	inline bool operator==(const Fixed8 &f) const {
		return f.val == val;
	}
	inline bool operator!=(const Fixed8 &f) const {
		return f.val != val;
	}
	inline bool operator<(const Fixed8 &f) const {
		return val < f.val;
	}
	inline bool operator<=(const Fixed8 &f) const {
		return val <= f.val;
	}
	inline bool operator>(const Fixed8 &f) const {
		return val >f.val;
	}
	inline bool operator>=(const Fixed8 &f) const {
		return val >=f.val;
	}

	//Unary
	inline Fixed8 operator -() const {
      Fixed8 ret(-val);
      return ret;
    }


	//functions
	Fixed8 sqrt()
	{
#ifdef ARM
		Fixed8 rv;
		rv.val = SQRT(val);
		rv.val <<=SHIFT;
		rv.val >>=SHIFTROOT;
		return rv;
#else
		Fixed8 rv;
		rv.val = (int)::sqrt(val);
		rv.val <<=SHIFT;
		rv.val >>=SHIFTROOT;
		return rv;
#endif
	}

	Fixed8 sqr()
	{
		int64 Big;
		Fixed8 rv;
		Big = static_cast<int64>(val*val);
		Big >>= SHIFT;
		rv.val = static_cast<int32>(Big);
		return rv;
	}

	inline static Fixed8 sin(int a)  //attention! the parameter a is not a standard sin parameter but has a range of 0-4095 for wavelength
	{
		int16 *SinTab = Fixed8::GetSinTab();
		return SinTab[a&4095];
	}

	inline static Fixed8 cos(int a) 
	{
		int16 *SinTab = Fixed8::GetSinTab();
		return SinTab[(a+1024)&4095];
	}

	//Conversions
	static inline Fixed8 fromDouble(const double d) {
		return static_cast<int32>(d * (static_cast<int32>(1) << SHIFT) + 0.5);
	}
	static inline double toDouble(const Fixed8 &f) {
		return static_cast<double>(f.val) / (static_cast<int32>(1) << SHIFT);
	}
	static inline Fixed8 fromInt(const int d) {
		return static_cast<int32>(d << SHIFT);
	}
	static inline int toInt(const Fixed8 &f) {
		return static_cast<int>(f.val >> SHIFT);
	}
};

#endif
