#include "math.h"

#ifdef __SYMBIAN32__
#include <libc/stdlib.h>
#include <libc/stdio.h>
#include <libc/string.h>
#else
#include <memory.h>
#endif



void InitMath()
{
	
	GLOBALS->PI = (int)(3.1415926535897932384626433832795f * 256);
	GLOBALS->TWO_PI = (int)(2.0f * 3.1415926535897932384626433832795f * 256);
	GLOBALS->HALF_PI = (int)(0.5f * 3.1415926535897932384626433832795f * 256);

	int dummy[16] = { (int)(1.0f * 256), (int)(0.0f * 256), (int)(0.0f * 256), (int)(0.0f * 256), (int)(0.0f * 256), (int)(1.0f * 256), (int)(0.0f * 256), (int)(0.0f * 256), (int)(0.0f * 256), (int)(0.0f * 256), (int)(1.0f * 256), (int)(0.0f * 256), (int)(0.0f * 256), (int)(0.0f * 256), (int)(0.0f * 256), (int)(1.0f * 256) };

	int i;
	for(i=0; i<16; i++)
	{
		GLOBALS->g_Matrix4Ident[i] = dummy[i];
	}
}

#define MATHABS(x) ((x)<Fixed8::fromInt(0)) ? (-x) : (x))


Matrix4::Matrix4() 
{ 
	memset( M, 0, sizeof(MATHTYPE)*16);
	M[0][0].val = M[1][1].val = M[2][2].val = M[3][3].val = 256;
};

Matrix4::Matrix4( const Matrix4 &sM ) { memcpy( M, sM.M, 64 ); }

Matrix4::Matrix4( const MATHTYPE Components[] ) { memcpy( M, Components, 64 ); }


Matrix4::Matrix4( const Vector3 &v) { Set( v ); }
 
Matrix4 &Matrix4::Reset()
{
	memcpy( M, GLOBALS->g_Matrix4Ident, 64 );
	return( *this );
}


Matrix4 &Matrix4::Set( MATHTYPE Components[] )
{
	memcpy( M, Components, 64 );
	return( *this ); 
}




/*
Matrix4 &Matrix4::Set( const Quaternion &rkQuat, const Vector3 &rkTranslation )
{
	rkQuat.ToMatrix4( this );
	M[0][3] = rkTranslation.M[0];
	M[1][3] = rkTranslation.M[1];
	M[2][3] = rkTranslation.M[2];
	return( *this );
}
*/


Matrix4 &Matrix4::Set( const Vector3 &v )
{
	MATHTYPE *pfDst = &M[0][0];

	*pfDst++ =  0.0f;
	*pfDst++ = -v.M[2];
	*pfDst++ =  v.M[1];
	*pfDst++ =  0.0f;

	*pfDst++ =  v.M[2];
	*pfDst++ =  0.0f;
	*pfDst++ = -v.M[0];
	*pfDst++ =  0.0f;

	*pfDst++ = -v.M[1];
	*pfDst++ =  v.M[0];
	*pfDst++ =  0.0f;
	*pfDst++ =  0.0f;

	*pfDst++ =  0.0f;
	*pfDst++ =  0.0f;
	*pfDst++ =  0.0f;
	*pfDst   =  1.0f;

	return( *this );
}

/*
Matrix4 &Matrix4::SetRotation( const Quaternion &rkQuat )
{
	rkQuat.ToMatrix4( this );
	return( *this );
}
*/


 // returns matrix which will rotate along the x axis
Matrix4 &Matrix4::SetXRotation( int Angle)
 {
	 Fixed8 cosangle,sinangle;
	 cosangle = Fixed8::cos(Angle);
	 sinangle = Fixed8::sin(Angle);
	 M[1][1] = cosangle;
	 M[1][2] = -sinangle;
	 M[2][1] = sinangle;
	 M[2][2] = cosangle;
	 return *this;
 }

 // returns matrix which will rotate along the y axis
 Matrix4 &Matrix4::SetYRotation( int Angle)
 {
	 Fixed8 cosangle,sinangle;
	 cosangle = Fixed8::cos(Angle);
	 sinangle = Fixed8::sin(Angle);
	 M[0][0] = cosangle;
	 M[0][2] = sinangle;
	 M[2][0] = -sinangle;
	 M[2][2] = cosangle;
	 return *this;
 }

 // returns matrix which will rotate along the z axis
 Matrix4 &Matrix4::SetZRotation( int Angle)
 {
	 Fixed8 cosangle,sinangle;
	 cosangle = Fixed8::cos(Angle);
	 sinangle = Fixed8::sin(Angle);
	 M[0][0] = cosangle;
	 M[0][1] = -sinangle;
	 M[1][0] = sinangle;
	 M[1][1] = cosangle;
	 return *this;
 }


Matrix4 &Matrix4::SetTranslation( const Vector3 &v )
{
	M[0][3] = v.M[0];
	M[1][3] = v.M[1];
	M[2][3] = v.M[2];
	return( *this );
}


Matrix4 &Matrix4::SetScaling( const Vector3 &v )
{
	M[0][0] = v.M[0];
	M[1][1] = v.M[1];
	M[2][2] = v.M[2];
	return( *this );
}



/// Returns a lookat transformation matrix
Matrix4 &Matrix4::Lookat(Vector3 &Eye, Vector3& Center, Vector3& Up)
{
	Vector3 Forward = Center;
	Forward -= Eye;
	Vector3 Side = (Forward ^ Up);
	Forward.Normalize();
	Side.Normalize();
	
	Vector3 Up2 = Side ^ Forward;
	
	Matrix4 m; //identity
	m.M[0][0] = Side.M[0];
	m.M[0][1] = Side.M[1];
	m.M[0][2] = Side.M[2];
	
	m.M[1][0] = Up2.M[0];
	m.M[1][1] = Up2.M[1];
	m.M[1][2] = Up2.M[2];
	
	m.M[2][0] = -Forward.M[0];
	m.M[2][1] = -Forward.M[1];
	m.M[2][2] = -Forward.M[2];
	
	Matrix4 trans;
	Vector3 invEye(Eye);
	invEye.neg();
	trans.SetTranslation(invEye);

	*this = (m * trans);
	return *this;
}



Matrix4 &Matrix4::Transpose()
{
	MATHTYPE fTemp       = M[1][0];
	M[1][0] = M[0][1];
	M[0][1] = fTemp;

	fTemp             = M[2][0];
	M[2][0] = M[0][2];
	M[0][2] = fTemp;

	fTemp             = M[3][0];
	M[3][0] = M[0][3];
	M[0][3] = fTemp;

	fTemp             = M[2][1];
	M[2][1] = M[1][2];
	M[1][2] = fTemp;

	fTemp             = M[3][1];
	M[3][1] = M[1][3];
	M[1][3] = fTemp;

	fTemp             = M[3][2];
	M[3][2] = M[2][3];
	M[2][3] = fTemp;

	return( *this );
}


Matrix4 &Matrix4::TransposeTo( Matrix4 *Mat )
{
	MATHTYPE *Dst = &Mat->M[0][0];
	MATHTYPE *Src = &M[0][0];

	*Dst++ = Src[0];
	*Dst++ = Src[4];
	*Dst++ = Src[8];
	*Dst++ = Src[12];
	*Dst++ = Src[1];
	*Dst++ = Src[5];
	*Dst++ = Src[9];
	*Dst++ = Src[13];
	*Dst++ = Src[2];
	*Dst++ = Src[6];
	*Dst++ = Src[10];
	*Dst++ = Src[14];
	*Dst++ = Src[3];
	*Dst++ = Src[7];
	*Dst++ = Src[11];
	*Dst   = Src[15];

	return( *Mat );
}


Vector3 Matrix4::GetColumn( int Col ) const
{
	return Vector3( M[ 0 ][ Col ], M[ 1 ][ Col ], M[ 2 ][ Col ] );
}


Matrix4 &Matrix4::operator =( const Matrix4 &Mat )
{
	memcpy( M, Mat.M, 64 );
	return( *this );
}

/*
Matrix4 &Matrix4::operator =( const Quaternion &rkQuat )
{
	rkQuat.ToMatrix4( this );
	
	M[0][3] = M[1][3] = M[2][3] = M[3][0] = M[3][1] = M[3][2] = 0.0f;
	M[3][3] = 1.0f;
	
	return( *this );
}
*/

Matrix4 Matrix4::operator *( const Matrix4 &Mat ) const
{
	Matrix4 Prod;
	
	for( int iRow = 0; iRow < 4; ++iRow )
		for( int iCol = 0; iCol < 4; ++iCol )
			Prod.M[ iRow ][ iCol ] =
			  M[ iRow ][ 0 ] * Mat.M[ 0 ][ iCol ] +
			  M[ iRow ][ 1 ] * Mat.M[ 1 ][ iCol ] +
			  M[ iRow ][ 2 ] * Mat.M[ 2 ][ iCol ] +
			  M[ iRow ][ 3 ] * Mat.M[ 3 ][ iCol ];
		
	return Prod;
}


Matrix4 &Matrix4::operator *=( const Matrix4 &Mat )
{
	MATHTYPE Prod[4][4];
	
	for( int iRow = 0; iRow < 4; ++iRow )
		for( int iCol = 0; iCol < 4; ++iCol )
			Prod[ iRow ][ iCol ] =
			  M[ iRow ][ 0 ] * Mat.M[ 0 ][ iCol ] +
			  M[ iRow ][ 1 ] * Mat.M[ 1 ][ iCol ] +
			  M[ iRow ][ 2 ] * Mat.M[ 2 ][ iCol ] +
			  M[ iRow ][ 3 ] * Mat.M[ 3 ][ iCol ];
		
	return Set( &Prod[0][0] );
}


Matrix4 Matrix4::operator *( MATHTYPE Scale ) const
{
	return( Matrix4( *this ) *= Scale );
}


Matrix4 &Matrix4::operator *=( MATHTYPE Scale )
{
	MATHTYPE *Val = &M[0][0];

	for( int i = 0; i < 16; ++i )
		*Val++ *= Scale;
		
	return( *this );
}


Matrix4 Matrix4::operator -( const Matrix4 &Mat ) const
{
	return( Matrix4( *this ) -= Mat );
}


Matrix4 &Matrix4::operator -=( const Matrix4 &Mat )
{
	MATHTYPE *Val = &M[0][0];
	const MATHTYPE *Src = &Mat.M[0][0];

	for( int i = 0; i < 16; ++i )
		*Val++ -= *Src++;
		
	return( *this );
}


Matrix4 Matrix4::operator +( const Matrix4 &Mat ) const
{
	return( Matrix4( *this ) += Mat );
}


Matrix4 &Matrix4::operator +=( const Matrix4 &Mat )
{
	MATHTYPE *Val = &M[0][0];
	const MATHTYPE *Src = &Mat.M[0][0];

	for( int i = 0; i < 16; ++i )
		*Val++ += *Src++;
		
	return( *this );
}


Vector3 Matrix4::operator *( const Vector3 &v ) const
{
	Vector3 rv;

	rv.M[0] = M[0][0] * v.M[0] + M[0][1] * v.M[1] + M[0][2] * v.M[2] + M[0][3];
	rv.M[1] = M[1][0] * v.M[0] + M[1][1] * v.M[1] + M[1][2] * v.M[2] + M[1][3];
	rv.M[2] = M[2][0] * v.M[0] + M[2][1] * v.M[1] + M[2][2] * v.M[2] + M[2][3];

	return rv;
}

Vector3 Matrix4::operator /( const Vector3 &v ) const  //inverse transform
{
	MATHTYPE x,y,z;
	Vector3 rv;

	x = v.M[0] + M[0][3];
	y = v.M[1] + M[1][3];
	z = v.M[2] + M[2][3];
	rv.M[0] = M[0][0] * x + M[0][1] * y + M[0][2] * z;
	rv.M[1] = M[1][0] * x + M[1][1] * y + M[1][2] * z;
	rv.M[2] = M[2][0] * x + M[2][1] * y + M[2][2] * z;

	return rv;
}

Matrix4 &Matrix4::Inverse()
{	
	int v, h;
	Matrix4 t;
	MATHTYPE tx = -M[0][3];
	MATHTYPE ty = -M[1][3];
	MATHTYPE tz = -M[2][3];
	for ( h = 0; h < 3; h++ ) for ( v = 0; v < 3; v++ ) t.M[h][v] = M[v][h]; //flip
	for ( h = 0; h < 4; h++ ) for ( v = 0; v < 4; v++ ) M[h][v] = t.M[h][v]; //copy
	M[0][3] = tx * M[0][0] + ty * M[0][1] + tz * M[0][2];
	M[1][3] = tx * M[1][0] + ty * M[1][1] + tz * M[1][2];
	M[2][3] = tx * M[2][0] + ty * M[2][1] + tz * M[2][2];

	return( *this );
}



bool Matrix4::operator ==( const Matrix4 &Mat ) const
{
	for( int iRow = 0; iRow < 4; ++iRow )
		for( int iCol = 0; iCol < 4; ++iCol )
			if( M[ iRow ][ iCol ] != Mat.M[ iRow ][ iCol ] )
				return false; 
			
	return true;
}


bool Matrix4::operator !=( const Matrix4 &Mat ) const
{
	for( int iRow = 0; iRow < 4; ++iRow )
		for( int iCol = 0; iCol < 4; ++iCol )
			if( M[ iRow ][ iCol ] == Mat.M[ iRow ][ iCol ] )
				return false; 
			
	return true;
}


      MATHTYPE *Matrix4::operator []( int iRow )       { return (MATHTYPE*)&M[ iRow ][ 0 ]; }
const MATHTYPE *Matrix4::operator []( int iRow ) const { return (MATHTYPE*)&M[ iRow ][ 0 ]; }




