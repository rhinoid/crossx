#if !defined(_MATH_INCLUDED)
#define _MATH_INCLUDED

#include <math.h>
#include "3d/fpoint8.h"
#include "3d/fpoint16.h"
#include "3d/fixed.h"

typedef Fixed8 MATHTYPE;

#define  RADTODEG    MATHTYPE(180.0/GLOBALS->PI)
#define  DEGTORAD    MATHTYPE(GLOBALS->PI/180.0)

void InitMath();

class Vector3;

// 4x4 matrix /////////////////////////////////////////////

class Matrix4
{
    private:
    
  

	public:

		/*! Components */
		MATHTYPE M[4][4]; // array indexed data storage


		/**
		* Reset components to identity matrix
		*/
		                                        Matrix4();

		/**
		* Set components
		* \param rkMatrix4                             Matrix4 to copy values from
		*/
		                                        Matrix4( const Matrix4 &Mat );

		/**
		* Set components
		* \param afComponents                         Array of floats to copy components from
		*/
		                                        Matrix4( const MATHTYPE Components[] );



		/**
		* Create a cross-product matrix M from vector, such that M * a = rkVector x a
		* \param rkVector                             Vector
		*/

		                                        Matrix4( const Vector3 &v );

		/**
		* Computes inverse matrix
		* \return                                     Resulting Matrix4 ref (this)
		*/
		 Matrix4                                &Inverse();

		/**
		* Reset components to identity matrix
		* \return                                     Resulting Matrix4 ref (this)
		*/
		 Matrix4                                &Reset();

		/**
		* Set components
		* \param afComponents                         Array of floats to copy components from
		* \return                                     Resulting Matrix4 ref (this)
		*/
		 Matrix4                                &Set( MATHTYPE Components[] );


		/**
		* Create a cross-product matrix M from vector, such that M * a = rkVector x a
		* \param rkVector                             Vector
		* \return                                     Resulting const Matrix4 ref (this)
		*/
		 Matrix4                                &Set( const Vector3 &v );


		 // returns matrix which will rotate along the x axis
		 Matrix4							    &SetXRotation( int Angle);
		 // returns matrix which will rotate along the y axis
		 Matrix4							    &SetYRotation( int Angle);
		 // returns matrix which will rotate along the z axis
		 Matrix4							    &SetZRotation( int Angle);

		/**
		* Set translation part of matrix from vector (does not touch rotation part)
		* \param rkTranslation                        Translation as vector
		* \return                                     Resulting Matrix4 ref (this)
		*/
		 Matrix4                                &SetTranslation( const Vector3 &v );

		/**
		* Set scaling part of matrix from vector (does not touch translation)
		* \param rkScaling                            Scaling as vector
		* \return                                     Resulting Matrox ref (this)
		*/
		 Matrix4                                &SetScaling( const Vector3 &v );


		 // calculate the matrix given a place, a direction and the up vector
		Matrix4									&Lookat(Vector3 &Eye, Vector3& Center, Vector3& Up);

		/**
		* Transpose matrix (modifies this matrix!)
		* \return                                     Resulting const Matrix4 ref (this)
		*/
		 Matrix4                                &Transpose();

		/**
		* Transpose matrix and store in given matrix (does not store in temporary space, DO NOT pass this as argument)
		* \param pkMatrix4                             Matrix4 receiving transpose of this matrix
		* \return                                     Resulting matrix (*pkMatrix4)
		*/
		 Matrix4                                &TransposeTo( Matrix4 *Mat );

		/**
		* Column access
		* \param iColumn                              Column index
		* \return                                     Vector (axis)
		*/
		 Vector3                               GetColumn( int Col ) const;


		/******* operators *******/

		/**
		* Assignment
		* \param rkMatrix4                             Matrix4 to copy values from
		* \return                                     Resulting Matrix4 ref (this)
		*/
		 Matrix4                                &operator =( const Matrix4 &Mat );


		/**
		* Matrix4 multiplication
		* \param rkMatrix4                             Matrix4 to multiply with, result = this * rkMatrix4
		* \return                                     Result matrix
		*/
		 Matrix4                                 operator *( const Matrix4 &Mat ) const;

		/**
		* Matrix4 multiplication and assignment
		* \param rkMatrix4                             Matrix4 to multiply with, this = this * rkMatrix4, result = this
		* \return                                     Resulting Matrix4 ref (this)
		*/
		 Matrix4                                &operator *=( const Matrix4 &Mat );

		/**
		* Scale matrix by scalar
		* \param fScale                               Scalar value
		* \return                                     Resulting Matrix4
		*/
		 Matrix4                                 operator * ( MATHTYPE Scale ) const;

		/**
		* Scale matrix by scalar
		* \param fScale                               Scalar value
		* \return                                     Resulting Matrix4 ref (this)
		*/
		 Matrix4                                &operator *= ( MATHTYPE Scale );

		/**
		* Matrix4 subtraction
		* \param rkMatrix4                             Matrix4 to subtract from this
		* \return                                     Resulting Matrix4
		*/
		 Matrix4                                 operator - ( const Matrix4 &Mat ) const;

		/**
		* Matrix4 subtraction and assignment
		* \param rkMatrix4                             Matrix4 to subtract from this
		* \return                                     Resulting Matrix4 ref (this)
		*/
		 Matrix4                                &operator -= ( const Matrix4 &Mat );

		/**
		* Matrix4 addition
		* \param rkMatrix4                             Matrix4 to add to this
		* \return                                     Resulting Matrix4
		*/
		 Matrix4                                 operator + ( const Matrix4 &Mat ) const;

		/**
		* Matrix4 addition and assignment
		* \param rkMatrix4                             Matrix4 to add to this
		* \return                                     Resulting Matrix4 ref (this)
		*/
		 Matrix4                                &operator += ( const Matrix4 &Mat );

		/**
		* Multiplication with vector (transform vector)
		* \param rkVector                             Vector to transform
		* \return                                     Result vector
		*/
		 Vector3                               operator *( const Vector3 &v ) const;

		/**
		* Division with vector (Inverse transform vector)
		* \param rkVector                             Vector to transform
		* \return                                     Result vector
		*/
		 Vector3                               operator /( const Vector3 &v ) const;

		/**
		* Compare with epsilon tolerance
		* \param rkMatrix4                             Matrix4 to compare with
		* \return                                     true if equal (in epsilon zone), false if not
		*/
		 bool                                   operator ==( const Matrix4 &Mat ) const;

		/**
		* Compare with epsilon tolerance
		* \param rkMatrix4                             Matrix4 to compare wtih
		* \return                                     true if not equal, false if equal (in epsilon zone)
		*/
		 bool                                   operator !=( const Matrix4 &Mat ) const;

		/**
		* Row access
		* \param iRow                                 Row to access
		* \return                                     float pointer to row
		*/
		 MATHTYPE                                 *operator []( int iRow );

		/**
		* Conversion to float pointer
		*/
		                                        operator MATHTYPE*() { return &M[0][0]; }

		/**
		* Row access
		* \param iRow                                 Row to access
		* \return                                     float pointer to row
		*/
		 const MATHTYPE                           *operator []( int iRow ) const;

		/**
		* Conversion to float pointer
		*/
		                                        operator const MATHTYPE*() const { return &M[0][0]; }

		/**
		* Dummy operator for Win32 std::vector DLL exports
		*/
		bool                                          operator < ( const Matrix4 &Mat ) const { return false; }

//		const Matrix4 Matrix4::IDENTITY( (const MATHTYPE*)g_Matrix4Ident );

};








class Vector3 {

public:
		MATHTYPE M[4]; // array indexed storage

//////////////////////////////////////////////////////////////////////////////////////
// Constructors

// constructs the vector (0.0, 0.0, 0.0)
inline Vector3() {
	M[0] = M[1] = M[2] = 0;
}

// constructs the vector (xyz, xyz, xyz)
inline Vector3(MATHTYPE xyz) {
	M[0] = M[1] = M[2] = xyz;
}

// constructs the vector (x, y, z)
inline Vector3(MATHTYPE x, MATHTYPE y, MATHTYPE z) {
	M[0] = x;
	M[1] = y;
	M[2] = z;
}


// constructs a copy of the vector v
inline Vector3(const Vector3 &v) {
	M[0] = v.M[0];
	M[1] = v.M[1];
	M[2] = v.M[2];
}

///////////////////////////////////////////////////
// Create a valid RGB value from the vector
int                                     ToRGB()
{
	int r,g,b;
	r = M[0].val>>(Fixed8::SHIFT-8);
	g = M[1].val>>(Fixed8::SHIFT-8);
	b = M[2].val>>(Fixed8::SHIFT-8);
	if(r<0)r=0;
	if(r>255)r=255;
	if(g<0)g=0;
	if(g>255)g=255;
	if(b<0)b=0;
	if(b>255)b=255;
	return (r<<16)|(g<<8)|b;
}


///////////////////////////////////////////////////
// methods for debugging purposes
///////////////////////////////////////////////////

///////////////////////////////////////////////////
// Dumps the data describing the current instance followed by a '\n'
inline void dump() {
}

///////////////////////////////////////////////////
// Dumps the data describing the current instance followed by a '\n'
// the string in indent is printed out before each line
inline void dump(char* indent) {
}

/// @relates vec3
/// Scales the passed vector to unit length and returns a reference to it
inline const Vector3& NormalizePrescale()
{
//	if(M[0].val!=0 || M[1].val!=0 || M[2].val!=0)
//	{
//		while(M[0].val<1024 && M[0].val>-1024 && M[1].val<1024 && M[1].val>-1024 && M[2].val<1024 && M[2].val>-1024)
//		{
//			M[0].val<<=1;
//			M[1].val<<=1;
//			M[2].val<<=1;
//		}
//	}

	EGL_Fixed lsquared = length16bitSquared().val;
	EGL_Fixed r = EGL_InvSqrt(lsquared);

	Fixed16 x,y,z;
	x= M[0].val<<8;
	y= M[1].val<<8;
	z= M[2].val<<8;
	x %= r;
	y %= r;
	z %= r;
	M[0] = x.val>>8;
	M[1] = y.val>>8;
	M[2] = z.val>>8;
	
	return *this;
}

inline const Vector3& Normalize()
{
	Fixed8 l = length();
	
	if ( l != 0 ) {
		M[0] /= l;
		M[1] /= l;
		M[2] /= l;
	}
	
	return *this;
}

inline const Vector3& Normalize16bit()
{
	Fixed16 l = length16bit();
	Fixed16 x,y,z;
	x = M[0].val<<8;
	y = M[1].val<<8;
	z = M[2].val<<8;
	if ( l != 0 ) {

		x /= l;
		y /= l;
		z /= l;
	}
	M[0].val = x.val>>8;
	M[1].val = y.val>>8;
	M[2].val = z.val>>8;
	
	return *this;
}

inline const Vector3& NormalizeCorrect()
{
	Fixed8 l = lengthCorrect();
	float len = (float)l.val / 65536.0f;
	
	if ( l != 0 ) {
		M[0].val = (int)((float)M[0].val / len);
		M[1].val = (int)((float)M[1].val / len);
		M[2].val = (int)((float)M[2].val / len);
	}
	
	return *this;
}

///////////////////////////////////////////////////
// methods using two vectors
// if the first one is omitted, 'this' is used instead
///////////////////////////////////////////////////



///////////////////////////////////////////////////
// multiplies every individual element of the 2 vectors

// returns result in a new instance
static inline Vector3 componentMult        (Vector3& v1, Vector3& v2) { return Vector3(v1.M[0]    * v2.M[0],  v1.M[1]    * v2.M[1],  v1.M[2]    * v2.M[2] ); }

// applied on the current instance
inline Vector3* componentMult         (Vector3& v) { this->M[0] *= v.M[0];  this->M[1] *= v.M[1];  this->M[2] *= v.M[2];  return this; }




///////////////////////////////////////////////////
// adds two vectors

// returns result in a new instance
static inline Vector3 add        (Vector3& v1, Vector3& v2) { return Vector3(v1.M[0]    + v2.M[0],  v1.M[1]    + v2.M[1],  v1.M[2]    + v2.M[2] ); }
inline Vector3 operator + (Vector3& v)  { return Vector3(this->M[0] + v.M[0],   this->M[1] + v.M[1],   this->M[2] + v.M[2]  ); }

// applied on the current instance
inline Vector3* add         (Vector3& v) { this->M[0] += v.M[0];  this->M[1] += v.M[1];  this->M[2] += v.M[2];  return this; }
inline Vector3& operator += (const Vector3& v) { this->M[0] += v.M[0];  this->M[1] += v.M[1];  this->M[2] += v.M[2];  return *this; }

///////////////////////////////////////////////////
// substracts the second from the first vector

// returns result in a new instance
static inline Vector3 sub        (Vector3& v1, Vector3& v2) { return Vector3(v1.M[0]    - v2.M[0],  v1.M[1]    - v2.M[1],  v1.M[2]    - v2.M[2] ); }
inline Vector3 operator - (const Vector3 &v)  { return Vector3(this->M[0] - v.M[0],   this->M[1] - v.M[1],   this->M[2] - v.M[2]  ); }

// applied on the current instance
inline Vector3* operator -= (Vector3& v) { this->M[0] -= v.M[0];  this->M[1] -= v.M[1];  this->M[2] -= v.M[2];  return this; }

///////////////////////////////////////////////////
// substracts the first from the second vector

// applied on the current instance
inline Vector3* xsub (Vector3& v) { this->M[0] = v.M[0] - this->M[0];  this->M[1] = v.M[1]  - this->M[1]; this->M[2] = v.M[2]  - this->M[2]; return this; }


///////////////////////////////////////////////////
// scalar-multiplication of two vectors

// returns result using two external vectors
static inline MATHTYPE smul       (Vector3& v1, Vector3& v2) { return v1.M[0]    * v2.M[0]  + v1.M[1]    * v2.M[1]  + v1.M[2]    * v2.M[2];  }

inline MATHTYPE operator * (Vector3& v)  { return this->M[0] * v.M[0]   + this->M[1] * v.M[1]   + this->M[2] * v.M[2];   }
inline MATHTYPE operator * (const Vector3& v)  { return this->M[0] * v.M[0]   + this->M[1] * v.M[1]   + this->M[2] * v.M[2];   }

// applied with the current instance (no changes will be made)
inline MATHTYPE smul (Vector3& v) { return this->M[0] % v.M[0]  + this->M[1] % v.M[1]  + this->M[2] % v.M[2];  }


///////////////////////////////////////////////////
// crossmultiplies the first with the second vector to obtain a normal vector to both

// returns result in a new instance
static inline Vector3 xmul       (Vector3& v1, Vector3& v2) { return Vector3(v1.M[1]    * v2.M[2]  - v1.M[2]    * v2.M[1],  v1.M[2]    * v2.M[0]  - v1.M[0]    * v2.M[2],  v1.M[0]    * v2.M[1]  - v1.M[1]    * v2.M[0] ); }
inline Vector3 operator ^ (Vector3& v)  { return Vector3(this->M[1] * v.M[2]   - this->M[2] * v.M[1],   this->M[2] * v.M[0]   - this->M[0] * v.M[2],   this->M[0] * v.M[1]   - this->M[1] * v.M[0]  ); }
inline Vector3 operator ^ (const Vector3 &v)  { return Vector3(this->M[1] * v.M[2]   - this->M[2] * v.M[1],   this->M[2] * v.M[0]   - this->M[0] * v.M[2],   this->M[0] * v.M[1]   - this->M[1] * v.M[0]  ); }


// applied on the current instance
inline Vector3* xmul        (Vector3& v) { return this->set(this->M[1] * v.M[2]   - this->M[2] * v.M[1],   this->M[2] * v.M[0]   - this->M[0] * v.M[2],   this->M[0] * v.M[1]   - this->M[1] * v.M[0]  ); }
inline Vector3* operator ^= (Vector3& v) { return this->set(this->M[1] * v.M[2]   - this->M[2] * v.M[1],   this->M[2] * v.M[0]   - this->M[0] * v.M[2],   this->M[0] * v.M[1]   - this->M[1] * v.M[0]  ); }


///////////////////////////////////////////////////
// crossmultiplies the second with the first vector to obtain a normal vector to both

// applied on the current instance
inline Vector3* xxmul (Vector3& v) { return this->set(v.M[1]  * this->M[2] - v.M[2]  * this->M[1], v.M[2]  * this->M[0] - v.M[0]  * this->M[2], v.M[0]  * this->M[1] - v.M[1]  * this->M[0]); }


///////////////////////////////////////////////////
// initializes the current instance with new values
inline Vector3* set        (MATHTYPE x, MATHTYPE y, MATHTYPE z) { this->M[0] = x;  this->M[1] = y;  this->M[2] = z;  return this; }
inline Vector3 &set        (Vector3& v) { this->M[0] = v.M[0];  this->M[1] = v.M[1];  this->M[2] = v.M[2];  return *this; }
//inline Vector3& operator = (Vector3& v) { this->M[0] = v.M[0];  this->M[1] = v.M[1];  this->M[2] = v.M[2];  return *this; }
inline Vector3& operator = (const Vector3 &v) { this->M[0] = v.M[0];  this->M[1] = v.M[1];  this->M[2] = v.M[2];  return *this; }

///////////////////////////////////////////////////
// multiplies (scales) a vector with a number (s)

// returns result in a new instance
inline Vector3 scale       (Vector3& v, MATHTYPE s) { return Vector3(v.M[0]     * s, v.M[1]     * s, v.M[2]     * s); }
inline Vector3 operator *  (MATHTYPE s)          { return Vector3(this->M[0] * s, this->M[1] * s, this->M[2] * s); }

// applied on the current instance
inline Vector3* scale (MATHTYPE s)               { this->M[0] *= s; this->M[1] *= s; this->M[2] *= s; return this; }
inline Vector3* operator *= (MATHTYPE s)         { this->M[0] *= s; this->M[1] *= s; this->M[2] *= s; return this; }



///////////////////////////////////////////////////
// projects the first onto the second vector
// b mustn't be (0.0, 0.0, 0.0)
// a onto b = (a*b)/(b*b)*b

// returns result in a new instance
inline Vector3 projectTo (Vector3& v1, Vector3& v2) { return Vector3(v2        ) * ((v1.M[0]  * v2.M[0]  + v1.M[1]  * v2.M[1]  + v1.M[2]  * v2.M[2] ) / (v2.M[0]  * v2.M[0]  + v2.M[1]  * v2.M[1]  + v2.M[2]  * v2.M[2] )); }


// applied on the current instance
inline Vector3& projectTo (Vector3& v) { Vector3 t; t = Vector3(v      ) * ((this->M[0] * v.M[0]  + this->M[1] * v.M[1]  + this->M[2] * v.M[2] ) / (v.M[0]  * v.M[0]  + v.M[1]  * v.M[1]  + v.M[2]  * v.M[2] )); this->set(t); return *this;}


///////////////////////////////////////////////////
// projects the second onto the first vector
// a mustn't be (0.0, 0.0, 0.0)
// b onto a = (b*a)/(a*a)*a

// applied on the current instance
inline Vector3* projectFrom (Vector3& v) { return this->scale((this->M[0] * v.M[0]  + this->M[1] * v.M[1]  + this->M[2] * v.M[2] ) / (this->M[0] * this->M[0] + this->M[1] * this->M[1] + this->M[2] * this->M[2])); }


///////////////////////////////////////////////////
// reflects the vector on the plane having the parameter-vector as normal

// applied on the current instance
// factor represents the amount of reflection by the surface:
// 0.0 = full absorbation -> result is parallel to the surface
// 1.0 = no absorbation -> same result as 'reflectTo'
//inline Vector3* reflectAbsorbedTo (PARAM_P, MATHTYPE factor) { return this->sub(projectTo(this, v) * (factor + 1)); }


///////////////////////////////////////////////////
// determines the sine of the smallest angle between two vectors
// none of the vectors must be (0.0, 0.0, 0.0)

// returns result using two external vectors
inline MATHTYPE sin (Vector3& v1, Vector3& v2) { return (MATHTYPE)(Vector3(v1.M[1]  * v2.M[2]  - v1.M[2]  * v2.M[1],  v1.M[2]  * v2.M[0]  - v1.M[0]  * v2.M[2],  v1.M[0]  * v2.M[1]  - v1.M[1]  * v2.M[0] ).sqr() / (v1.sqr()                   * v2.sqr()                  )).sqrt(); }

// applied with the current instance (no changes will be made)
inline MATHTYPE sin (Vector3& v) { return (MATHTYPE)(Vector3(this->M[1] * v.M[2]  - this->M[2] * v.M[1],  this->M[2] * v.M[0]  - this->M[0] * v.M[2],  this->M[0] * v.M[1]  - this->M[1] * v.M[0] ).sqr() / (this->sqr() * v.sqr()                )).sqrt(); }


///////////////////////////////////////////////////
// determines the cosine of the smallest angle between two vectors
// none of the vectors must be (0.0, 0.0, 0.0)

// returns result using two external vectors
inline MATHTYPE cos (Vector3& v1, Vector3& v2) { return (v1.M[0]  * v2.M[0]  + v1.M[1]  * v2.M[1]  + v1.M[2]  * v2.M[2] ) / (MATHTYPE)(v1.sqr()* v2.sqr()).sqrt(); }

// applied with the current instance (no changes will be made)
inline MATHTYPE cos (Vector3& v) { return (this->M[0] * v.M[0]  + this->M[1] * v.M[1]  + this->M[2] * v.M[2] ) / (MATHTYPE)(this->sqr() * v.sqr() ).sqrt(); }


///////////////////////////////////////////////////
// determines the smallest angle between two vectors
// none of the vectors must be (0.0, 0.0, 0.0)

// returns result using two external vectors
//inline MATHTYPE angle (Vector3& v1, Vector3& v2) { return (MATHTYPE)acos((v1.M[0]  * v2.M[0]  + v1.M[1]  * v2.M[1]  + v1.M[2]  * v2.M[2] ) / (v1.sqr() * v2.sqr()).sqrt()); }

// applied with the current instance (no changes will be made)
//inline MATHTYPE angle (Vector3& v) { return (MATHTYPE)acos((this->M[0] * v.M[0]  + this->M[1] * v.M[1]  + this->M[2] * v.M[2] ) / (this->sqr() * v.sqr() ).sqrt()); }


///////////////////////////////////////////////////
// computes the area of the parallelogram described by the vectors

// returns result using two external vectors
//inline MATHTYPE affineArea (Vector3& v1, Vector3& v2) { return (MATHTYPE)sqrt(Vector3(v1.M[1]  * v2.M[2]  - v1.M[2]  * v2.M[1],  v1.M[2]  * v2.M[0]  - v1.M[0]  * v2.M[2],  v1.M[0]  * v2.M[1]  - v1.M[1]  * v2.M[0] ).sqr()); }

// applied with the current instance (no changes will be made)
//inline MATHTYPE affineArea (Vector3& v) { return (MATHTYPE)sqrt(Vector3(this->M[1] * v.M[2]  - this->M[2] * v.M[1],  this->M[2] * v.M[0]  - this->M[0] * v.M[2],  this->M[0] * v.M[1]  - this->M[1] * v.M[0] ).sqr()); }


///////////////////////////////////////////////////
// computes the area of the triangle described by the vectors

// returns result using two external vectors
//inline MATHTYPE triangleArea (Vector3& v1, Vector3& v2) { return (MATHTYPE)sqrt(Vector3(v1.M[1]  * v2.M[2]  - v1.M[2]  * v2.M[1],  v1.M[2]  * v2.M[0]  - v1.M[0]  * v2.M[2],  v1.M[0]  * v2.M[1]  - v1.M[1]  * v2.M[0] ).sqr()) / 2; }

// applied with the current instance (no changes will be made)
//inline MATHTYPE triangleArea (Vector3& v) { return (MATHTYPE)sqrt(Vector3(this->M[1] * v.M[2]  - this->M[2] * v.M[1],  this->M[2] * v.M[0]  - this->M[0] * v.M[2],  this->M[0] * v.M[1]  - this->M[1] * v.M[0] ).sqr()) / 2; }



///////////////////////////////////////////////////
// returns true if both vectors are equal (or unequal in case of operator '!=')

// returns result using two external vectors
inline bool equals      (Vector3& v1, Vector3& v2) { if (v1.M[0]    != v2.M[0] ) return false; if (v1.M[1]    != v2.M[1] ) return false; if (v1.M[2]    != v2.M[2] ) return false; return true;  }
inline bool operator == (Vector3& v)  { if (this->M[0] != v.M[0]  ) return false; if (this->M[1] != v.M[1]  ) return false; if (this->M[2] != v.M[2]  ) return false; return true;  }
inline bool operator != (Vector3& v)  { if (this->M[0] != v.M[0]  ) return true;  if (this->M[1] != v.M[1]  ) return true;  if (this->M[2] != v.M[2]  ) return true;  return false; }

// applied with the current instance (no changes will be made)
inline bool equals (Vector3& v) { if (this->M[0] != v.M[0] ) return false; if (this->M[1] != v.M[1] ) return false; if (this->M[2] != v.M[2] ) return false; return true; }


///////////////////////////////////////////////////
// methods using one vector
// if the vector is omitted, 'this' is used instead
///////////////////////////////////////////////////





///////////////////////////////////////////////////
// multiplies (scales) a vector with the reziprocal value of a number (1/s)
// s mustn't be 0

// returns result in a new instance
inline Vector3 antiscale  (Vector3& v, MATHTYPE s) { return Vector3(v.M[0]     / s, v.M[1]     / s, v.M[2]     / s); }
inline Vector3 operator / (MATHTYPE s)          { return Vector3(this->M[0] / s, this->M[1] / s, this->M[2] / s); }

// applied on the current instance
inline Vector3* antiscale (MATHTYPE s) { this->M[0] /= s; this->M[1] /= s; this->M[2] /= s; return this; }
inline Vector3* operator /= (MATHTYPE s)         { this->M[0] /= s; this->M[1] /= s; this->M[2] /= s; return this; }


///////////////////////////////////////////////////
// computes the length of a vector
inline MATHTYPE length (Vector3& v) { return (MATHTYPE)(v.M[0] % v.M[0]  +  v.M[1] % v.M[1]  +  v.M[2] % v.M[2] ).sqrt(); }
inline MATHTYPE lengthCorrect ()        
{
	float l,x,y,z; 
	x = (float)this->M[0].val;
	y = (float)this->M[1].val;
	z = (float)this->M[2].val;
	l = x*x + y*y + z*z;
	return Fixed8::fromDouble(sqrt(l));

//	return (MATHTYPE)(this->M[0] % this->M[0]  +  this->M[1] % this->M[1]  +  this->M[2] % this->M[2]).sqrt(); 
}
inline MATHTYPE length ()        
{
	return (MATHTYPE)(this->M[0] % this->M[0]  +  this->M[1] % this->M[1]  +  this->M[2] % this->M[2]).sqrt(); 
}
inline Fixed16 length16bit ()
{
	Fixed16 x,y,z;
	x.val = this->M[0].val<<8;
	y.val = this->M[1].val<<8;
	z.val = this->M[2].val<<8;
	x = x*x;
	y = y*y;
	z = z*z;
	x +=y;
	x +=z;
	return x.sqrt();
}
inline Fixed16 length16bitSquared ()
{
	Fixed16 x,y,z;
	x.val = this->M[0].val<<8;
	y.val = this->M[1].val<<8;
	z.val = this->M[2].val<<8;
	x = x%x;
	y = y%y;
	z = z%z;
	x +=y;
	x +=z;
	return x;
}


///////////////////////////////////////////////////
// computes the square of a vector
inline MATHTYPE sqr (Vector3& v) { return v.M[0]     % v.M[0]     + v.M[1]     % v.M[1]     + v.M[2]     % v.M[2];     }
inline MATHTYPE sqr ()        { return this->M[0] % this->M[0] + this->M[1] % this->M[1] + this->M[2] % this->M[2]; }


///////////////////////////////////////////////////
// scales a vector to ensure, it has a length of 1.0
// vector mustn't be (0.0, 0.0, 0.0)

// returns result in a new instance
inline Vector3 unit (Vector3& v) { return antiscale(v, length(v)); }

// applied on the current instance
inline Vector3* unit () { return this->antiscale(this->length()); }


///////////////////////////////////////////////////
// changes the length of a vector to 1/l (circle inversion)
// vector mustn't be (0.0, 0.0, 0.0)

// returns result in a new instance
inline Vector3 reziprocal (Vector3& v) { return antiscale(v, sqr(v)); }

// applied on the current instance
inline Vector3* reziprocal () { return this->antiscale(this->sqr()); }



///////////////////////////////////////////////////
// inverts the orientation of a vector

// returns result in a new instance
inline Vector3 neg (Vector3& v) { return Vector3(-v.M[0],  -v.M[1],  -v.M[2] ); }

// applied on the current instance
inline Vector3* neg () { this->M[0] = -this->M[0]; this->M[1] = -this->M[1]; this->M[2] = -this->M[2]; return this; }


///////////////////////////////////////////////////
// returns a copy of a vector as new instance
inline Vector3 copy (Vector3& v) { return Vector3(v.M[0],     v.M[1],     v.M[2]    ); }
inline Vector3 copy ()        { return Vector3(this->M[0], this->M[1], this->M[2]); }


///////////////////////////////////////////////////
// sets the current instance to (0.0, 0.0, 0.0)
inline Vector3* clear () { this->M[0] = 0; this->M[1] = 0; this->M[2] = 0; return this; }


// Clamp a vector (used for colors) (minimizes and maximizes the components of the vector)
inline const Vector3& Clamp(MATHTYPE min, MATHTYPE max)
{
	if(M[0]<min)M[0]=min;
	if(M[1]<min)M[1]=min;
	if(M[2]<min)M[2]=min;
	if(M[0]>max)M[0]=max;
	if(M[1]>max)M[1]=max;
	if(M[2]>max)M[2]=max;

	return *this;
}

};


/**
  * \param a       First value
  * \param b       Second value
  * \return        Min of a and b
  */
template <class T> inline const T                &MIN( const T &a, const T &b ) { return( ( a < b ) ? a : b ); }

/**
  * \param a       First value
  * \param b       Second value
  * \return        Max of a and b
  */
template <class T> inline const T                &MAX( const T &a, const T &b ) { return( ( a > b ) ? a : b ); }

/**
  * \param a       First value
  * \param b       Second value
  * \param c       Third value
  * \return        Min of a, b and c
  */
template <class T> inline const T                &MIN3( const T &a, const T &b, const T &c ) { return( ( a < b ) ? ( ( a < c ) ? a : c ) : ( ( b < c ) ? b : c ) ); }

/**
  * \param a       First value
  * \param b       Second value
  * \param c       Third value
  * \return        Max of a, b and c
  */
template <class T> inline const T                &MAX3( const T &a, const T &b, const T &c ) { return( ( a > b ) ? ( ( a > c ) ? a : c ) : ( ( b > c ) ? b : c ) ); }

/**
  * \param a       First value
  * \param b       Second value
  * \param min     Receives min value
  * \param max     Receives max value
  */
template <class T> inline void                    MINMAX( const T &a, const T &b, T &min, T &max ) { if( a < b ) { min = a; max = b; } else { min = b; max = a; } }

/**
  * \param a       First value
  * \param b       Second value
  * \param c       Third value
  * \param min     Receives min value
  * \param max     Receives max value
  */
template <class T> inline void                    MINMAX3( const T &a, const T &b, const T &c, T &min, T &max ) { min = MIN3( a, b, c ); max = MAX3( a, b, c ); }

/**
  * Clamp value
  * \param val     Value
  * \param min     Min limit
  * \param max     Max limit
  * \return        Min if (val<min), max if (val>max), otherwise val
  */
template <class T> inline const T                 CLAMP( const T &val, const T &min, const T &max ) { if( val <= min ) return min; else if( val >= max ) return max; return val; }








#endif
