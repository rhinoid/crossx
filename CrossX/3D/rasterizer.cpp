#include "engine.h"

#define RAST_FASTBILERP

////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////24 bit versions!
#define RAST_24BIT

//versions with zbuffer
// Texturemapped Yes, Shading=Gouraud, Zbuffer=ON, Filtering=Bilineair
#define RAST_TM
#undef RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#define RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=Gouraud, Zbuffer=ON, Filtering=Bilineair
#define RAST_TM
#define RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#define RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=flat, Zbuffer=ON, Filtering=Bilineair
#define RAST_TM
#undef RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=flat, Zbuffer=ON, Filtering=Bilineair
#define RAST_TM
#define RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=none, Zbuffer=ON, Filtering=Bilineair
#define RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=none, Zbuffer=ON, Filtering=Bilineair
#define RAST_TM
#define RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

//-
// Texturemapped Yes, Shading=Gouraud, Zbuffer=ON, Filtering=None
#define RAST_TM
#undef RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=Gouraud, Zbuffer=ON, Filtering=None
#define RAST_TM
#define RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=flat, Zbuffer=ON, Filtering=None
#define RAST_TM
#undef RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=flat, Zbuffer=ON, Filtering=None
#define RAST_TM
#define RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=none, Zbuffer=ON, Filtering=None
#define RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=none, Zbuffer=ON, Filtering=None
#define RAST_TM
#define RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

//-
// Texturemapped No, Shading=Gouraud, Zbuffer=ON, Filtering=None
#undef  RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#define RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped No, Shading=flat, Zbuffer=ON, Filtering=None
#undef  RAST_TM
#undef RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped No, Shading=none, Zbuffer=ON, Filtering=None
#undef  RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

//versions without zbuffer
// Texturemapped Yes, Shading=Gouraud, Zbuffer=OFF, Filtering=Bilineair
#define RAST_TM
#undef RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#define RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=Gouraud, Zbuffer=OFF, Filtering=Bilineair
#define RAST_TM
#define RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#define RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=flat, Zbuffer=OFF, Filtering=Bilineair
#define RAST_TM
#undef RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(Persp) Yes, Shading=flat, Zbuffer=OFF, Filtering=Bilineair
#define RAST_TM
#define RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=none, Zbuffer=OFF, Filtering=Bilineair
#define RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=none, Zbuffer=OFF, Filtering=Bilineair
#define RAST_TM
#define RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

//-
// Texturemapped Yes, Shading=Gouraud, Zbuffer=OFF, Filtering=None
#define RAST_TM
#undef RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=Gouraud, Zbuffer=OFF, Filtering=None
#define RAST_TM
#define RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=flat, Zbuffer=OFF, Filtering=None
#define RAST_TM
#undef RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=flat, Zbuffer=OFF, Filtering=None
#define RAST_TM
#define RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=none, Zbuffer=OFF, Filtering=None
#define RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=none, Zbuffer=OFF, Filtering=None
#define RAST_TM
#define RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

//-
// Texturemapped No, Shading=Gouraud, Zbuffer=OFF, Filtering=None
#undef  RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#define RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
#include "rasterizer.base"

// Texturemapped No, Shading=flat, Zbuffer=OFF, Filtering=None
#undef  RAST_TM
#undef RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped No, Shading=none, Zbuffer=OFF, Filtering=None
#undef  RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#undef  RAST_ZBUF
//#include "rasterizer.base"



////////////////////////////////////////////////////////16 bit versions!(565)
#undef RAST_24BIT

//versions with zbuffer
// Texturemapped Yes, Shading=Gouraud, Zbuffer=ON, Filtering=Bilineair
#define RAST_TM
#undef RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#define RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=Gouraud, Zbuffer=ON, Filtering=Bilineair
#define RAST_TM
#define RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#define RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=flat, Zbuffer=ON, Filtering=Bilineair
#define RAST_TM
#undef RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=flat, Zbuffer=ON, Filtering=Bilineair
#define RAST_TM
#define RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=none, Zbuffer=ON, Filtering=Bilineair
#define RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(ersp) Yes, Shading=none, Zbuffer=ON, Filtering=Bilineair
#define RAST_TM
#define RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

//-
// Texturemapped Yes, Shading=Gouraud, Zbuffer=ON, Filtering=None
#define RAST_TM
#undef RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=Gouraud, Zbuffer=ON, Filtering=None
#define RAST_TM
#define RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=flat, Zbuffer=ON, Filtering=None
#define RAST_TM
#undef RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=flat, Zbuffer=ON, Filtering=None
#define RAST_TM
#define RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=none, Zbuffer=ON, Filtering=None
#define RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=none, Zbuffer=ON, Filtering=None
#define RAST_TM
#define RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

//-
// Texturemapped No, Shading=Gouraud, Zbuffer=ON, Filtering=None
#undef  RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#define RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped No, Shading=flat, Zbuffer=ON, Filtering=None
#undef  RAST_TM
#undef RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped No, Shading=none, Zbuffer=ON, Filtering=None
#undef  RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#define RAST_ZBUF
//#include "rasterizer.base"

//versions without zbuffer
// Texturemapped Yes, Shading=Gouraud, Zbuffer=OFF, Filtering=Bilineair
#define RAST_TM
#undef RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#define RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=Gouraud, Zbuffer=OFF, Filtering=Bilineair
#define RAST_TM
#define RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#define RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=flat, Zbuffer=OFF, Filtering=Bilineair
#define RAST_TM
#undef RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=flat, Zbuffer=OFF, Filtering=Bilineair
#define RAST_TM
#define RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=none, Zbuffer=OFF, Filtering=Bilineair
#define RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#undef RAST_ZBUF
#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=none, Zbuffer=OFF, Filtering=Bilineair
#define RAST_TM
#define RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#define RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

//-
// Texturemapped Yes, Shading=Gouraud, Zbuffer=OFF, Filtering=None
#define RAST_TM
#undef RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=Gouraud, Zbuffer=OFF, Filtering=None
#define RAST_TM
#define RAST_PERSP
#undef RAST_FLAT
#define RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=flat, Zbuffer=OFF, Filtering=None
#define RAST_TM
#undef RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=flat, Zbuffer=OFF, Filtering=None
#define RAST_TM
#define RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped Yes, Shading=none, Zbuffer=OFF, Filtering=None
#define RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped(persp) Yes, Shading=none, Zbuffer=OFF, Filtering=None
#define RAST_TM
#define RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

//-
// Texturemapped No, Shading=Gouraud, Zbuffer=OFF, Filtering=None
#undef  RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#define RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped No, Shading=flat, Zbuffer=OFF, Filtering=None
#undef  RAST_TM
#undef RAST_PERSP
#define RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"

// Texturemapped No, Shading=none, Zbuffer=OFF, Filtering=None
#undef  RAST_TM
#undef RAST_PERSP
#undef  RAST_FLAT
#undef  RAST_GOURAUD
#undef  RAST_BILERP
#undef RAST_ZBUF
//#include "rasterizer.base"


void Rasterize(RenderContext *rc)
{
	if(rc->m_RenderFlags&RENDERFLAG_24BIT) // should we render in 24 bit?
	{
		if(rc->m_RenderFlags&RENDERFLAG_BILERP)
		{
			if(rc->m_RenderFlags&RENDERFLAG_ZBUFFER)
			{
				if(rc->m_RenderFlags&RENDERFLAG_TEXTUREMAPPING)
				{
					if(rc->m_RenderFlags&RENDERFLAG_PERSPCORRECT)
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
						//	FillTriangle_TM_PC_FS_ZB_BF(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
					//		FillTriangle_TM_PC_GS_ZB_BF(rc);
						}

						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
						//	FillTriangle_TM_PC_ZB_BF(rc);
						}
					}
					else
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
					//		FillTriangle_TM_FS_ZB_BF(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
					//		FillTriangle_TM_GS_ZB_BF(rc);
						}

						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
					//		FillTriangle_TM_ZB_BF(rc);
						}
					}
				}
				else //texturemapped
				{
					if(rc->m_RenderFlags&RENDERFLAG_FLAT)
					{
					//	FillTriangle_FS_ZB(rc); //bilerp has no effect
					}

					if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
					{
					//	FillTriangle_GS_ZB(rc); //bilerp has no effect
					}
				}

				//FillTriangle_TM_ZB_BF(rc);
			}
			else	//zbuffer
			{
				if(rc->m_RenderFlags&RENDERFLAG_TEXTUREMAPPING)
				{
					if(rc->m_RenderFlags&RENDERFLAG_PERSPCORRECT)
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
						//	FillTriangle_TM_PC_FS_BF(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
						//	FillTriangle_TM_PC_GS_BF(rc);
						}
						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
						//	FillTriangle_TM_PC_BF(rc);
						}
					}
					else
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
						//	FillTriangle_TM_FS_BF(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
						//	FillTriangle_TM_GS_BF(rc);
						}
						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
						//	FillTriangle_TM_BF(rc);
						}
					}
				}
				else //texture mapped
				{
					if(rc->m_RenderFlags&RENDERFLAG_FLAT)
					{
					//	FillTriangle_FS(rc); //bilinear filter has no effect
					}

					if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
					{
					//	FillTriangle_GS(rc);
					}
				}
			}	//zbuffer
		}
		else  //bilerp
		{
			if(rc->m_RenderFlags&RENDERFLAG_ZBUFFER)
			{
				if(rc->m_RenderFlags&RENDERFLAG_TEXTUREMAPPING)
				{
					if(rc->m_RenderFlags&RENDERFLAG_PERSPCORRECT)
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
						//	FillTriangle_TM_PC_FS_ZB(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
						//	FillTriangle_TM_PC_GS_ZB(rc);
						}
						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
						//	FillTriangle_TM_PC_ZB(rc);
						}
					}
					else
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
							FillTriangle_TM_FS_ZB(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
							FillTriangle_TM_GS_ZB(rc);
						}
						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
						//	FillTriangle_TM_ZB(rc);
						}
					}
				}
				else //texturemapped
				{
					if(rc->m_RenderFlags&RENDERFLAG_FLAT)
					{
					//	FillTriangle_FS_ZB(rc);
					}

					if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
					{
					//	FillTriangle_GS_ZB(rc);
					}
				}
			}
			else //zbuffer
			{
				if(rc->m_RenderFlags&RENDERFLAG_TEXTUREMAPPING)
				{
					if(rc->m_RenderFlags&RENDERFLAG_PERSPCORRECT)
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
					//		FillTriangle_TM_PC_FS(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
					//		FillTriangle_TM_PC_GS(rc);
						}

						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
					//		FillTriangle_TM_PC(rc);
						}
					}
					else
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
					//		FillTriangle_TM_FS(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
					//		FillTriangle_TM_GS(rc);
						}

						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
					//		FillTriangle_TM(rc);
						}
					}
				}
				else //texture mapped
				{
					if(rc->m_RenderFlags&RENDERFLAG_FLAT)
					{
				//		FillTriangle_FS(rc);
					}

					if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
					{
						FillTriangle_GS(rc);
					}
				}
			}	//zbuffer
		}	//bilerp
	}
	else //16 bit (565)
	{
		if(rc->m_RenderFlags&RENDERFLAG_BILERP)
		{
			if(rc->m_RenderFlags&RENDERFLAG_ZBUFFER)
			{
				if(rc->m_RenderFlags&RENDERFLAG_TEXTUREMAPPING)
				{
					if(rc->m_RenderFlags&RENDERFLAG_PERSPCORRECT)
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
				//			FillTriangle_16B_TM_PC_FS_ZB_BF(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
				//			FillTriangle_16B_TM_PC_GS_ZB_BF(rc);
						}

						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
				//			FillTriangle_16B_TM_PC_ZB_BF(rc);
						}
					}
					else
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
				//			FillTriangle_16B_TM_FS_ZB_BF(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
				//			FillTriangle_16B_TM_GS_ZB_BF(rc);
						}

						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
				//			FillTriangle_16B_TM_ZB_BF(rc);
						}
					}
				}
				else //texturemapped
				{
					if(rc->m_RenderFlags&RENDERFLAG_FLAT)
					{
				//		FillTriangle_16B_FS_ZB(rc); //bilerp has no effect
					}

					if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
					{
				//		FillTriangle_16B_GS_ZB(rc); //bilerp has no effect
					}
				}

//					FillTriangle_TM_ZB_BF(rc);
			}
			else	//zbuffer
			{
				if(rc->m_RenderFlags&RENDERFLAG_TEXTUREMAPPING)
				{
					if(rc->m_RenderFlags&RENDERFLAG_PERSPCORRECT)
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
				//			FillTriangle_16B_TM_PC_FS_BF(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
				//			FillTriangle_16B_TM_PC_GS_BF(rc);
						}

						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
				//			FillTriangle_16B_TM_PC_BF(rc);
						}
					}
					else
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
				//			FillTriangle_16B_TM_FS_BF(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
				//			FillTriangle_16B_TM_GS_BF(rc);
						}

						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
				//			FillTriangle_16B_TM_BF(rc);
						}
					}
				}
				else //texture mapped
				{
					if(rc->m_RenderFlags&RENDERFLAG_FLAT)
					{
				//		FillTriangle_16B_FS(rc); //bilinear filter has no effect
					}

					if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
					{
				//		FillTriangle_16B_GS(rc);
					}
				}
			}	//zbuffer
		}
		else  //bilerp
		{
			if(rc->m_RenderFlags&RENDERFLAG_ZBUFFER)
			{
				if(rc->m_RenderFlags&RENDERFLAG_TEXTUREMAPPING)
				{
					if(rc->m_RenderFlags&RENDERFLAG_PERSPCORRECT)
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
				//			FillTriangle_16B_TM_PC_FS_ZB(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
				//			FillTriangle_16B_TM_PC_GS_ZB(rc);
						}

						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
				//			FillTriangle_16B_TM_PC_ZB(rc);
						}
					}
					else
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
							FillTriangle_16B_TM_FS_ZB(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
							FillTriangle_16B_TM_GS_ZB(rc);
						}

						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
							FillTriangle_16B_TM_ZB(rc);
						}
					}
				}
				else //texturemapped
				{
					if(rc->m_RenderFlags&RENDERFLAG_FLAT)
					{
				//		FillTriangle_16B_FS_ZB(rc);
					}

					if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
					{
				//		FillTriangle_16B_GS_ZB(rc);
					}
				}
			}
			else //zbuffer
			{
				if(rc->m_RenderFlags&RENDERFLAG_TEXTUREMAPPING)
				{
					if(rc->m_RenderFlags&RENDERFLAG_PERSPCORRECT)
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
				//			FillTriangle_16B_TM_PC_FS(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
				//			FillTriangle_16B_TM_PC_GS(rc);
						}

						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
				//			FillTriangle_16B_TM_PC(rc);
						}
					}
					else
					{
						if(rc->m_RenderFlags&RENDERFLAG_FLAT)
						{
				//			FillTriangle_16B_TM_FS(rc);
						}

						if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
						{
				//			FillTriangle_16B_TM_GS(rc);
						}

						if((rc->m_RenderFlags&(RENDERFLAG_GOURAUD|RENDERFLAG_FLAT))==0) //no shading just texture mapping?
						{
				//			FillTriangle_16B_TM(rc);
						}
					}
				}
				else //texture mapped
				{
					if(rc->m_RenderFlags&RENDERFLAG_FLAT)
					{
				//		FillTriangle_16B_FS(rc);
					}

					if(rc->m_RenderFlags&RENDERFLAG_GOURAUD)
					{
				//		FillTriangle_16B_GS(rc);
					}
				}
			}	//zbuffer
		}	//bilerp
	}
}
