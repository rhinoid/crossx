
#ifndef _H_RASTERIZER
#define _H_RASTERIZER

typedef unsigned int uint32;

typedef int FIXRAS;
#define RASFIXSHIFT (8)
#define RASFIXMASK (0xff)
#define RASFIXONE (0x100)


#ifdef __SYMBIAN32__

//Not really necessary...the __int64 way works too... have to find out which one works fastest

#ifdef NULL    //to remove compile warnings in gnu
#undef NULL
#endif

/*
#include <e32std.h>
#include <e32def.h>
inline int RASFPDIV(int src,int divi)
{
	TInt64 src64(src);
	TInt64 divi64(divi);
	src64 <<= RASFIXSHIFT;
	src64 /= divi64;
	return src64.Low();
}

inline int RASFPMUL(int src,int mult)
{
	TInt64 src64(src);
	TInt64 mult64(mult);
	src64 *= mult64;
	src64 >>= RASFIXSHIFT;
	return src64.Low();
}

inline int RASFPMULDIV(int src,int mult,int divi)
{
	TInt64 src64(src);
	TInt64 mult64(mult);
	TInt64 divi64(divi);
	src64 *= mult64;
	src64 /= divi64;
	return src64.Low();
}
*/

#define RASFPDIV(src,divi) ((int)(((signed __int64)(src)<<RASFIXSHIFT)/(divi)))
#define RASFPMUL(src,mult) ((int)(((signed __int64)(src)*(mult))>>RASFIXSHIFT))
#define RASFPMULDIV(src,mult,divi) ((int)(((signed __int64)(src)*(mult))/(divi)))

#else

//#define RASFPDIV(src,divi) ((int)(((signed __int64)(src)<<RASFIXSHIFT)/(divi)))
//#define RASFPMUL(src,mult) ((int)(((signed __int64)(src)*(mult))>>RASFIXSHIFT))
//#define RASFPMULDIV(src,mult,divi) ((int)(((signed __int64)(src)*(mult))/(divi)))


#ifdef ARM
	extern "C" int Mul64_8(int a, int b);
	extern "C" unsigned int FDIV(unsigned int a, unsigned int b);
	extern "C" int FDIVS(int a, int b);
	extern "C" int FDIVS8(int a, int b);
//#define RASFPDIV(src,divi)          ((int)(FDIVS((src),(divi))))
#define RASFPDIV(src,divi) ((int)(((src)<<RASFIXSHIFT)/(divi)))
#define RASFPMUL(src,mult) ((int) Mul64_8((src), (mult)) )

//#define RASFPMULDIV(src,mult,divi) ((int)(((src)*(mult))/(divi)))
inline int RASFPMULDIV(int src, int mult, int divi)
{
	int d;
	d = divi>>2;
	if(d==0)d=1;
	return ((src>>2)*mult)/d;
}

#else
#define RASFPDIV(src,divi) ((int)(((src)<<RASFIXSHIFT)/(divi)))
#define RASFPMUL(src,mult) ((int)(((src)*(mult))>>RASFIXSHIFT))
//#define RASFPMULDIV(src,mult,divi) ((int)(((src)*(mult))/(divi)))
//#define RASFPMULDIV(src,mult,divi) ((int)(((signed __int64)(src)*(mult))/(divi)))

//#define RASFPMULDIV(src,mult,divi) ((int)(((src)*(mult))/(divi)))
inline int RASFPMULDIV(int src, int mult, int divi)
{
	int d;
	d = divi>>2;
	if(d==0)d=1;
	return ((src>>2)*mult)/d;
}
#endif
#endif

#define RENDERFLAG_FLAT			(1)
#define RENDERFLAG_GOURAUD		(2)
#define RENDERFLAG_TEXTUREMAPPING	(4)
#define RENDERFLAG_PERSPCORRECT	(8)
#define RENDERFLAG_ZBUFFER		(16)
#define RENDERFLAG_BILERP		(32)
#define RENDERFLAG_BFCULLING	(64)
#define RENDERFLAG_24BIT	    (128)

//if 24bit is not specified, then renderer will assume 16 bit (565)


#define ADDER (RASFIXONE/2)
//#define FILLCONV (256)
#define FILLCONV (0)


#define RAST_MINX (10)
#define RAST_MINY (10)
#define RAST_MAXX (200)
#define RAST_MAXY (200)


//if fixed point has fractional part then leave, otherwise, take the next integer
#define CEIL(s,d)\
	if((s)&RASFIXMASK)\
	{\
		(d)=((s)>>RASFIXSHIFT)+1;\
	}\
	else\
	{\
		(d)=(s)>>RASFIXSHIFT;\
	}


#define SWAP(x,y) \
	{ \
		int t; \
		t = (y); \
		(y) = (x); \
		(x) = t; \
	}


struct RenderContext
{
	void           *m_DestBuf; //could be 888 as well as 565
	int				m_DestPitch;

	int				m_ClipMinX;
	int				m_ClipMaxX;
	int				m_ClipMinY;
	int				m_ClipMaxY;

	unsigned int   *m_TextBuf;
	int				m_TextPitch;
	int				m_TextShift; //how do we need to shift to go down one line? (2log(width))

	int            *m_ZBuffer;		// Pitch of ZBuf should be same as destbuf

	FIXRAS			x[3];			//Point of triangle to be rendered
	FIXRAS			y[3];
	FIXRAS			z[3];

	int				c[3];     //Colors of triangle points

	FIXRAS			u[3];		//Texture coordinates
	FIXRAS			v[3];

	int				m_RenderFlags;	// Flags on how to render Triangle.
};

// given a certain rendercontext containing all information, rasterize the triangle
void Rasterize(RenderContext *rc);


//separate triangle rasterize functions

//24 bit versions

// normal variants
void FillTriangle(RenderContext *rc);
void FillTriangle_GS(RenderContext *rc);
void FillTriangle_TM(RenderContext *rc);

// zbuffered variants
void FillTriangle_ZB(RenderContext *rc);
void FillTriangle_GS_ZB(RenderContext *rc);
void FillTriangle_TM_ZB(RenderContext *rc);

// normal variants (bilineair filtered)
void FillTriangle_BF(RenderContext *rc);
void FillTriangle_GS_BF(RenderContext *rc);
void FillTriangle_TM_BF(RenderContext *rc);

// zbuffered variants (bilineair filtered)
void FillTriangle_ZB_BF(RenderContext *rc);
void FillTriangle_GS_ZB_BF(RenderContext *rc);
void FillTriangle_TM_ZB_BF(RenderContext *rc);


//16 bit versions

// normal variants
void FillTriangle_16B(RenderContext *rc);
void FillTriangle_16B_GS(RenderContext *rc);
void FillTriangle_16B_TM(RenderContext *rc);

// zbuffered variants
void FillTriangle_16B_ZB(RenderContext *rc);
void FillTriangle_16B_GS_ZB(RenderContext *rc);
void FillTriangle_16B_TM_ZB(RenderContext *rc);

// normal variants (bilineair filtered)
void FillTriangle_16B_BF(RenderContext *rc);
void FillTriangle_16B_GS_BF(RenderContext *rc);
void FillTriangle_16B_TM_BF(RenderContext *rc);

// zbuffered variants (bilineair filtered)
void FillTriangle_16B_ZB_BF(RenderContext *rc);
void FillTriangle_16B_GS_ZB_BF(RenderContext *rc);
void FillTriangle_16B_TM_ZB_BF(RenderContext *rc);



#define rbmask 0xFF00FF
#define agmask 0xFF00FF00

inline uint32 FastBilerp32(uint32 a, uint32 b, uint32 c, uint32 d, uint32 xp, uint32 yp)
{
#define arb (a & rbmask)
#define brb (b & rbmask)
#define crb (c & rbmask)
#define drb (d & rbmask)

#define aag (a & agmask)
#define bag (b & agmask)
#define cag (c & agmask)
#define dag (d & agmask)

	uint32 agd1 = bag - aag;
	uint32 agd2 = dag - cag;
	uint32 rbd1 = brb - arb;
	agd1 >>= 8;
	uint32 rbd2 = drb - crb;
	agd2 >>= 8;

	rbd1 *= xp;  b = arb;
	agd1 *= xp;  rbd1 >>= 8;
	rbd2 *= xp;  d = crb;
	agd2 *= xp;  rbd2 >>= 8;

	b += rbd1;
	a += agd1;

	d += rbd2;
	c += agd2;

	b &= rbmask;
	a &= agmask;
	c &= agmask;
	d &= rbmask;

	// ---- d is now rb for the bottom
	//      c is     ag for the bottom
	//      b is     rb for the top
	//      a is     ag for the top

	c -= a;  // agd
	d -= b;  // rbd
	c >>= 8;

	d *= yp;
	c *= yp;
	d >>= 8;

	a += c;
	b += d;

	a &= agmask;
	b &= rbmask;

	return a | b;

#undef aag
#undef bag
#undef cag
#undef dag

#undef arb
#undef brb
#undef crb
#undef drb
}



inline uint32 Bilerp32(uint32 a, uint32 b, uint32 c, uint32 d, uint32 xp, uint32 yp)
{
	int r1,g1,b1,r2,g2,b2,r3,g3,b3,r4,g4,b4;
	r1 = a&0xff0000>>16;
	g1 = a&0xff00>>8;
	b1 = a&0xff;
	r2 = b&0xff0000>>16;
	g2 = b&0xff00>>8;
	b2 = b&0xff;
	r3 = c&0xff0000>>16;
	g3 = c&0xff00>>8;
	b3 = c&0xff;
	r4 = d&0xff0000>>16;
	g4 = d&0xff00>>8;
	b4 = d&0xff;

	r1 = ((r2*xp) + (r1*(255-xp)))>>8;
	r3 = ((r4*xp) + (r3*(255-xp)))>>8;
	g1 = ((g2*xp) + (g1*(255-xp)))>>8;
	g3 = ((g4*xp) + (g3*(255-xp)))>>8;
	b1 = ((b2*xp) + (b1*(255-xp)))>>8;
	b3 = ((b4*xp) + (b3*(255-xp)))>>8;

	r1 = ((r3*yp) + (r1*(255-yp)))>>8;
	g1 = ((g3*yp) + (g1*(255-yp)))>>8;
	b1 = ((b3*yp) + (b1*(255-yp)))>>8;

	return ((r1&0xff)<<16)+((g1&0xff)<<8)+(b1&0xff);
}


// combine 2 colors by multplicating every component
#if 1
inline uint32 ModulateColor(uint32 a, uint32 b)
{
	uint32 cr,cg,cb;
	cr = ((a&0xff0000)>>16)*((b&0xff0000)>>16);
	cg = ((a&0xff00)>>8)*((b&0xff00)>>8);
	cb = (a&0xff)*(b&0xff);
	cr>>=8;
	cg>>=8;
	cb>>=8;
	return ((cr&0xff)<<16)|((cg&0xff)<<8)|(cb&0xff);
}
#else
// combine 2 colors by multplicating every component
inline uint32 ModulateColor(uint32 a, uint32 b)
{
	a>>=1;
	b>>=1;
	a&=0x7f7f7f;
	b&=0x7f7f7f;
	return (a+b);
}
#endif


#endif

