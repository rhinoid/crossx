

// Symbian implementation
#ifdef __SYMBIAN32__

//#include "CMixerThread.h"
#include <e32svr.h>
#include <stdlib.h>
#include <stdio.h>
#include "AudioHal_symbian.h"


#if 0
#define LOG(x)  { \
	  FILE *log; \
	  log = fopen("d:\\system\\apps\\syntrax\\log.txt","a"); \
	  if(log) \
{ \
		  fwrite((x), strlen(x), 1, log); \
		  fclose(log); \
		  fflush(log); \
} \
}
#else
#define LOG(x)
#endif


AudioHal* AudioHal::NewL(SyntraxRenderFunc *SynFunc, SyntraxThreadCB *ThreadStart, SyntraxThreadCB *ThreadStop, void *Syntrax)
{
	AudioHal* self = new( ELeave )AudioHal();
	CleanupStack::PushL( self );
	self->ConstructL(SynFunc, ThreadStart, ThreadStop, Syntrax);
	CleanupStack::Pop( self );
	return self;
}

void AudioHal::ConstructL(SyntraxRenderFunc *SynFunc, SyntraxThreadCB *ThreadStart, SyntraxThreadCB *ThreadStop, void *Syntrax)
{
	User::LeaveIfError( iMixerThread.Create( _L("Mixer"),
				CMixerThread::ThreadFunction,
                KDefaultStackSize,
                NULL,
                NULL) );

	//
	// Give all possible priority to audio
	//
	iMixerThread.SetPriority( EPriorityRealTime );
	RThread().SetPriority( EPriorityAbsoluteLow );
	
	User::LeaveIfError( iShared.iAliveMutex.CreateLocal( 1 ) );
	User::LeaveIfError( iShared.iMutex.CreateLocal() );
	iShared.SynFunc = SynFunc;
	iShared.ThreadStart = ThreadStart;
	iShared.ThreadStop = ThreadStop;
	iShared.Syntrax = Syntrax;
	iShared.m_Error = 0;
	iMixerThread.SetInitialParameter( &iShared );
	iMixerThread.Resume();

	//give the mixerthread som little time to start running
#ifdef __SYMBIAN32__
	User::After(2000000); //2 seconds delay
#endif

	iPaused = ETrue;  // The mixer starts of in paused mode so we have time to set the initial configuration
}

AudioHal::AudioHal()
{
		iShared.m_Error = 0;
		iPaused = ETrue;
}

AudioHal::~AudioHal()
{
	if( iPaused )
		{
		SendCmd( ECmdStartMixer );
		}
	SendCmd( ECmdDestroyMixer );
	//
	iShared.iAliveMutex.Wait();
	iShared.iAliveMutex.Close();
	iShared.iMutex.Close();
	
}

int AudioHal::QueryError()
{
	int error;
	error = iShared.m_Error;
	iShared.m_Error = 0;
	return error;
}

void AudioHal::Lock()
{
	iShared.iMutex.Wait();
}

void AudioHal::Unlock()
{
	iShared.iMutex.Signal();
}

void AudioHal::SetQuality(int frequency, int nrofbuffers, int channels)
{
	iShared.m_Frequency = frequency;
	iShared.m_NrOfBuffers = nrofbuffers;
	iShared.m_NrOfChannels = channels;
}


void AudioHal::Pause()
{
	// check if mutex is already in wait ( pause mode )
	if( iPaused )
		{
		return;
		}

	iPaused = ETrue;

	SendCmd( ECmdStopMixer );
}

void AudioHal::Resume()
{
	if( !iPaused )
		{
		return;
		}
	iPaused = EFalse;
	iShared.m_Error = 0;

	SendCmd( ECmdStartMixer );
}

void AudioHal::SendCmd( TMixerCmd aCmd )
{
	iShared.iCmd = aCmd;
	iMixerThread.RaiseException( EExcUserInterrupt );
}

///////////////
///mixerthread

TInt CMixerThread::ThreadFunction( TAny* aData )
{
	TAudioShared& shared = *((TAudioShared*)aData);
	
	// tell client we're alive
	// signaled off when destroying this thread
	shared.iAliveMutex.Wait();
	
	CMixerThread* mixerThread = CMixerThread::Create( aData );
	if( mixerThread == NULL )
	{
		return KErrGeneral;
	}
	
	// if we're still here, activescheduler has been constructed
	// start wait loop which runs until it's time to end the thread
	CActiveScheduler::Start();
	delete mixerThread;
	
	// tell owning thread it's safe to exit
	shared.iAliveMutex.Signal();
	
    return KErrNone;
}


CMixerThread* CMixerThread::Create( TAny* aData )
{
	CMixerThread* self = new CMixerThread( aData );
	if( self == NULL ) return self;
	if( self->Construct() != KErrNone )
	{
		delete self;
		return NULL;
	}
	
	return self;
}

TInt CMixerThread::Construct()
{
	// create cleanup stack
	iCleanupStack = CTrapCleanup::New();
	if( iCleanupStack == NULL )
	{
		return KErrNoMemory;
	}
	
	TInt err = KErrNone;
	TRAP( err, ConstructL() );
	
	return err;
}

void CMixerThread::ConstructL()
{
	m_ErrorNotifyFlg = 0;
	// create active scheduler
	iActiveScheduler = new( ELeave )CActiveScheduler;
	CActiveScheduler::Install( iActiveScheduler );
	
	// store pointer of this class to thread local store
	// for use in ExcHandler ( static function )
	Dll::SetTls( this );
	RThread().SetExceptionHandler( ExcHandler, 0xffffffff );
}

CMixerThread::CMixerThread( TAny* aData )
: iShared( *((TAudioShared*)aData) )
{
}

CMixerThread::~CMixerThread()
{
	delete iStream;
	delete iActiveScheduler;
	delete iCleanupStack;
}

void CMixerThread::ExcHandler( TExcType aExc )
{
	// exception handler entry function
	CMixerThread* mixerPointer = (CMixerThread*)Dll::Tls();
	mixerPointer->HandleException( aExc );
}

void CMixerThread::HandleException( TExcType aExc )
{
	// handle exceptions
	// exception type EExcUserInterrupt is reserved for inter-thread communication
	switch( aExc )
	{
	case EExcUserInterrupt:				// Command from client
		{
			switch( iShared.iCmd )
			{
			case ECmdStartMixer:
				{
					m_ErrorNotifyFlg = 1;
					StartMixer();
					break;
				}
			case ECmdStopMixer:
				{
					m_ErrorNotifyFlg = 0; //when mixer is stopped errors wont appear anymore
					StopMixer();
					break;
				}
			case ECmdDestroyMixer:
				{
					CActiveScheduler::Stop();		// Exit
					break;
				}
				
			}
			break;
		}
	default:
		{
			// if unknown exception, just exit this thread
			CActiveScheduler::Stop();				// Exit
			break;
		}
	}
	
}


void CMixerThread::StartMixer()
{
	
	// first allocate buffers needed and set correct samplerate
	
	
	//	iSet.iFlags =  TMdaAudioDataSettings::ENoNetworkRouting;
	iSet.iCaps = 0;
	iSet.iMaxVolume = 0;
	//  iSet.iVolume = 1;
	
	int bufsize;
	bufsize = iShared.m_Frequency/iShared.m_NrOfBuffers;
	
	iShared.m_BufferSize = bufsize; //nr of actual samples in buffer
	
	if(iShared.m_NrOfChannels==2) bufsize*=2; //extra doubling because of stereo
	
	iBuffer = new( ELeave )TInt16[ bufsize ]; //actual buffer can be 2 times as big due to stereo
	
	iBufferPtr.Set( TPtrC8( (TUint8*)iBuffer, bufsize*2 ) ); //doubling because of bytes vs words
	
	iStream = CMdaAudioOutputStream::NewL( *this );
	iStream->Open( &iSet );
}


void CMixerThread::StopMixer()
{
	iStream->Stop();
	delete iStream;
	iStream = NULL;
	
	// free used buffers
	delete iBuffer;
}

void CMixerThread::FillBuffer()
{
	if(iStream)
	{
		//		iShared.ThreadStart(iShared.Syntrax);
		// wait for access to shared data
		if(iShared.iMutex.Count()>=1) // there will be no waiting
		{
			iShared.iMutex.Wait();
			
			iShared.SynFunc(iShared.Syntrax, iBuffer, iShared.m_BufferSize);
			
			iShared.iMutex.Signal();
		}
		else // the mutex would probably lock, so we'll just fill the buffer with nothing, effectively muting sound
		{
			TInt16* buf2 = iBuffer;
			int i;
			for(i=0; i<iShared.m_BufferSize; i++)
			{
				*buf2++ = 0;
			}
		}
		// write 16-bit buffer to CMdaAudioOutputStream
		iStream->WriteL( iBufferPtr );
		//		iShared.ThreadStop(iShared.Syntrax);
	}
}

void CMixerThread::MaoscBufferCopied( TInt aError, const TDesC8& /*aBuffer*/ )
{
	if( aError )
	{
		iError = aError;
		if(m_ErrorNotifyFlg)iShared.m_Error = 1;
	}
	else
	{
		FillBuffer();
	}
}

void CMixerThread::MaoscOpenComplete( TInt aError )
{
	if( aError )
	{
		iError = aError;
		if(m_ErrorNotifyFlg)iShared.m_Error = 1;
	}
	else
	{
		iStream->SetVolume( iStream->MaxVolume() );
		
		//NOW do the settings!
#ifdef SERIES60
		//as i have found out the series 60 at the moment only seems to support 16khz/mono
		iSet.iSampleRate = TMdaAudioDataSettings::ESampleRate16000Hz;
		iSet.iChannels = TMdaAudioDataSettings::EChannelsMono;
#else
		switch(iShared.m_Frequency)
		{
			
		case 11025:
			iSet.iSampleRate = TMdaAudioDataSettings::ESampleRate11025Hz;
			break;
		default:
		case 22050:
			iSet.iSampleRate = TMdaAudioDataSettings::ESampleRate22050Hz;
			break;
		case 44100:
			iSet.iSampleRate = TMdaAudioDataSettings::ESampleRate44100Hz;
			break;
		}
		if(iShared.m_NrOfChannels==1)
		{
			iSet.iChannels = TMdaAudioDataSettings::EChannelsMono;
		}
		else
		{
			iSet.iChannels = TMdaAudioDataSettings::EChannelsStereo;
		}
#endif  //SERIES60
		
		iStream->SetAudioPropertiesL(iSet.iSampleRate, iSet.iChannels);
		//	iStream->SetPriority(EPriorityNormal, EMdaPriorityPreferenceNone);
		
		FillBuffer();
	}
}

void CMixerThread::MaoscPlayComplete( TInt aError )
{
	if( aError )
	{
		iError = aError;
		if(m_ErrorNotifyFlg)iShared.m_Error = 1;
	}
	else
	{
		iStream->SetVolume( iStream->MaxVolume() );
		FillBuffer();
	}
}





#endif //symbian

