#ifdef __SYMBIAN32__


#ifndef _H_AUDIOHAL
#define _H_AUDIOHAL


typedef void(SyntraxRenderFunc)(void *, void *, int);
typedef void(SyntraxThreadCB)(void *);




// INCLUDES
#include <E32Base.h>
#include <e32std.h>
#include <mdaaudiooutputstream.h>
#include <mda\common\audio.h>

// ENUMERATIONS
enum TMixerCmd
{
	ECmdStartMixer = 0,
	ECmdStopMixer,
	ECmdDestroyMixer
};

class TAudioShared
{
public:
	/// for thread end signaling
	RSemaphore	iAliveMutex;

	/// for sample attribute change signaling
	RMutex	iMutex;

	/// pause flag
	TBool	iPaused;

	/// Command parameter
	TMixerCmd	iCmd;

	int				m_Frequency;
	int				m_NrOfBuffers;
	int				m_NrOfChannels;
	int				m_BufferSize;
	int				m_Error;

	void *Syntrax;	//pointer to the syntrax instance
	SyntraxRenderFunc *SynFunc; //function to syntrax which will render the buffer
	SyntraxThreadCB *ThreadStart; //function to syntrax to time when thread starts
	SyntraxThreadCB *ThreadStop; //function to syntrax to time when thread stops

};

// CLASS DECLARATION

class CMixerThread : CBase , MMdaAudioOutputStreamCallback
{
public:

	/// Thread entry point
	static TInt ThreadFunction( TAny* aData );

	/// Default destructor
	~CMixerThread();
	
private:

	/// Default constructor
	/// @param aData data pointer from creator thread
	CMixerThread( TAny* aData );

	/// Two phased constructor
	/// @param aData data pointer from creator thread
	static CMixerThread* Create( TAny* aData );

	/// Second phase constructor
	TInt Construct();

	/// Second phase constructor which can leave
	void ConstructL();

	/// static exception handler function
	/// @param aExc exception code
	static void ExcHandler( TExcType aExc );

	/// exception handler function
	/// @param aExc exception code
	void HandleException( TExcType aExc );

	/// starts mixer
	void StartMixer();

	/// stops mixer
	void StopMixer();

private: // MMdaAudioOutputStreamCallback
	
	void MaoscPlayComplete( TInt aError );
	void MaoscBufferCopied( TInt aError, const TDesC8& aBuffer );
	void MaoscOpenComplete( TInt aError );


private:
	
	/// Fill mixing buffer with new data
	void FillBuffer();


	
private:
	int						m_ErrorNotifyFlg;

	CTrapCleanup*			iCleanupStack;
	CActiveScheduler*		iActiveScheduler;
	CMdaAudioOutputStream*	iStream;
	TMdaAudioDataSettings	iSet;

	TInt16*					iBuffer;	// buffer to CMdaAudioOutput
	TPtrC8					iBufferPtr;	// pointer to iBuffer
	
	TInt					iError;		// contains CMdaAudioOutput errors

	TAudioShared&			iShared;	// reference to shared data with client


};


class AudioHal	: public CBase
{
public:
	/// Two-phased constructor
	static AudioHal* NewL(SyntraxRenderFunc *SynFunc, SyntraxThreadCB *ThreadStart, SyntraxThreadCB *ThreadStop, void *Syntrax);

	/// Default destructor
	~AudioHal();

private:
	/// Default constructor
	AudioHal();

	/// Second phase constructor
	void ConstructL(SyntraxRenderFunc *SynFunc, SyntraxThreadCB *ThreadStart, SyntraxThreadCB *ThreadStop, void *Syntrax);
public:
	int QueryError();	// check if some kind of error with the audiodevice has happened

	void Unlock();	// unlock audiodevice/rendering
	void Lock();	// Temporarily lock audiodevice to make sure there will be nor renderbuffer callback

	void SetQuality(int frequency, int nrofbuffers, int channels);	//set quality for when the playback device is again started

	void Pause();	// free playback device
	void Resume();	// start playback device
private:

	void SendCmd( TMixerCmd aCmd );

private:
	TAudioShared	iShared;		// shared data with mixer thread
	RThread			iMixerThread;	// handle to mixer thread
	TBool			iPaused;        // paused flag
};


#endif //ifdef _H_AUDIOHAL

#endif //symbian
