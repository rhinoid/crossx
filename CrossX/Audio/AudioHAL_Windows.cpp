


// windows implementation
#if defined(WIN32) || defined(_WIN32_WCE)

#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
#include "AudioHal.h"

void CALLBACK AudioHALCB(HWAVEOUT hwo, UINT uMsg, DWORD dwInstance, DWORD dwParam1, DWORD dwParam2)
{
	AudioHal *Class;
	WAVEHDR *wavehdr;

	if(uMsg == MM_WOM_DONE)
	{
		wavehdr = (WAVEHDR *)dwParam1;
		Class = (AudioHal *)dwInstance;
		Class->RenderWave(wavehdr);
	}
}

AudioHal* AudioHal::NewL(SyntraxRenderFunc *RenderCallback, SyntraxThreadCB *PlaybackStart, SyntraxThreadCB *PlaybackStop, void *Syntrax)
{
	AudioHal* self = new AudioHal();
	self->ConstructL(RenderCallback, PlaybackStart, PlaybackStop, Syntrax);
	return self;
}

AudioHal::AudioHal()
{
}

/// Default destructor
AudioHal::~AudioHal()
{
}

int		AudioHal::QueryError()
{
	return m_LastError;
}

/// Second phase constructor
void	AudioHal::ConstructL(SyntraxRenderFunc *RenderCallback, SyntraxThreadCB *PlaybackStart, SyntraxThreadCB *PlaybackStop, void *Syntrax)
{
	m_LastError = 0;
	m_Paused = 1;
	m_RenderCallback = RenderCallback;
	m_PlaybackStart = PlaybackStart;
	m_PlaybackStop = PlaybackStop;
	m_ParentClass = Syntrax;
	m_LockFlg = 0;
}

void	AudioHal::Unlock()
{
	m_LockFlg = 0;
}

void	AudioHal::Lock()
{
	m_LockFlg = 1;
}

void	AudioHal::SetQuality(int frequency, int buffersize, int channels)
{
	m_Frequency = frequency;
	m_BufferSize = buffersize;
	m_Channels = channels;
}

void	AudioHal::Pause()
{
	if(m_Paused) return;
	m_Paused=1;

	//stop the audiodevice
	if(m_AudioDevice)
	{
		int i;
		waveOutReset(m_AudioDevice);
		for(i=0;i<NROFAUDIOBUFFERS;i++)
		{
			waveOutUnprepareHeader(m_AudioDevice, &m_WaveHeaders[i], sizeof(WAVEHDR));
			free(m_WaveHeaders[i].lpData);
		}
		waveOutClose(m_AudioDevice);
		m_AudioDevice = 0;
	}
}









void	AudioHal::Resume()
{
	if(m_Paused==0) return;
	m_Paused=0;

	// Start the audiodevice
	int nrdev;
	int i;
	nrdev = waveOutGetNumDevs();
	if(nrdev)
	{
		int destdev;
		destdev = 0;   // device nr	should actually be made chooseable so the user can use another soundcard

		WAVEOUTCAPS caps;
		waveOutGetDevCaps(destdev, &caps, sizeof(WAVEOUTCAPS));
		if(caps.dwFormats&WAVE_FORMAT_4S16 || caps.dwFormats&WAVE_FORMAT_4M16 || caps.dwFormats&WAVE_FORMAT_2S16 || caps.dwFormats&WAVE_FORMAT_2M16 || caps.dwFormats&WAVE_FORMAT_1S16 || caps.dwFormats&WAVE_FORMAT_1M16)
		{
			WAVEFORMATEX waveformat;
			waveformat.wFormatTag = WAVE_FORMAT_PCM;
			waveformat.nChannels = m_Channels;
			waveformat.nSamplesPerSec = m_Frequency;
			waveformat.nAvgBytesPerSec = m_Frequency*2*(m_Channels);
			waveformat.nBlockAlign = 2*(m_Channels);  // 4
			waveformat.wBitsPerSample = 16;
			waveformat.cbSize = 0;

			int rc;
			rc = waveOutOpen(&m_AudioDevice, destdev, &waveformat, (unsigned long)AudioHALCB, (unsigned long)this, CALLBACK_FUNCTION );
			if(rc !=MMSYSERR_NOERROR)
			{
				m_LastError=1; //could not open audiodevice
				return;
			}

			for(i=0;i<NROFAUDIOBUFFERS;i++)
			{
				m_WaveHeaders[i].lpData = (char *) malloc((m_Frequency*2*(m_Channels))/m_BufferSize);
				m_WaveHeaders[i].dwBufferLength = /*44100*4;*/ (m_Frequency*2*(m_Channels))/m_BufferSize;
				m_WaveHeaders[i].dwBytesRecorded = 0;
				m_WaveHeaders[i].dwUser = 1234+i;
				m_WaveHeaders[i].dwFlags = 0;
				m_WaveHeaders[i].dwLoops = 0;
				m_WaveHeaders[i].lpNext = 0;
				m_WaveHeaders[i].reserved = 0;
				memset(m_WaveHeaders[i].lpData, 0, (m_Frequency*2*(m_Channels))/m_BufferSize);
				if(waveOutPrepareHeader(m_AudioDevice, &m_WaveHeaders[i], sizeof(WAVEHDR)) !=MMSYSERR_NOERROR)
				{
					m_LastError=1; //prepheader error
					return;
				}
				if(waveOutWrite(m_AudioDevice, &m_WaveHeaders[i], sizeof(WAVEHDR))!=MMSYSERR_NOERROR)
				{
					m_LastError=1; //waveoutwrite error
					return;
				}
			}
		}
		else
		{
			m_LastError = 1; //no 16bit support
			return;
		}
	}
	else
	{
		m_LastError = 1; //no audiodevices on system
	}

}

void AudioHal::RenderWave(WAVEHDR *wavehdr)
{
	if(m_Paused) return;
	if(m_LockFlg==0) m_RenderCallback(m_ParentClass, wavehdr->lpData, m_Frequency/m_BufferSize);
	else memset(wavehdr->lpData, 0, m_Frequency/m_BufferSize);
	waveOutWrite(m_AudioDevice, wavehdr, sizeof(WAVEHDR));

	//	Mix((short *)wavehdr->lpData,  FREQUENCY/BUFFERSIZE, FREQUENCY, NUMCHANS);
}

#endif










