#if defined(WIN32) || defined(_WIN32_WCE)

#ifndef _H_AUDIOHAL
#define _H_AUDIOHAL

typedef void(SyntraxRenderFunc)(void *, void *, int);
typedef void(SyntraxThreadCB)(void *);



#include <mmsystem.h>
#include <mmreg.h>

#define NROFAUDIOBUFFERS (2)

class AudioHal
{
public:
	static AudioHal* NewL(SyntraxRenderFunc *RenderCallback, SyntraxThreadCB *PlaybackStart, SyntraxThreadCB *PlaybackStop, void *Syntrax);

	/// Default destructor
	~AudioHal();
	AudioHal();

	int		QueryError();	// check if some kind of error with the audiodevice has happened

	/// Second phase constructor
	void	ConstructL(SyntraxRenderFunc *RenderCallback, SyntraxThreadCB *PlaybackStart, SyntraxThreadCB *PlaybackStop, void *Syntrax);
	void	Unlock();		// unlock audiodevice/rendering
	void	Lock();			// Temporarily lock audiodevice to make sure there will be nor renderbuffer callback

	void	SetQuality(int frequency, int buffersize, int channels);	//set quality for when the playback device is again started

	void	Pause();		// free playback device
	void	Resume();		// start playback device

public:
	void	RenderWave(WAVEHDR *wavehdr);		// don't call directly... is callback function

private:
	SyntraxRenderFunc  *m_RenderCallback;
	SyntraxThreadCB    *m_PlaybackStart;
	SyntraxThreadCB    *m_PlaybackStop;
	void               *m_ParentClass;

	int					m_Paused;
	int					m_LastError;

	int					m_Frequency;
	int					m_BufferSize;
	int					m_Channels;

	int					m_LockFlg;

	HWAVEOUT			m_AudioDevice;
	WAVEHDR				m_WaveHeaders[NROFAUDIOBUFFERS];

};

#endif //ifdef _H_AUDIOHAL

#endif //win32

