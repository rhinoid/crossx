#include "../crossx.h"
#include "utils/globals.h"

CGame::CGame()
{
	Globals::Init();
}

CGame::~CGame()
{
}

void CGame::CRSX_SetBackbuf(CSurface *BackBuffer)
{
	m_BackBuffer = BackBuffer;
	m_Width = BackBuffer->GetWidth();
	m_Height = BackBuffer->GetHeight();
}

CSurface	*CGame::GetBackBuffer()
{
	return m_BackBuffer;
}

int			 CGame::GetWidth()
{
	return m_Width;
}

int			 CGame::GetHeight()
{
	return m_Height;
}

