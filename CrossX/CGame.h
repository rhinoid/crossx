class CGame
{
public:
	CGame();
	virtual ~CGame();

//methods which must be overridden
	virtual bool Init(void)=0;

	virtual bool HandleKey(CROSSX_KEY KeyCode)=0;
	virtual bool HandleKeyUp(CROSSX_KEY KeyCode)=0;
	virtual bool HandleKeyDown(CROSSX_KEY KeyCode)=0;

	virtual bool HandleMouseDown(int x, int y)=0;
	virtual bool HandleMouseMove(int x, int y)=0;
	virtual bool HandleMouseUp(int x, int y)=0;

	virtual bool Tick(int DeltaT)=0;

//methods usable from within the game class
	CSurface	*GetBackBuffer();
	int			 GetWidth();
	int			 GetHeight();

	//internal stuff
	void		 CRSX_SetBackbuf(CSurface *BackBuffer);
private:
	CSurface    *m_BackBuffer;
	int			 m_Width;
	int			 m_Height;
};
