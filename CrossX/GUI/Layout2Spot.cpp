#include "Gui/Layout2Spot.h"
#include "2d/SurfaceCache.h"
#include "Utils/Globals.h"
#include "Utils/Debug.h"


/////////////////////////////////////////
// Layout2Spot Methods
/////////////////////////////////////////


Layout2Spot::Layout2Spot()
{
}

Layout2Spot::~Layout2Spot()
{
}

void Layout2Spot::Convert(CSpotMaster *SpotMaster, LayoutMgr *Layout, char Delimiter)
{
	XString Name = "";
	m_Delimiter = Delimiter;
	m_SpotMaster = SpotMaster;

	if(Layout->m_Container == 0) return;
	SpotMaster->DelAll();
	
	Convert(SpotMaster, Layout->m_Container, Name, 0, 0);
}

void Layout2Spot::Convert(CSpotMaster *SpotMaster, Container *Cont, XString Path, int ParentX, int ParentY)
{
    CSpot *NewSurface;

    if(Cont->m_Name.length()>0)  //does the layout contain a gui name?
    {
        // recreate this layout element as gui element
        NewSurface = ParseGUISurface(Cont->m_Name);
        if(NewSurface == 0) return;  //some kind of parse error


		XString Name;
		Name =  NewSurface->GetID();
		if(Path == "")
		{
			Path = Name;     // also needed in the following recurse
		}
		else
		{
			Path = Path + "\\" + Name;     // also needed in the following recurse
		}
		NewSurface->SetID(Path);  // append the correct path
        SpotMaster->AddSpot(NewSurface);

		int RelX, RelY;
		int w,h;
		RelX = Cont->m_X-ParentX;
		RelY = Cont->m_Y-ParentY;
		NewSurface->SetPos(RelX, RelY); // the gui pos is relative to its parents
		w = Cont->m_X2 - Cont->m_X;
		h = Cont->m_Y2 - Cont->m_Y;
		NewSurface->SetParam(PARAM_SPOT_DIMENSIONS, (void *)(w+(h<<16)));

        ParentX += RelX;
        ParentY += RelY;
    }

    //recurse
    if(Cont->GetNrOfContainers()>0)
    {
        int i;
        for(i=0; i<Cont->GetNrOfContainers(); i++)
        {
            Convert(SpotMaster, Cont->GetContainerNr(i), Path, ParentX, ParentY);
        }
    }
}



CSpot *Layout2Spot::ParseGUISurface(XString ParseString)
{
    CSpot *NewSurface;
    XString Name;
    XString GuiName;
    XString FlagsString;

    m_CurParseString = ParseString;
    m_CurParseIdx = 0;
    NewSurface = 0;

    Name = ReturnToken();
    GuiName = ReturnToken();
//    FlagsString = ReturnToken();
//    Flags = Integer.parseInt(FlagsString);

    if(GuiName.CompareNoCase("color")==0)
    {
        NewSurface = ScanColor();
    }
    if(GuiName.CompareNoCase("tile")==0)
    {
        NewSurface = ScanTile();
    }
    if(GuiName.CompareNoCase("text")==0)
    {
        NewSurface = ScanText();
    }
    if(GuiName.CompareNoCase("3d")==0)
    {
        NewSurface = Scan3D();
    }
    if(GuiName.CompareNoCase("gradient")==0)
    {
        NewSurface = ScanGradient();
    }
    if(GuiName.CompareNoCase("graphic")==0)
    {
        NewSurface = ScanGraphic();
    }
    if(GuiName.CompareNoCase("switch")==0)
    {
        NewSurface = ScanSwitch();
    }
    if(NewSurface == 0) return 0;

    NewSurface->SetID(Name);

    return NewSurface;

}

CSpotColor  *Layout2Spot::ScanColor()
{
    CSpotColor *NewColor;

    int NormalColor, HighlightColor;
    XString NormalText, HighlightText;

    NormalText = ReturnToken();
    HighlightText = ReturnToken();

    NormalColor = ConvertHex(NormalText);
    HighlightColor = ConvertHex(HighlightText);

    NewColor = new CSpotColor(m_SpotMaster, "", 0, 0, 0, 0, NormalColor, HighlightColor);

    return NewColor;
}

CSpotGradient  *Layout2Spot::ScanGradient()
{
    CSpotGradient *NewGradient;

    int NormalTL, NormalTR, NormalBL, NormalBR;
    int SelectTL, SelectTR, SelectBL, SelectBR;
    XString Buffer;

    Buffer = ReturnToken();
    NormalTL = ConvertHex(Buffer);
    Buffer = ReturnToken();
    NormalTR = ConvertHex(Buffer);
    Buffer = ReturnToken();
    NormalBL = ConvertHex(Buffer);
    Buffer = ReturnToken();
    NormalBR = ConvertHex(Buffer);
    Buffer = ReturnToken();

    SelectTL = ConvertHex(Buffer);
    Buffer = ReturnToken();
    SelectTR = ConvertHex(Buffer);
    Buffer = ReturnToken();
    SelectBL = ConvertHex(Buffer);
    Buffer = ReturnToken();
    SelectBR = ConvertHex(Buffer);

    NewGradient = new CSpotGradient(m_SpotMaster, "", 0, 0, 0, 0, NormalTL, NormalTR, NormalBL, NormalBR, SelectTL, SelectTR, SelectBL, SelectBR);

    return NewGradient;
}

CSpotTile *Layout2Spot::ScanTile()
{
    CSpotTile *NewFill;
    XString NormalName, SelectName, Borders;
    int BorderMask;

    NormalName = ReturnToken();
    SelectName = ReturnToken();
    Borders = ReturnToken();

    BorderMask = 0;
    if(Borders.length()<=4)
    {
        if(Borders.find('l')>=0 || Borders.find('L')>=0) BorderMask |= TILEBORDER_LEFT;
        if(Borders.find('r')>=0 || Borders.find('R')>=0) BorderMask |= TILEBORDER_RIGHT;
        if(Borders.find('t')>=0 || Borders.find('T')>=0) BorderMask |= TILEBORDER_TOP;
        if(Borders.find('b')>=0 || Borders.find('B')>=0) BorderMask |= TILEBORDER_BOTTOM;
    }

    NewFill = new CSpotTile(m_SpotMaster, "", 0, 0, 0, 0, BorderMask, NormalName, SelectName);
    return NewFill;
}

CSpotGraphic *Layout2Spot::ScanGraphic()
{
    CSpotGraphic *NewGraphic;
    XString GraphicFile;

    GraphicFile = ReturnToken();

    NewGraphic = new CSpotGraphic(m_SpotMaster, "", 0, 0, 0, 0, GraphicFile);
    return NewGraphic;
}

CSpotSwitch *Layout2Spot::ScanSwitch()
{
    CSpotSwitch *NewSwitch;
    XString SwitchNormal1, SwitchSelect1;
    XString SwitchNormal2, SwitchSelect2;

    SwitchNormal1 = ReturnToken();
    SwitchSelect1 = ReturnToken();
    SwitchNormal2 = ReturnToken();
    SwitchSelect2 = ReturnToken();

    NewSwitch = new CSpotSwitch(m_SpotMaster, "", 0, 0, 0, 0, SwitchNormal1, SwitchSelect1, SwitchNormal2, SwitchSelect2 );
    return NewSwitch;
}

CSpotText *Layout2Spot::ScanText()
{
    CSpotText *NewText;
    XString NormalText, HighlightText, Text, Font;
    int NormalColor, HighlightColor;
    int HorAlignment,VerAlignment;

    NormalText = ReturnToken();
    HighlightText = ReturnToken();
    Font = ReturnToken();
    Text = ReturnToken();

    NormalColor = ConvertHex(NormalText);
    HighlightColor = ConvertHex(HighlightText);

    VerAlignment = SPOT_TEXT_ALIGNMIN;
    if(Text.length() > 0)
    {
        bool RemoveFirstCharFlag=false;
        if(Text.at(0) == '^')
        {
            VerAlignment = SPOT_TEXT_ALIGNMIN;
            RemoveFirstCharFlag=true;
        }
        else if(Text.at(0) == '-')
        {
            VerAlignment = SPOT_TEXT_ALIGNCENTER;
            RemoveFirstCharFlag=true;
        }
        else if(Text.at(0) == 'v')
        {
            VerAlignment = SPOT_TEXT_ALIGNMAX;
            RemoveFirstCharFlag=true;
        }
        if( RemoveFirstCharFlag )
        {
            Text.SubString(1);
        }
    }
    HorAlignment = SPOT_TEXT_ALIGNMIN;
    if(Text.length() > 0)
    {
        bool RemoveFirstCharFlag=false;
        if(Text.at(0) == '<')
        {
            HorAlignment = SPOT_TEXT_ALIGNMIN;
            RemoveFirstCharFlag=true;
        }
        else if(Text.at(0) == '|')
        {
            HorAlignment = SPOT_TEXT_ALIGNCENTER;
            RemoveFirstCharFlag=true;
        }
        else if(Text.at(0) == '>')
        {
            HorAlignment = SPOT_TEXT_ALIGNMAX;
            RemoveFirstCharFlag=true;
        }
        if( RemoveFirstCharFlag )
        {
            Text.SubString(1);
        }
    }

    NewText = new CSpotText(m_SpotMaster, "", 0, 0, 0, 0, NormalColor, HighlightColor, Font, Text);
	NewText->SetParam(PARAM_SPOT_TEXTHALIGNMENT, (void *)HorAlignment);
	NewText->SetParam(PARAM_SPOT_TEXTVALIGNMENT, (void *)VerAlignment);

    return NewText;
}

CSpot3D  *Layout2Spot::Scan3D()
{
    CSpot3D *New3D;

    XString ModelName;
    ModelName = ReturnToken();

    New3D = new CSpot3D(m_SpotMaster, "", 0, 0, 0, 0, ModelName);

    return New3D;
}

XString Layout2Spot::ReturnToken()
{
    XString TokenBuf = "";
    char Byte;
    if(m_CurParseIdx >= (uint32)m_CurParseString.length()) return "";
    Byte = m_CurParseString.at(m_CurParseIdx++);
    while(Byte != m_Delimiter)
    {
        TokenBuf += Byte;
        if(m_CurParseIdx >= (uint32)m_CurParseString.length()) return TokenBuf;

        Byte = m_CurParseString.at(m_CurParseIdx++);
    }
    if(Byte=='<' || Byte=='>')m_CurParseIdx--; //in case of these step back one
    return TokenBuf;
}

int Layout2Spot::ConvertHex(XString Text)
{
    int HexNumber;
	uint32 Idx;

    HexNumber=0;
    for(Idx=0; Idx<(uint32)Text.length(); Idx++)
    {
        char Digit;
        HexNumber <<=4;
        Digit = Text.at(Idx);
        if(Digit>='0' && Digit<='9')
        {
            HexNumber += Digit-'0';
        }
        else
        {
            HexNumber += Digit-'a'+10;
        }
    }
    return HexNumber;
}
