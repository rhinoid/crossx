#ifndef _H_LAYOUT2SPOT
#define _H_LAYOUT2SPOT

#include "gui/spot.h"
#include "gui/spotcolor.h"
#include "gui/spotgradient.h"
#include "gui/spottile.h"
#include "gui/spottext.h"
#include "gui/spot3d.h"
#include "gui/spotgraphic.h"
#include "gui/spotswitch.h"
#include "layout/layoutmgr.h"
#include "utils/types.h"

///////////////////////////////////////////////////////////
class Layout2Spot
{
public:
	Layout2Spot();
	~Layout2Spot();

	void			Convert(CSpotMaster *SpotMaster, LayoutMgr *Layout, char Delimiter);

private:
	void			Convert(CSpotMaster *SpotMaster, Container *Cont, XString Path, int ParentX, int ParentY);
	CSpot          *ParseGUISurface(XString ParseString);

	CSpotColor     *ScanColor();
	CSpotGradient  *ScanGradient();
	CSpotTile      *ScanTile();
	CSpotText      *ScanText();
	CSpot3D        *Scan3D();
	CSpotGraphic   *ScanGraphic();
	CSpotSwitch    *ScanSwitch();

	XString			ReturnToken();
	int				ConvertHex(XString Text);

    XString			m_CurParseString;	// used for the parsing routines so they don't have to pass extra variables around (and more importantly back)
    uint32			m_CurParseIdx;

	char			m_Delimiter;		// delimiter used
	CSpotMaster    *m_SpotMaster;
};

#endif
