#ifndef _CSPOT_H
#define _CSPOT_H

#include "2d/surface.h"
#include "gui/spotmaster.h"
#include "utils/xstring.h"

///////////////////////////////////////////////////////////
// forward declarations
class CSpotMaster;


///////////////////////////////////////////////////////////


// The spot classes

class CSpot
{
public:
	CSpot(CSpotMaster *Parent, XString Id, int x, int y, int w, int h);
	virtual ~CSpot();

	// tree functions
	void	AddSpot(CSpot *NewSpot);			// Add a child control
	void	DelSpot(XString SpotName);			// remove a first level child control. (so not a path). Path delete is done through the spotmaster

	void    DelAll();							// Removes all child controls
	// traversal functions
	CSpot  *GetChild();							//returns first child control (if any)
	void	SetNextNode(CSpot *NextNode);
	CSpot  *GetNextNode(void);
	void	SetParentNode(CSpot *ParentNode);
	CSpot  *GetParentNode(void);

	// search functions
	CSpot  *SearchControl(XString Path);        // return the child control which matches this path
	CSpot  *GetHit(int x,int y);				// return the top child control which contains this relative point

	// virtuals
	virtual void MouseMsg(MOUSEMSG Msg, int Param1, int Param2) = 0;
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);    // should be called AFTER the implementation of the class which inherits from CSPot

	virtual void *GetParam(SPOTPARAMS Param);	// get value of param indicated by button/dial/slider
	virtual void  SetParam(SPOTPARAMS Param, void *Value);	// set param for button/dial/slider

	virtual	bool MessageOveride(XString ButtonPath, XString ButtonName, MOUSEMSG Msg, int Param1, int Param2);	// only called if the message overide has been set to this control (return true if message should be passed on, if false then it is blocked)

	//basic properties
	int		GetX();
	int		GetY();
	int		GetX2();
	int		GetY2();
	int		GetW();
	int		GetH();
	XString	GetID();							// Get Current name of this control
	void	SetID(XString Name);				// Change Current name of this control
	XString GetPath();							// Get the full path of this control
	void	SetPos(int x, int y);
	CSpotMaster    *GetSpotMaster() { return m_SpotMaster; }

protected:
	CSpotMaster    *m_SpotMaster;
	CSpot          *m_Parent;
	XString			m_ID;
	int				m_Enabled;
	int				m_X;
	int				m_Y;
	int				m_W;
	int				m_H;
	int				m_Transparency;				// Transparency
	CSpot          *m_NextBrother;				// next 'brother' button (linked list of brothers)
	CSpot          *m_Child;					// first child button (linked list with child and his brothers)
};

#endif
