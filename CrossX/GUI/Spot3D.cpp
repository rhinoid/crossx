#include "gui/spotmaster.h"
#include "gui/spot3d.h"

/////////////////////////////////////////
// CSpot3D Methods
/////////////////////////////////////////
#ifdef USE3DENGINE
CSpot3D::CSpot3D(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, XString ModelName) : CSpot(Parent, Id, x, y, w, h)
{
	m_Yaw = 0;
	m_Pitch = 0;
	m_Roll = 0;
	m_Zoom = 0;
	m_Distance = 180;
	m_Model = new ObjectR3d();
	m_Model->Load(ModelName, 1.0f);
	m_ModelDestructFlg = 1;
}

CSpot3D::~CSpot3D()
{
	if(m_ModelDestructFlg) delete m_Model;
}

void CSpot3D::MouseMsg(MOUSEMSG Msg, int x, int y)
{
	switch(Msg)
	{
	case MMSG_NOTHING:
		//ignore
		break;
	case MMSG_NORMAL:
		break;
	case MMSG_HIGHLIGHT:
		break;
	case MMSG_SELECT:
		break;
	case MMSG_MOUSEMOVE:
		m_Pitch += x;
		m_Yaw += x;
		break;
	}
}

void CSpot3D::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	Pipeline *Pl = m_SpotMaster->GetPipeLine();
	SRect Dimensions = SRect(m_X + OffsetX, m_Y + OffsetY, m_X + OffsetX + m_W, m_Y + OffsetY + m_H);
	DestSurface->SetClipping(Dimensions);
	Pl->SetViewPort(Dimensions);

	//local rotations
	Matrix4 YawMat, PitchMat;
	YawMat.SetXRotation(m_Yaw);
	PitchMat.SetYRotation(m_Pitch);
	Matrix4 LocalMatrix;
	LocalMatrix = PitchMat * YawMat;
	Pl->SetLocalMatrix(LocalMatrix);


	//initialize camera
	Vector3 Up;
	Vector3 Direction;
	Vector3 Location;
	Camera Cam;
	Location.set(0.0f, 0.0f, (float)m_Distance);
//	YawMat.SetXRotation(0);
//	PitchMat.SetYRotation(0);
//	Matrix4 CameraMatrix = PitchMat * YawMat;
	Matrix4 CameraMatrix;
	Direction.set(Fixed8::fromInt(0), Fixed8::fromInt(0), Fixed8::fromInt(-1));
	Direction = CameraMatrix*Direction;
	Up.set(Fixed8::fromInt(0), Fixed8::fromInt(1), Fixed8::fromInt(0));
	Up = CameraMatrix*Up;
	Cam.Set(Location, Location-Direction, Up);
	Pl->SetCamera(&Cam);

	Pl->SetRenderFlags(EF_ENVMAPPING);
	Pl->SetBuffer(DestSurface);

	m_Model->Render(Pl);

	CSpot::Draw(DestSurface, OffsetX, OffsetY);
}

void *CSpot3D::GetParam(SPOTPARAMS Param)
{
	void *rc;
	rc = CSpot::GetParam(Param);
	if((int)rc!=-1) return rc;

	switch(Param)
	{
	case PARAM_SPOT_3D_YAW:
		return (void *)m_Yaw;
		break;
	case PARAM_SPOT_3D_ROLL:
		return (void *)m_Roll;
		break;
	case PARAM_SPOT_3D_PITCH:
		return (void *)m_Pitch;
		break;
	case PARAM_SPOT_3DOBJECT:
		return (void *)m_Model;
		break;
	case PARAM_SPOT_3D_DISTANCE:
		return (void *)m_Distance;
		break;
	default:
		break;
	}
	return 0;
}

void CSpot3D::SetParam(SPOTPARAMS Param, void *Value)
{
	CSpot::SetParam(Param, Value);

	switch(Param)
	{
	case PARAM_SPOT_3D_YAW:
		m_Yaw = (int)Value;
		break;
	case PARAM_SPOT_3D_ROLL:
		m_Roll = (int)Value;
		break;
	case PARAM_SPOT_3D_PITCH:
		m_Pitch = (int)Value;
		break;
	case PARAM_SPOT_3D_DISTANCE:
		m_Distance = (int)Value;
		break;
	case PARAM_SPOT_3DOBJECT:
		m_Model = (ObjectR3d *)Value;;
		m_ModelDestructFlg = 0;
		break;
	default:
		break;
	}
}
#endif

