#ifndef _H_SPOT3D
#define _H_SPOT3D

#include "gui/spot.h"

///////////////////////////////////////////////////////////

#ifdef USE3DENGINE

class CSpot3D : public CSpot
{
public:
	CSpot3D(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, XString ModelName);
	~CSpot3D();

	virtual void MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);

	virtual void *GetParam(SPOTPARAMS Param);	// get value of param indicated by button/dial/slider
	virtual void  SetParam(SPOTPARAMS Param, void *Value);	// set param for button/dial/slider
private:

	ObjectR3d  *m_Model;
	int			m_ModelDestructFlg; //if 0 then the model will not be cleaned up if the button is destroyed
	int			m_Pitch;
	int			m_Yaw;
	int			m_Roll;
	int			m_Distance;
	Fixed8		m_Zoom;
};
#endif


#endif
