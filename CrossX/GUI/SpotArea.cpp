#include "gui/spotarea.h"


/////////////////////////////////////////
// CSpotArea Methods
/////////////////////////////////////////

CSpotArea::CSpotArea(CSpotMaster *Parent, XString Id, int x, int y, int w, int h) : CSpot(Parent, Id, x, y, w, h)
{
	m_Transparency = 255;
}

CSpotArea::~CSpotArea()
{
}

void CSpotArea::MouseMsg(MOUSEMSG Msg, int x, int y)
{
}

void CSpotArea::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	CSpot::Draw(DestSurface, OffsetX, OffsetY);
}

void *CSpotArea::GetParam(SPOTPARAMS Param)
{
	void *rc;
	rc = CSpot::GetParam(Param);
	if((int)rc!=-1) return rc;

#if 0   // if parameters are added, remove this if   (is used to suppress warnings)
	switch(Param)
	{
	default:
		break;
	}
#endif

	return 0;
}

void CSpotArea::SetParam(SPOTPARAMS Param, void *Value)
{
	CSpot::SetParam(Param, Value);

#if 0   // if parameters are added, remove this if   (is used to suppress warnings)
	switch(Param)
	{
	default:
		break;
	}
#endif

//	SetDirty();
}

