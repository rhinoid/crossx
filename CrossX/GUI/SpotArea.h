#ifndef _H_SPOTAREA
#define _H_SPOTAREA

#include "gui/spot.h"

///////////////////////////////////////////////////////////

class CSpotArea : public CSpot
{
public:
	CSpotArea(CSpotMaster *Parent, XString Id, int x, int y, int w, int h);
	~CSpotArea();

	virtual void MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);

	virtual void *GetParam(SPOTPARAMS Param);	// get value of param indicated by button/dial/slider
	virtual void  SetParam(SPOTPARAMS Param, void *Value);	// set param for button/dial/slider
private:
};



#endif
