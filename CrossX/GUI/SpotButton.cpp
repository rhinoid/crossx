#include "gui/spotbutton.h"
#include "gui/spotfont.h"
#include "2d/surfacecache.h"
#include "utils/globals.h"
#include "utils/debug.h"
#include "utils/clib.h"


/////////////////////////////////////////
// CSpotButton Methods
/////////////////////////////////////////


CSpotButton::CSpotButton(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, char *ResIDNormal, char *ResIDHigh) : CSpot(Parent, Id, x, y, w, h)
{
	m_InternalState = BUTT_NORMAL;

	m_Transparency = 255;

	m_Normal = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDNormal, GetSpotMaster()->GetRootSurface()->GetMode());
	m_Highlight = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDHigh, GetSpotMaster()->GetRootSurface()->GetMode());
}

CSpotButton::~CSpotButton()
{
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Normal);
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Highlight);
}

void *CSpotButton::GetParam(SPOTPARAMS Param)
{
	void *rc;
	rc = CSpot::GetParam(Param);
	if((int)rc!=-1) return rc;

	switch(Param)
	{
	default:
		break;
	}
	return 0;
}

void CSpotButton::SetParam(SPOTPARAMS Param, void *Value)
{
	CSpot::SetParam(Param, Value);

	switch(Param)
	{
	default:
		break;
	}
}

void CSpotButton::MouseMsg(MOUSEMSG Msg, int x, int y)
{
	BUTTSTATES NewState;

	NewState = m_InternalState;

	switch(Msg)
	{
	case MMSG_NOTHING:
		//ignore
		break;
	case MMSG_NORMAL:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_HIGHLIGHT:
		NewState = BUTT_HIGHLIGHT;
		break;
	case MMSG_SELECT:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_MOUSEMOVE:
		//ignore
		break;
	}

	if(m_InternalState != NewState)
	{
		m_InternalState = NewState;
	}
}

void CSpotButton::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	CSurface *SelSurface=0;
	switch(m_InternalState)
	{
	case BUTT_NORMAL:
		SelSurface = m_Normal;
		break;
	case BUTT_HIGHLIGHT:
		SelSurface = m_Highlight;
		break;
	default: ASSERT(0);
		break;
	}
	ASSERT(SelSurface);
	DestSurface->Blit(SelSurface, 0, 0, m_X + OffsetX, m_Y + OffsetY, m_W, m_H, m_Transparency);

	CSpot::Draw(DestSurface, OffsetX, OffsetY);
}
