#ifndef _H_SPOTBUTTON
#define _H_SPOTBUTTON

#include "gui/spot.h"

///////////////////////////////////////////////////////////
class CSpotButton : public CSpot
{
public:
	CSpotButton(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, char *ResIDNormal, char *ResIDHigh);
	CSpotButton();
	~CSpotButton();

	virtual void MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);

	virtual void *GetParam(SPOTPARAMS Param);	// get value of param indicated by button/dial/slider
	virtual void  SetParam(SPOTPARAMS Param, void *Value);	// set param for button/dial/slider
private:
	
	BUTTSTATES	m_InternalState;		// internal state of button

	CSurface   *m_Normal;
	CSurface   *m_Highlight;

};



#endif
