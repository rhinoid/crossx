#include "gui/spotcolor.h"

/////////////////////////////////////////
// CSpotColor Methods
/////////////////////////////////////////

CSpotColor::CSpotColor(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, CROSSX_COLOR32 NormColor, CROSSX_COLOR32 SelectColor) : CSpot(Parent, Id, x, y, w, h)
{
	m_InternalState = BUTT_NORMAL;
	m_NormColor = NormColor;
	m_SelectColor = SelectColor;
	m_Transparency = 255;
}

CSpotColor::~CSpotColor()
{
}

void CSpotColor::MouseMsg(MOUSEMSG Msg, int x, int y)
{
	BUTTSTATES NewState;

	NewState = m_InternalState;

	switch(Msg)
	{
	case MMSG_NOTHING:
		//ignore
		break;
	case MMSG_NORMAL:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_HIGHLIGHT:
		NewState = BUTT_HIGHLIGHT;
		break;
	case MMSG_SELECT:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_MOUSEMOVE:
		//ignore
		break;
	}

	if(m_InternalState != NewState)
	{
		m_InternalState = NewState;
	}
}

void CSpotColor::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	CROSSX_COLOR32 Color = 0;
	switch(m_InternalState)
	{
	case BUTT_NORMAL:
		Color = m_NormColor;
		break;
	case BUTT_HIGHLIGHT:
		Color = m_SelectColor;
		break;
	default: ASSERT(0);
		break;
	}
	if(m_Transparency==255)
	{
		DestSurface->Clear(m_X + OffsetX, m_Y + OffsetY, m_W, m_H, Color);
	}
	else
	{
		DestSurface->Clear(m_X + OffsetX, m_Y + OffsetY, m_W, m_H, Color, m_Transparency);
	}

	// call superclass
	CSpot::Draw(DestSurface, OffsetX, OffsetY);
}

void *CSpotColor::GetParam(SPOTPARAMS Param)
{
	void *rc;
	rc = CSpot::GetParam(Param);
	if((int)rc!=-1) return rc;

	switch(Param)
	{
	case PARAM_SPOT_COLORNORMAL:
		return (void *)m_NormColor;
		break;
	case PARAM_SPOT_COLORSELECT:
		return (void *)m_SelectColor;
		break;
	default:
		break;
	}
	return 0;
}

void CSpotColor::SetParam(SPOTPARAMS Param, void *Value)
{
	CSpot::SetParam(Param, Value);

	switch(Param)
	{
	case PARAM_SPOT_COLORNORMAL:
		m_NormColor = (int) Value;
		break;
	case PARAM_SPOT_COLORSELECT:
		m_SelectColor = (int) Value;
		break;
	default:
		break;
	}
}
