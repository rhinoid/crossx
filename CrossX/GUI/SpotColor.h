#ifndef _H_SPOTCOLOR
#define _H_SPOTCOLOR

#include "gui/spot.h"

///////////////////////////////////////////////////////////

class CSpotColor : public CSpot
{
public:
	CSpotColor(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, CROSSX_COLOR32 NormalColor, CROSSX_COLOR32 SelectColor);
	~CSpotColor();

	virtual void MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);

	virtual void *GetParam(SPOTPARAMS Param);	// get value of param indicated by button/dial/slider
	virtual void  SetParam(SPOTPARAMS Param, void *Value);	// set param for button/dial/slider
private:

	BUTTSTATES	m_InternalState;		// internal state of button

	CROSSX_COLOR32 m_NormColor;
	CROSSX_COLOR32 m_SelectColor;
};


#endif
