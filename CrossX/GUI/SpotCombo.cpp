#include "gui/spotcombo.h"
#include "gui/spotfont.h"
#include "gui/spotmaster.h"
#include "gui/spotarea.h"
#include "gui/spotGraphic.h"
#include "gui/spotButton.h"
#include "gui/spotverslider.h"
#include "2d/surfacecache.h"
#include "utils/globals.h"
#include "utils/debug.h"
#include "utils/clib.h"
#include <stdlib.h>
#include <stdio.h>


#if 0  //###

/////////////////////////////////////////
// CSpotCombo Methods
/////////////////////////////////////////


CSpotCombo::CSpotCombo(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, char *ResIDNormal, char *ResIDHigh, int ComboItemWidth, int ComboItemHeight, char *ResIDItemNormal, char *ResIDItemHigh, int SliderWidth, int SliderHeight, char *ResIDSliderNormal, char *ResIDSliderHigh) : CSpot(Parent, Id, x, y, w, h)
{
	m_InternalState = BUTT_NORMAL;

	m_Transparency = 255;

	m_Normal = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDNormal, m_Parent->GetRootSurface()->GetMode());
	m_Highlight = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDHigh, m_Parent->GetRootSurface()->GetMode());

	m_Color      = 0;


	m_ResIDItemNormal = ResIDItemNormal;
	m_ResIDItemHigh   = ResIDItemHigh;

	m_NrOfItemNames = 0;
	m_ItemNames = 0;

	m_NrOfComboItems = 0;

	m_CurItem = 0;

	m_ComboItemWidth = ComboItemWidth;
	m_ComboItemHeight = ComboItemHeight;
	m_SliderWidth = SliderWidth;
	m_SliderHeight = SliderHeight;
	m_SliderResIDNormal = ResIDSliderNormal;
	m_SliderResIDHigh = ResIDSliderHigh;

	m_ComboItemSpacing = ComboItemHeight;

	m_SliderOffsetX = m_ComboItemWidth;
	m_SliderOffsetY = h;

	m_SliderBackgroundBottom = 0;
	m_SliderBackgroundMiddle = 0;
	m_SliderBackgroundUpper = 0;

	m_ComboItemColor = 0;
	m_TextVisible = 1;

	m_TextClipWidth = w;

	m_ComboDir = 0;		//0 is down, 1 is up

	m_MaxItemsVisible = 4;

	m_Font = 0;
	m_TextAlignment = SPOT_TEXT_ALIGNLEFT;
}

CSpotCombo::~CSpotCombo()
{
	int i;
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Normal);
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Highlight);

	for(i=0; i<m_NrOfItemNames; i++)
	{
		delete [] m_ItemNames[i];
	}

	if(m_ItemNames) free(m_ItemNames);

}


void *CSpotCombo::GetParam(SPOTPARAMS Param)
{
	void *rc;
	rc = CSpot::GetParam(Param);
	if((int)rc!=-1) return rc;

	switch(Param)
	{
	case PARAM_SPOT_FONT:
		return (void *)m_Font;
		break;
	case PARAM_SPOT_COLOR:
		return (void *)m_Color;
		break;
	case PARAM_SPOT_NUMBER:
		return (void *)m_CurItem;
	case PARAM_SPOT_TEXTALIGNMENT:
		return (void *)m_TextAlignment;
		break;
	case PARAM_SPOT_TEXTCLIPX:
		return (void *)m_TextClipWidth;
		break;
	default:
		break;
	}
	return 0;
}

void CSpotCombo::SetParam(SPOTPARAMS Param, void *Value)
{
	CSpot::SetParam(Param, Value);

	switch(Param)
	{
	case PARAM_SPOT_RESETCONTENT:
		{
			int i;
			for(i=0; i<m_NrOfItemNames; i++)
			{
				delete [] m_ItemNames[i];
			}
			if(m_ItemNames) free(m_ItemNames);
			m_ItemNames = 0;
			m_NrOfItemNames = 0;
		}
		break;
	case PARAM_SPOT_ADDTEXT:
		{
			char *Text;
			Text = (char *) Value;
			if(m_NrOfItemNames==0)	//exception
			{
				m_NrOfItemNames++;
				m_ItemNames = (char **) malloc(4);
			}
			else
			{
				m_NrOfItemNames++;
				m_ItemNames = (char **) realloc(m_ItemNames, m_NrOfItemNames*4);
			}
			m_ItemNames[m_NrOfItemNames-1] = new char[stdlib_strlen(Text)+1];
			stdlib_strcpy(m_ItemNames[m_NrOfItemNames-1], Text);
		}
		break;
	case PARAM_SPOT_FONT:
		m_Font = (CSpotFont *) Value;
		break;
	case PARAM_SPOT_TEXTALIGNMENT:
		m_TextAlignment = (SPOTTEXTALIGNMENT) (int)Value;
		break;
	case PARAM_SPOT_COLOR:
		m_Color = (int) Value;
		break;
	case PARAM_SPOT_NUMBER:
		if(m_NrOfItemNames)
		{
			m_CurItem = (int) Value;
			if(m_CurItem>=m_NrOfItemNames)
			{
				m_CurItem = m_NrOfItemNames-1;
			}
		}
		break;
	case PARAM_SPOT_COMBOSLIDEROFFSETX:
		m_SliderOffsetX = (int) Value;
		break;
	case PARAM_SPOT_COMBOSLIDEROFFSETY:
		m_SliderOffsetY = (int) Value;
		break;
	case PARAM_SPOT_COMBOITEMSPACING:
		m_ComboItemSpacing = (int) Value;
		break;
	case PARAM_SPOT_COMBOITEMTRANSPARENCY:
		m_ComboItemTransparency = (int) Value;
		break;
	case PARAM_SPOT_COMBOITEMCOLOR:
		m_ComboItemColor = (int) Value;
		break;
	case PARAM_SPOT_COMBOSLIDERBACKUPPER:
		m_SliderBackgroundUpper = (char *) Value;
		break;
	case PARAM_SPOT_COMBOSLIDERBACKMIDDLE:
		m_SliderBackgroundMiddle = (char *) Value;
		break;
	case PARAM_SPOT_COMBOSLIDERBACKBOTTOM:
		m_SliderBackgroundBottom = (char *) Value;
		break;
	case PARAM_SPOT_COMBOMAXITEMS:
		m_MaxItemsVisible = (int) Value;
		break;
	case PARAM_SPOT_SHOWTEXT:
		m_TextVisible = (int) Value;
		break;
	case PARAM_SPOT_COMBODIR:
		m_ComboDir = (int) Value;
		break;
	case PARAM_SPOT_TEXTCLIPX:
		m_TextClipWidth = (int) Value;
		break;
	default:
		break;
	}
	SetDirty();
}

void CSpotCombo::MouseMsg(MOUSEMSG Msg, int x, int y)
{
	int i;
	BUTTSTATES NewState;

	NewState = m_InternalState;

	switch(Msg)
	{
	case MMSG_NOTHING:
		//ignore
		break;
	case MMSG_NORMAL:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_HIGHLIGHT:
		NewState = BUTT_HIGHLIGHT;
		break;
	case MMSG_SELECT:
		NewState = BUTT_NORMAL;

		// open or close combobox
		if(m_NrOfComboItems)	// is open, will be closed now
		{
			m_Parent->DelButtons(SPOT_ID_COMBOBOX, SPOT_ID_COMBOBOX_END);
			m_NrOfComboItems = 0;
			m_Parent->SetMessageOveride(0);
			m_Parent->SetFocus(m_PrevFocusLow, m_PrevFocusHigh);
		}
		else	// was closed..lets open it
		{
			int ScrollAmnt;

			m_NrOfComboItems = m_MaxItemsVisible;
			if(m_NrOfItemNames<m_MaxItemsVisible)
			{
				m_NrOfComboItems = m_NrOfItemNames;
			}

			int StartPosY;
			StartPosY = GetY();
			if(m_ComboDir)
			{
				StartPosY -= (m_ComboItemSpacing*(m_NrOfComboItems+1));
			}

			ScrollAmnt = m_NrOfItemNames-m_NrOfComboItems;
			m_Parent->AddSpot(new CSpotArea(m_Parent, SPOT_ID_COMBOBOX, 0, 0, m_Parent->GetWidth(), m_Parent->GetHeight()));
			if(ScrollAmnt>0)
			{
				if(m_SliderBackgroundUpper!=0 && m_SliderBackgroundMiddle!=0 && m_SliderBackgroundBottom!=0)
				{
					for (i=0; i<m_NrOfComboItems; i++)
					{
						if(i==0)
						{
							m_Parent->AddSpot(new CSpotGraphic(m_Parent, SPOT_ID_COMBOBOX+i+2+ScrollAmnt, GetX()+m_SliderOffsetX, StartPosY+((i+1)*m_ComboItemSpacing), 16, m_ComboItemSpacing, m_SliderBackgroundUpper));
						}
						else
						{
							if(i==(m_NrOfComboItems-1))
							{
								m_Parent->AddSpot(new CSpotGraphic(m_Parent, SPOT_ID_COMBOBOX+i+2+ScrollAmnt, GetX()+m_SliderOffsetX, StartPosY+((i+1)*m_ComboItemSpacing), 16, m_ComboItemSpacing, m_SliderBackgroundBottom));
							}
							else
							{
								m_Parent->AddSpot(new CSpotGraphic(m_Parent, SPOT_ID_COMBOBOX+i+2+ScrollAmnt, GetX()+m_SliderOffsetX, StartPosY+((i+1)*m_ComboItemSpacing), 16, m_ComboItemSpacing, m_SliderBackgroundMiddle));
							}
						}
					}
				}
			}
			for(i=0; i<m_NrOfComboItems; i++)
			{
				m_Parent->AddSpot(new CSpotButton(m_Parent, SPOT_ID_COMBOBOX+i+2, GetX(), StartPosY+((i+1)*m_ComboItemSpacing), m_ComboItemWidth, m_ComboItemHeight, m_ResIDItemNormal, m_ResIDItemHigh));
				m_Parent->SetParam(SPOT_ID_COMBOBOX+i+2, PARAM_SPOT_COLOR, (void *)m_ComboItemColor );
				m_Parent->SetParam(SPOT_ID_COMBOBOX+i+2, PARAM_SPOT_TEXT, m_ItemNames[i]);
				m_Parent->SetParam(SPOT_ID_COMBOBOX+i+2, PARAM_SPOT_TEXTCLIPX, (void *)m_TextClipWidth);
				m_Parent->SetParam(SPOT_ID_COMBOBOX+i+2, PARAM_SPOT_TRANSPARENCY, (void *)m_ComboItemTransparency);
				if(m_ItemNames[i][0] == '-')
				{
					m_Parent->SetParam(SPOT_ID_COMBOBOX+i+2, PARAM_SPOT_ENABLE, 0);
				}
			}
			if(ScrollAmnt>0)
			{
				m_Parent->AddSpot(new CSpotVerSlider(m_Parent, SPOT_ID_COMBOBOX+1,  GetX()+m_SliderOffsetX, StartPosY+m_SliderOffsetY, m_SliderWidth, m_SliderHeight, 0, ScrollAmnt, m_ComboItemSpacing*(m_NrOfComboItems-1), m_SliderResIDNormal, m_SliderResIDHigh));
			}
			
			m_Parent->GetFocus(&m_PrevFocusLow, &m_PrevFocusHigh); // in order to restore the focus
			m_Parent->SetFocus(0, 65535);
			m_Parent->SetMessageOveride(this);
		}


		break;
	case MMSG_MOUSEMOVE:
		//ignore
		break;
	}

	if(m_InternalState != NewState)
	{
		m_InternalState = NewState;
		SetDirty();
	}
}

void CSpotCombo::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	CSurface *SelSurface;
	switch(m_InternalState)
	{
	case BUTT_NORMAL:
		SelSurface = m_Normal;
		break;
	case BUTT_HIGHLIGHT:
		SelSurface = m_Highlight;
		break;
	default: ASSERT(0);
		break;
	}
	ASSERT(SelSurface);
	DestSurface->Blit(SelSurface, 0, 0, m_X - OffsetX, m_Y - OffsetY, m_W, m_H, m_Transparency);
	if(m_NrOfItemNames>0 && m_TextVisible!=0)
	{
		if(m_Font)
		{
			m_Font->Draw(DestSurface, m_X - OffsetX+6, m_Y - OffsetY+2, m_TextClipWidth, m_H, m_ItemNames[m_CurItem], m_Transparency, m_Color, m_TextAlignment, 0);
		}
		else
		{
			CSpotFont *SystemFont;
			SystemFont = m_Parent->GetFont();
			if(SystemFont)SystemFont->Draw(DestSurface, m_X - OffsetX+6, m_Y - OffsetY+2, m_TextClipWidth, m_H, m_ItemNames[m_CurItem], m_Transparency, m_Color, m_TextAlignment, 0);
		}
	}
}


bool CSpotCombo::MessageOveride(int ButtonID, MOUSEMSG Msg, int Param1, int Param2)
{

	if(ButtonID == GetID()) // is it a message for this control itself? then pass it on!
	{
		return true;
	}

	// mis geklikt?
	if(ButtonID==SPOT_ID_COMBOBOX)
	{
		m_Parent->DelButtons(SPOT_ID_COMBOBOX, SPOT_ID_COMBOBOX_END);
		m_NrOfComboItems = 0;
		m_Parent->SetMessageOveride(0);
		return false;
	}

	// scrollbar
	if(ButtonID==SPOT_ID_COMBOBOX+1)
	{
		int i;
		int val;
		val = (int)m_Parent->GetParam(SPOT_ID_COMBOBOX+1, PARAM_SPOT_NUMBER);

		// set new texts
		for(i=0; i<m_NrOfComboItems; i++)
		{
			m_Parent->SetParam(SPOT_ID_COMBOBOX+i+2, PARAM_SPOT_TEXT, m_ItemNames[i+val]);
		}
		return true;
	}

	// item geselect?
	if(ButtonID>=(SPOT_ID_COMBOBOX+2) &&  ButtonID<SPOT_ID_COMBOBOX_END)
	{
		if(Msg==MMSG_SELECT)
		{
			int val;
			val = (int)m_Parent->GetParam(SPOT_ID_COMBOBOX+1, PARAM_SPOT_NUMBER);
			val += ButtonID-(SPOT_ID_COMBOBOX+2);
			m_Parent->SetMessageOveride(0);
			
			m_CurItem = val;
			SetDirty();

			m_Parent->DelButtons(SPOT_ID_COMBOBOX, SPOT_ID_COMBOBOX_END);
			m_NrOfComboItems = 0;

			m_Parent->SendMsg(GetID(), SPOT_BUTTON_ITEMSELECTED, val, 0);
			return false;
		}
		return true;
	}

	return false;
}

#endif
