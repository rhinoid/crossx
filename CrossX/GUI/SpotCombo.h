#ifndef _H_SPOTCOMBO
#define _H_SPOTCOMBO

#include "gui/spot.h"

#if 0 //@@@

///////////////////////////////////////////////////////////
class CSpotCombo : public CSpot
{
public:
	CSpotCombo(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, char *ResIDNormal, char *ResIDHigh, int ComboItemWidth, int ComboItemHeight, char *ResIDItemNormal, char *ResIDItemHigh, int SliderWidth, int SliderHeight, char *ResIDSliderNormal, char *ResIDSliderHigh );
	CSpotCombo();
	~CSpotCombo();

	virtual void MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);

	virtual void *GetParam(SPOTPARAMS Param);	// get value of param indicated by button/dial/slider
	virtual void  SetParam(SPOTPARAMS Param, void *Value);	// set param for button/dial/slider

	virtual bool MessageOveride(int ButtonID, MOUSEMSG Msg, int Param1, int Param2);

private:
	
	BUTTSTATES	m_InternalState;		// internal state of button

	char	   *m_ResIDItemNormal;		// filename or ID of graphic for individual combo box items
	char	   *m_ResIDItemHigh;

	int			m_NrOfItemNames;		// nr of items in combobox
	char      **m_ItemNames;			// names of combo items

	int			m_NrOfComboItems;		// how many of these graphics are on the screen? (only if the combo box is currently open)

	int			m_CurItem;				// current active combo item

	int			m_Color;				// color on button

	int			m_ComboItemWidth;		// width of comboitem graphic
	int			m_ComboItemHeight;		// width of comboitem graphic
	int			m_SliderWidth;			// width of comboitem graphic
	int			m_SliderHeight;			// width of comboitem graphic
	char	   *m_SliderResIDNormal;	// graphic of normal state sliderknob
	char	   *m_SliderResIDHigh;		// graphic of normal state sliderknob

	int			m_TextClipWidth;		// Width for text clipping...  defaults to button width

	int			m_SliderOffsetX;		// where does the slider start?
	int			m_SliderOffsetY;		// where does the slider start?
	int			m_MaxItemsVisible;			// how many items are visible if combo opens up? defaults to 4

	int			m_ComboItemSpacing;		// spacing of comboitems (defaults to m_ComboItemHeight)

	int			m_ComboItemTransparency;	//transparency for comboboxitems

	int			m_PrevFocusLow;			// previous focus of spotmaster when combobox was opened in order to restore it properly
	int			m_PrevFocusHigh;	

	char 	   *m_SliderBackgroundUpper;	// does the slider have a background graphic where it slides on? if supplied, should be same height as m_ComboItemSpacing 
	char 	   *m_SliderBackgroundMiddle;	// does the slider have a background graphic where it slides on? middle piece
	char 	   *m_SliderBackgroundBottom;	// does the slider have a background graphic where it slides on? lower piece

	int			m_ComboItemColor;			// color of texts on comboitems

	int			m_TextVisible;			// text visible on button?

	int			m_ComboDir;				// should the combo go up or down?

	CSurface   *m_Normal;
	CSurface   *m_Highlight;

	CSpotFont  *m_Font;						// if null, the system default font is used
	SPOTTEXTALIGNMENT	m_TextAlignment;
};

#endif //@@@

#endif
