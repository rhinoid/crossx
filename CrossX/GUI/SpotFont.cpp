#include "spot.h"
#include "gui/spotfont.h"
#include <stdlib.h>
#include <stdio.h>
#include "utils/clib.h"
#include "utils/debug.h"

/////////////////////////////////////////
// CSpotFont Methods
/////////////////////////////////////////

CSpotFont::CSpotFont()
{
	m_Surface = 0;
}

CSpotFont::~CSpotFont()
{
}

void CSpotFont::Init(CSurface *Surface, XString Name, int FontWidth, int FontHeight, int FontFirst, int FontLast, int FontSpacing)
{
	m_Surface = Surface;
	m_FontWidth = FontWidth;
	m_FontHeight = FontHeight;
	m_FontFirst = FontFirst;
	m_FontLast = FontLast;
	m_Name = Name;
	m_FontSpacing = FontSpacing;
}

int CSpotFont::GetWidth(const char *Text)
{
	int Width, TextLen;
	TextLen = stdlib_strlen(Text);
	Width = TextLen * m_FontWidth;
	if(TextLen>0) Width += m_FontSpacing*(TextLen-1);
	return Width;
}

void CSpotFont::DrawInternal(CSurface *DestSurface, int x, int y, int w, int h, const char *Text, int Transparency, int Color, SPOTTEXTALIGNMENT Alignment)
{
	int Width;

	//do text aligment thingy
	switch(Alignment)
	{
	case SPOT_TEXT_ALIGNMIN:
		break;
	case SPOT_TEXT_ALIGNCENTER:
		Width = GetWidth(Text);
		x += (w-Width)/2;
		break;
	case SPOT_TEXT_ALIGNMAX:
		Width = GetWidth(Text);
		x += w-Width;
		break;
	}

	if(m_Surface)	// is a font installed?
	{
		int PosX, PosY;

		PosX = x;
		PosY = y;

		while(*Text)
		{
			int Char;

			Char = *Text++;
			if(Char==0x0a || Char==0x0d)
			{
				if(Char==0x0a)
				{
					PosX = x;
					PosY +=m_FontHeight;
					PosY ++;
					if((PosY+m_FontHeight) > h) break;
				}
			}
			else
			{
				if(Char>=m_FontFirst && Char<m_FontLast)
				{
					Char -= m_FontFirst;

					Char *= m_FontWidth;

					DestSurface->BlitColor(m_Surface, Char, 0, PosX, PosY, m_FontWidth, m_FontHeight, Transparency, Color);
				}

				PosX += m_FontWidth;
				PosX += m_FontSpacing;
			}
			if(PosX-x >= w) return;
		}
	}
}

void CSpotFont::Draw(CSurface *DestSurface, int x, int y, int w, int h, XString Text, int Transparency, int Color, SPOTTEXTALIGNMENT Alignment, int TextBreak)
{
	if(TextBreak==0)
	{
		DrawInternal(DestSurface, x, y, w, h, Text.c_str(), Transparency, Color, Alignment);
		return;
	}

	char **Lines;
	Lines = BreakText(Text.c_str(),w);
	if(Lines)
	{
		int LineNr=0;
		while(Lines[LineNr]!=0)
		{
			DrawInternal(DestSurface, x, y+(m_FontHeight*LineNr), w, h, Lines[LineNr], Transparency, Color, Alignment);
			free(Lines[LineNr]);
			LineNr++;
		}
		free(Lines);
	}

}


#define NEWLINECHAR '^'
#define ADDLINETOARRAY(Idx, Len) 			Lines[LineNr] = (char *)malloc(Len+1); \
			stdlib_memcpy(Lines[LineNr], Text+Idx, Len); \
			Lines[LineNr][Len]=0; \
			LineNr++; \
			Lines = (char **)realloc(Lines, (LineNr+1)*4); \
			Lines[LineNr]=0;

char **CSpotFont::BreakText(const char *Text, int Width)
{
	char **Lines=0; // array where the broken lines will be stored
	int	LineNr=0;   // The Number of stored lines so far

	int textlen = stdlib_strlen(Text);
	if (textlen == 0) return 0;

	int startIndex=0;
	int lastSpaceIndex = 0;
	int currentWidth = m_FontSpacing;

	Lines = (char **)malloc((LineNr+1)*4);
	Lines[LineNr]=0;

	
	for (int index=0; index<textlen; index++)
	{
		//Check for a new line character
		if (Text[index] == NEWLINECHAR ||Text[index] == 0x0d)
		{
			ADDLINETOARRAY(startIndex, index-startIndex);

			startIndex = index+1;
			lastSpaceIndex = index+1;
			currentWidth = m_FontWidth;
			continue;
		}

		if (Text[index] == ' ') lastSpaceIndex = index;

		currentWidth += (m_FontSpacing+m_FontWidth);
		if (currentWidth >= Width)
		{
			//This line of text extends the width
			if (lastSpaceIndex == startIndex)
			{
				//there is one word that won't fit on the entire line, just cut it
				ADDLINETOARRAY(startIndex, index-startIndex);
				
				startIndex = index;
				lastSpaceIndex = index;
				currentWidth = m_FontWidth;
			}
			else
			{
				//Normal cut
				ADDLINETOARRAY(startIndex, lastSpaceIndex-startIndex);

				index = lastSpaceIndex+1;
				startIndex = lastSpaceIndex+1;
				currentWidth = m_FontWidth;
			}
		}
	}

	if (startIndex < textlen)
	{
		ADDLINETOARRAY(startIndex, textlen-startIndex);
	}
	return Lines;
}
