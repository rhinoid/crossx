#ifndef _H_SPOTFONT
#define _H_SPOTFONT

#include "gui/spot.h"

///////////////////////////////////////////////////////////

class CSpotFont
{
public:
	CSpotFont();
	~CSpotFont();

	void Init(CSurface *Surface, XString Name, int FontWidth, int FontHeight, int FontFirst, int FontLast, int FontSpacing);
	void Draw(CSurface *DestSurface, int x, int y, int w, int h, XString Text, int Transparency, int Color, SPOTTEXTALIGNMENT Alignment, int TextBreak);
	int  GetWidth(const char *Text); //return width of text in pixels

	//Font properties
	int  GetFontSpacing() { return m_FontSpacing;}
	int  GetFontWidth() { return m_FontWidth;}
	int  GetFontHeight() { return m_FontHeight;}
	XString GetName() { return m_Name; }

private:
	void	DrawInternal(CSurface *DestSurface, int x, int y, int w, int h, const char *Text, int Transparency, int Color, SPOTTEXTALIGNMENT Alignment);
	char  **BreakText(const char *Text, int Width);

	XString m_Name;			// Logical name of font
	CSurface *m_Surface;	//The actual bitmap
	int		m_FontWidth;	// how wide is each individual char cell?
	int		m_FontHeight;	// how high is each char cell?
	int		m_FontFirst;	// which ascii code is the first which can be displayed?
	int		m_FontLast;		// which ascii code is the last which can be displayed?
	int		m_FontSpacing;  // how much extra pixels are between every character (in the destination picture)
};

#endif
