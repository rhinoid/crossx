#include "gui/spotgradient.h"

/////////////////////////////////////////
// CSpotGradient Methods
/////////////////////////////////////////

CSpotGradient::CSpotGradient(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, CROSSX_COLOR32 NormalTL, CROSSX_COLOR32 NormalTR, CROSSX_COLOR32 NormalBL, CROSSX_COLOR32 NormalBR, CROSSX_COLOR32 SelectTL, CROSSX_COLOR32 SelectTR, CROSSX_COLOR32 SelectBL, CROSSX_COLOR32 SelectBR) : CSpot(Parent, Id, x, y, w, h)
{
	m_InternalState = BUTT_NORMAL;
	m_NormTL = NormalTL;
	m_NormTR = NormalTR;
	m_NormBL = NormalBL;
	m_NormBR = NormalBR;
	m_SelectTL = SelectTL;
	m_SelectTR = SelectTR;
	m_SelectBL = SelectBL;
	m_SelectBR = SelectBR;
	m_Transparency = 255;
}

CSpotGradient::~CSpotGradient()
{
}

void CSpotGradient::MouseMsg(MOUSEMSG Msg, int x, int y)
{
	BUTTSTATES NewState;

	NewState = m_InternalState;

	switch(Msg)
	{
	case MMSG_NOTHING:
		//ignore
		break;
	case MMSG_NORMAL:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_HIGHLIGHT:
		NewState = BUTT_HIGHLIGHT;
		break;
	case MMSG_SELECT:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_MOUSEMOVE:
		//ignore
		break;
	}

	if(m_InternalState != NewState)
	{
		m_InternalState = NewState;
	}
}

void CSpotGradient::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	CROSSX_COLOR32 ColorTL=0;
	CROSSX_COLOR32 ColorTR=0;
	CROSSX_COLOR32 ColorBL=0;
	CROSSX_COLOR32 ColorBR=0;
	switch(m_InternalState)
	{
	case BUTT_NORMAL:
		ColorTL = m_NormTL;
		ColorTR = m_NormTR;
		ColorBL = m_NormBL;
		ColorBR = m_NormBR;
		break;
	case BUTT_HIGHLIGHT:
		ColorTL = m_SelectTL;
		ColorTR = m_SelectTR;
		ColorBL = m_SelectBL;
		ColorBR = m_SelectBR;
		break;
	default: ASSERT(0);
		break;
	}

	DestSurface->ClearGradient(m_X + OffsetX, m_Y + OffsetY, m_W, m_H, ColorTL, ColorTR, ColorBL, ColorBR, m_Transparency);

	// call superclass
	CSpot::Draw(DestSurface, OffsetX, OffsetY);
}

void *CSpotGradient::GetParam(SPOTPARAMS Param)
{
	void *rc;
	rc = CSpot::GetParam(Param);
	if((int)rc!=-1) return rc;

	switch(Param)
	{
	default:
		break;
	}
	return 0;
}

void CSpotGradient::SetParam(SPOTPARAMS Param, void *Value)
{
	CSpot::SetParam(Param, Value);

	switch(Param)
	{
	default:
		break;
	}
}
