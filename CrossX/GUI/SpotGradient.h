#ifndef _H_SPOTGRADIENT
#define _H_SPOTGRADIENT

#include "gui/spot.h"

///////////////////////////////////////////////////////////

class CSpotGradient : public CSpot
{
public:
	CSpotGradient(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, CROSSX_COLOR32 NormalTL, CROSSX_COLOR32 NormalTR, CROSSX_COLOR32 NormalBL, CROSSX_COLOR32 NormalBR, CROSSX_COLOR32 SelectTL, CROSSX_COLOR32 SelectTR, CROSSX_COLOR32 SelectBL, CROSSX_COLOR32 SelectBR);
	~CSpotGradient();

	virtual void MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);

	virtual void *GetParam(SPOTPARAMS Param);	// get value of param indicated by button/dial/slider
	virtual void  SetParam(SPOTPARAMS Param, void *Value);	// set param for button/dial/slider
private:

	BUTTSTATES	m_InternalState;		// internal state of button

	CROSSX_COLOR32 m_NormTL;
	CROSSX_COLOR32 m_NormTR;
	CROSSX_COLOR32 m_NormBL;
	CROSSX_COLOR32 m_NormBR;
	CROSSX_COLOR32 m_SelectTL;
	CROSSX_COLOR32 m_SelectTR;
	CROSSX_COLOR32 m_SelectBL;
	CROSSX_COLOR32 m_SelectBR;
};


#endif
