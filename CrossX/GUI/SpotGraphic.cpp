#include "gui/spotgraphic.h"
#include "2d/surfacecache.h"
#include "utils/globals.h"

/////////////////////////////////////////
// CSpotGraphic Methods
/////////////////////////////////////////


CSpotGraphic::CSpotGraphic(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, XString ResIDNormal) : CSpot(Parent, Id, x, y, w, h)
{
	m_Transparency = 255;

	m_Normal = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDNormal, GetSpotMaster()->GetRootSurface()->GetMode());
}

CSpotGraphic::~CSpotGraphic()
{
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Normal);
}

void CSpotGraphic::MouseMsg(MOUSEMSG Msg, int x, int y)
{
}

void CSpotGraphic::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	DestSurface->Blit(m_Normal, 0, 0, m_X + OffsetX, m_Y + OffsetY, m_W, m_H, m_Transparency);

	CSpot::Draw(DestSurface, OffsetX, OffsetY);
}
