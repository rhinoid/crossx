#ifndef _H_SPOTGRAPHIC
#define _H_SPOTGRAPHIC

#include "gui/spot.h"

///////////////////////////////////////////////////////////

class CSpotGraphic : public CSpot
{
public:
	CSpotGraphic(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, XString ResIDNormal);
	CSpotGraphic();
	~CSpotGraphic();

	virtual void MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);
private:
	
	CSurface   *m_Normal;
};


#endif
