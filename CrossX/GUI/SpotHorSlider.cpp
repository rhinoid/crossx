#include "gui/spothorslider.h"
#include "2d/surfacecache.h"
#include "utils/globals.h"
#include "utils/debug.h"

/////////////////////////////////////////
// CSpotHorSlider Methods
/////////////////////////////////////////


CSpotHorSlider::CSpotHorSlider(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int Min, int Max, int SlideWidth, char *ResIDNormal, char *ResIDHigh) : CSpot(Parent, Id, x, y, w, h)
{
	m_InternalState = BUTT_NORMAL;

	m_Transparency = 255;

	m_Normal = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDNormal, GetSpotMaster()->GetRootSurface()->GetMode());
	m_Highlight = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDHigh, GetSpotMaster()->GetRootSurface()->GetMode());

	m_Min = Min;
	m_Max = Max;

	m_SlideX1 = x;
	m_SlideX2 = x+SlideWidth;
}

CSpotHorSlider::~CSpotHorSlider()
{
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Normal);
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Highlight);
}

void CSpotHorSlider::MouseMsg(MOUSEMSG Msg, int x, int y)
{
	int NewX;
	BUTTSTATES NewState;

	NewState = m_InternalState;

	switch(Msg)
	{
	case MMSG_NOTHING:
		//ignore
		break;
	case MMSG_NORMAL:
		NewState = BUTT_NORMAL;
		m_StartX = x;
		break;
	case MMSG_HIGHLIGHT:
		m_StartX = x;
		NewState = BUTT_HIGHLIGHT;
		break;
	case MMSG_SELECT:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_MOUSEMOVE:
		// try dragging the button
		NewX = GetX() + (x-m_StartX);
		
		if(NewX< m_SlideX1) NewX = m_SlideX1;
		if(NewX> m_SlideX2) NewX = m_SlideX2;

		if(NewX!=GetX())
		{
			m_StartX += (NewX-GetX());
			SetPos(NewX, GetY());
		}

		break;
	}

	if(m_InternalState != NewState)
	{
		m_InternalState = NewState;
	}
}

void *CSpotHorSlider::GetParam(SPOTPARAMS Param)
{
	void *rc;
	rc = CSpot::GetParam(Param);
	if((int)rc!=-1) return rc;

	switch(Param)
	{
	case PARAM_SPOT_NUMBER:
		{
			int Value;
			Value = GetX()-m_SlideX1;
			Value *= (m_Max-m_Min);
			Value /= (m_SlideX2-m_SlideX1);
			Value += m_Min;
			return (void *)Value;
		}
		break;
	default:
		break;
	}
	return 0;
}

void CSpotHorSlider::SetParam(SPOTPARAMS Param, void *Value)
{
	CSpot::SetParam(Param, Value);

	switch(Param)
	{
	case PARAM_SPOT_NUMBER:
		{
			int Val;
			Val = (int) Value;
			Val -= m_Min;
			Val *= (m_SlideX2-m_SlideX1);
			Val /= (m_Max-m_Min);
			Val += m_SlideX1;

			if(Val< m_SlideX1) Val = m_SlideX1;
			if(Val> m_SlideX2) Val = m_SlideX2;

			if(Val!=GetX())
			{
				SetPos(Val, GetY());
			}
		}
		break;
	default:
		break;
	}
}


void CSpotHorSlider::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	CSurface *SelSurface=0;
	switch(m_InternalState)
	{
	case BUTT_NORMAL:
		SelSurface = m_Normal;
		break;
	case BUTT_HIGHLIGHT:
		SelSurface = m_Highlight;
		break;
	default: ASSERT(0);
		break;
	}
	ASSERT(SelSurface);
	DestSurface->Blit(SelSurface, 0, 0, m_X + OffsetX, m_Y + OffsetY, m_W, m_H, m_Transparency);

	CSpot::Draw(DestSurface, OffsetX, OffsetY);
}
