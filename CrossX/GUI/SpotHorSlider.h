#ifndef _H_SPOTHORSLIDER
#define _H_SPOTHORSLIDER

#include "gui/spot.h"

///////////////////////////////////////////////////////////
class CSpotHorSlider : public CSpot
{
public:
	CSpotHorSlider(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int Min, int Max, int SlideWidth, char *ResIDNormal, char *ResIDHigh);
	CSpotHorSlider();
	~CSpotHorSlider();

	virtual void MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);

	virtual void *GetParam(SPOTPARAMS Param);	// get value of param indicated by button/dial/slider
	virtual void  SetParam(SPOTPARAMS Param, void *Value);	// set param for button/dial/slider
private:
	
	BUTTSTATES	m_InternalState;		// internal state of button

	int		m_Min;
	int		m_Max;

	int		m_StartX;

	int		m_SlideX1;
	int		m_SlideX2;

	CSurface   *m_Normal;
	CSurface   *m_Highlight;
};

#endif
