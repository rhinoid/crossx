#include "gui/spotknob.h"
#include "2d/surfacecache.h"
#include "utils/globals.h"
#include "utils/debug.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

/////////////////////////////////////////
// CSpotKnob Methods
/////////////////////////////////////////


CSpotKnob::CSpotKnob(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int Min, int Max, int NrOfFrames, int StartFrame, char *Filenames) : CSpot(Parent, Id, x, y, w, h)
{
	int i;

	m_Min = Min;
	m_Max = Max;
	m_CurNumber = 0;
	m_StartNumber = m_CurNumber;	// reset vals
	m_Transparency = 255;
	m_NrOfSurfaces = NrOfFrames;

	m_Surfaces = new CSurface * [m_NrOfSurfaces];

	for(i=0; i<m_NrOfSurfaces; i++)
	{
		char FileBuf[256];
		sprintf(FileBuf, Filenames, StartFrame+i);
		m_Surfaces[i] = CSurfaceCache::GetSurfaceCache()->LoadSurface(FileBuf, GetSpotMaster()->GetRootSurface()->GetMode());
	}
}


CSpotKnob::~CSpotKnob()
{
	int i;

	for(i=0; i<m_NrOfSurfaces; i++)
	{
		CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Surfaces[i]);
	}
	delete [] m_Surfaces;
}

void CSpotKnob::MouseMsg(MOUSEMSG Msg, int x, int y)
{
	switch(Msg)
	{
	case MMSG_NOTHING:
		//ignore
		break;
	case MMSG_NORMAL:
		m_StartY = y;
		m_StartNumber = m_CurNumber;	// reset vals
		break;
	case MMSG_HIGHLIGHT:
		m_StartY = y;
		m_StartNumber = m_CurNumber;	// reset vals
		break;
	case MMSG_SELECT:
		break;
	case MMSG_MOUSEMOVE:
//		m_CurNumber = m_StartNumber+ (y-m_StartY);	// set new value when the user drags

		// calculate angle of mouse with knob and adjust knob accordingly
		int v1x,v1y,v2x,v2y;	// the 2 vectors between which we'll calc the angle using the dot product:  v1(v1x, v1y) and v2(v2x, v2y)   =>  (v1x*v2x + v2x*v2y) / (sqr(v1x*v1x+v1y*v1y)*sqr(v2x*v2x+v2y*v2y)) (=cos of angle)

		v1x = GetX()+(m_W/2);	// hoek berekenen van bovenkant knob
		v1y = GetY();

		v2x = x;
		v2y = y;

		v1x -= GetX()+(m_W/2);
		v1y -= GetY()+(m_H/2);
		v2x -= GetX()+(m_W/2);
		v2y -= GetY()+(m_H/2);

		// calc dotproduct
		float deler, noemer;

		noemer = (float)(v1x*v2x + v1y*v2y);
		deler = (float)sqrt(v1x*v1x+v1y*v1y)*(float)sqrt(v2x*v2x+v2y*v2y);
		noemer /= deler;

		noemer = (float)acos(noemer);
		noemer *= (m_Max-m_Min);
		noemer /= (float)3.14159265359;

		if(x>(GetX()+(m_W/2)))	// are we on the right hand side?
		{
			noemer = ((m_Max-m_Min)/2) + (noemer);
		}
		else	// left hand side
		{
			noemer = ((m_Max-m_Min)/2) - (noemer);
		}

		m_CurNumber = (int)noemer;
		if(m_CurNumber<m_Min) m_CurNumber = m_Min;
		if(m_CurNumber>m_Max) m_CurNumber = m_Max;

		break;
	}

}

void *CSpotKnob::GetParam(SPOTPARAMS Param)
{
	void *rc;
	rc = CSpot::GetParam(Param);
	if((int)rc!=-1) return rc;

	switch(Param)
	{
	case PARAM_SPOT_NUMBER:
		return (void *)m_CurNumber;
		break;
	default:
		break;
	}
	return 0;
}

void CSpotKnob::SetParam(SPOTPARAMS Param, void *Value)
{
	CSpot::SetParam(Param, Value);

	switch(Param)
	{
	case PARAM_SPOT_NUMBER:
		m_CurNumber = (int)Value;
		break;
	default:
		break;
	}
}



void CSpotKnob::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	// calc which frame we must display
	int FrameNr;

	FrameNr  = m_CurNumber-m_Min;
	FrameNr *= m_NrOfSurfaces;
	FrameNr /= (m_Max-m_Min);
	if(FrameNr>=m_NrOfSurfaces) FrameNr=m_NrOfSurfaces-1;
	DestSurface->Blit(m_Surfaces[FrameNr], 0, 0, m_X + OffsetX, m_Y + OffsetY, m_W, m_H, m_Transparency);

	CSpot::Draw(DestSurface, OffsetX, OffsetY);
}

