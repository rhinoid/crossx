#ifndef _H_SPOTKNOB
#define _H_SPOTKNOB

#include "gui/spot.h"

///////////////////////////////////////////////////////////

class CSpotKnob : public CSpot
{
public:
	CSpotKnob(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int Min, int Max, int NrOfFrames, int StartFrame, char *Filenames); // min and max are the min and max values alllowed. nrofframes is the nr of frames made for the animation. startframe is the nr with whcih we start loading (the filename must have an %d in it)
	CSpotKnob();
	~CSpotKnob();

	virtual void MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);

	virtual void *GetParam(SPOTPARAMS Param);	// get value of param indicated by button/dial/slider
	virtual void  SetParam(SPOTPARAMS Param, void *Value);	// set param for button/dial/slider
private:
	
	BUTTSTATES	m_InternalState;		// internal state of button
	
	int			m_StartNumber;	// value the control was just before we started dragging
	int			m_CurNumber;	// value the control was just before we started dragging
	int			m_Min;
	int			m_Max;
	int			m_NrOfSurfaces;
	int			m_StartY;		// first place where the user clicked... this way we way how much the user has 'dragged' the number
	CSurface  **m_Surfaces;
};

#endif
