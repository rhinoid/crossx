#include "Gui/Spot.h"
#include "Gui/SpotArea.h"
#include "Gui/SpotColor.h"
#include "Gui/SpotFont.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "Utils/Clib.h"
#include "Utils/Debug.h"



/////////////////////////////////////////
// CSpotMaster Methods
/////////////////////////////////////////



CSpotMaster::CSpotMaster()
{
#ifdef USE3DENGINE
	m_PipeLine = 0;
#endif

	m_ActiveFlg = false;
	m_EnabledFlg = false;
}


CSpotMaster::~CSpotMaster()
{
}

int CSpotMaster::Init(CSurface *Display, int x, int y, int w, int h, CROSSX_COLOR32 BackColor,SPOT_CALLBACK CallBack, int Context)
{
	// test for absurd arguments
	ASSERT(x >= 0);
	ASSERT(x <  2560);
	ASSERT(y >= 0);
	ASSERT(y <  3200);

	ASSERT(x+w <= 2560);
	ASSERT(y+h <= 3200);

	ASSERT(Display);	// a root surface is necesarry


	m_RootSurface = Display;

	m_CallBack = CallBack;
	m_Context = Context;

	m_X = x;
	m_Y = y;
	m_W = w;
	m_H = h;

	m_CurButton =0;
	if(BackColor == -1)
	{
		m_RootControl = new CSpotArea(this, "root", 0, 0, w, h);
	}
	else
	{
		m_RootControl = new CSpotColor(this, "root", 0, 0, w, h, BackColor, BackColor);
	}
	m_MessageOveride = 0;

	m_FocusLowID = -1;
	m_FocusHighID = -1;

	m_ActiveFlg = true;
	m_EnabledFlg = true;

	return 1;
}

void	CSpotMaster::Enable(int EnableFlg)
{
	m_EnabledFlg = EnableFlg;
}

int		CSpotMaster::GetWidth(void)
{
	return m_W;
}

int		CSpotMaster::GetHeight(void)
{
	return m_H;
}


int CSpotMaster::AddFont(XString FileName, XString Name, int FirstLetter, int NrOfLetters, int FontSpacing)
{
	CSpotFont NewFont;

	CSurface *FontSurface = CSurfaceCache::GetSurfaceCache()->LoadSurface(FileName, m_RootSurface->GetMode());
	if(FontSurface)
	{
		NewFont.Init(FontSurface, Name, FontSurface->GetWidth()/NrOfLetters, FontSurface->GetHeight(), FirstLetter, FirstLetter + NrOfLetters, FontSpacing);
		m_FontCollection.Add(NewFont);
		return 1;
	}

	return 0;
}

CSpotFont *CSpotMaster::GetFont(XString Name)
{
	int FontItr;
	//Cycle trough all fonts in the font list
	for(FontItr=0; FontItr<m_FontCollection.Size(); FontItr++)
	{
		CSpotFont &CurFont = m_FontCollection.Get(FontItr);

		if(CurFont.GetName().CompareNoCase(Name) == 0)
		{
			return &CurFont;
		}
	}
	return 0;
}


CSurface *CSpotMaster::GetRootSurface(void)
{
	return m_RootSurface;
}

void CSpotMaster::SetPipeLine(Pipeline *p)
{
#ifdef USE3DENGINE
	m_PipeLine = p;
	m_PipeLine->SetBuffer(m_RootSurface);
#endif
}

void CSpotMaster::Update()
{
	CSpot *SpotIt;

	CSurface *OffScreenSurface;

	if(m_EnabledFlg==0) return;

#ifdef USE3DENGINE
	m_PipeLine->BeginFrame(1);
#endif


// now create an offscreen Surface the size of the root surface
//	OffScreenSurface = CSurface::Factory(m_RootSurface->GetMode());
//	ASSERT(OffScreenSurface);
//	OffScreenSurface->Create(m_RootSurface->GetWidth(), m_RootSurface->GetHeight());

// draw all buttons on the temp bitmap

	SpotIt = m_RootControl;
	while(SpotIt)
	{
		// Draw actual Spot element
		SpotIt->Draw(m_RootSurface, 0, 0);
		SpotIt = SpotIt->GetNextNode();
	}
	m_RootSurface->RemoveClipping();

// draw the offscreenBM onto the window
//	m_RootSurface->BlitNoAlpha(OffScreenSurface, 0, 0, 0, 0, OffScreenSurface->GetWidth(), OffScreenSurface->GetHeight(), 255);

// clear up temp surface
//	delete OffScreenSurface;

#ifdef USE3DENGINE
	m_PipeLine->EndFrame();
#endif

}


void CSpotMaster::DelButton(XString Id)
{
	CSpot *Control;

	Control = SearchControl(Id);
	if(Control != 0 && Control != m_RootControl)
	{
		CSpot *Parent;
		Parent = Control->GetParentNode();


		XString ControlName;
		int Idx;
		Idx = Id.reversefind('/');
		ControlName.assign(Id.c_str()+Idx+1, Id.length()-Idx+1);

		Parent->DelSpot(ControlName);
	}
}


void CSpotMaster::DelAll()
{
	CSpot *CurIt;

	CurIt = m_RootControl;
	while(CurIt)
	{
		CurIt->DelAll();
		CurIt = CurIt->GetNextNode();
	}

	m_RootControl->SetNextNode(0);
	m_CurButton = 0;
}

void CSpotMaster::MoveButton(XString Id, int NewX, int NewY)
{
	CSpot *Control;

	Control = SearchControl(Id);
	if(Control != 0)
	{
		Control->SetPos(NewX, NewY);
	}
}


void CSpotMaster::SetParam(XString Id, SPOTPARAMS Param, void *Value)
{
	CSpot *Control;

	Control = SearchControl(Id);
	if(Control != 0) Control->SetParam(Param, Value);
}


void *CSpotMaster::GetParam(XString Id, SPOTPARAMS Param)
{
	CSpot *Control;

	Control = SearchControl(Id);
	if(Control != 0) return Control->GetParam(Param);

	return 0;
}


void CSpotMaster::AddSpot(CSpot *NewSpot)
{
	m_RootControl->AddSpot(NewSpot);
}


void	CSpotMaster::MouseDown(int x, int y)
{
	CSpot *NewCurButton;


	if(m_EnabledFlg==0) return;

	x -= m_X;
	y -= m_Y;


	// was er nog een andere button gedrukt?  dan deze eerst inactive maken, we hebben blijkbaar de button los gemist
	if(m_CurButton)
	{
		SendMouseMsg(m_CurButton, MMSG_NORMAL, x, y);
		m_CurButton = 0;
	}

	// kijken welke button gedrukt wordt.   dit wordt de nieuwe curbutton
	NewCurButton = m_RootControl->GetHit(x, y); //returns the top control that contains this point

	// inform button of new state info
	if(NewCurButton)	// was a button pressed?
	{
		m_CurButton = NewCurButton;
		if(SendMouseMsg(m_CurButton, MMSG_HIGHLIGHT, x, y))  // inform button to go to its highlight state,
		{
			if(ButtonValid(m_CurButton))		//button could have been erased by previous message
			{
				// send message to the app that button was pressed
				SendMsg(m_CurButton->GetPath(), m_CurButton->GetID(), SPOT_BUTTON_PRESSED, x, y);
			}
		}
	}
//	Update(false);
}

void	CSpotMaster::MouseUp(int x, int y)
{
	CSpot *NewCurButton;
	CSpot *Button;

	if(0)return; //@@@
	if(m_EnabledFlg==0) return;

	// check if there was a current button..  if not then ignore this message since it is intended for noone
	if(m_CurButton==0) return;

	x -= m_X;
	y -= m_Y;

	// now check to see above which button the mouse was released... if it is the same as the current button we perform an action
	NewCurButton = m_RootControl->GetHit(x, y); //returns the top control that contains this point

	// inform button of new state info
	Button = m_CurButton;
	if(NewCurButton == m_CurButton)
	{
		if(SendMouseMsg(Button, MMSG_SELECT, x, y))  // inform button to go to its selected state,
		{
			if(ButtonValid(Button))		//button could have been erased by previous message
			{
				// send message to the app that button should perform its action
				SendMsg(Button->GetPath(), Button->GetID(), SPOT_BUTTON_ACTION, x, y);
			}

			if(ButtonValid(Button))		//button could have been erased by previous message
			{
				// send message to the app that button was released
				SendMsg(Button->GetPath(), Button->GetID(), SPOT_BUTTON_RELEASED, x, y);
			}
		}
	}
	else
	{
		if(SendMouseMsg(Button, MMSG_NORMAL, x, y))  // inform button to go to its normal state,
		{
			if(ButtonValid(Button))		//button could have been erased by previous message
			{
				// send message to the app that button was released
				SendMsg(Button->GetPath(), Button->GetID(), SPOT_BUTTON_RELEASED, x, y);
			}
		}
	}
	m_CurButton = 0;
}

void	CSpotMaster::MouseMove(int x, int y)
{
	if(m_EnabledFlg==0) return;
	
	// check if there was a current button..  if not then ignore this message since it is intended for noone
	if(m_CurButton==0) return;

	x -= m_X;
	y -= m_Y;

	// route the message through to the button
	if(SendMouseMsg(m_CurButton, MMSG_MOUSEMOVE, x, y))  // inform button of the message
	{
		if(ButtonValid(m_CurButton))		//button could have been erased by previous message
		{
			// send message to the app that mouse moved over a button
			SendMsg(m_CurButton->GetPath(), m_CurButton->GetID(), SPOT_BUTTON_MOUSEMOVE, x, y);
		}
	}
}


bool CSpotMaster::ButtonValid(CSpot *Button, CSpot *StartControl)
{
	CSpot *SpotIt;
	if(StartControl==0)
	{
		StartControl = m_RootControl;
	}
	SpotIt = StartControl;
	while(SpotIt)
	{
		CSpot *Child;
		Child = SpotIt->GetChild();
		if(Child)
		{
			if(ButtonValid(Button, Child)==true)
			{
				return true;
			}
		}
		if(SpotIt == Button)
		{
			return true;
		}
		SpotIt = SpotIt->GetNextNode();
	}
	return false;
}


void	CSpotMaster::SendMsg(XString ButtonPath, XString ButtonName, SPOTMESSAGE Msg, int Param1, int Param2)
{
//	if(m_MessageOveride)
//	{
//		if(m_MessageOveride->MessageOveride(ButtonID, Msg, m_Context, Param1, Param2)==false)
//		{
//			// message is not passed on
//			return;
//		}
//	}
	m_CallBack(ButtonPath, ButtonName, Msg, m_Context, Param1, Param2);
}

bool	CSpotMaster::SendMouseMsg(CSpot *Button, MOUSEMSG Msg, int Param1, int Param2)
{
	if(Button)
	{
		// check if we have a message overide, if so sent the message to that control first and decide if we sent the message to the rightful control afterward
		if(m_MessageOveride)
		{
			if(m_MessageOveride->MessageOveride(Button->GetPath(), Button->GetID(), Msg, Param1, Param2)==false)
			{
				// message is not passed on
				return false;
			}
		}
		Button->MouseMsg(Msg, Param1, Param2);
	}
	return true;
}



void	CSpotMaster::SetMessageOveride(CSpot *Control)
{
	m_MessageOveride = Control;
}


void	CSpotMaster::SetFocus(int StartId, int EndId)
{
	m_FocusLowID = StartId;
	m_FocusHighID = EndId;
}

void	CSpotMaster::GetFocus(int *StartId, int *EndId)
{
	*StartId = m_FocusLowID;
	*EndId = m_FocusHighID;
}

CSpot  *CSpotMaster::SearchControl(XString Path)
{
	return m_RootControl->SearchControl(Path);
}
