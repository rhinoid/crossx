#ifndef _H_SPOTMASTER
#define _H_SPOTMASTER

#include "Utils/XString.h"
#include "Utils/StorageVector.h"

#define USE3DENGINE     //If defined, special 3d extensions are used (define this when you include this file)

enum MOUSEMSG 
{
	MMSG_NOTHING = 1,
	MMSG_NORMAL,			// return to prev state (mouse released somewhere else then on button)
	MMSG_HIGHLIGHT,			// go to highlight state...someone is pressing with the stylus on your button
	MMSG_SELECT,			// the user has lifted the stylus when it was over your button... the button is now activated!
	MMSG_MOUSEMOVE,			// you get mouse moves whenever your button was pressed and the stylus is moved
};


enum SPOTMESSAGE
{
	SPOT_BUTTON_ACTION = 1,	// button was pressed (so user clicked on it AND released it while standing on the button)
	SPOT_BUTTON_PRESSED,	// button is pressed....  but not yet released
	SPOT_BUTTON_RELEASED,	// button is released...  this may or may not have been ON the actual button..so don't use this to perform an action!
	SPOT_BUTTON_MOUSEMOVE,	// mouse is moved while the button is still selected
	SPOT_BUTTON_ITEMSELECTED,	// An item has been selected (combobox)   (In Param1 is the selected itemnr)
	SPOT_BUTTON_LOCATOR,	// in the waveedit box, a new location of the sample/loop/end point has been selected. in param 1 is the new location in samples
};

enum SPOTTEXTALIGNMENT
{
	SPOT_TEXT_ALIGNMIN = 0,
	SPOT_TEXT_ALIGNCENTER,
	SPOT_TEXT_ALIGNMAX
};



// Parameters the GUI elements can handle
enum SPOTPARAMS
{
	// what numeric value holds the control (valid for sliders, knobs, numbers, comboboxes etc)
	PARAM_SPOT_NUMBER = 1,		// int value

	// what text value holds the control (valid for buttons/labels etc)
	PARAM_SPOT_TEXT,			// char pointer

	// How is the text aligned horizontally?
	PARAM_SPOT_TEXTHALIGNMENT,   // SPOT_TEXT_ALIGNMIN, SPOT_TEXT_ALIGNMIDDLE, SPOT_TEXT_ALIGNMAX

	// How is the text aligned vertically?
	PARAM_SPOT_TEXTVALIGNMENT,   // SPOT_TEXT_ALIGNMIN, SPOT_TEXT_ALIGNMIDDLE, SPOT_TEXT_ALIGNMAX

	// Does text break off at the edges?
	PARAM_SPOT_TEXTBREAK,

	// X-Offset of text in control
	PARAM_SPOT_TEXTOFFSETX,

	// Y-Offset of text in control
	PARAM_SPOT_TEXTOFFSETY,

	// Width of clipping of text in a control (in a button for instance)
	PARAM_SPOT_TEXTCLIPX,

	// Set a specific font for the control (valid for buttons/labels etc)
	PARAM_SPOT_FONT,			// CSpotFont pointer

	// What color should the control (or the text) be drawn in?
	PARAM_SPOT_COLORNORMAL,			// int  (0RGB)

	// What select color should the control (or the text) be drawn in?
	PARAM_SPOT_COLORSELECT,			// int  (0RGB)

	// What color should the control be drawn in?
	PARAM_SPOT_TRANSPARENCY,	// int  (0-255)

	// show the currently selected item on top of the button/combobox (defaults to true)
	PARAM_SPOT_SHOWTEXT,	// 0-false  or 1-true

	// Enable or disable a control (if disabled, no messages are handled/send to the app)
	PARAM_SPOT_ENABLE,		// 0-false  or 1-true

	// Change the size of the control
	PARAM_SPOT_DIMENSIONS,  //upperword = height,  lowword = width

// comboboxes
	// Removes all the texts in a combobox
	PARAM_SPOT_RESETCONTENT,

	// Add a text entry for a combobox
	PARAM_SPOT_ADDTEXT,			// char pointer

	// set the horizontal offset for the slider in respect to the combobox
	PARAM_SPOT_COMBOSLIDEROFFSETX,	// int value

	// set the horizontal offset for the slider in respect to the combobox
	PARAM_SPOT_COMBOSLIDEROFFSETY,	// int value

	// set the distance between the combo items (so you can overlap them for instance)
	PARAM_SPOT_COMBOITEMSPACING,	// int value

	// set the transparency for the individual comboitems
	PARAM_SPOT_COMBOITEMTRANSPARENCY,	// int  (0-255)

	// set the color for the individual comboitems texts
	PARAM_SPOT_COMBOITEMCOLOR,	// int  (00RRGGBB)

	// supply a background graphic where the slider moves on. This is the upper part (the graphic should be as high as the value set with PARAM_SPOT_COMBOITEMSPACING) width should ne same as sliderknob
	PARAM_SPOT_COMBOSLIDERBACKUPPER, // char pointer

	// supply a background graphic where the slider moves on. This is the middle part (the graphic should be as high as the value set with PARAM_SPOT_COMBOITEMSPACING) width should ne same as sliderknob
	PARAM_SPOT_COMBOSLIDERBACKMIDDLE, // char pointer

	// supply a background graphic where the slider moves on. This is the bottom part (the graphic should be as high as the value set with PARAM_SPOT_COMBOITEMSPACING) width should ne same as sliderknob
	PARAM_SPOT_COMBOSLIDERBACKBOTTOM, // char pointer

	// How many items of the combo box are dropped before a scrollbar appears?
	PARAM_SPOT_COMBOMAXITEMS, // int value (1 - ...)

	// should combo show down or upwards? (defaults to down)
	PARAM_SPOT_COMBODIR,      // int value (0=down, 1 is up)
//wave edit control

	// Set a new datasource for the waveedit control
	PARAM_SPOT_WAVESETSOURCE, // char * or short *

	// Set a new datadest for the waveedit control... this table is not used to display..but will receive any edits the data might get (through the users mouse movements)
	PARAM_SPOT_WAVESET2NDDEST, // char * or short *


	// Set new samplelength of datasource
	PARAM_SPOT_WAVESETLENGTH,	// nr of samples

	// Set if the wave can be edited or not
	PARAM_SPOT_WAVEEDITENABLE, // 0 = false,  1 = true  (defaults to true)

	// Set a visible startpointline
	PARAM_SPOT_SETSTARTPOINT,  // -1 is off     0 - samplelength

	// Set a visible startpointline
	PARAM_SPOT_SETLOOPPOINT,  // -1 is off     0 - samplelength

	// Set a visible startpointline
	PARAM_SPOT_SETENDPOINT,  // -1 is off     0 - samplelength

//3d Control
	PARAM_SPOT_3D_YAW,       // Yaw of 3d object
	PARAM_SPOT_3D_ROLL,      // Roll of 3d object
	PARAM_SPOT_3D_PITCH,     // Pitch of 3d object
	PARAM_SPOT_3D_DISTANCE,  // Distance to 3D object
	PARAM_SPOT_3DOBJECT,     // A user defined 3d object... (this one will not be deleted if the control is destructed)

};


// Special (reserved) Spot button ID's
#define SPOT_ID_USER     (0)
#define SPOT_ID_COMBOBOX (16384)
#define SPOT_ID_COMBOBOX_END (16384+127)



// callback to send msgs to parent app
typedef void (*SPOT_CALLBACK)(XString ButtonPath, XString ButtonName, SPOTMESSAGE Msg, int Context, int Param1, int Param2);


enum BUTTSTATES 
{
	BUTT_NORMAL,
	BUTT_HIGHLIGHT,
};

///////////////////////////////////////////////////////////

#include "2d/SRect.h"
#include "2d/Surface.h"
#include "2d/SurfaceCache.h"
#include "Utils/XString.h"

#ifdef USE3DENGINE
#include "3d/Engine.h"
#else
class Pipeline;
#endif

///////////////////////////////////////////////////////////
class CSpotFont;
class CSpot;


typedef StorageVector<CSpotFont> FontVector;


class CSpotMaster
{
public:
	CSpotMaster();
	~CSpotMaster();

	// external functions
	int		Init(CSurface *Display, int x, int y, int w, int h, CROSSX_COLOR32 BackColor, SPOT_CALLBACK CallBack, int Context);

	CSpotFont *GetFont(XString Name);					// Gets the global system font

	void	DelButton(XString Id);						// Remove GUI elements
	void	DelAll();									// Remove All GUI elements

	void    AddSpot(CSpot *GuiElement);					// Add a GUI element to the system (0 if error, 1 if good)
	int     AddFont(XString FileName, XString Name, int FirstLetter, int NrOfLetters, int FontSpacing);				// Add a GUI Font to the system 

	void	MoveButton(XString Id, int NewX, int NewY);		// Move a GUI element

	void   *GetParam(XString Id, SPOTPARAMS Param);			// Sets all kind of GUI element specific params
	void	SetParam(XString Id, SPOTPARAMS Param, void *Value);			// retrieves GUI element specific params

	void	Update();									// Update the screen

	void	Enable(int EnableFlg);						// If 0, spot will not update its screen anymore and not respond to mouse input

	int		GetWidth(void);								// Gets width of current Spot controlled canvas
	int		GetHeight(void);							// Gets height of current Spot controlled canvas

	// report functions (app has to call these)
	void	MouseDown(int x, int y);
	void	MouseUp(int x, int y);
	void	MouseMove(int x, int y);

	CSurface *GetRootSurface(void);

	//internal functions
	bool	ButtonValid(CSpot *Button, CSpot *StartControl=0);	//is this a valid button? (and start looking for it starting with the StartControl (can be left 0 for a start from the root)
	void	SetFocus(int StartId=-1, int EndId=-1);	// if set a valid range, only enables input from these buttons. Very practical to get input from a few buttons without removing all the others (for instance in a modal dialog box)
	void	GetFocus(int *StartId, int *EndId); //get current focus range
	void	SetMessageOveride(CSpot *Control);	// set that one control temporarily gets all messages instead of user (handy for comboboxes)

	Pipeline *GetPipeLine() { return m_PipeLine; }
	void SetPipeLine(Pipeline *p);


	// not typically used from the app itself, since this will send a message to the app and not the control
	void	SendMsg(XString ButtonPath, XString ButtonName, SPOTMESSAGE Msg, int Param1, int Param2);	// send msg to app
private:
	bool	SendMouseMsg(CSpot *Button, MOUSEMSG Msg, int Param1, int Param2);	// send msg to control (returns true if message may also be sent to app)

	CSpot  *SearchControl(XString Path); // Given a unique path, this function return the corresponding control


	int				m_ActiveFlg;			// is the SpotMaster active?
	int				m_EnabledFlg;			// is updates and mouse enabled?
	int				m_X;
	int				m_Y;
	int				m_W;
	int				m_H;

	Pipeline		*m_PipeLine;

	SPOT_CALLBACK	m_CallBack;
	int				m_Context;

	CSurface	   *m_RootSurface;		// The root surface (the window)

	//font vars
	FontVector      m_FontCollection;	// Collection of fonts

	// but manager vars
	CSpot          *m_RootControl;		// Root control

	
	CSpot          *m_CurButton;		// Which button is currently selected (pressed on with pointer)  0 if none

	CSpot	       *m_MessageOveride;	// If set, this control gets all messages instead of user

	int				m_FocusLowID;		// low id of buttons which are only enabled
	int				m_FocusHighID;		// high id of buttons which are enabled
};


#endif
