#include "gui/spotmover.h"
#include "2d/surfacecache.h"
#include "utils/globals.h"
#include "utils/debug.h"


/////////////////////////////////////////
// CSpotMover Methods
/////////////////////////////////////////


CSpotMover::CSpotMover(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int SlideW, int SlideH, char *ResIDNormal, char *ResIDHigh) : CSpot(Parent, Id, x, y, w, h)
{
	m_InternalState = BUTT_NORMAL;

	m_Transparency = 255;

	m_Normal = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDNormal, GetSpotMaster()->GetRootSurface()->GetMode());
	m_Highlight = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDHigh, GetSpotMaster()->GetRootSurface()->GetMode());

	m_SlideX1 = x;
	m_SlideY1 = y;
	m_SlideX2 = x+SlideW;
	m_SlideY2 = y+SlideH;
}

CSpotMover::~CSpotMover()
{
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Normal);
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Highlight);
}

void *CSpotMover::GetParam(SPOTPARAMS Param)
{
	void *rc;
	rc = CSpot::GetParam(Param);
	if((int)rc!=-1) return rc;

#if 0   // if parameters are added, remove this if   (is used to suppress warnings)
	switch(Param)
	{
	default:
		break;
	}
#endif
	return 0;
}

void CSpotMover::SetParam(SPOTPARAMS Param, void *Value)
{
	CSpot::SetParam(Param, Value);

#if 0   // if parameters are added, remove this if   (is used to suppress warnings)
	switch(Param)
	{
	default:
		break;
	}
#endif
}

void CSpotMover::MouseMsg(MOUSEMSG Msg, int x, int y)
{
	int NewX, NewY;
	BUTTSTATES NewState;

	NewState = m_InternalState;

	switch(Msg)
	{
	case MMSG_NOTHING:
		//ignore
		break;
	case MMSG_NORMAL:
		NewState = BUTT_NORMAL;
		m_StartX = x;
		m_StartY = y;
		break;
	case MMSG_HIGHLIGHT:
		m_StartX = x;
		m_StartY = y;
		NewState = BUTT_HIGHLIGHT;
		break;
	case MMSG_SELECT:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_MOUSEMOVE:
		// try dragging the button
		NewX = GetX() + (x-m_StartX);
		NewY = GetY() + (y-m_StartY);
		
		if(NewX< m_SlideX1) NewX = m_SlideX1;
		if(NewX> m_SlideX2) NewX = m_SlideX2;
		if(NewY< m_SlideY1) NewY = m_SlideY1;
		if(NewY> m_SlideY2) NewY = m_SlideY2;

		if(NewX!=GetX() || NewY!=GetY())
		{
			m_StartX += (NewX-GetX());
			m_StartY += (NewY-GetY());
			SetPos(NewX, NewY);
		}
			
		break;
	}

	if(m_InternalState != NewState)
	{
		m_InternalState = NewState;
	}
}

void CSpotMover::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	CSurface *SelSurface=0;
	switch(m_InternalState)
	{
	case BUTT_NORMAL:
		SelSurface = m_Normal;
		break;
	case BUTT_HIGHLIGHT:
		SelSurface = m_Highlight;
		break;
	default: ASSERT(0);
		break;
	}
	ASSERT(SelSurface);
	DestSurface->Blit(SelSurface, 0, 0, m_X + OffsetX, m_Y + OffsetY, m_W, m_H, m_Transparency);

	CSpot::Draw(DestSurface, OffsetX, OffsetY);
}
