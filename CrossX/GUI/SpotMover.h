#ifndef _H_SPOTMOVER
#define _H_SPOTMOVER

#include "gui/spot.h"

///////////////////////////////////////////////////////////
class CSpotMover : public CSpot
{
public:
	CSpotMover(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int SlideW, int SlideH, char *ResIDNormal, char *ResIDHigh);
	CSpotMover();
	~CSpotMover();

	virtual void MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);

	virtual void *GetParam(SPOTPARAMS Param);	// get value of param indicated by button/dial/slider
	virtual void  SetParam(SPOTPARAMS Param, void *Value);	// set param for button/dial/slider
private:
	
	BUTTSTATES	m_InternalState;		// internal state of button

	int			m_SlideX1;
	int			m_SlideY1;
	int			m_SlideX2;
	int			m_SlideY2;

	int			m_StartX;
	int			m_StartY;

	CSurface   *m_Normal;
	CSurface   *m_Highlight;
};

#endif
