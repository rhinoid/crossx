#include "gui/spotnumber.h"
#include "gui/spotfont.h"
#include "2d/surfacecache.h"
#include "utils/globals.h"
#include "utils/debug.h"
#include <stdio.h>


/////////////////////////////////////////
// CSpotNumber Methods
/////////////////////////////////////////


CSpotNumber::CSpotNumber(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int Min, int Max) : CSpot(Parent, Id, x, y, w, h)
{
	m_Min = Min;
	m_Max = Max;
	m_CurNumber = Min;
	m_StartNumber = m_CurNumber;	// reset vals
	m_Transparency = 255;
	m_Color = 0;

	m_Font = 0;
	m_TextHAlignment = SPOT_TEXT_ALIGNMIN;
}

CSpotNumber::~CSpotNumber()
{
}

void CSpotNumber::MouseMsg(MOUSEMSG Msg, int x, int y)
{
	switch(Msg)
	{
	case MMSG_NOTHING:
		//ignore
		break;
	case MMSG_NORMAL:
		m_StartY = y;
		m_StartNumber = m_CurNumber;	// reset vals
		break;
	case MMSG_HIGHLIGHT:
		m_StartY = y;
		m_StartNumber = m_CurNumber;	// reset vals
		break;
	case MMSG_SELECT:
		break;
	case MMSG_MOUSEMOVE:
		m_CurNumber = m_StartNumber+ ((y-m_StartY)>>2);	// set new value when the user drags
		if(m_CurNumber<m_Min) m_CurNumber = m_Min;
		if(m_CurNumber>m_Max) m_CurNumber = m_Max;
		break;
	}

}

void *CSpotNumber::GetParam(SPOTPARAMS Param)
{
	void *rc;
	rc = CSpot::GetParam(Param);
	if((int)rc!=-1) return rc;

	switch(Param)
	{
	case PARAM_SPOT_FONT:
		return (void *)m_Font;
		break;
	case PARAM_SPOT_NUMBER:
		return (void *)m_CurNumber;
		break;
	case PARAM_SPOT_COLORNORMAL:
		return (void *)m_Color;
		break;
	case PARAM_SPOT_TEXTHALIGNMENT:
		return (void *)m_TextHAlignment;
		break;
	default:
		break;
	}
	return 0;
}

void CSpotNumber::SetParam(SPOTPARAMS Param, void *Value)
{
	CSpot::SetParam(Param, Value);

	switch(Param)
	{
	case PARAM_SPOT_NUMBER:
		m_CurNumber = (int)Value;
		break;
	case PARAM_SPOT_COLORNORMAL:
		m_Color = (int) Value;
		break;
	case PARAM_SPOT_FONT:
		m_Font = (CSpotFont *) Value;
		break;
	case PARAM_SPOT_TEXTHALIGNMENT:
		m_TextHAlignment = (SPOTTEXTALIGNMENT)(int) Value;
		break;
	default:
		break;
	}
}


void CSpotNumber::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	char dummyBuf[16];

	sprintf(dummyBuf, "%03d", m_CurNumber);

	if(m_Font)
	{
		m_Font->Draw(DestSurface, m_X + OffsetX, m_Y + OffsetY, m_W, m_H, dummyBuf, m_Transparency, m_Color, m_TextHAlignment, 0);
	}
	else
	{
		CSpotFont *SystemFont;
		SystemFont = GetSpotMaster()->GetFont("");   //@@@
		if(SystemFont)SystemFont->Draw(DestSurface, m_X + OffsetX, m_Y + OffsetY, m_W, m_H, dummyBuf, m_Transparency, m_Color, m_TextHAlignment, 0);
	}

	CSpot::Draw(DestSurface, OffsetX, OffsetY);
}

