#ifndef _H_SPOTNUMBER
#define _H_SPOTNUMBER

#include "gui/spot.h"

///////////////////////////////////////////////////////////

class CSpotNumber : public CSpot
{
public:
	CSpotNumber(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int Min, int Max);
	CSpotNumber();
	~CSpotNumber();

	virtual void MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);

	virtual void *GetParam(SPOTPARAMS Param);	// get value of param indicated by button/dial/slider
	virtual void  SetParam(SPOTPARAMS Param, void *Value);	// set param for button/dial/slider
private:
	int		m_CurNumber;
	int		m_Min;
	int		m_Max;

	int		m_Color;

	int		m_StartY;		// first place where the user clicked... this way we way how much the user has 'dragged' the number
	int		m_StartNumber;	// value the control was just before we started dragging

	CSpotFont *m_Font;						// if null, the system default font is used
	SPOTTEXTALIGNMENT	m_TextHAlignment;
};

#endif
