#include "gui/spotswitch.h"
#include "2d/surfacecache.h"
#include "gui/spotfont.h"
#include "utils/globals.h"
#include "utils/clib.h"
#include "utils/debug.h"

/////////////////////////////////////////
// CSpotSwitch Methods
/////////////////////////////////////////


CSpotSwitch::CSpotSwitch(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, XString ResIDNormal, XString ResIDHigh, XString ResIDNormal2, XString ResIDHigh2) : CSpot(Parent, Id, x, y, w, h)
{
	m_InternalState = BUTT_NORMAL;

	m_Transparency = 255;

	m_Normal = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDNormal, GetSpotMaster()->GetRootSurface()->GetMode());
	m_Highlight = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDHigh, GetSpotMaster()->GetRootSurface()->GetMode());
	m_Normal2 = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDNormal2, GetSpotMaster()->GetRootSurface()->GetMode());
	m_Highlight2 = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDHigh2, GetSpotMaster()->GetRootSurface()->GetMode());

	m_SwitchedState = 0;
}

CSpotSwitch::~CSpotSwitch()
{
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Normal);
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Highlight);
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Normal2);
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Highlight2);
}


void *CSpotSwitch::GetParam(SPOTPARAMS Param)
{
	void *rc;
	rc = CSpot::GetParam(Param);
	if((int)rc!=-1) return rc;

	switch(Param)
	{
	case PARAM_SPOT_NUMBER:
		return (void *)m_SwitchedState;
		break;
	default:
		break;
	}
	return 0;
}

void CSpotSwitch::SetParam(SPOTPARAMS Param, void *Value)
{
	CSpot::SetParam(Param, Value);

	switch(Param)
	{
	case PARAM_SPOT_NUMBER:
		m_SwitchedState = (int)Value;
		break;
	default:
		break;
	}
}

void CSpotSwitch::MouseMsg(MOUSEMSG Msg, int x, int y)
{
	BUTTSTATES NewState;

	NewState = m_InternalState;

	switch(Msg)
	{
	case MMSG_NOTHING:
		//ignore
		break;
	case MMSG_NORMAL:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_HIGHLIGHT:
		NewState = BUTT_HIGHLIGHT;
		break;
	case MMSG_SELECT:
		NewState = BUTT_NORMAL;
		m_SwitchedState ^= 1;		// switch button
		break;
	case MMSG_MOUSEMOVE:
		//ignore
		break;
	}

	if(m_InternalState != NewState)
	{
		m_InternalState = NewState;
	}
}

void CSpotSwitch::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	CSurface *SelSurface=0;
	switch(m_InternalState)
	{
	case BUTT_NORMAL:
		SelSurface = (m_SwitchedState==0) ? m_Normal : m_Normal2;
		break;
	case BUTT_HIGHLIGHT:
		SelSurface = (m_SwitchedState==0) ? m_Highlight : m_Highlight2;
		break;
	default: ASSERT(0);
		break;
	}
	ASSERT(SelSurface);
	DestSurface->Blit(SelSurface, 0, 0, m_X + OffsetX, m_Y + OffsetY, m_W, m_H, m_Transparency);

	CSpot::Draw(DestSurface, OffsetX, OffsetY);
}
