#ifndef _H_SPOTSWITCH
#define _H_SPOTSWITCH

#include "gui/spot.h"

///////////////////////////////////////////////////////////

class CSpotSwitch : public CSpot
{
public:
	CSpotSwitch(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, XString ResIDNormal, XString ResIDHigh, XString ResIDNormal2, XString ResIDHigh2);
	CSpotSwitch();
	~CSpotSwitch();

	virtual void MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);

	virtual void *GetParam(SPOTPARAMS Param);	// get value of param indicated by button/dial/slider
	virtual void  SetParam(SPOTPARAMS Param, void *Value);	// set param for button/dial/slider
private:
	
	BUTTSTATES	m_InternalState;		// internal state of button

	int			m_SwitchedState;			// if 0, switch is off...  if 1, switch is on!

	CSurface   *m_Normal;
	CSurface   *m_Highlight;
	CSurface   *m_Normal2;
	CSurface   *m_Highlight2;
};


#endif
