#include <stdlib.h>
#include <stdio.h>
#include "gui/spottext.h"
#include "gui/spotfont.h"
#include "utils/clib.h"

/////////////////////////////////////////
// CSpotText Methods
/////////////////////////////////////////

CSpotText::CSpotText(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int ColorNormal, int ColorSelect, XString FontName, XString Text) : CSpot(Parent, Id, x, y, w, h)
{
	m_TextBreak = false;
	m_FontName = FontName;
	m_BrokenText = 0;
	m_ColorNormal = ColorNormal;
	m_ColorSelect = ColorSelect;
	m_Transparency = 255;
	m_TextOffsetX = 0;
	m_TextOffsetY = 0;
	m_TextHAlignment = SPOT_TEXT_ALIGNMIN;
	m_TextVAlignment = SPOT_TEXT_ALIGNCENTER;
	m_InternalState = BUTT_NORMAL;
	m_Text = Text;
	BreakText();
}

CSpotText::~CSpotText()
{
	RemoveBrokenText();
}

void CSpotText::MouseMsg(MOUSEMSG Msg, int x, int y)
{
	BUTTSTATES NewState;

	NewState = m_InternalState;

	switch(Msg)
	{
	case MMSG_NOTHING:
		//ignore
		break;
	case MMSG_NORMAL:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_HIGHLIGHT:
		NewState = BUTT_HIGHLIGHT;
		break;
	case MMSG_SELECT:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_MOUSEMOVE:
		//ignore
		break;
	}

	if(m_InternalState != NewState)
	{
		m_InternalState = NewState;
	}
}

void CSpotText::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	int Color;

	switch(m_InternalState)
	{
	case BUTT_HIGHLIGHT:
		Color = m_ColorSelect;
		break;
	case BUTT_NORMAL:
	default:
		Color = m_ColorNormal;
		break;
	}

	// set clipping region for text
	int cx,cy;
	SRect ClipRect;
	cx = OffsetX+m_X;
	cy = OffsetY+m_Y;
	ClipRect.SetRect(cx, cy, cx + m_W, cy + m_H);
	DestSurface->SetClipping(ClipRect);
	if(m_BrokenText)
	{
		int TotalHeight=0;
		int AlignY=0;
		char **Ptr = m_BrokenText;
		CSpotFont *DrawFont;

		DrawFont = GetSpotMaster()->GetFont(m_FontName);

		while(*Ptr)
		{
			Ptr++;
			TotalHeight += DrawFont->GetFontHeight();
		}

		switch(m_TextVAlignment)
		{
		case SPOT_TEXT_ALIGNMIN:
			break;
		case SPOT_TEXT_ALIGNMAX:
			AlignY = m_H - TotalHeight;
			break;
		case SPOT_TEXT_ALIGNCENTER:
			AlignY = (m_H - TotalHeight)/2;
			break;

		}

		if(DrawFont)
		{
			char **Ptr = m_BrokenText;
			int TextY=0;
			while(*Ptr)
			{
				DrawFont->Draw(DestSurface, m_X + OffsetX + m_TextOffsetX, m_Y + OffsetY + m_TextOffsetY + TextY + AlignY, m_W, m_H, *Ptr, m_Transparency, Color, m_TextHAlignment, 0);
				Ptr++;
				TextY += DrawFont->GetFontHeight();
			}
		}
	}

	CSpot::Draw(DestSurface, OffsetX, OffsetY);
}

void *CSpotText::GetParam(SPOTPARAMS Param)
{
	void *rc;
	rc = CSpot::GetParam(Param);
	if((int)rc!=-1) return rc;

	switch(Param)
	{
	case PARAM_SPOT_FONT:
		return (void *)m_FontName.c_str();
		break;
	case PARAM_SPOT_COLORNORMAL:
		return (void *)m_ColorNormal;
		break;
	case PARAM_SPOT_COLORSELECT:
		return (void *)m_ColorSelect;
		break;
	case PARAM_SPOT_TEXTHALIGNMENT:
		return (void *)m_TextHAlignment;
		break;
	case PARAM_SPOT_TEXTVALIGNMENT:
		return (void *)m_TextVAlignment;
		break;
	case PARAM_SPOT_TEXTBREAK:
		return (void *)m_TextBreak;
	case PARAM_SPOT_TEXTOFFSETX:
		return (void *)m_TextOffsetX;
	case PARAM_SPOT_TEXTOFFSETY:
		return (void *)m_TextOffsetY;
	default:
		break;
	}
	return 0;
}

void CSpotText::SetParam(SPOTPARAMS Param, void *Value)
{
	CSpot::SetParam(Param, Value);

	switch(Param)
	{
	case PARAM_SPOT_TEXT:
		m_Text = (char *)Value;
		RemoveBrokenText();
		BreakText();
		break;
	case PARAM_SPOT_DIMENSIONS:
		RemoveBrokenText();
		BreakText();
		break;
	case PARAM_SPOT_FONT:
		m_FontName = (char *) Value;
		RemoveBrokenText();
		BreakText();
		break;
	case PARAM_SPOT_COLORNORMAL:
		m_ColorNormal = (int) Value;
		break;
	case PARAM_SPOT_COLORSELECT:
		m_ColorSelect = (int) Value;
		break;
	case PARAM_SPOT_TEXTHALIGNMENT:
		m_TextHAlignment = (SPOTTEXTALIGNMENT) (int)Value;
		break;
	case PARAM_SPOT_TEXTVALIGNMENT:
		m_TextVAlignment = (SPOTTEXTALIGNMENT) (int)Value;
		break;
	case PARAM_SPOT_TEXTBREAK:
		m_TextBreak = (int)Value;
		break;
	case PARAM_SPOT_TEXTOFFSETX:
		m_TextOffsetX = (int)Value;
		break;
	case PARAM_SPOT_TEXTOFFSETY:
		m_TextOffsetY = (int)Value;
		break;
	default:
		break;
	}
}

void CSpotText::RemoveBrokenText()
{
	char **Ptr;
	if(m_BrokenText==0) return;

	Ptr = m_BrokenText;
	while(*Ptr != 0)
	{
		free(*Ptr);
		Ptr++;
	}
	free (m_BrokenText);
	m_BrokenText = 0;
}

#define NEWLINECHAR '^'
#define ADDLINETOARRAY(Idx, Len) 			Lines[LineNr] = (char *)malloc(Len+1); \
			stdlib_memcpy(Lines[LineNr], m_Text.c_str()+Idx, Len); \
			Lines[LineNr][Len]=0; \
			LineNr++; \
			Lines = (char **)realloc(Lines, (LineNr+1)*4); \
			Lines[LineNr]=0;


void CSpotText::BreakText()
{
	char **Lines=0; // array where the broken lines will be stored
	int	LineNr=0;   // The Number of stored lines so far

	int textlen = m_Text.length();
	if (textlen == 0) return;
	if(m_W == 0) return;

	int startIndex=0;
	int lastSpaceIndex = 0;
	int currentWidth;
	int fontWidth;
	int fontSpacing;
	CSpotFont *Font;
	Font = GetSpotMaster()->GetFont(m_FontName);
	if(Font==0) return;
	fontSpacing = Font->GetFontSpacing();
	fontWidth = Font->GetFontWidth();

	currentWidth = fontSpacing;

	Lines = (char **)malloc((LineNr+1)*4);
	Lines[LineNr]=0;

	
	for (int index=0; index<textlen; index++)
	{
		//Check for a new line character
		if (m_Text.at(index) == NEWLINECHAR ||m_Text.at(index) == 0x0d)
		{
			ADDLINETOARRAY(startIndex, index-startIndex);

			startIndex = index+1;
			lastSpaceIndex = index+1;
			currentWidth = fontWidth;
			continue;
		}

		currentWidth += (fontSpacing+fontWidth);
		if (m_Text.at(index) == ' ') 
		{
			lastSpaceIndex = index;

			if (currentWidth >= m_W)
			{
				//This line of text extends the width
				if (lastSpaceIndex == startIndex)
				{
					//there is one word that won't fit on the entire line, just cut it
					ADDLINETOARRAY(startIndex, index-startIndex);
					
					startIndex = index;
					lastSpaceIndex = index;
					currentWidth = fontWidth;
				}
				else
				{
					//Normal cut
					ADDLINETOARRAY(startIndex, lastSpaceIndex-startIndex);

					index = lastSpaceIndex+1;
					startIndex = lastSpaceIndex+1;
					currentWidth = fontWidth;
				}
			}
		}
	}

	if (startIndex < textlen)
	{
		ADDLINETOARRAY(startIndex, textlen-startIndex);
	}

	m_BrokenText = Lines;
}
