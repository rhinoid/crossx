#ifndef _H_SPOTTEXT
#define _H_SPOTTEXT

#include "gui/spot.h"

class CSpotText : public CSpot
{
public:
	CSpotText(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int ColorNormal, int ColorSelect, XString FontName, XString Text);
	~CSpotText();

	virtual void		MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void		Draw(CSurface *DestSurface, int OffsetX, int OffsetY);

	virtual void	   *GetParam(SPOTPARAMS Param);	// get value of param indicated by button/dial/slider
	virtual void		SetParam(SPOTPARAMS Param, void *Value);	// set param for button/dial/slider
private:
	void				RemoveBrokenText();		// Clean up previously broken text
	void				BreakText();

	BUTTSTATES			m_InternalState;		// internal state of button

	int					m_ColorNormal;
	int					m_ColorSelect;
	SPOTTEXTALIGNMENT	m_TextHAlignment;
	SPOTTEXTALIGNMENT	m_TextVAlignment;
	int                 m_TextBreak;            // does text break of at the edge?

	int					m_TextOffsetX;			// x offset of text in control
	int					m_TextOffsetY;			// y offset of text in control

	XString             m_Text;	                // Original text
	char              **m_BrokenText;           // broken text
	XString             m_FontName;				// Logical name of the used font
}; 


#endif
