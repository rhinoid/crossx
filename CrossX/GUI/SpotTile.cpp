#include "gui/spotTile.h"
#include "2d/surfacecache.h"
#include "utils/globals.h"

/////////////////////////////////////////
// CSpotTile Methods
/////////////////////////////////////////


CSpotTile::CSpotTile(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int Borders, XString ResIDNormal, XString ResIDSelect) : CSpot(Parent, Id, x, y, w, h)
{
	m_InternalState = BUTT_NORMAL;

	m_BorderMask = Borders;

	m_Transparency = 255;

	m_Normal = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDNormal, GetSpotMaster()->GetRootSurface()->GetMode());
	m_Select = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDSelect, GetSpotMaster()->GetRootSurface()->GetMode());
}

CSpotTile::~CSpotTile()
{
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Normal);
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Select);
}

void CSpotTile::MouseMsg(MOUSEMSG Msg, int x, int y)
{
	BUTTSTATES NewState;

	NewState = m_InternalState;

	switch(Msg)
	{
	case MMSG_NOTHING:
		//ignore
		break;
	case MMSG_NORMAL:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_HIGHLIGHT:
		NewState = BUTT_HIGHLIGHT;
		break;
	case MMSG_SELECT:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_MOUSEMOVE:
		//ignore
		break;
	}

	if(m_InternalState != NewState)
	{
		m_InternalState = NewState;
	}
}

void CSpotTile::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	CSurface *SelSurface=0;
	switch(m_InternalState)
	{
	case BUTT_NORMAL:
		SelSurface = m_Normal;
		break;
	case BUTT_HIGHLIGHT:
		SelSurface = m_Select;
		break;
	default: ASSERT(0);
		break;
	}
	ASSERT(SelSurface);

    int FrameLookupTab[9] = {4,4,4,4,4,4,4,4,4};
    int FrameNr=0;
    int x, y, xs, ys, w, h;

    // fill in the framelookuptab according to the bordermask
    if ( (m_BorderMask & TILEBORDER_LEFT) != 0)
    {
        FrameLookupTab[0] -= 1;
        FrameLookupTab[3] -= 1;
        FrameLookupTab[6] -= 1;
    }
    if ( (m_BorderMask & TILEBORDER_TOP) != 0)
    {
        FrameLookupTab[0] -= 3;
        FrameLookupTab[1] -= 3;
        FrameLookupTab[2] -= 3;
    }
    if ( (m_BorderMask & TILEBORDER_RIGHT) != 0)
    {
        FrameLookupTab[2] += 1;
        FrameLookupTab[5] += 1;
        FrameLookupTab[8] += 1;
    }
    if ( (m_BorderMask & TILEBORDER_BOTTOM) != 0)
    {
        FrameLookupTab[6] += 3;
        FrameLookupTab[7] += 3;
        FrameLookupTab[8] += 3;
    }

    xs = (OffsetX+m_X);
    ys = (OffsetY+m_Y);
    w = SelSurface->GetWidth()/9;
    h = SelSurface->GetHeight();

    int StartX, XEnd, DeltaX;
    int StartY, YEnd, DeltaY;

    StartX = xs + w;
    StartY = ys + h;

    XEnd = xs + m_W - w - w;
    YEnd = ys + m_H - h - h;

    int NumPiecesX = (XEnd-StartX) / w;
    int NumPiecesY = (YEnd-StartY) / h;

    if((NumPiecesX * w) != (XEnd-StartX)) NumPiecesX++;  //take 1 extra
    if((NumPiecesY * h) != (YEnd-StartY)) NumPiecesY++;  //take 1 extra

    StartX<<=8;
    StartY<<=8;
    XEnd<<=8;
    YEnd<<=8;

    DeltaX = (XEnd-StartX) / NumPiecesX;
    DeltaY = (YEnd-StartY) / NumPiecesY;

    int PosX, PosY;
    PosX = StartX;
    PosY = StartY;

	int NextPosX, NextPosY;
	int ThisWidth, ThisHeight;
    for(y=0; y<=NumPiecesY; y++)
    {
		NextPosY = PosY + DeltaY;
		ThisHeight = (NextPosY>>8)-(PosY>>8);

        PosX = StartX;
        for(x=0; x<=NumPiecesX; x++)
        {
			NextPosX = PosX + DeltaX;
			ThisWidth = (NextPosX>>8)-(PosX>>8);
            if(x==0) // do left side too
            {
				DestSurface->Blit(SelSurface, w*FrameLookupTab[3], 0, (PosX>>8)-w, (PosY>>8), w, ThisHeight, m_Transparency);
            }
            else if(x==(NumPiecesX)) // do right side too
            {
				DestSurface->Blit(SelSurface, w*FrameLookupTab[5], 0, (PosX>>8)+ThisWidth, (PosY>>8), w, ThisHeight, m_Transparency);
            }
            if(y==0) // do top side too
            {
				DestSurface->Blit(SelSurface, w*FrameLookupTab[1], 0, (PosX>>8), (PosY>>8)-h, ThisWidth, h, m_Transparency);
            }
            else if(y==(NumPiecesY)) // do bottom side too
            {
				DestSurface->Blit(SelSurface, w*FrameLookupTab[7], 0, (PosX>>8), (PosY>>8)+ThisHeight, ThisWidth, h, m_Transparency);
            }
			DestSurface->Blit(SelSurface, w*FrameLookupTab[4], 0, (PosX>>8), (PosY>>8), ThisWidth, ThisHeight, m_Transparency);

            PosX += DeltaX;
        }
        PosY += DeltaY;
    }
    PosX -= DeltaX;
    PosY -= DeltaY;
    //corners
	DestSurface->Blit(SelSurface, w*FrameLookupTab[0], 0, xs, ys, w, h, m_Transparency);
	DestSurface->Blit(SelSurface, w*FrameLookupTab[2], 0, (PosX>>8)+ThisWidth, ys, w, h, m_Transparency);
	DestSurface->Blit(SelSurface, w*FrameLookupTab[6], 0, xs, (PosY>>8)+ThisHeight, w, h, m_Transparency);
	DestSurface->Blit(SelSurface, w*FrameLookupTab[8], 0, (PosX>>8)+ThisWidth, (PosY>>8)+ThisHeight, w, h, m_Transparency);

	CSpot::Draw(DestSurface, OffsetX, OffsetY);
}
