#ifndef _H_SPOTTILE
#define _H_SPOTTILE

#include "gui/spot.h"

///////////////////////////////////////////////////////////

#define TILEBORDER_LEFT   (1)
#define TILEBORDER_RIGHT  (2)
#define TILEBORDER_TOP    (4)
#define TILEBORDER_BOTTOM (8)

class CSpotTile : public CSpot
{
public:
	CSpotTile(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int Borders, XString ResIDNormal, XString ResIDSelect);
	CSpotTile();
	~CSpotTile();

	virtual void MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);
private:
	BUTTSTATES	m_InternalState;		// internal state of button

	int			m_BorderMask;  // see defines...  which borders of the tilemap are displayed?

	CSurface   *m_Normal;
	CSurface   *m_Select;
};


#endif
