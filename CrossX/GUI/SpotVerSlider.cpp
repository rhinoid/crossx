#include "gui/spotverslider.h"
#include "2d/surfacecache.h"
#include "utils/globals.h"
#include "utils/debug.h"

/////////////////////////////////////////
// CSpotVerSlider Methods
/////////////////////////////////////////


CSpotVerSlider::CSpotVerSlider(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int Min, int Max, int SlideHeight, char *ResIDNormal, char *ResIDHigh) : CSpot(Parent, Id, x, y, w, h)
{
	m_InternalState = BUTT_NORMAL;

	m_Transparency = 255;

	m_Normal = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDNormal, GetSpotMaster()->GetRootSurface()->GetMode());
	m_Highlight = CSurfaceCache::GetSurfaceCache()->LoadSurface(ResIDHigh, GetSpotMaster()->GetRootSurface()->GetMode());

	m_Min = Min;
	m_Max = Max;

	m_SlideY1 = y;
	m_SlideY2 = y+SlideHeight;
}

CSpotVerSlider::~CSpotVerSlider()
{
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Normal);
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Highlight);
}

void CSpotVerSlider::MouseMsg(MOUSEMSG Msg, int x, int y)
{
	int NewY;
	BUTTSTATES NewState;

	NewState = m_InternalState;

	switch(Msg)
	{
	case MMSG_NOTHING:
		//ignore
		break;
	case MMSG_NORMAL:
		NewState = BUTT_NORMAL;
		m_StartY = y;
		break;
	case MMSG_HIGHLIGHT:
		m_StartY = y;
		NewState = BUTT_HIGHLIGHT;
		break;
	case MMSG_SELECT:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_MOUSEMOVE:
		// try dragging the button
		NewY = GetY() + (y-m_StartY);
		
		if(NewY< m_SlideY1) NewY = m_SlideY1;
		if(NewY> m_SlideY2) NewY = m_SlideY2;

		if(NewY!=GetY())
		{
			m_StartY += (NewY-GetY());
			SetPos(GetX(), NewY);
		}

		break;
	}

	if(m_InternalState != NewState)
	{
		m_InternalState = NewState;
	}
}

void *CSpotVerSlider::GetParam(SPOTPARAMS Param)
{
	void *rc;
	rc = CSpot::GetParam(Param);
	if((int)rc!=-1) return rc;

	switch(Param)
	{
	case PARAM_SPOT_NUMBER:
		{
			int Value;
			Value = GetY()-m_SlideY1;
			Value *= (m_Max-m_Min);
			Value /= (m_SlideY2-m_SlideY1);
			return (void *)Value;
		}
		break;
	default:
		break;
	}
	return 0;
}

void CSpotVerSlider::SetParam(SPOTPARAMS Param, void *Value)
{
	CSpot::SetParam(Param, Value);

	switch(Param)
	{
	case PARAM_SPOT_NUMBER:
		{
			int Val;
			Val = (int) Value;
			Val *= (m_SlideY2-m_SlideY1);
			Val /= (m_Max-m_Min);
			Val += m_SlideY1;

			if(Val< m_SlideY1) Val = m_SlideY1;
			if(Val> m_SlideY2) Val = m_SlideY2;

			if(Val!=GetY())
			{
				SetPos(GetX(), Val);
			}
		}
		break;
	default:
		break;
	}
}


void CSpotVerSlider::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	CSurface *SelSurface=0;
	switch(m_InternalState)
	{
	case BUTT_NORMAL:
		SelSurface = m_Normal;
		break;
	case BUTT_HIGHLIGHT:
		SelSurface = m_Highlight;
		break;
	default: ASSERT(0);
		break;
	}
	ASSERT(SelSurface);
	DestSurface->Blit(SelSurface, 0, 0, m_X + OffsetX, m_Y + OffsetY, m_W, m_H, m_Transparency);

	CSpot::Draw(DestSurface, OffsetX, OffsetY);
}

