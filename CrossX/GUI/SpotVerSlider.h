#ifndef _H_SPOTVERSLIDER
#define _H_SPOTVERSLIDER

#include "gui/spot.h"

///////////////////////////////////////////////////////////
class CSpotVerSlider : public CSpot
{
public:
	CSpotVerSlider(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int Min, int Max, int SlideHeight, char *ResIDNormal, char *ResIDHigh);
	CSpotVerSlider();
	~CSpotVerSlider();

	virtual void MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);

	virtual void *GetParam(SPOTPARAMS Param);	// get value of param indicated by button/dial/slider
	virtual void  SetParam(SPOTPARAMS Param, void *Value);	// set param for button/dial/slider
private:
	
	BUTTSTATES	m_InternalState;		// internal state of button

	int		m_Min;
	int		m_Max;

	int		m_StartY;

	int		m_SlideY1;
	int		m_SlideY2;

	CSurface   *m_Normal;
	CSurface   *m_Highlight;
};

#endif
