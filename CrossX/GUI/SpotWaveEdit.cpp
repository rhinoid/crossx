#include "gui/spotwaveedit.h"
//#include "gui/spotmaster.h"
#include "2d/surfacecache.h"
#include "utils/globals.h"
#include "utils/debug.h"
#include <stdlib.h>
#include <stdio.h>

/////////////////////////////////////////
// CSpotWaveEdit Methods
/////////////////////////////////////////


CSpotWaveEdit::CSpotWaveEdit(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int OffsetX, int OffsetY, int WaveWidth, int WaveHeight, int ByteFlg, int SampleLength, int DataOffset, void *DataBuffer, char *Normal) : CSpot(Parent, Id, x, y, w, h)
{
	m_InternalState = BUTT_NORMAL;

	m_Transparency = 255;

	m_Normal = CSurfaceCache::GetSurfaceCache()->LoadSurface(Normal, GetSpotMaster()->GetRootSurface()->GetMode());

	m_Color      = 0;

	m_OffsetX = OffsetX;
	m_OffsetY = OffsetY;
	m_WaveWidth = WaveWidth;
	m_WaveHeight = WaveHeight;
	m_DataBuffer = DataBuffer;
	m_DataBuffer2 = 0;				//no 2nd data buffer per default (used to have 2 buffers with similar wave data)

	m_SampleLength = SampleLength;
	m_ByteFlg = ByteFlg;
	m_DataOffset = DataOffset;

	m_StartPoint = -1;
	m_LoopPoint = -1;
	m_EndPoint = -1;

	m_EditEnable = 1;

}

CSpotWaveEdit::~CSpotWaveEdit()
{
	CSurfaceCache::GetSurfaceCache()->RemoveSurface(m_Normal);
}


void *CSpotWaveEdit::GetParam(SPOTPARAMS Param)
{
	void *rc;
	rc = CSpot::GetParam(Param);
	if((int)rc!=-1) return rc;

	switch(Param)
	{
	case PARAM_SPOT_COLORNORMAL:
		return (void *)m_Color;
		break;
	default:
		break;
	}
	return 0;
}

void CSpotWaveEdit::SetParam(SPOTPARAMS Param, void *Value)
{
	CSpot::SetParam(Param, Value);

	switch(Param)
	{
	case PARAM_SPOT_COLORNORMAL:
		m_Color = (int) Value;
		break;
	case PARAM_SPOT_WAVESETSOURCE:
		m_DataBuffer = Value;
		break;
	case PARAM_SPOT_WAVESET2NDDEST:
		m_DataBuffer2 = Value;
		break;
	case PARAM_SPOT_WAVESETLENGTH:
		m_SampleLength = (int) Value;
		break;
	case PARAM_SPOT_WAVEEDITENABLE:
		m_EditEnable = (int) Value;
		break;
	case PARAM_SPOT_SETSTARTPOINT:
		m_StartPoint = (int) Value;
		break;
	case PARAM_SPOT_SETLOOPPOINT:
		m_LoopPoint = (int) Value;
		break;
	case PARAM_SPOT_SETENDPOINT:
		m_EndPoint = (int) Value;
		break;
	default:
		break;
	}
}

void CSpotWaveEdit::MouseMsg(MOUSEMSG Msg, int x, int y)
{
	BUTTSTATES NewState;

	NewState = m_InternalState;

	switch(Msg)
	{
	case MMSG_NOTHING:
		//ignore
		break;
	case MMSG_NORMAL:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_HIGHLIGHT:
		NewState = BUTT_HIGHLIGHT;
		PlotPix(x, y, false);
		if(m_EditEnable==0)
		{
			int PosX;
			PosX = x - m_OffsetX;
			PosX -= m_X;
			if(PosX>=0 && PosX<m_WaveWidth)
			{
				PosX *= m_SampleLength;
				PosX /= m_WaveWidth;
				GetSpotMaster()->SendMsg(GetPath(), GetID(), SPOT_BUTTON_LOCATOR, PosX, 0);
			}
		}
		break;
	case MMSG_SELECT:
		NewState = BUTT_NORMAL;
		break;
	case MMSG_MOUSEMOVE:
		if(m_InternalState == BUTT_HIGHLIGHT)
		{
			PlotPix(x, y, true);
		}
		break;
	}

	if(m_InternalState != NewState)
	{
		m_InternalState = NewState;
	}
}


void CSpotWaveEdit::PlotPix(int x, int y, bool InterpolateFlg)
{
	if(!m_EditEnable) return;
	x -= GetX();
	y -= GetY();
	char *WavePtr8;
	short *WavePtr16;
	char *WavePtr8Shadow;
	short *WavePtr16Shadow;
	int PosX, Sample;
	int SampleFixedPoint;
	int	DeltaFixedPoint;
	int StartX, EndX;
	int StartY, EndY;

	PosX = x - m_OffsetX;
	Sample = y - m_OffsetY;
	if(Sample < 0) Sample = 0;
	if(Sample>= m_WaveHeight) Sample = m_WaveHeight-1;
	if(PosX < 0) PosX = 0;
	if(PosX>= m_WaveWidth) PosX = m_WaveWidth-1;

	if(InterpolateFlg==false)
	{
		m_PrevPosX = PosX;
		m_PrevPosY = Sample;
	}

	if(PosX<m_PrevPosX)
	{
		StartX = PosX;
		EndX = m_PrevPosX;
		StartY = Sample;
		EndY = m_PrevPosY;
	}
	else
	{
		StartX = m_PrevPosX;
		EndX = PosX;
		StartY = m_PrevPosY;
		EndY = Sample;
	}
	

	int	  SamplePos;	//fixed 8 bit
	int	  SampleStep;	//fixed 8bit
	//prep fixed point
	SampleStep = m_SampleLength;
	SampleStep <<= 8;
	SampleStep /= m_WaveWidth;
	SamplePos = 0;


	StartX *= SampleStep;
	EndX *= SampleStep;
	EndX += (SampleStep-1);

	int temp;
	SampleFixedPoint = StartY<<8;
	temp = EndY<<8;
	if(m_ByteFlg==1)
	{
		SampleFixedPoint *= 256;
		temp *= 256;
	}
	else
	{
		SampleFixedPoint *= 65536;
		temp *= 65536;
	}
	SampleFixedPoint /= m_WaveHeight;
	temp             /= m_WaveHeight;
	SampleFixedPoint -= m_DataOffset<<8;
	temp             -= m_DataOffset<<8;

	DeltaFixedPoint = ((temp<<8)-(SampleFixedPoint<<8))/((EndX+1)-StartX);

	WavePtr8 = (char *)m_DataBuffer;
	WavePtr16 = (short *)m_DataBuffer;
	WavePtr8Shadow = (char *)m_DataBuffer2;
	WavePtr16Shadow = (short *)m_DataBuffer2;
	while(1)	//drawline from prevpos -> pos
	{
		int CurSample;
		CurSample = SampleFixedPoint>>8;
		if(m_ByteFlg==1)
		{
			CurSample +=1;
			WavePtr8[StartX>>8] = CurSample;
			if(WavePtr8Shadow) WavePtr8Shadow[StartX>>8] = CurSample;
		}
		else
		{
			CurSample += 0x100;
			WavePtr16[StartX>>8] = CurSample;
			if(WavePtr16Shadow) WavePtr16Shadow[StartX>>8] = CurSample;
		}
		SampleFixedPoint += DeltaFixedPoint;
		StartX+=256;
		if(StartX>EndX) break;
	}

	m_PrevPosX = PosX;
	m_PrevPosY = Sample;

}

void CSpotWaveEdit::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	char *WavePtr8;
	short *WavePtr16;
	int i;
	int	  SamplePos;	//fixed 8 bit
	int	  SampleStep;	//fixed 8bit

	DestSurface->Blit(m_Normal, 0, 0, m_X + OffsetX, m_Y + OffsetY, m_W, m_H, m_Transparency);

// first draw start/loop and endpoints

	if(m_StartPoint!=-1)
	{
		int PosX, PosY;
		PosX = m_StartPoint * m_WaveWidth;
		PosX /= m_SampleLength;

		for(PosY=0; PosY<=m_WaveHeight; PosY++)
		{
			DestSurface->PlotPix(m_X + PosX + OffsetX + m_OffsetX, m_Y + PosY + OffsetY +  m_OffsetY, 0x00ffff, m_Transparency);
		}
	}

	if(m_LoopPoint!=-1)
	{
		int PosX, PosY;
		PosX = m_LoopPoint * m_WaveWidth;
		PosX /= m_SampleLength;

		for(PosY=0; PosY<=m_WaveHeight; PosY++)
		{
			DestSurface->PlotPix(m_X + PosX + OffsetX + m_OffsetX, m_Y + PosY + OffsetY +  m_OffsetY, 0xffff00, m_Transparency);
		}
	}

	if(m_EndPoint!=-1)
	{
		int PosX, PosY;
		PosX = m_EndPoint * m_WaveWidth;
		PosX /= m_SampleLength;

		for(PosY=0; PosY<=m_WaveHeight; PosY++)
		{
			DestSurface->PlotPix(m_X + PosX + OffsetX + m_OffsetX, m_Y + PosY + OffsetY +  m_OffsetY, 0xff0000, m_Transparency);
		}
	}


	//prep fixed point
	SampleStep = m_SampleLength;
	SampleStep <<= 8;
	SampleStep /= m_WaveWidth;
	SamplePos = 0;


	WavePtr8 = (char *)m_DataBuffer;
	WavePtr16 = (short *)m_DataBuffer;
	for(i=0; i<m_WaveWidth; i++)
	{
		int Sample;

		Sample = 0;

		if(m_ByteFlg==1)
		{
			Sample = WavePtr8[SamplePos>>8];
			Sample += m_DataOffset;
			Sample *= m_WaveHeight;
			Sample /= 256;
		}
		else
		{
			Sample = WavePtr16[SamplePos>>8];
			Sample += m_DataOffset;
			Sample *= m_WaveHeight;
			Sample /= 65536;
		}
		SamplePos += SampleStep;

		DestSurface->PlotPix(m_X + OffsetX + i + m_OffsetX, m_Y + OffsetY + Sample + m_OffsetY, m_Color, m_Transparency);
	}

	CSpot::Draw(DestSurface, OffsetX, OffsetY);
}
