#ifndef _H_SPOTWAVEEDIT
#define _H_SPOTWAVEEDIT

#include "gui/spot.h"

///////////////////////////////////////////////////////////
class CSpotWaveEdit : public CSpot
{
public:
	CSpotWaveEdit(CSpotMaster *Parent, XString Id, int x, int y, int w, int h, int OffsetX, int OffsetY, int WaveWidth, int WaveHeight, int ByteFlg, int SampleLength, int DataOffset, void *DataBuffer, char *Normal);
	CSpotWaveEdit();
	~CSpotWaveEdit();

	virtual void MouseMsg(MOUSEMSG Msg, int x, int y);
	virtual void Draw(CSurface *DestSurface, int OffsetX, int OffsetY);

	virtual void *GetParam(SPOTPARAMS Param);	// get value of param indicated by button/dial/slider
	virtual void  SetParam(SPOTPARAMS Param, void *Value);	// set param for button/dial/slider
private:
	void		PlotPix(int x, int y, bool InterpolateFlg);
	
	BUTTSTATES	m_InternalState;		// internal state of button

	int			m_Color;				// color of pixels

	int			m_OffsetX;				// offset of wave in graphic
	int			m_OffsetY;				// offset of wave in graphic
	int			m_WaveWidth;			// size of wave in graphic
	int			m_WaveHeight;			// size of wave in graphic

	int			m_ByteFlg;				// byte or word samples?
	int			m_SampleLength;			// Length of sample to display
	int			m_DataOffset;			// data offset (where is the 0-line on the sample)
	
	void	   *m_DataBuffer;			// actual wave bytes
	void	   *m_DataBuffer2;			// actual wave bytes (shadow image)

	int			m_PrevPosX;				// prev pos for line drawing
	int			m_PrevPosY;				// prev pos for line drawing

	int			m_EditEnable;			// is the control editable?

	int			m_StartPoint;			// where should the start point be drawn? (-1 is off)
	int			m_LoopPoint;			// ""
	int			m_EndPoint;				// ""

	CSurface   *m_Normal;
};

#endif
