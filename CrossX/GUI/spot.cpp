#include "spot.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "utils/clib.h"
#include "utils/debug.h"


/////////////////////////////////////////
// CSpot Methods
/////////////////////////////////////////

CSpot::CSpot(CSpotMaster *Parent, XString Id, int x, int y, int w, int h)
{
	ASSERT(w>0);
	ASSERT(h>0);

	m_Child = 0;
	m_SpotMaster = Parent;
	m_NextBrother = 0;
	m_Parent = 0;
	m_ID = Id;
	m_X = x;
	m_Y = y;
	m_W = w;
	m_H = h;
	m_Transparency = 255;
	m_Enabled = 1;
}


CSpot::~CSpot()
{
}

void CSpot::DelAll()
{
	CSpot *CurIt;

	CurIt = GetChild();
	while(CurIt)
	{
		CSpot *NextIt;
		CurIt->DelAll();
		NextIt = CurIt->GetNextNode();
		delete CurIt;
		CurIt = NextIt;
	}
	m_Child = 0;
}

void	CSpot::SetNextNode(CSpot *NextNode)
{
	m_NextBrother = NextNode;
}

CSpot   *CSpot::GetNextNode(void)
{
	return m_NextBrother;
}

void	CSpot::SetParentNode(CSpot *ParentNode)
{
	m_Parent = ParentNode;
}

CSpot   *CSpot::GetParentNode(void)
{
	return m_NextBrother;
}

int		CSpot::GetX()
{
	return m_X;
}

int		CSpot::GetY()
{
	return m_Y;
}

int		CSpot::GetX2()
{
	return m_X+m_W;
}

int		CSpot::GetY2()
{
	return m_Y+m_H;
}

int		CSpot::GetW()
{
	return m_W;
}

int		CSpot::GetH()
{
	return m_H;
}

XString		CSpot::GetID()
{
	return m_ID;
}

void		CSpot::SetID(XString Name)
{
	m_ID = Name;
}

XString CSpot::GetPath()
{
	if(m_Parent==0)
	{
		return m_ID;
	}
	else
	{
		XString ParentName;
		ParentName = m_Parent->GetPath();
		if(ParentName=="root")
		{
			return m_ID;
		}
		else
		{
			return m_Parent->GetPath() + "\\" + m_ID;
		}
	}
}

void	CSpot::SetPos(int x, int y)
{
	m_X = x;
	m_Y = y;
}

void *CSpot::GetParam(SPOTPARAMS Param)
{
	switch(Param)
	{
	case PARAM_SPOT_TRANSPARENCY:
		return (void *)m_Transparency;
		break;
	case PARAM_SPOT_ENABLE:
		return (void *)m_Enabled;
		break;
	default:
		break;
	}
	return (void *)-1;
}

void CSpot::SetParam(SPOTPARAMS Param, void *Value)
{
	switch(Param)
	{
	case PARAM_SPOT_TRANSPARENCY:
		m_Transparency = (int) Value;
		break;
	case PARAM_SPOT_ENABLE:
		m_Enabled = (int) Value;
		break;
	case PARAM_SPOT_DIMENSIONS:
		{
			int IntValue;
			int w,h;
			IntValue = (int)Value;
			h = IntValue>>16;
			w = IntValue&0xffff;
			m_W = w;
			m_H = h;
		}
		break;
	default:
		break;
	}
}

bool CSpot::MessageOveride(XString ButtonPath, XString ButtonName, MOUSEMSG Msg, int Param1, int Param2)
{
	return true;
}


void CSpot::AddSpot(CSpot *NewSpot)
{
	CSpot *SpotIt;
	ASSERT(NewSpot);

	XString Path = NewSpot->GetID();   // thing has just been made... is still a full path
	XString PathElement;
	int Idx;
	Idx = Path.find('\\', 0);
	if(Idx<0) // last element of path? then add it to the list
	{
		// uitzondering... het is de eerste button in de list
		if(m_Child == 0)
		{
			m_Child = NewSpot;
			NewSpot->SetNextNode(0);
			NewSpot->SetParentNode(this);   // link back to our parent
			return;
		}

		// einde van list zoeken
		SpotIt = m_Child;
		while(SpotIt->GetNextNode() != 0) SpotIt = SpotIt->GetNextNode();

		// button aan eind toevoegen
		SpotIt->SetNextNode(NewSpot);
		NewSpot->SetNextNode(0);		// nieuwe end of list marker
		NewSpot->SetParentNode(this);   // link back to our parent
		return;
	}
	else // parse first bit of path... and recurse
	{
		PathElement.assign(Path.c_str(), Idx);

		Path.SubString(Idx+1);

		//now patch the name of the control to match the subpath
		NewSpot->SetID(Path);

		// now search for the parent control
		SpotIt = m_Child;
		while(SpotIt != 0)
		{
			if(SpotIt->GetID() == PathElement)
			{
				SpotIt->AddSpot(NewSpot);   // And recurse
				return;
			}
			SpotIt = SpotIt->GetNextNode();
		}
	}
}

CSpot  *CSpot::GetChild()
{
	return m_Child;
}

void CSpot::DelSpot(XString ControlName)
{
	CSpot *SpotIt;
	CSpot *PrevSpotIt;
	PrevSpotIt = 0;
	SpotIt = m_Child;
	while(SpotIt->GetNextNode() != 0)
	{
		if(SpotIt->GetID() == ControlName) // found the control
		{
			if(SpotIt == m_Child) //uitzondering
			{
				m_Child = SpotIt->GetNextNode();

				delete SpotIt;
				return;
			}
			else
			{
				PrevSpotIt->SetNextNode(SpotIt->GetNextNode());

				delete SpotIt;
				return;
			}
		}
		PrevSpotIt = SpotIt;
		SpotIt = SpotIt->GetNextNode();
	}
}

CSpot  *CSpot::SearchControl(XString Path)
{
	int Idx;
	XString PathElement;

	Idx = Path.find('\\', 0);
	if(Idx==0) // last element of path?
	{
		PathElement = Path;
	}
	else // parse first bit of path... and recurse
	{
		PathElement.assign(Path.c_str(), Idx);

		Path.assign(Path.c_str()+Idx+1, Path.length()-Idx+1);
	}

	// search control with the specific name
	CSpot *SpotIt;
	SpotIt = m_Child;
	while(SpotIt->GetNextNode() != 0)
	{
		if(SpotIt->GetID() == PathElement) // found the control
		{
			if(Idx==0) // last control of path?
			{
				return this;
			}
			else
			{
				return SpotIt->SearchControl(Path);
			}
		}
		SpotIt = SpotIt->GetNextNode();
	}
	return 0;
}


CSpot  *CSpot::GetHit(int x, int y)
{
	CSpot *BestHit;
	CSpot *SpotIt;

	BestHit = 0;
	if(x>=m_X && x<(m_X+m_W) && y>=m_Y && y<(m_Y+m_H))
	{
		if(m_Enabled)
		{
			if(m_ID.at(0)=='I' && m_ID.at(1)=='D' && m_ID.at(2)=='_') // Name has to start with ID_... otherwise it is not a button but just a gui element
			{
			BestHit = this;
			}
		}
	}

	SpotIt = m_Child;
	while(SpotIt)
	{
		CSpot *NewBestHit;
		NewBestHit = SpotIt->GetHit(x - m_X, y - m_Y);
		if(NewBestHit)
		{
			BestHit = NewBestHit;
		}

		SpotIt = SpotIt->GetNextNode();
	}
	return BestHit;
}


void CSpot::Draw(CSurface *DestSurface, int OffsetX, int OffsetY)
{
	CSpot *SpotIT;

	OffsetX += m_X;
	OffsetY += m_Y;

	SpotIT = m_Child;
	while(SpotIT)
	{
   		// Set clipping so no drawing appears outside of this parent button region
		int cx, cy;
		SRect ClipRect;
		cx = OffsetX;
		cy = OffsetY;
		ClipRect.SetRect(cx, cy, cx + m_W, cy + m_H);
		DestSurface->SetClipping(ClipRect);
		SpotIT->Draw(DestSurface, OffsetX, OffsetY);

		SpotIT = SpotIT->GetNextNode();
	}
}
