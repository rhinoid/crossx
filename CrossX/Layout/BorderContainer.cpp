#include "layout/bordercontainer.h"
#include "layout/layoutmgr.h"

//Image container
BorderContainer::~BorderContainer()
{
	if(m_Container) delete m_Container;
}

int		    BorderContainer::GetMinX()
{
	if(m_Container) return m_Container->GetMinX();
	return LAYOUT_VALUEDONTCARE;
}

int		    BorderContainer::GetMinY()
{
	if(m_Container) return m_Container->GetMinY();
	return LAYOUT_VALUEDONTCARE;
}

int		    BorderContainer::GetNrOfContainers()
{
	return (m_Container==0) ? 0 : 1;
}

Container  *BorderContainer::GetContainerNr(int Idx)
{
	return m_Container;
}


int BorderContainer::SetWidth(int PosX, int Width)
{
	int MinX = GetMinX();
	if(Width<MinX && MinX!=LAYOUT_VALUEDONTCARE) // requested width smaller then minimal width?
	{
		Width = MinX; //correct it
	}

	m_X = PosX;
	m_X2 = PosX+Width;

	// calc the correct image pos
	int LeftX, RightX;
	if(m_LeftPrefX == LAYOUT_VALUEDONTCARE) m_LeftPrefX = 0;
	if(m_LeftPrefX&LAYOUT_PERCENTAGE && m_LeftPrefX>0)
	{
		int Pref;
		Pref = m_LeftPrefX & ~LAYOUT_PERCENTAGE;
		LeftX = (((m_X2-m_X)*Pref)/100)+m_X;
	}
	else
	{
		if(m_LeftPrefX<0) LeftX = m_X2+m_LeftPrefX;
		else LeftX = m_X+m_LeftPrefX;
	}
	if(m_RightPrefX == LAYOUT_VALUEDONTCARE) m_RightPrefX = 100|LAYOUT_PERCENTAGE;
	if(m_RightPrefX&LAYOUT_PERCENTAGE && m_RightPrefX>0)
	{
		int Pref;
		Pref = m_RightPrefX & ~LAYOUT_PERCENTAGE;
		RightX = (((m_X2-m_X)*Pref)/100)+m_X;
	}
	else
	{
		if(m_RightPrefX<0) RightX = m_X2+m_RightPrefX;
		else RightX = m_X+m_RightPrefX;
	}
	//check for out of bounds condition
	if(LeftX<m_X) LeftX = m_X;
	if(LeftX>m_X2) LeftX = m_X2;
	if(RightX<m_X) RightX = m_X;
	if(RightX>m_X2) RightX = m_X2;

	//check for right and left overlap
	if(RightX<LeftX)
	{
		LeftX = (RightX+LeftX)/2;
		RightX = LeftX;
	}

	m_X = LeftX;
	m_X2 = RightX;

	//propagate settings
	if(m_Container) m_Container->SetWidth(m_X, m_X2-m_X);

	return Width;
}


int BorderContainer::SetHeight(int PosY, int Height)
{
	int MinY = GetMinY();
	if(Height<MinY && MinY!=LAYOUT_VALUEDONTCARE) // requested width smaller then minimal width?
	{
		Height = MinY; //correct it
	}

	m_Y = PosY;
	m_Y2 = PosY+Height;

	// calc the correct image pos
	int TopY, BottomY;
	if(m_TopPrefY == LAYOUT_VALUEDONTCARE) m_TopPrefY = 0;
	if(m_TopPrefY&LAYOUT_PERCENTAGE && m_TopPrefY>0)
	{
		int Pref;
		Pref = m_TopPrefY & ~LAYOUT_PERCENTAGE;
		TopY = (((m_Y2-m_Y)*Pref)/100)+m_Y;
	}
	else
	{
		if(m_TopPrefY<0) TopY = m_Y2+m_TopPrefY;
		else TopY = m_Y+m_TopPrefY;
	}
	if(m_BottomPrefY == LAYOUT_VALUEDONTCARE) m_BottomPrefY = 100|LAYOUT_PERCENTAGE;
	if(m_BottomPrefY&LAYOUT_PERCENTAGE && m_BottomPrefY>0)
	{
		int Pref;
		Pref = m_BottomPrefY & ~LAYOUT_PERCENTAGE;
		BottomY = (((m_Y2-m_Y)*Pref)/100)+m_Y;
	}
	else
	{
		if(m_BottomPrefY<0) BottomY = m_Y2+m_BottomPrefY;
		else BottomY = m_Y+m_BottomPrefY;
	}

	//check for out of bounds condition
	if(TopY<m_Y) TopY = m_Y;
	if(TopY>m_Y2) TopY = m_Y2;
	if(BottomY<m_Y) BottomY = m_Y;
	if(BottomY>m_Y2) BottomY = m_Y2;

	//check for right and left overlap
	if(BottomY<TopY)
	{
		TopY = (TopY+BottomY)/2;
		BottomY = TopY;
	}

	m_Y = TopY;
	m_Y2 = BottomY;
	
	//propagate settings
	if(m_Container) m_Container->SetHeight(m_Y, m_Y2-m_Y);

	return Height;
}

