#ifndef _H_BORDERCONTAINER
#define _H_BORDERCONTAINER

#include "layout/container.h"

class BorderContainer : public Container
{
public:
	BorderContainer() : m_Container(0), m_LeftPrefX(0), m_RightPrefX(0), m_TopPrefY(0), m_BottomPrefY(0) {};
	~BorderContainer();
	int		    GetNrOfContainers();
	Container  *GetContainerNr(int Idx);

	int			GetMinX();
	int			GetMinY();

	int         SetWidth(int PosX, int Width);
	int         SetHeight(int PosY, int Height);

	//border specific parameters
	Container  *m_Container;    //Optional subcontainer 
	int			m_LeftPrefX;	//left margin
	int			m_RightPrefX;	//right margin
	int			m_TopPrefY;		//top margin
	int			m_BottomPrefY;	//bottom margin
};

#endif
