#ifndef _H_CONTAINER
#define _H_CONTAINER

#include "utils/xstring.h"

class Container 
{
public:
	Container() {};
	virtual ~Container();

	virtual int		    GetNrOfContainers() = 0;
	virtual Container  *GetContainerNr(int Idx) = 0;
	virtual int			GetMinX() = 0;
	virtual int			GetMinY() = 0;

	virtual int         SetWidth(int PosX, int Width) = 0;
	virtual int         SetHeight(int PosY, int Height) = 0;

	XString m_Name;			// name of this container.

	// The resulting coordinates of this container after layouting
	int   m_X;
	int   m_Y;
	int   m_X2;
	int   m_Y2;
};

#endif
