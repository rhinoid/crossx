#include "layout/fixedcontainer.h"
#include "layout/layoutmgr.h"
#include <stdlib.h>

//Image container
FixedContainer::~FixedContainer()
{
	if(m_FileName) free(m_FileName);
}

int		    FixedContainer::GetMinX()
{
    if(m_FixedWidth == LAYOUT_VALUEDONTCARE)
    {
        if(m_Container) return m_Container->GetMinX();
        return LAYOUT_VALUEDONTCARE;
    }
    else
    {
        if(m_Container==0) return m_FixedWidth;
		int ContWidth = m_Container->GetMinX();
		if(ContWidth == LAYOUT_VALUEDONTCARE) return m_FixedWidth;
		if(ContWidth>m_FixedWidth) return ContWidth;
        return m_FixedWidth;
    }
}

int		    FixedContainer::GetMinY()
{
    if(m_FixedHeight == LAYOUT_VALUEDONTCARE)
    {
        if(m_Container) return m_Container->GetMinY();
        return LAYOUT_VALUEDONTCARE;
    }
    else
    {
        if(m_Container==0) return m_FixedHeight;
		int ContHeight = m_Container->GetMinY();
		if(ContHeight == LAYOUT_VALUEDONTCARE) return m_FixedHeight;
		if(ContHeight>m_FixedHeight) return ContHeight;
        return m_FixedHeight;
    }
}

int		    FixedContainer::GetNrOfContainers()
{
	return (m_Container) ? 1 : 0;
}

Container  *FixedContainer::GetContainerNr(int Idx)
{
	return m_Container;
}


int FixedContainer::SetWidth(int PosX, int Width)
{
	int MinWidth = GetMinX();
    if(MinWidth != LAYOUT_VALUEDONTCARE)
    {
        if (Width < MinWidth) // requested width smaller then minimal width?
        {
            Width = MinWidth; //correct it
        }
    }

	m_X = PosX;
	m_X2 = PosX+Width;

	// calc the correct image pos
    int UseWidth;
    UseWidth = (m_FixedWidth == LAYOUT_VALUEDONTCARE) ? Width : m_FixedWidth; //max size or fixed width?

    int ImagePosX;
    if(((m_PrefX & LAYOUT_PERCENTAGE)!=0) && (m_PrefX>0))
    {
        int Pref;
        Pref = m_PrefX & ~LAYOUT_PERCENTAGE;
        ImagePosX = ((((m_X2-m_X)-UseWidth)*Pref)/100)+m_X;
    }
    else
    {
        if(m_PrefX<0) ImagePosX = (m_X2-UseWidth)+m_PrefX;
        else ImagePosX = m_X+m_PrefX;
    }
    //check for out of bounds condition
    if(ImagePosX<m_X) ImagePosX = m_X;
    if(ImagePosX>(m_X2-UseWidth)) ImagePosX = m_X2-UseWidth;
    m_X = ImagePosX;
    m_X2 = ImagePosX+UseWidth;

    //propagate settings if necessary
    if(m_Container)
    {
        m_Container->SetWidth(m_X, m_X2-m_X);
    }

    return Width;
}


int FixedContainer::SetHeight(int PosY, int Height)
{
	int MinHeight = GetMinY();
    if(MinHeight != LAYOUT_VALUEDONTCARE)
    {
        if (Height < MinHeight) // requested width smaller then minimal width?
        {
            Height = MinHeight; //correct it
        }
    }

    m_Y = PosY;
    m_Y2 = PosY+Height;

    // calc the correct image pos
    int UseHeight;
    UseHeight = (m_FixedHeight == LAYOUT_VALUEDONTCARE) ? Height : m_FixedHeight; //max size or fixed height?

    int ImagePosY;
    if(((m_PrefY & LAYOUT_PERCENTAGE)!=0) && (m_PrefY>0))
    {
        int Pref;
        Pref = m_PrefY & ~LAYOUT_PERCENTAGE;
        ImagePosY = ((((m_Y2-m_Y)-UseHeight)*Pref)/100)+m_Y;
    }
    else
    {
        if(m_PrefY<0) ImagePosY = (m_Y2-UseHeight)+m_PrefY;
        else ImagePosY = m_Y+m_PrefY;
    }
    //check for out of bounds condition
    if(ImagePosY<m_Y) ImagePosY = m_Y;
    if(ImagePosY>(m_Y2-UseHeight)) ImagePosY = m_Y2-UseHeight;

    m_Y = ImagePosY;
    m_Y2 = ImagePosY+UseHeight;

    //propagate settings if necessary
    if(m_Container)
    {
        m_Container->SetHeight(m_Y, m_Y2-m_Y);
    }
    return Height;
}
