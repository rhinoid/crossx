#ifndef _H_FIXEDCONTAINER
#define _H_FIXEDCONTAINER

#include "layout/container.h"

class FixedContainer : public Container
{
public:
	FixedContainer() : m_Container(0), m_FixedWidth(0), m_FixedHeight(0), m_FileName(0) {};
	~FixedContainer();
	int		    GetNrOfContainers();
	Container  *GetContainerNr(int Idx);

	int			GetMinX();
	int			GetMinY();

	int         SetWidth(int PosX, int Width);
	int         SetHeight(int PosY, int Height);

	int			m_PrefX;
	int			m_PrefY;
	
	
	// Fixed Specific parameters
	Container  *m_Container;    //Optional subcontainer 
	char       *m_FileName;		// if this is not give, the width and height should be given.
	int			m_FixedWidth;  // these follow from the image
	int			m_FixedHeight; // these follow from the image 

};

#endif
