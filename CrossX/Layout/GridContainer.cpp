#include "layout/gridcontainer.h"
#include "layout/layoutmgr.h"
#include <stdlib.h>

//Grid container
GridContainer::~GridContainer()
{
	int i;
	for(i=0; i<(m_NumHorGrids*m_NumVerGrids); i++)
	{
		delete m_Containers[i];
	}
	delete [] (m_Containers);
}

int			GridContainer::GetMinX()
{
	int x;
	int TotalWidth = 0;
	bool TotalWidthDontCareFlag = true; //if 1 element has a specific width, the total width is not a dontcare anymore

	//add all max width per column together
	for(x=0; x<m_NumHorGrids; x++)
	{
		int ColumnWidth;
		ColumnWidth = GetColumnWidth(x);
		if(ColumnWidth != LAYOUT_VALUEDONTCARE)
		{
			TotalWidthDontCareFlag = false;
			TotalWidth += ColumnWidth;
		}
	}
	if(TotalWidthDontCareFlag)return LAYOUT_VALUEDONTCARE;
	return TotalWidth;
}

int			GridContainer::GetMinY()
{
	int y;
	int TotalHeight = 0;
	bool TotalHeightDontCareFlag = true; //if 1 element has a specific height, the total height is not a dontcare anymore

	//add all max height per row together
	for(y=0; y<m_NumVerGrids; y++)
	{
		int RowHeight;
		RowHeight = GetRowHeight(y);
		if(RowHeight != LAYOUT_VALUEDONTCARE)
		{
			TotalHeightDontCareFlag = false;
			TotalHeight += RowHeight;
		}
	}
	if(TotalHeightDontCareFlag)return LAYOUT_VALUEDONTCARE;
	return TotalHeight;
}

int	 GridContainer::GetColumnWidth(int x)
{
	int y;
	int ColumnWidth = LAYOUT_VALUEDONTCARE;

	//obtain minimal height of row possible
	for(y=0; y<m_NumVerGrids; y++)
	{
		int ElementWidth;
		ElementWidth = GetContainerNr(y*m_NumHorGrids + x)->GetMinX();
		if(ElementWidth != LAYOUT_VALUEDONTCARE)
		{
			if(ElementWidth > ColumnWidth || ColumnWidth==LAYOUT_VALUEDONTCARE) ColumnWidth = ElementWidth;
		}
	}
	return ColumnWidth;
}

int	 GridContainer::GetRowHeight(int y)
{
	int x;
	int RowHeight = LAYOUT_VALUEDONTCARE;

	//obtain minimal height of row possible
	for(x=0; x<m_NumHorGrids; x++)
	{
		int ElementHeight;
		ElementHeight = GetContainerNr(y*m_NumHorGrids + x)->GetMinY();
		if(ElementHeight != LAYOUT_VALUEDONTCARE)
		{
			if(ElementHeight > RowHeight || RowHeight==LAYOUT_VALUEDONTCARE) RowHeight = ElementHeight;
		}
	}
	return RowHeight;
}

int GridContainer::SetWidth(int PosX, int Width)
{
	int MinWidth = GetMinX();
	if(Width<MinWidth && MinWidth!=LAYOUT_VALUEDONTCARE) // requested width smaller then minimal width?
	{
		Width = GetMinX(); //correct it
	}

	int *ColumnMinWidths;
	int *ColumnConfirmedWidths;
	int ColumnWidth;
	int i;
	int x;

	ColumnMinWidths = new int[m_NumHorGrids];
	ColumnConfirmedWidths = new int[m_NumHorGrids];
	for(i=0; i<m_NumHorGrids; i++)
	{
		ColumnMinWidths[i] = GetColumnWidth(i);
		ColumnConfirmedWidths[i] = LAYOUT_VALUEDONTCARE;
	}

	int DistributeWidth;
	DistributeWidth = Width;
	while(true)
	{
		for(x=0; x<m_NumHorGrids; x++)
		{
			ColumnWidth = ((DistributeWidth*(x+1))/m_NumHorGrids) - ((DistributeWidth*x)/m_NumHorGrids);
			if(ColumnConfirmedWidths[x]==LAYOUT_VALUEDONTCARE)
			{
				if(ColumnWidth < ColumnMinWidths[x] && ColumnMinWidths[x] != LAYOUT_VALUEDONTCARE)
				{
					DistributeWidth -= ColumnMinWidths[x];
					ColumnConfirmedWidths[x] = ColumnMinWidths[x];
					break; //break the loop and redistribute with the remained
				}
			}
		}
		if(x==m_NumHorGrids)break;
	}

	// count the number of free gridcolumns
	int FreeGridColumns=0;
	for(x=0; x<m_NumHorGrids; x++)
	{
		if(ColumnConfirmedWidths[x] == LAYOUT_VALUEDONTCARE) FreeGridColumns++;
	}

	// propagate settings through grid
	int Pos = PosX;
	for(x=0; x<m_NumHorGrids; x++)
	{
		int y;
		if(ColumnConfirmedWidths[x] == LAYOUT_VALUEDONTCARE)
		{
			ColumnWidth = ((DistributeWidth*(x+1))/FreeGridColumns) - ((DistributeWidth*x)/FreeGridColumns);
		}
		else
		{
			ColumnWidth = ColumnConfirmedWidths[x];
		}
		for(y=0; y<m_NumVerGrids; y++)
		{
			m_Containers[y*m_NumHorGrids + x]->SetWidth(Pos, ColumnWidth);
		}
		Pos += ColumnWidth;
	}

	delete ColumnMinWidths;
	delete ColumnConfirmedWidths;


	// global container settings
	m_X = PosX;
	m_X2 = PosX+Width;
	return Width;
}


int GridContainer::SetHeight(int PosY, int Height)
{
	int MinHeight = GetMinY();
	if(Height<MinHeight && MinHeight!=LAYOUT_VALUEDONTCARE) // requested height smaller then minimal height?
	{
		Height = GetMinY(); //correct it
	}

	int *RowMinHeights;
	int *RowConfirmedHeights;
	int RowHeight;
	int i;
	int y;

	RowMinHeights = new int[m_NumVerGrids];
	RowConfirmedHeights = new int[m_NumVerGrids];
	for(i=0; i<m_NumVerGrids; i++)
	{
		RowMinHeights[i] = GetRowHeight(i);
		RowConfirmedHeights[i] = LAYOUT_VALUEDONTCARE;
	}

	int DistributeHeight;
	DistributeHeight = Height;
	while(true)
	{
		for(y=0; y<m_NumVerGrids; y++)
		{
			RowHeight = ((DistributeHeight*(y+1))/m_NumVerGrids) - ((DistributeHeight*y)/m_NumVerGrids);
			if(RowConfirmedHeights[y]==LAYOUT_VALUEDONTCARE)
			{
				if(RowHeight < RowMinHeights[y] && RowMinHeights[y] != LAYOUT_VALUEDONTCARE)
				{
					DistributeHeight -= RowMinHeights[y];
					RowConfirmedHeights[y] = RowMinHeights[y];
					break; //break the loop and redistribute with the remained
				}
			}
		}
		if(y==m_NumVerGrids)break;
	}

	// count the number of free gridcolumns
	int FreeGridRows=0;
	for(y=0; y<m_NumVerGrids; y++)
	{
		if(RowConfirmedHeights[y] == LAYOUT_VALUEDONTCARE) FreeGridRows++;
	}

	// propagate settings through grid
	int Pos = PosY;
	for(y=0; y<m_NumVerGrids; y++)
	{
		int x;
		if(RowConfirmedHeights[y] == LAYOUT_VALUEDONTCARE)
		{
			RowHeight = ((DistributeHeight*(y+1))/FreeGridRows) - ((DistributeHeight*y)/FreeGridRows);
		}
		else
		{
			RowHeight = RowConfirmedHeights[y];
		}
		for(x=0; x<m_NumHorGrids; x++)
		{
			m_Containers[y*m_NumHorGrids + x]->SetHeight(Pos, RowHeight);
		}
		Pos += RowHeight;
	}

	delete [] RowMinHeights;
	delete [] RowConfirmedHeights;

	m_Y = PosY;
	m_Y2 = PosY+Height;
	return Height;
}


int		    GridContainer::GetNrOfContainers()
{
	return m_NumHorGrids*m_NumVerGrids;
}


Container  *GridContainer::GetContainerNr(int Idx)
{
	if(Idx>=0 && Idx<GetNrOfContainers())	return m_Containers[Idx];
	return 0;
}
