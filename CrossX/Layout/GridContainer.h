#ifndef _H_GRIDCONTAINER
#define _H_GRIDCONTAINER

#include "layout/container.h"

class GridContainer : public Container
{
public:
	GridContainer() : m_NumHorGrids(0), m_NumVerGrids(0),m_Containers(0)  {};
	~GridContainer();
	int		    GetNrOfContainers();
	Container  *GetContainerNr(int Idx);

	int			GetMinX();
	int			GetMinY();

	int         SetWidth(int PosX, int Width);
	int         SetHeight(int PosY, int Height);

	int			m_NumHorGrids;
	int			m_NumVerGrids;
	Container **m_Containers; //the sub containers

private:
	int			GetColumnWidth(int x); //get average width of column
	int			GetRowHeight(int y); //get average height of row
};

#endif
