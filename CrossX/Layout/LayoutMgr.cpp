#include "layout/layoutmgr.h"
#include "layout/splitcontainer.h"
#include "layout/fixedcontainer.h"
#include "layout/bordercontainer.h"
#include "layout/gridcontainer.h"
#include "layout/listcontainer.h"
#include "utils/clib.h"
#include "utils/debug.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "tinyxml/tinyxml.h"
#include "utils/globals.h"
#include "2d/Surface32.h"

//Layoutscreen
LayoutMgr::~LayoutMgr()
{
	if(m_Container) delete m_Container;
}

bool LayoutMgr::CalcLayout(int Width, int Height)
{
	int minx, miny;

	if(m_Container==0) return false;
	minx = m_Container->GetMinX();
	miny = m_Container->GetMinY();
    if((minx&LAYOUT_MASKVALUE) > Width) return false;
    if((miny&LAYOUT_MASKVALUE) > Height) return false;

	m_Container->SetWidth(0, Width);
	m_Container->SetHeight(0, Height);

	return true;
}

bool LayoutMgr::InitLayoutXML(XString Filename)
{
	Filename = MakeFullPath(Filename);

	//parse the config file
	TiXmlDocument doc(Filename.c_str());
	bool loadOkay = doc.LoadFile();
	if (loadOkay)
	{
		TiXmlHandle hDoc(&doc);
		TiXmlElement* pElem;

		// block: root
		pElem=hDoc.FirstChildElement().Element();
		// should always have a valid root but handle gracefully if it does
		if (!pElem) return false;
		if(stdlib_stricmp(pElem->Value(), "layout")!=0) return false; //first element should be 'layout'


		TiXmlElement* pChildElem=0;
		m_Container = ParseContainer(pElem, &pChildElem); // return the root container
	}
	else
	{
		return false;
	}

	return (m_Container!=0);
}

Container *LayoutMgr::ParseContainer(TiXmlElement* hParentElem, TiXmlElement** phChildElem)
{
	if(*phChildElem == 0)
	{
		*phChildElem = hParentElem->FirstChildElement( );
	}
	else
	{
		*phChildElem = (*phChildElem)->NextSiblingElement();
	}

	if(*phChildElem == 0) return 0; // there is no child

	const char *pKey=(*phChildElem)->Value();
	if(stdlib_stricmp(pKey, "split")==0) //SplitContainer?
	{
		return ParseSplit(*phChildElem);
	}
	if(stdlib_stricmp(pKey, "fixed")==0) //FixedContainer?
	{
		return ParseFixed(*phChildElem);
	}
	if(stdlib_stricmp(pKey, "grid")==0) //GridContainer?
	{
		return ParseGrid(*phChildElem);
	}
	if(stdlib_stricmp(pKey, "list")==0) //ListContainer?
	{
		return ParseList(*phChildElem);
	}
	if(stdlib_stricmp(pKey, "border")==0) //BorderContainer?
	{
		return ParseBorder(*phChildElem);
	}
	return 0;
}

Container *LayoutMgr::ParseSplit(TiXmlElement* pElem)
{
	SplitContainer *NewSplit;
	NewSplit = new SplitContainer();

	//parse attributes
	TiXmlAttribute* pAttrib=pElem->FirstAttribute();
	while (pAttrib)
	{
		if(stdlib_stricmp(pAttrib->Name(), "name") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewSplit->m_Name = Val;
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "type") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				if(stdlib_strnicmp(Val, "hor", 3)==0)
				{
					NewSplit->m_Type = LAYOUT_SPLITHOR;
				}
				else
				{
					NewSplit->m_Type = LAYOUT_SPLITVER;
				}
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "min") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewSplit->m_Min = ParseValue(Val);
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "max") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewSplit->m_Max = ParseValue(Val);
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "pref") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewSplit->m_Pref = ParseValue(Val);
			}
		}

		pAttrib=pAttrib->Next();
	}

	// now scan for the 2 embedded containers
	TiXmlElement* pChildElem=0;
	NewSplit->m_Containers[0] = ParseContainer(pElem, &pChildElem);
	if(NewSplit->m_Containers[0] == 0)
	{
		ASSERT(0);  // EXPECTED 2 childs for the split element
		return 0;
	}
	NewSplit->m_Containers[1] = ParseContainer(pElem, &pChildElem);
	if(NewSplit->m_Containers[1] == 0)
	{
		ASSERT(0);  // EXPECTED 2 childs for the split element
		return 0;
	}

	return NewSplit;
}

Container *LayoutMgr::ParseFixed(TiXmlElement* pElem)
{
	FixedContainer *NewFixed;
	NewFixed = new FixedContainer();

	//parse attributes
	TiXmlAttribute* pAttrib=pElem->FirstAttribute();
	while (pAttrib)
	{
		if(stdlib_stricmp(pAttrib->Name(), "name") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewFixed->m_Name = Val;
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "width") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				if(strlen(Val) > 0)
				{
					if(Val[0]>='0' && Val[0]<='9')
					{
				NewFixed->m_FixedWidth = atoi(Val);
			}
					else
					{
						CSurface32 Surf32;
						if(Surf32.Load(Val))
						{
							NewFixed->m_FixedWidth = Surf32.GetWidth();
						}
						else
						{
							//give an error here?  (@@@ add logging)
							NewFixed->m_FixedWidth = 1;
						}
					}
				}
				else
				{
					NewFixed->m_FixedWidth = LAYOUT_VALUEDONTCARE;
				}
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "height") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				if(strlen(Val) > 0)
				{
					if(Val[0]>='0' && Val[0]<='9')
					{
				NewFixed->m_FixedHeight = atoi(Val);
			}
					else
					{
						CSurface32 Surf32;
						if(Surf32.Load(Val))
						{
							NewFixed->m_FixedHeight = Surf32.GetHeight();
						}
						else
						{
							//give an error here?  (@@@ add logging)
							NewFixed->m_FixedHeight = 1;
						}
					}
				}
				else
				{
					NewFixed->m_FixedWidth = LAYOUT_VALUEDONTCARE;
				}
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "placingx") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewFixed->m_PrefX = ParseValue(Val);
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "placingy") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewFixed->m_PrefY = ParseValue(Val);
			}
		}

		pAttrib=pAttrib->Next();
	}

	// now scan for 1 optional embedded container
	TiXmlElement* pChildElem=0;
	NewFixed->m_Container = ParseContainer(pElem, &pChildElem);

	return NewFixed;
}


Container *LayoutMgr::ParseBorder(TiXmlElement* pElem)
{
	BorderContainer *NewBorder;
	NewBorder = new BorderContainer();

	//parse attributes
	TiXmlAttribute* pAttrib=pElem->FirstAttribute();
	while (pAttrib)
	{
		if(stdlib_stricmp(pAttrib->Name(), "name") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewBorder->m_Name = Val;
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "left") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewBorder->m_LeftPrefX = ParseValue(Val);
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "right") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewBorder->m_RightPrefX = ParseValue(Val);
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "top") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewBorder->m_TopPrefY = ParseValue(Val);
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "bottom") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewBorder->m_BottomPrefY = ParseValue(Val);
			}
		}

		pAttrib=pAttrib->Next();
	}

	// now scan for 1 optional embedded container
	TiXmlElement* pChildElem=0;
	NewBorder->m_Container = ParseContainer(pElem, &pChildElem);

	return NewBorder;
}

Container *LayoutMgr::ParseGrid(TiXmlElement* pElem)
{
	GridContainer *NewGrid;
	NewGrid = new GridContainer();

	//parse attributes
	TiXmlAttribute* pAttrib=pElem->FirstAttribute();
	while (pAttrib)
	{
		if(stdlib_stricmp(pAttrib->Name(), "name") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewGrid->m_Name = Val;
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "sizex") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewGrid->m_NumHorGrids = atoi(Val);
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "sizey") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewGrid->m_NumVerGrids = atoi(Val);
			}
		}

		pAttrib=pAttrib->Next();
	}

	// now scan for the embedded containers
	TiXmlElement* pChildElem=0;
	int i;
	NewGrid->m_Containers = new Container * [NewGrid->m_NumHorGrids*NewGrid->m_NumVerGrids];
	for(i=0; i<NewGrid->m_NumHorGrids*NewGrid->m_NumVerGrids; i++)
	{
		NewGrid->m_Containers[i] = ParseContainer(pElem, &pChildElem);
		if(NewGrid->m_Containers[i] == 0)
		{
			ASSERT(0);  // Not enough childs defined for the grid element
			return 0;
		}
	}

	return NewGrid;
}


Container *LayoutMgr::ParseList(TiXmlElement* pElem)
{
	ListContainer *NewList;
	NewList = new ListContainer();

	//parse attributes
	TiXmlAttribute* pAttrib=pElem->FirstAttribute();
	while (pAttrib)
	{
		if(stdlib_stricmp(pAttrib->Name(), "name") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewList->m_Name = Val;
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "size") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				NewList->m_NumElements = atoi(Val);
			}
		}
		if(stdlib_stricmp(pAttrib->Name(), "type") == 0)
		{
			const char *Val = pAttrib->Value();
			if(Val)
			{
				if(stdlib_strnicmp(Val, "hor", 3)==0)
				{
					NewList->m_ListType = LAYOUT_LISTHOR;
				}
				else
				{
					NewList->m_ListType = LAYOUT_LISTVER;
				}
			}
		}

		pAttrib=pAttrib->Next();
	}

	// now scan for the embedded containers
	TiXmlElement* pChildElem=0;
	int i;
	NewList->m_Containers = new Container * [NewList->m_NumElements];
	for(i=0; i<NewList->m_NumElements; i++)
	{
		NewList->m_Containers[i] = ParseContainer(pElem, &pChildElem);
		if(NewList->m_Containers[i] == 0)
		{
			ASSERT(0);  // Not enough childs defined for the list element
			return 0;
		}
	}

	return NewList;
}


//returns a layout value (taken percentage and sign into account)
int LayoutMgr::ParseValue(const char *Buffer)
{
	int Value;
	if(Buffer[0]==0) return LAYOUT_VALUEDONTCARE;

	Value = atoi(Buffer);
	if(Buffer[stdlib_strlen(Buffer)-1] == '%')
	{
		Value |= LAYOUT_PERCENTAGE;
	}
	return Value;
}

