#ifndef _H_LAYOUTMGR
#define _H_LAYOUTMGR

#include "layout/container.h"
#include "Utils/XString.h"

//these defines can be used with the Pref Values
#define LAYOUT_PERCENTAGE (0x20000000) // if a value has this bit set, the value is interpreted as an percentage instead of an absolute
#define LAYOUT_VALUEDONTCARE (0x40000000)  // if a value is this value...the value is ignored and not used

#define LAYOUT_MASKVALUE (0x00ffffff)        // only the pure value

class TiXmlElement;

class LayoutMgr
{
public:
	LayoutMgr() : m_Container(0) {};
	~LayoutMgr();
	bool InitLayoutXML(XString Filename);
	bool CalcLayout(int Width, int Height); // calc layout and return true if succeeded

	Container *m_Container;

private:
	Container  *ParseContainer(TiXmlElement* hParentElem, TiXmlElement** phChildElem);
	Container  *ParseSplit(TiXmlElement* pElem);
	Container  *ParseFixed(TiXmlElement* pElem);
	Container  *ParseGrid(TiXmlElement* pElem);
	Container  *ParseList(TiXmlElement* pElem);
	Container  *ParseBorder(TiXmlElement* pElem);
	
	int			ParseValue(const char *Buffer);

};


/* Layout definition

values which should be default values can be skipped
pref values can be absolute positive(pixels from the left side), absolute negative (pixels from the right side) or percentage (add the % sign)
commandlines can start with and end with whitespace...
comments may only be inserted in the whitespace area and should be put between '/'s    (so:  /hello/ is a comment)

split:name,hor|ver,min,max,pref
>
  split:name,hor|ver,min,max,pref
  >
    fixed:name,width,height,prefx,prefy
    border:name,leftprefx,rightprefx,topprefy,bottomprefy
  <
  fixed:name,width,height,prefx,prefy
<


Description of individual elements:
fixed: is a represntation of an image or other fixed size thing... the fixed has a fixed width and height. The position of fizedsize 
       rectangle can be given by the prefx and prefy values. prefx is the preferred positioning horizontally and prefy vertically.
	   Fixed has no childs

border: is a way to create a smaller space within the layout. you can set the preferred margins in the rectangle. (left, right, top, bottom)
        Border can have 1 or 0 childs. (as defined in the layout)

split:  splits a layout in 2 parts (vertically or horizontally). the preferred position of the split can be given.
        Splits have 2 childs

grid: is a spacial subdivision which evenly splits a grid of containers
        Grid can have any number of childs (as defined in the grid)



actual example

split:,ver,,,-80
>
  grid:testgrid,3,3
  >
    border:,,,,
    border:,,,,
    border:,,,,
    border:,,,,
    border:,,,,
    border:,,,,
    border:,,,,
    border:,,,,
    border:,,,,
  <
  split:,hor,,,50%
  >
    fixed:leftbutton,,40,40,10,50%
	border:testcontent,0,10%90%10%90%
  <
<

*/

#endif
