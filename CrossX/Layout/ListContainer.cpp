#include "layout/layoutmgr.h"
#include "layout/container.h"
#include "layout/listcontainer.h"
#include <stdlib.h>

ListContainer::~ListContainer()
{
	int i;
	for(i=0; i<m_NumElements; i++)
	{
		delete m_Containers[i];
	}
	delete [] (m_Containers);
}


//OVERRIDABLES (VIRTUAL FUNCTIONS)
int	ListContainer::GetNrOfContainers()
{
    return m_NumElements;
}

Container   *ListContainer::GetContainerNr(int Idx)
{
    if(Idx>=0 && Idx<GetNrOfContainers())	return m_Containers[Idx];
    return 0;
}

int	ListContainer::GetMinX()
{
    int y;
    if(m_ListType == LAYOUT_LISTVER)
    {
        int ColumnWidth = LAYOUT_VALUEDONTCARE;

        //obtain minimal width of column possible
        for (y = 0; y < m_NumElements; y++)
        {
            int ElementWidth;
            ElementWidth = GetContainerNr(y)->GetMinX();
            if (ElementWidth != LAYOUT_VALUEDONTCARE)
            {
                if (ElementWidth > ColumnWidth || ColumnWidth == LAYOUT_VALUEDONTCARE)
                    ColumnWidth = ElementWidth;
            }
        }
        return ColumnWidth;
    }
    else
    {
        return LAYOUT_VALUEDONTCARE;   // this list could be much bigger then the available space, so we don't care what is allocated for us
    }
}

int	ListContainer::GetMinY()
{
    int x;
    if(m_ListType == LAYOUT_LISTHOR)
    {
        int RowHeight = LAYOUT_VALUEDONTCARE;

        //obtain minimal height of row possible
        for (x = 0; x < m_NumElements; x++)
        {
            int ElementHeight;
            ElementHeight = GetContainerNr(x)->GetMinY();
            if (ElementHeight != LAYOUT_VALUEDONTCARE)
            {
                if (ElementHeight > RowHeight || RowHeight == LAYOUT_VALUEDONTCARE)
                    RowHeight = ElementHeight;
            }
        }
        return RowHeight;
    }
    else
    {
        return LAYOUT_VALUEDONTCARE;   // this list could be much bigger then the available space, so we don't care what is allocated for us
    }
}

int ListContainer::SetWidth(int PosX, int Width)
{
    int MinWidth = GetMinX();
    if(Width<MinWidth && MinWidth!=LAYOUT_VALUEDONTCARE) // requested width smaller then minimal width?
    {
        Width = MinWidth; //correct it
    }

    if(m_ListType == LAYOUT_LISTHOR)
    {
        int x;
        int StartX = PosX;
        for(x=0; x<m_NumElements; x++)  // propagate the width according to the individual elements needs
        {
            StartX += m_Containers[x]->SetWidth(StartX, 1);  // we offer them 1 pixel... if they need more they report it
        }
    }
    else
    {
        int x;
        for(x=0; x<m_NumElements; x++)  // propagate width of list to all elements
        {
            m_Containers[x]->SetWidth(PosX, Width);
        }
    }

    // global container settings
    m_X = PosX;
    m_X2 = PosX+Width;
    return Width;
}

int ListContainer::SetHeight(int PosY, int Height)
{
    int MinHeight = GetMinY();
    if(Height<MinHeight && MinHeight!=LAYOUT_VALUEDONTCARE) // requested width smaller then minimal width?
    {
        Height = MinHeight; //correct it
    }

    if(m_ListType == LAYOUT_LISTVER)
    {
        int y;
        int StartY = PosY;
        for(y=0; y<m_NumElements; y++)  // propagate the height according to the individual elements needs
        {
            StartY += m_Containers[y]->SetHeight(StartY, 1);  // we offer them 1 pixel... if they need more they report it
        }
    }
    else
    {
        int y;
        for(y=0; y<m_NumElements; y++)  // propagate height of list to all elements
        {
            m_Containers[y]->SetHeight(PosY, Height);
        }
    }

    // global container settings
    m_Y = PosY;
    m_Y2 = PosY+Height;
    return Height;
}

