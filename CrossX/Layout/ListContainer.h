#ifndef _H_LISTCONTAINER
#define _H_LISTCONTAINER

#define LAYOUT_LISTHOR (0)
#define LAYOUT_LISTVER (1)

class ListContainer : public Container
{
public:
	ListContainer() : m_ListType(LAYOUT_LISTHOR), m_NumElements(0),m_Containers(0)  {};
	~ListContainer();
	int		    GetNrOfContainers();
	Container  *GetContainerNr(int Idx);

	int			GetMinX();
	int			GetMinY();

	int         SetWidth(int PosX, int Width);
	int         SetHeight(int PosY, int Height);


	int			m_ListType;	// see constants
	int			m_NumElements;
	Container **m_Containers; //the sub containers
};

#endif
