#include "layout/splitcontainer.h"
#include "layout/layoutmgr.h"
#include <stdlib.h>

//Split container
SplitContainer::~SplitContainer()
{
	if(m_Containers[0]) delete m_Containers[0];
	if(m_Containers[1]) delete m_Containers[1];
}

int			SplitContainer::GetMinX()
{
	if(m_Type == LAYOUT_SPLITHOR)
	{
		int width1, width2;
		width1 = m_Containers[0]->GetMinX();
		width2 = m_Containers[1]->GetMinX();

		if(width1==LAYOUT_VALUEDONTCARE && width2==LAYOUT_VALUEDONTCARE) return LAYOUT_VALUEDONTCARE;
		if(width1==LAYOUT_VALUEDONTCARE) return width2;
		if(width2==LAYOUT_VALUEDONTCARE) return width1;
		return (width1+width2);
	}
	else
	{
		int width1, width2;
		width1 = m_Containers[0]->GetMinX();
		width2 = m_Containers[1]->GetMinX();

		if(width1==LAYOUT_VALUEDONTCARE && width2==LAYOUT_VALUEDONTCARE) return LAYOUT_VALUEDONTCARE;
		if(width1==LAYOUT_VALUEDONTCARE) return width2;
		if(width2==LAYOUT_VALUEDONTCARE) return width1;
		if(width1>width2) return width1;
		if(width2>width1) return width2;
		return width1;
	}
	return LAYOUT_VALUEDONTCARE;
}


int			SplitContainer::GetMinY()
{
	if(m_Type == LAYOUT_SPLITVER)
	{
		int height1, height2;
		height1 = m_Containers[0]->GetMinY();
		height2 = m_Containers[1]->GetMinY();

		if(height1==LAYOUT_VALUEDONTCARE && height2==LAYOUT_VALUEDONTCARE) return LAYOUT_VALUEDONTCARE;
		if(height1==LAYOUT_VALUEDONTCARE) return height2;
		if(height2==LAYOUT_VALUEDONTCARE) return height1;
		return (height1+height2);
	}
	else
	{
		int height1, height2;
		height1 = m_Containers[0]->GetMinY();
		height2 = m_Containers[1]->GetMinY();

		if(height1==LAYOUT_VALUEDONTCARE && height2==LAYOUT_VALUEDONTCARE) return LAYOUT_VALUEDONTCARE;
		if(height1==LAYOUT_VALUEDONTCARE) return height2;
		if(height2==LAYOUT_VALUEDONTCARE) return height1;
		if(height1>height2) return height1;
		if(height2>height1) return height2;
		return height1;
	}
	return LAYOUT_VALUEDONTCARE;
}


int SplitContainer::SetWidth(int PosX, int Width)
{
	if(Width<GetMinX()) // requested width smaller then minimal width?
	{
		Width = GetMinX(); //correct it
	}

	if(m_Type == LAYOUT_SPLITHOR)
	{
		//apply split preferences to width
		int width1, width2;
		if(m_Pref&LAYOUT_PERCENTAGE && m_Pref>0)
		{
			width1 = (Width*(m_Pref&~LAYOUT_PERCENTAGE))/100;
			width2 = Width-width1;
		}
		else
		{
			if(m_Pref<0)
			{
				width2 = -m_Pref;
				width1 = Width-width2;
			}
			else
			{
				width1 = m_Pref;
				width2 = Width-width1;
			}
			if(width1<0)
			{
				width1=0;
				width2=Width;
			}
			if(width2<0)
			{
				width2=0;
				width1=Width;
			}
		}

		//check if width1 is big enough...if not adjust
		int minx;
		minx = m_Containers[0]->GetMinX();
		if(minx != LAYOUT_VALUEDONTCARE && width1 < minx)
		{
			width1 = minx;
			width2 = Width-width1;
		}

		//check if width2 is big enough...if not adjust
		minx = m_Containers[1]->GetMinX();
		if(minx != LAYOUT_VALUEDONTCARE && width2 < minx)
		{
			width2 = minx;
			width1 = Width-width2;
		}

		// propagate settings
		m_Containers[0]->SetWidth(PosX, width1);
		m_Containers[1]->SetWidth(PosX+width1, width2);
	}
	else
	{
		//just propagate... the width didn't affect us
		m_Containers[0]->SetWidth(PosX, Width);
		m_Containers[1]->SetWidth(PosX, Width);
	}

	m_X = PosX;
	m_X2 = PosX+Width;
	return Width;
}


int SplitContainer::SetHeight(int PosY, int Height)
{
	if(Height<GetMinY()) // requested height smaller then minimal Height?
	{
		Height = GetMinY(); //correct it
	}

	if(m_Type == LAYOUT_SPLITVER)
	{
		//apply split preferences to height
		int height1, height2;
		if(m_Pref&LAYOUT_PERCENTAGE && m_Pref>0)
		{
			height1 = (Height*(m_Pref&~LAYOUT_PERCENTAGE))/100;
			height2 = Height-height1;
		}
		else
		{
			if(m_Pref<0)
			{
				height2 = -m_Pref;
				height1 = Height-height2;
			}
			else
			{
				height1 = m_Pref;
				height2 = Height-height1;
			}
			if(height1<0)
			{
				height1=0;
				height2=Height;
			}
			if(height2<0)
			{
				height2=0;
				height1=Height;
			}
		}

		//check if height1 is big enough...if not adjust
		int miny;
		miny = m_Containers[0]->GetMinY();
		if(miny != LAYOUT_VALUEDONTCARE && height1 < miny)
		{
			height1 = miny;
			height2 = Height-height1;
		}

		//check if width2 is big enough...if not adjust
		miny = m_Containers[1]->GetMinY();
		if(miny != LAYOUT_VALUEDONTCARE && height2 < miny)
		{
			height2 = miny;
			height1 = Height-height2;
		}

		// propagate settings
		m_Containers[0]->SetHeight(PosY, height1);
		m_Containers[1]->SetHeight(PosY+height1, height2);
	}
	else
	{
		//just propagate... the height didn't affect us
		m_Containers[0]->SetHeight(PosY, Height);
		m_Containers[1]->SetHeight(PosY, Height);
	}

	m_Y = PosY;
	m_Y2 = PosY+Height;
	return Height;
}


int		    SplitContainer::GetNrOfContainers()
{
	return 2;
}


Container  *SplitContainer::GetContainerNr(int Idx)
{
	if(Idx>=0 && Idx<2)	return m_Containers[Idx];
	return 0;
}

