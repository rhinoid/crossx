#ifndef _H_SPLITCONTAINER
#define _H_SPLITCONTAINER

#include "layout/container.h"

enum SPLITTYPE
{
	LAYOUT_SPLITVER = 0,
	LAYOUT_SPLITHOR
} ;


class SplitContainer : public Container
{
public:
	SplitContainer() : m_Min(0), m_Max(0),m_Pref(0)  {};
	~SplitContainer();
	int		    GetNrOfContainers();
	Container  *GetContainerNr(int Idx);

	int			GetMinX();
	int			GetMinY();

	int         SetWidth(int PosX, int Width);
	int         SetHeight(int PosY, int Height);

	SPLITTYPE	m_Type; // the type of split
	int			m_Min;  // minimal allowed split
	int			m_Max;  // maximal allowed split
	int			m_Pref; // preferred split

	Container  *m_Containers[2]; //the 2 sub containers
};

#endif
