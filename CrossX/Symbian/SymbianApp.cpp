/*
* ============================================================================
*  Name     : CSymbianApp from SymbianApp.cpp
*  Part of  : symbian
*  Created  : 30/12/2002 by Reinier
*  Implementation notes:
*
*     Initial content was generated by Series 60 AppWizard.
*  Version  :
*  Copyright: Overloaded
* ============================================================================
*/

// INCLUDE FILES
#include    "SymbianApp.h"
#include    "SymbianDocument.h"

// ================= MEMBER FUNCTIONS =======================

// ---------------------------------------------------------
// CSymbianApp::AppDllUid()
// Returns application UID
// ---------------------------------------------------------
//
TUid CSymbianApp::AppDllUid() const
    {
    return KUidsymbian;
    }

   
// ---------------------------------------------------------
// CSymbianApp::CreateDocumentL()
// Creates CSymbianDocument object
// ---------------------------------------------------------
//
CApaDocument* CSymbianApp::CreateDocumentL()
    {
    return CSymbianDocument::NewL( *this );
    }

// ================= OTHER EXPORTED FUNCTIONS ==============
//
// ---------------------------------------------------------
// NewApplication() 
// Constructs CSymbianApp
// Returns: created application object
// ---------------------------------------------------------
//
EXPORT_C CApaApplication* NewApplication()
    {
    return new CSymbianApp;
    }

// ---------------------------------------------------------
// E32Dll(TDllReason) 
// Entry point function for EPOC Apps
// Returns: KErrNone: No error
// ---------------------------------------------------------
//
GLDEF_C TInt E32Dll( TDllReason )
    {
    return KErrNone;
    }

// End of File  

