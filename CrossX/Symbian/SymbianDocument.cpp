/*
* ============================================================================
*  Name     : CSymbianDocument from SymbianDocument.h
*  Part of  : symbian
*  Created  : 30/12/2002 by Reinier
*  Implementation notes:
*     Initial content was generated by Series 60 AppWizard.
*  Version  :
*  Copyright: Overloaded
* ============================================================================
*/

// INCLUDE FILES
#include "SymbianDocument.h"
#include "SymbianAppUi.h"

// ================= MEMBER FUNCTIONS =======================

// constructor
CSymbianDocument::CSymbianDocument(CEikApplication& aApp)
: CAknDocument(aApp)    
    {
    }

// destructor
CSymbianDocument::~CSymbianDocument()
    {
    }

// EPOC default constructor can leave.
void CSymbianDocument::ConstructL()
    {
    }

// Two-phased constructor.
CSymbianDocument* CSymbianDocument::NewL(
        CEikApplication& aApp)     // CSymbianApp reference
    {
    CSymbianDocument* self = new (ELeave) CSymbianDocument( aApp );
    CleanupStack::PushL( self );
    self->ConstructL();
    CleanupStack::Pop();

    return self;
    }
    
// ----------------------------------------------------
// CSymbianDocument::CreateAppUiL()
// constructs CSymbianAppUi
// ----------------------------------------------------
//
CEikAppUi* CSymbianDocument::CreateAppUiL()
    {
    return new (ELeave) CSymbianAppUi;
    }

// End of File  
