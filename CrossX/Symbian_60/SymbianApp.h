/*
* ============================================================================
*  Name     : CSymbianApp from SymbianApp.h
*  Part of  : symbian
*  Created  : 30/12/2002 by Reinier
*  Description:
*     Declares main application class.
*  Version  :
*  Copyright: Overloaded
* ============================================================================
*/

#ifndef SYMBIANAPP_H
#define SYMBIANAPP_H

// INCLUDES
#include <aknapp.h>

// CONSTANTS
// UID of the application
const TUid KUidsymbian = { 0x0B6BE65F };

// CLASS DECLARATION

/**
* CSymbianApp application class.
* Provides factory to create concrete document object.
* 
*/
class CSymbianApp : public CAknApplication
    {
    
    public: // Functions from base classes
    private:

        /**
        * From CApaApplication, creates CSymbianDocument document object.
        * @return A pointer to the created document object.
        */
        CApaDocument* CreateDocumentL();
        
        /**
        * From CApaApplication, returns application's UID (KUidsymbian).
        * @return The value of KUidsymbian.
        */
        TUid AppDllUid() const;
    };

#endif

// End of File

