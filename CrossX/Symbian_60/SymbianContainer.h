/*
* ============================================================================
*  Name     : CSymbianContainer from SymbianContainer.h
*  Part of  : symbian
*  Created  : 29/11/2002 by Reinier
*  Description:
*     Declares container control for application.
*  Version  :
*  Copyright: Overloaded
* ============================================================================
*/

#ifndef SYMBIANCONTAINER_H
#define SYMBIANCONTAINER_H

// INCLUDES
#include <coecntrl.h>
   
// FORWARD DECLARATIONS
class CEikLabel;        // for example labels

// CLASS DECLARATION

#include "crossx.h"

/**
*  CSymbianContainer  container control class.
*  
*/
class CSymbianContainer : public CCoeControl, MCoeControlObserver
    {
    public: // Constructors and destructor
        
        /**
        * EPOC default constructor.
        * @param aRect Frame rectangle for container.
        */
        void ConstructL(const TRect& aRect);

        /**
        * Destructor.
        */
        ~CSymbianContainer();

    public: // New functions

    public: // Functions from base classes

    private: // Functions from base classes

       /**
        * From CoeControl,SizeChanged.
        */
        void SizeChanged();

       /**
        * From CoeControl,CountComponentControls.
        */
        TInt CountComponentControls() const;

       /**
        * From CCoeControl,ComponentControl.
        */
        CCoeControl* ComponentControl(TInt aIndex) const;

       /**
        * From CCoeControl,Draw.
        */
        void Draw(const TRect& aRect) const;

       /**
        * From ?base_class ?member_description
        */
        // event handling section
        // e.g Listbox events
        void HandleControlEventL(CCoeControl* aControl,TCoeEvent aEventType);


public:
		//SYMBIAN FRAMEWORK ADDITIONS

		bool HandleKey(TStdScanCode KeyCode);
		bool HandleKeyUp(TStdScanCode KeyCode);
		bool HandleKeyDown(TStdScanCode KeyCode);
        
		bool HeartBeat(void);
		void DrawGraphics();

		// Timer related function
		static TInt Period(TAny* aPtr);
		TInt Tick(void);

		int			m_Width;			// width of offscreen bitmap
		int			m_Height;			// height of offscreen bitmap
		int			m_Stride;			// stride of offscreen bitmap
		int			m_ExitFlg;
		int         m_InitFlag;

	  private: //data

		//SYMBIAN FRAMEWORK ADDITIONS
		CPeriodic* iPeriodicTimer;		// timer
		CFbsBitmap m_OffscreenBitmap;	// offscreen bitmap where we'll draw on
		TDisplayMode m_PixelFormat;		// pixelformat of offscreen bitmap

		int			m_LightDelay;		// delay counter to make sure we keep the backlight on
		TInt64			m_LastTick;


		CGame      *m_Game;
		CSurface16  m_BackBuffer;			// Wrapper for the Symbian buffer

    };

#endif

// End of File
