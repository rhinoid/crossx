/*
* ============================================================================
*  Name     : CSymbianDocument from SymbianDocument.h
*  Part of  : symbian
*  Created  : 30/12/2002 by Reinier
*  Description:
*     Declares document for application.
*  Version  :
*  Copyright: Overloaded
* ============================================================================
*/

#ifndef SYMBIANDOCUMENT_H
#define SYMBIANDOCUMENT_H

// INCLUDES
#include <akndoc.h>
   
// CONSTANTS

// FORWARD DECLARATIONS
class  CEikAppUi;

// CLASS DECLARATION

/**
*  CSymbianDocument application class.
*/
class CSymbianDocument : public CAknDocument
    {
    public: // Constructors and destructor
        /**
        * Two-phased constructor.
        */
        static CSymbianDocument* NewL(CEikApplication& aApp);

        /**
        * Destructor.
        */
        virtual ~CSymbianDocument();

    public: // New functions

    protected:  // New functions

    protected:  // Functions from base classes

    private:

        /**
        * EPOC default constructor.
        */
        CSymbianDocument(CEikApplication& aApp);
        void ConstructL();

    private:

        /**
        * From CEikDocument, create CSymbianAppUi "App UI" object.
        */
        CEikAppUi* CreateAppUiL();
    };

#endif

// End of File

