// HelloWorld_CExampleAppView.cpp
// ------------------------------
//
// Copyright (c) 2000 Symbian Ltd.  All rights reserved.
//

////////////////////////////////////////////////////////////////////////
//
// Source file for the implementation of the 
// application view class - CExampleAppView
//
////////////////////////////////////////////////////////////////////////

#include "CrossX_symbian.h"

//
//             Constructor for the view.
//
CExampleAppView::CExampleAppView()
	{
	}


//             Static NewL() function to start the standard two
//             phase construction.
//
CExampleAppView* CExampleAppView::NewL(const TRect& aRect)
	{
	CExampleAppView* self = new(ELeave) CExampleAppView();
	CleanupStack::PushL(self);
	self->ConstructL(aRect);
	CleanupStack::Pop();
	return self;
	}


//
//             Destructor for the view.
//
CExampleAppView::~CExampleAppView()
	{
	delete iPeriodicTimer;
	iPeriodicTimer = NULL;
	}


//             Second phase construction.
//
void CExampleAppView::ConstructL(const TRect& aRect)
{
	CreateWindowL();
	           // Extent of the control. This is
	           // the whole rectangle available to application.
	           // The rectangle is passed to us from the application UI.
	SetRect(aRect);
	SetExtentToWholeScreen();
			   // At this stage, the control is ready to draw so
	           // we tell the UI framework by activating it.
	ActivateL();


	// these values should be filled out by some kind of system query
	CWindowGc& gc = SystemGc();
	CWsScreenDevice *pWDevice=(CWsScreenDevice *)gc.Device();
	TPixelsAndRotation pix;
	pWDevice->GetDefaultScreenSizeAndRotation(pix);
	m_Width=pix.iPixelSize.iWidth;
	m_Height=pix.iPixelSize.iHeight;

	m_InitFlag = 1;
	m_ExitFlg = 0;
	m_PixelFormat = EColor4K;

	// create offscreen bitmap
	m_OffscreenBitmap.Create(TSize(m_Width,m_Height), m_PixelFormat);

	// Create a periodic timer
	iPeriodicTimer = CPeriodic::NewL(CActive::EPriorityStandard);
	iPeriodicTimer->Start(1,1,TCallBack(CExampleAppView::Period,this));

	TTime currentTime;
	currentTime.HomeTime();
	m_LastTick = currentTime.Int64();

	//toggle the backlight on
	m_LightDelay = 0;
}


//             Drawing the view - in this example, 
//             consists of drawing a simple outline rectangle
//             and then drawing the text in the middle.
//             We use the Normal font supplied by the UI.
//
//             In this example, we don't use the redraw
//             region because it's easier to redraw to
//             the whole client area.
//
void CExampleAppView::Draw(const TRect& /*aRect*/) const
{
}



// This function is called by the periodic timer
TInt CExampleAppView::Period(TAny *aPtr)
{
    //returning a value of TRUE indicates the callback should be done again
	return ((CExampleAppView*)aPtr)->Tick();
}

// non static variant of the timer function
TInt CExampleAppView::Tick()
{
	TInt RetVal;
	// make sure the backlight stays on
	m_LightDelay++;
	m_LightDelay&=63;
	if(m_LightDelay==0)
	{
		User::ResetInactivityTime(); 
	}

	if(m_InitFlag==0)
	{
		// Calculate elapsed time from last timer event in microseconds
		TTime currentTime;
		currentTime.HomeTime();
		TInt64 currentTick = currentTime.Int64();
		TInt64 frameTime = currentTick - m_LastTick;
		m_LastTick = currentTick;

		RetVal = m_Game->Tick(frameTime.GetTInt());
	}
	else
	{
		unsigned short *PixelBuffer;
		m_OffscreenBitmap.LockHeap(ETrue);
		PixelBuffer = (unsigned short *) m_OffscreenBitmap.DataAddress();
		m_BackBuffer.Create(PixelBuffer, m_Width, m_Height);
		m_OffscreenBitmap.UnlockHeap();

		m_Game = new CROSSX_GAMECLASS;
		m_Game->CRSX_SetBackbuf(&m_BackBuffer);	//Link the game to the backsurface
		m_Game->Init();						//initialize game
		m_InitFlag = 0;
	}

	// draw the graphics on screen
	CWindowGc& gc = SystemGc();
	gc.Activate(*DrawableWindow());
	gc.BitBlt(TPoint(0,0), &m_OffscreenBitmap);
	gc.Deactivate();

	if(m_ExitFlg)
	{
		User::Exit(0);
	}
	return RetVal ? 1 : 0;
}





bool CExampleAppView::HandleKey(TStdScanCode KeyCode)
{
	return false;
}


bool CExampleAppView::HandleKeyDown(TStdScanCode KeyCode)
{
	m_Game->HandleKeyDown(CROSSX_KEY_FIRE);
	return false;
}

bool CExampleAppView::HandleKeyUp(TStdScanCode KeyCode)
{
	m_Game->HandleKeyUp(CROSSX_KEY_FIRE);
	return false;
}

void CExampleAppView::HandlePointerEventL(const TPointerEvent& aPointerEvent)
{
    switch (aPointerEvent.iType)
    {
    case TPointerEvent::EButton1Down:
		m_Game->HandleMouseDown(aPointerEvent.iPosition.iX, aPointerEvent.iPosition.iY);
        break;

    case TPointerEvent::EButton1Up:
		m_Game->HandleMouseUp(aPointerEvent.iPosition.iX, aPointerEvent.iPosition.iY);
        break;

    case TPointerEvent::EDrag:
		m_Game->HandleMouseMove(aPointerEvent.iPosition.iX, aPointerEvent.iPosition.iY);
        break;
    default:
    break;
    }
}
