// HelloWorld.h
// ------------
//
// Copyright (c) 2000 Symbian Ltd.  All rights reserved.
//

////////////////////////////////////////////////////////////////////
// HelloWorld 
// ----------
//
//
// The class definitions for the simple example application
// containing a single view with the text "Hello World !" drawn
// on it.
//
// The class definitions are:
//
// CExampleApplication 
// CExampleAppUi
// CExampleAppView
// CExampleDocument
//
//
////////////////////////////////////////////////////////////////////
#ifndef __HELLOWORLD_H
#define __HELLOWORLD_H

#include "crossx.h"

#include <coeccntx.h>

#include <eikenv.h>
#include <eikappui.h>
#include <eikapp.h>
#include <eikdoc.h>
#include <eikmenup.h>

#include <eikon.hrh>

#include <crossx.rsg>



////////////////////////////////////////////////////////////////////////
//
// CExampleApplication
//
////////////////////////////////////////////////////////////////////////

class CExampleApplication : public CEikApplication
	{
private: 
	           // Inherited from class CApaApplication
	CApaDocument* CreateDocumentL();
	TUid AppDllUid() const;
	};

////////////////////////////////////////////////////////////////////////
//
// CExampleAppView
//
////////////////////////////////////////////////////////////////////////
class CGame;

class CExampleAppView : public CCoeControl
    {
public:
	static CExampleAppView* NewL(const TRect& aRect);
	CExampleAppView();
	~CExampleAppView();
    void ConstructL(const TRect& aRect);

public:
	           // Inherited from CCoeControl
	void Draw(const TRect& /*aRect*/) const;

	//SYMBIAN FRAMEWORK ADDITIONS
	bool Init(void);

	bool HandleKey(TStdScanCode KeyCode);
	bool HandleKeyUp(TStdScanCode KeyCode);
	bool HandleKeyDown(TStdScanCode KeyCode);
    
	void HandlePointerEventL(const TPointerEvent& aPointerEvent);

	TInt Tick(void);
	static TInt Period(TAny* aPtr);


	int			m_Width;			// width of offscreen bitmap
	int			m_Height;			// height of offscreen bitmap
	int			m_ExitFlg;
	int			m_LightDelay;		// delay counter to make sure we keep the backlight on
	int         m_InitFlag;

	CFbsBitmap m_OffscreenBitmap;	// offscreen bitmap where we'll draw on
	TDisplayMode m_PixelFormat;		// pixelformat of offscreen bitmap
	CPeriodic* iPeriodicTimer;		// timer
	TInt64	m_LastTick;				// How much time did the frame draw take?

	CGame      *m_Game;
	CSurface16  m_BackBuffer;			// Wrapper for the Symbian buffer


private:
    };


////////////////////////////////////////////////////////////////////////
//
// CExampleAppUi
//
////////////////////////////////////////////////////////////////////////
class CExampleAppUi : public CEikAppUi
    {
public:
    void ConstructL();
	~CExampleAppUi();

private:
              // Inherirted from class CEikAppUi
	void HandleCommandL(TInt aCommand);
	
        virtual TKeyResponse HandleKeyEventL(
            const TKeyEvent& aKeyEvent,TEventCode aType);


private:
	CExampleAppView* iAppView;
	};


////////////////////////////////////////////////////////////////////////
//
// CExampleDocument
//
////////////////////////////////////////////////////////////////////////
class CExampleDocument : public CEikDocument
	{
public:
	static CExampleDocument* NewL(CEikApplication& aApp);
	CExampleDocument(CEikApplication& aApp);
	void ConstructL();
private: 
	           // Inherited from CEikDocument
	CEikAppUi* CreateAppUiL();
	};


#endif

