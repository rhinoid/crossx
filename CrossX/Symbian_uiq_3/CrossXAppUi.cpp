// HelloWorldAppUi.cpp
//
// � Symbian Software Ltd 2005. All rights reserved.
//

#include "CrossXAppUi.h"
#include "CrossXView.h"

/**
2nd stage construction of the App UI.
Create view and add it to the framework.
The framework will take over the ownership.
*/
void CHelloWorldAppUi::ConstructL()
	{
	// Calls ConstructL that initiate the standard values. 
	CQikAppUi::ConstructL();

	// Create the view and add it to the framework
	m_HelloWorldView = CHelloWorldView::NewLC(*this);
	AddViewL(*m_HelloWorldView);
	CleanupStack::Pop(m_HelloWorldView);
	}

//             Called by the UI framework when a command has been issued.
//             In this example, a command can originate through a 
//             hot-key press or by selection of a menu item.
//             The command Ids are defined in the .hrh file
//             and are 'connected' to the hot-key and menu item in the
//             resource file.
//             Note that the EEikCmdExit is defined by the UI
//             framework and is pulled in by including eikon.hrh
//


void CHelloWorldAppUi::HandleCommandL(TInt aCommand)
	{
	switch (aCommand)
		{

	case EEikCmdExit: 
		Exit();
		break;
		}
	}


TKeyResponse CHelloWorldAppUi::HandleKeyEventL(const TKeyEvent& Event/*aKeyEvent*/,TEventCode Code/*aType*/)
{
	switch(Code)
	{
	case EEventKeyDown:
		if(Event.iScanCode == EStdKeyHash)	// hash key is fast exit
		{
			Exit();
			return EKeyWasConsumed;
		}

		if(m_HelloWorldView->HandleKeyDown((TStdScanCode)Event.iScanCode))
		{
			return EKeyWasConsumed;
		}
		break;

	case EEventKeyUp:
		if(m_HelloWorldView->HandleKeyUp((TStdScanCode)Event.iScanCode))
		{
			return EKeyWasConsumed;
		}
		break;

	case EEventKey:
		if(m_HelloWorldView->HandleKey((TStdScanCode)Event.iScanCode))
		{
			return EKeyWasConsumed;
		}
		break;
	}
    return EKeyWasNotConsumed;
}
