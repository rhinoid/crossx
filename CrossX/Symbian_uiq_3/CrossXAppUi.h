// HelloWorldAppUi.h
//
// � Symbian Software Ltd 2005. All rights reserved.
//

#ifndef HELLOWORLDAPPUI_H
#define HELLOWORLDAPPUI_H

#include <QikAppUi.h>

class CHelloWorldView;
/**
This class represents the application UI in hello world application,
It has responsibility to create the view.
*/
class CHelloWorldAppUi : public CQikAppUi
    {
public:
	// from CQikAppUi
	void ConstructL();

              // Inherirted from class CEikAppUi
	void HandleCommandL(TInt aCommand);

    virtual TKeyResponse HandleKeyEventL(
        const TKeyEvent& aKeyEvent,TEventCode aType);


	CHelloWorldView* m_HelloWorldView;
	};

#endif // HELLOWORLDAPPUI_H
