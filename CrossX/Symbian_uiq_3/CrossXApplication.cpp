// HelloWorldApplication.cpp
//
// � Symbian Software Ltd 2005. All rights reserved.
//

#include <eikstart.h>

#include "CrossXApplication.h"
#include "CrossXDocument.h"
#include "CrossXExternalInterface.h" // contains the applications UID

/**
The function is called by the UI framework to ask for the 
application's UID. The returned value is defined by the 
constant KUidHelloWorldApp and must match the second value 
defined in the project definition file.
*/
TUid CHelloWorldApplication::AppDllUid() const
	{
	return KUidHelloWorldApp;
	}

/**
This function is called by the UI framework at application start-up. 
It creates an instance of the document class.
*/
CApaDocument* CHelloWorldApplication::CreateDocumentL()
	{
	return CHelloWorldDocument::NewL(*this);
	}

/**
The function is called by the framework immediately after it has started the 
application's EXE. It is called by the framework and is expected to have 
exactly this prototype.
Function is a non-leaving function, this because we need to create the 
application object or return NULL on failure. Therefore we can�t use the 
new(ELeave) operator.

@return Instance of the application class.
*/
CApaApplication* NewApplication()
	{
	return new CHelloWorldApplication;
	}

/**
E32Main() contains the program's start up code, the entry point for an EXE.
*/
GLDEF_C TInt E32Main()
	{
	return EikStart::RunApplication(NewApplication);
	}
