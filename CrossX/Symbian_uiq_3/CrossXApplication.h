// HelloWorldApplication.h
//
// � Symbian Software Ltd 2005. All rights reserved.
//

#ifndef HELLOWORLDAPPAPPLICATION_H
#define HELLOWORLDAPPAPPLICATION_H

#include <QikApplication.h>

/**
This class is the entry point to the application.
*/
class CHelloWorldApplication : public CQikApplication
	{
private: 
	CApaDocument* CreateDocumentL();
	TUid AppDllUid() const;
	CApaApplication* NewApplication();
	};

#endif // HELLOWORLDAPPAPPLICATION_H
