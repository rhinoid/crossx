// HelloWorldDocument.cpp
//
// � Symbian Software Ltd 2005. All rights reserved.
//

#include <s32strm.h>
#include <QikApplication.h>

#include "CrossXDocument.h"
#include "CrossXAppUi.h"
#include "CrossXExternalInterface.h"

/**
Creates and constructs the document. This is called by
CHelloWorldApplication::CreateDocumentL() which in turn is called by the 
UI framework.
*/
CHelloWorldDocument* CHelloWorldDocument::NewL(CQikApplication& aApp)
	{
	CHelloWorldDocument* self = new (ELeave) CHelloWorldDocument(aApp);
	CleanupStack::PushL(self);
	self->ConstructL();
	CleanupStack::Pop(self);
	return self;
	}

/**
The constructor of the document class just passes the
supplied reference to the constructor initialization list.
*/
CHelloWorldDocument::CHelloWorldDocument(CQikApplication& aApp)
	: CQikDocument(aApp)
	{
	}

/**
2nd stage construction of the model.
All code that shall be called in initializing phase and might leave shall be 
added here.
*/
void CHelloWorldDocument::ConstructL()
	{
	}

/**
This is called by the UI framework as soon as the document has been created. 
It creates an instance of the ApplicationUI. The Application UI class is an 
instance of a CEikAppUi derived class.
*/
CEikAppUi* CHelloWorldDocument::CreateAppUiL()
	{
	return new (ELeave) CHelloWorldAppUi;
	}
