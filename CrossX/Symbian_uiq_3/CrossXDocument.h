// HelloWorldDocument.h
//
// � Symbian Software Ltd 2005. All rights reserved.
//

#ifndef HELLOWORLDDOCUMENT_H
#define HELLOWORLDDOCUMENT_H

#include <QikDocument.h>

/**
This class represents the document in hello world application,
For an UIQ application to work properly the document class need to be
derived from CQikDocument.
*/
class CHelloWorldDocument : public CQikDocument
	{
public:
	static CHelloWorldDocument* NewL(CQikApplication& aApp);
	
private: 
	// from CQikDocument
	CEikAppUi* CreateAppUiL();
	
	CHelloWorldDocument(CQikApplication& aApp);
	void ConstructL();
	};

#endif // HELLOWORLDDOCUMENT_H
