// HelloWorldExternalInterface.h
//
// � Symbian Software Ltd 2005. All rights reserved.
//

#ifndef HELLOWORLDEXTERNALINTERFACE_H
#define HELLOWORLDEXTERNALINTERFACE_H

/**
Identifies the hello world application�s UID3.
*/
const TUid KUidHelloWorldApp = {0xE0001001};

/**
Identifies the hello world application�s view.
Each view need an unique UID in the application.
*/
const TUid KUidHelloWorldView = {0x00000001};

#endif // HELLOWORLDEXTERNALINTERFACE_H

