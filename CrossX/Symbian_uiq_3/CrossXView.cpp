// HelloWorldView.cpp
//
// � Symbian Software Ltd 2005. All rights reserved.
//

#include <QikCommand.h>
#include <crossx.rsg>
#include <eiklabel.h> // CEikLabel

#include "CrossXAppUi.h"
#include "CrossXView.h"
#include "CrossXExternalInterface.h"


#define HEAPSIZE (1024*1024*4)

/**
Creates and constructs the view.

@param aAppUi Reference to the AppUi
@return Pointer to a CHelloWorldView object
*/
CHelloWorldView* CHelloWorldView::NewLC(CQikAppUi& aAppUi)
	{
	CHelloWorldView* self = new(ELeave) CHelloWorldView(aAppUi);
	CleanupStack::PushL(self);
	self->ConstructL();
	return self;
	}

/**
Constructor for the view.
Passes the application UI reference to the construction of the super class.

KNullViewId should normally be passed as parent view for the applications 
default view. The parent view is the logical view that is normally activated 
when a go back command is issued. KNullViewId will activate the system 
default view. 

@param aAppUi Reference to the application UI
*/
CHelloWorldView::CHelloWorldView(CQikAppUi& aAppUi) 
	: CQikViewBase(aAppUi, KNullViewId)
	{
	}

/**
Destructor for the view
*/
CHelloWorldView::~CHelloWorldView()
	{
	delete iIdleTimer;
	iIdleTimer = NULL;
	}

/**
2nd stage construction of the App UI.
*/
void CHelloWorldView::ConstructL()
	{
//	// Calls ConstructL that initialises the standard values. 
//	// This should always be called in the concrete view implementations.
	CQikViewBase::ConstructL();
	CreateWindowL();
//	           // Extent of the control. This is
//	           // the whole rectangle available to application.
//	           // The rectangle is passed to us from the application UI.
	SetExtentToWholeScreen();
//			   // At this stage, the control is ready to draw so
//	           // we tell the UI framework by activating it.


	SetLayoutManagerL(NULL);
	
	TQikViewMode NewMode;
	NewMode = ViewMode();
	NewMode.SetAppTitleBar(false);
	NewMode.SetButtonOrSoftkeyBar(false);
	NewMode.SetStatusBar(false);
	NewMode.SetToolbar(false);
	NewMode.SetFullscreen();
	SetViewModeL(NewMode);
	
	ActivateL();

//	SetExtentToWholeScreen();


	// these values should be filled out by some kind of system query
	CWindowGc& gc = SystemGc();
	CWsScreenDevice *pWDevice=(CWsScreenDevice *)gc.Device();
	TPixelsAndRotation pix;
	pWDevice->GetDefaultScreenSizeAndRotation(pix);
	m_Width=pix.iPixelSize.iWidth;
	m_Height=pix.iPixelSize.iHeight;

	
//	m_Width = 176*2;
//	m_Height = 208*2;
	m_ExitFlg = 0;
	m_PixelFormat = EColor4K;

	// create offscreen bitmap
	m_OffscreenBitmap.Create(TSize(m_Width,m_Height), m_PixelFormat);


	TTime currentTime;
	currentTime.HomeTime();
	m_LastTick = currentTime.Int64();


	// start timer
	iIdleTimer = CIdle::NewL(CActive::EPriorityIdle);
	iIdleTimer->Start(TCallBack(CHelloWorldView::Period,this));


	//toggle the backlight on
	m_LightDelay = 0;
	m_InitFlag = 1;

	m_SizeToggle=0;
	}

void CHelloWorldView::SizeChanged()
    {  
//    DrawNow();
//	m_SizeToggle+=1;
//	m_SizeToggle&=1;
//	if(m_SizeToggle)	SetExtentToWholeScreen();
   
    }

void CHelloWorldView::Draw( const TRect& /*aRect*/ ) const
    {
  	}

	
/**
Inherited from CQikViewBase and called upon by the UI Framework. 
It creates the view from resource.
*/
void CHelloWorldView::ViewConstructL()
	{
	// Loads information about the UI configurations this view supports
	// together with definition of each view.	
//	ViewConstructFromResourceL(R_HELLOWORLD_UI_CONFIGURATIONS);
//
//	SetExtentToWholeScreen();
	}

/**
Returns the view Id

@return Returns the Uid of the view
*/
TVwsViewId CHelloWorldView::ViewId()const
	{
	return TVwsViewId(KUidHelloWorldApp, KUidHelloWorldView);
	}

/*
Handles all commands in the view.
Called by the UI framework when a command has been issued.
The command Ids are defined in the .hrh file.

@param aCommand The command to be executed
@see CQikViewBase::HandleCommandL
*/
void CHelloWorldView::HandleCommandL(CQikCommand& aCommand)
	{
	switch(aCommand.Id())
		{
		// Just issue simple info messages to show that
		// the commands have been selected
		
		// Go back and exit command will be passed to the CQikViewBase to handle.
		default:
			CQikViewBase::HandleCommandL(aCommand);
			break;
		}
	}

// This function is called by the periodic timer
TInt CHelloWorldView::Period(TAny *aPtr)
{
    //returning a value of TRUE indicates the callback should be done again
	return ((CHelloWorldView*)aPtr)->Tick();
}

// non static variant of the timer function
TInt CHelloWorldView::Tick()
{
	TInt RetVal=1;

	TTime nowtime;
	nowtime.UniversalTime();
	TTimeIntervalMicroSeconds tti;
	tti = nowtime.MicroSecondsFrom(m_TimeMark);
	m_TimeMark = nowtime;
	int millis = (tti.Int64()&0xffffffff)  / 1000;
	if (millis >= 1 && millis < 50)
	{
		int wait = 50-millis;
		User::After(TTimeIntervalMicroSeconds32(wait*1000));
	}	
	
	// make sure the backlight stays on
	m_LightDelay++;
	m_LightDelay&=63;
	if(m_LightDelay==0)
	{
		User::ResetInactivityTime(); 
	}


	// do the stuff	
	if(m_InitFlag==0)
	{
		// Calculate elapsed time from last timer event in microseconds
		TTime currentTime;
		currentTime.HomeTime();
		TInt64 currentTick = currentTime.Int64();
		TInt64 frameTime = currentTick - m_LastTick;
		m_LastTick = currentTick;


/*
		unsigned short * PixelBuffer;
		m_OffscreenBitmap.LockHeap(ETrue);
		PixelBuffer = (unsigned short *) m_OffscreenBitmap.DataAddress();
		m_OffscreenBitmap.UnlockHeap();
		int x,y;
		for(y=0; y<m_Height; y++)
		{
			for(x=0; x<m_Width; x++)
			{
				*PixelBuffer++ = y*x +m_LightDelay;
			}
		}
*/
		unsigned short * PixelBuffer;
		m_OffscreenBitmap.LockHeap(ETrue);
		PixelBuffer = (unsigned short *) m_OffscreenBitmap.DataAddress();
		m_BackBuffer.SetBuffer(PixelBuffer);
		RetVal = m_Game->Tick((int)frameTime);
		m_OffscreenBitmap.UnlockHeap();


		

	}
	else
	{
		RHeap *heap = UserHeap::ChunkHeap(NULL,HEAPSIZE,HEAPSIZE);   //make sure we can allocate more memory
		User::SwitchHeap(heap);

		unsigned short * PixelBuffer;
		m_OffscreenBitmap.LockHeap(ETrue);
		PixelBuffer = (unsigned short *) m_OffscreenBitmap.DataAddress();
		m_BackBuffer.Create(PixelBuffer, m_Width, m_Height);

		m_Game = new CROSSX_GAMECLASS;
		m_Game->CRSX_SetBackbuf(&m_BackBuffer);	//Link the game to the backsurface
		m_Game->Init();						//initialize game

		m_OffscreenBitmap.UnlockHeap();
		m_InitFlag = 0;
	}

	// draw the graphics on screen
	CWindowGc& gc = SystemGc();
	gc.Activate(*DrawableWindow());
	gc.BitBlt(TPoint(0,0), &m_OffscreenBitmap);
	gc.Deactivate();

	if(m_ExitFlg)
	{
		User::Exit(0);
	}
	return RetVal;
}





bool CHelloWorldView::HandleKey(TStdScanCode KeyCode)
{
	return false;
}


bool CHelloWorldView::HandleKeyDown(TStdScanCode KeyCode)
{
	m_ExitFlg = 1;
	return false;
}

bool CHelloWorldView::HandleKeyUp(TStdScanCode KeyCode)
{
	return false;
}


/*
TKeyResponse CExampleAppView::OfferKeyEventL( const TKeyEvent& event, TEventCode type )
{
    switch (type)
    {
    case EEventKeyDown:
    	HandleKeyDown(event.iScanCode);
        break;

    case EEventKeyUp:
    	HandleKeyUp(event.iScanCode);
        break;

    case EEventKey:
        break;

    default:
        (void)0;
    }

    return EKeyWasConsumed;
}
*/
void CHelloWorldView::HandlePointerEventL(const TPointerEvent& aPointerEvent)
{
    switch (aPointerEvent.iType)
    {
    case TPointerEvent::EButton1Down:
        if(m_Game)m_Game->HandleMouseDown( aPointerEvent.iPosition.iX, aPointerEvent.iPosition.iY );
		if(aPointerEvent.iPosition.iY<30 && aPointerEvent.iPosition.iX<30)
		{
			m_ExitFlg = 1;
		}
        break;

    case TPointerEvent::EButton1Up:
        if(m_Game)m_Game->HandleMouseUp( aPointerEvent.iPosition.iX, aPointerEvent.iPosition.iY );
        break;

    case TPointerEvent::EDrag:
        if(m_Game)m_Game->HandleMouseMove( aPointerEvent.iPosition.iX, aPointerEvent.iPosition.iY );
        break;
    default:
    break;
    }
}
