// HelloWorldView.h
//
// � Symbian Software Ltd 2005. All rights reserved.
//

#ifndef HELLOWORLDVIEW_H
#define HELLOWORLDVIEW_H

#include <QikViewBase.h>
#include <FBS.H>
#include "crossx.h"


/**
A very simple view that displays the text "Hello world", drawn using the default title 
font supplied by UIQ. It also consist of three commands that will bring up infoprints.
*/
class CHelloWorldView : public CQikViewBase
	{
public:
	static CHelloWorldView* NewLC(CQikAppUi& aAppUi);
	~CHelloWorldView();
	
	// from CQikViewBase
	TVwsViewId ViewId()const;
	void HandleCommandL(CQikCommand& aCommand);

	void SizeChanged();
	void Draw( const TRect& /*aRect*/ ) const;
	
protected: 
	// from CQikViewBase
	void ViewConstructL();
	
public:
	CHelloWorldView(CQikAppUi& aAppUi);
	void ConstructL();

	//SYMBIAN FRAMEWORK ADDITIONS
	bool Init(void);

	bool HandleKey(TStdScanCode KeyCode);
	bool HandleKeyUp(TStdScanCode KeyCode);
	bool HandleKeyDown(TStdScanCode KeyCode);
    
	void HandlePointerEventL(const TPointerEvent& aPointerEvent);

	TInt Tick(void);
	static TInt Period(TAny* aPtr);


	CGame      *m_Game;
	CSurface16  m_BackBuffer;			// Wrapper for the Symbian buffer

	int         m_SizeToggle;
	int			m_Width;			// width of offscreen bitmap
	int			m_Height;			// height of offscreen bitmap
	int			m_ExitFlg;
	int			m_LightDelay;		// delay counter to make sure we keep the backlight on

	CFbsBitmap m_OffscreenBitmap;	// offscreen bitmap where we'll draw on
	TDisplayMode m_PixelFormat;		// pixelformat of offscreen bitmap

	TInt64	m_LastTick;				// How much time did the frame draw take?
	CIdle    * iIdleTimer;
	TTime      m_TimeMark;
	
	int     m_InitFlag;



	};

#endif // HELLOWORLDVIEW_H
