/**************************************************************
	LZARI.C -- A Data Compression Program
	(tab = 4 spaces)
***************************************************************
	4/7/1989 Haruhiko Okumura
	Use, distribute, and modify this program freely.
	Please send me your improved versions.
		PC-VAN		SCIENCE
		NIFTY-Serve	PAF01022
		CompuServe	74050,1022
**************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//#include <ctype.h>
#include "packer.h"

/********** Bit I/O **********/



Packer::Packer()
{
	InitMembers();
}

Packer::~Packer()
{
}

void Packer::InitMembers(void)
{

	inbuffer = 0;
	outbuffer = 0;
	infile = 0;
	outfile = 0;
	textsize = 0;
	codesize = 0;
	printcount = 0;
	buffer = 0;
	mask = 0;
	memset(text_buf, 0, sizeof(text_buf));
	match_position = 0;
	match_length = 0;
	memset(lson, 0, sizeof(lson));
	memset(rson, 0, sizeof(rson));
	memset(dad, 0, sizeof(dad));
	low = 0;
	high = 0;
	value = 0;
	shifts = 0;
	memset(char_to_sym, 0, sizeof(char_to_sym));
	memset(sym_to_char, 0, sizeof(sym_to_char));
	memset(sym_freq, 0, sizeof(sym_freq));
	memset(sym_cum, 0, sizeof(sym_cum));

	low = 0;
	high = PACKER_Q4;
	value = 0;
	shifts = 0;
	textsize = 0;
	codesize = 0;
	printcount = 0;
}

void Packer::Error(char *message)
{
//	printf("\n%s\n", message);
//	exit(EXIT_FAILURE);
}


/********** LZSS with multiple binary trees **********/


void Packer::InitTree(void)  /* Initialize trees */
{
	int  i;

	/* For i = 0 to N - 1, rson[i] and lson[i] will be the right and
	   left children of node i.  These nodes need not be initialized.
	   Also, dad[i] is the parent of node i.  These are initialized to
	   NIL (= N), which stands for 'not used.'
	   For i = 0 to 255, rson[N + i + 1] is the root of the tree
	   for strings that begin with character i.  These are initialized
	   to NIL.  Note there are 256 trees. */

	for (i = PACKER_N + 1; i <= PACKER_N + 256; i++) rson[i] = PACKER_NIL;	/* root */
	for (i = 0; i < PACKER_N; i++) dad[i] = PACKER_NIL;	/* node */
}

void Packer::InsertNode(int r)
	/* Inserts string of length F, text_buf[r..r+F-1], into one of the
	   trees (text_buf[r]'th tree) and returns the longest-match position
	   and length via the global variables match_position and match_length.
	   If match_length = F, then removes the old node in favor of the new
	   one, because the old one will be deleted sooner.
	   Note r plays double role, as tree node and position in buffer. */
{
	int  i, p, cmp, temp;
	unsigned char  *key;

	cmp = 1;  key = &text_buf[r];  p = PACKER_N + 1 + key[0];
	rson[r] = lson[r] = PACKER_NIL;  match_length = 0;
	for ( ; ; ) {
		if (cmp >= 0) {
			if (rson[p] != PACKER_NIL) p = rson[p];
			else {  rson[p] = r;  dad[r] = p;  return;  }
		} else {
			if (lson[p] != PACKER_NIL) p = lson[p];
			else {  lson[p] = r;  dad[r] = p;  return;  }
		}
		for (i = 1; i < PACKER_F; i++)
			if ((cmp = key[i] - text_buf[p + i]) != 0)  break;
		if (i > PACKER_THRESHOLD) {
			if (i > match_length) {
				match_position = (r - p) & (PACKER_N - 1);
				if ((match_length = i) >= PACKER_F) break;
			} else if (i == match_length) {
				if ((temp = (r - p) & (PACKER_N - 1)) < match_position)
					match_position = temp;
			}
		}
	}
	dad[r] = dad[p];  lson[r] = lson[p];  rson[r] = rson[p];
	dad[lson[p]] = r;  dad[rson[p]] = r;
	if (rson[dad[p]] == p) rson[dad[p]] = r;
	else                   lson[dad[p]] = r;
	dad[p] = PACKER_NIL;  /* remove p */
}

void Packer::DeleteNode(int p)  /* Delete node p from tree */
{
	int  q;
	
	if (dad[p] == PACKER_NIL) return;  /* not in tree */
	if (rson[p] == PACKER_NIL) q = lson[p];
	else if (lson[p] == PACKER_NIL) q = rson[p];
	else {
		q = lson[p];
		if (rson[q] != PACKER_NIL) {
			do {  q = rson[q];  } while (rson[q] != PACKER_NIL);
			rson[dad[q]] = lson[q];  dad[lson[q]] = dad[q];
			lson[q] = lson[p];  dad[lson[p]] = q;
		}
		rson[q] = rson[p];  dad[rson[p]] = q;
	}
	dad[q] = dad[p];
	if (rson[dad[p]] == p) rson[dad[p]] = q;
	else                   lson[dad[p]] = q;
	dad[p] = PACKER_NIL;
}

/********** Arithmetic Compression **********/

/*  If you are not familiar with arithmetic compression, you should read
		I. E. Witten, R. M. Neal, and J. G. Cleary,
			Communications of the ACM, Vol. 30, pp. 520-540 (1987),
	from which much have been borrowed.  */


void Packer::StartModel(void)  /* Initialize model */
{
	int ch, sym, i;
	
	sym_cum[PACKER_N_CHAR] = 0;
	for (sym = PACKER_N_CHAR; sym >= 1; sym--) {
		ch = sym - 1;
		char_to_sym[ch] = sym;  sym_to_char[sym] = ch;
		sym_freq[sym] = 1;
		sym_cum[sym - 1] = sym_cum[sym] + sym_freq[sym];
	}
	sym_freq[0] = 0;  /* sentinel (!= sym_freq[1]) */
	position_cum[PACKER_N] = 0;
	for (i = PACKER_N; i >= 1; i--)
		position_cum[i - 1] = position_cum[i] + 10000 / (i + 200);
			/* empirical distribution function (quite tentative) */
			/* Please devise a better mechanism! */
}

void Packer::UpdateModel(int sym)
{
	int i, c, ch_i, ch_sym;
	
	if (sym_cum[0] >= PACKER_MAX_CUM) {
		c = 0;
		for (i = PACKER_N_CHAR; i > 0; i--) {
			sym_cum[i] = c;
			c += (sym_freq[i] = (sym_freq[i] + 1) >> 1);
		}
		sym_cum[0] = c;
	}
	for (i = sym; sym_freq[i] == sym_freq[i - 1]; i--) ;
	if (i < sym) {
		ch_i = sym_to_char[i];    ch_sym = sym_to_char[sym];
		sym_to_char[i] = ch_sym;  sym_to_char[sym] = ch_i;
		char_to_sym[ch_i] = sym;  char_to_sym[ch_sym] = i;
	}
	sym_freq[i]++;
	while (--i >= 0) sym_cum[i]++;
}

void Packer::Output(int bit)  /* Output 1 bit, followed by its complements */
{
	PutBit(bit);
	for ( ; shifts > 0; shifts--) PutBit(! bit);
}

void Packer::EncodeChar(int ch)
{
	int  sym;
	unsigned long int  range;

	sym = char_to_sym[ch];
	range = high - low;
	high = low + (range * sym_cum[sym - 1]) / sym_cum[0];
	low +=       (range * sym_cum[sym    ]) / sym_cum[0];
	for ( ; ; ) {
		if (high <= PACKER_Q2) Output(0);
		else if (low >= PACKER_Q2) {
			Output(1);  low -= PACKER_Q2;  high -= PACKER_Q2;
		} else if (low >= PACKER_Q1 && high <= PACKER_Q3) {
			shifts++;  low -= PACKER_Q1;  high -= PACKER_Q1;
		} else break;
		low += low;  high += high;
	}
	UpdateModel(sym);
}

void Packer::EncodePosition(int position)
{
	unsigned long int  range;

	range = high - low;
	high = low + (range * position_cum[position    ]) / position_cum[0];
	low +=       (range * position_cum[position + 1]) / position_cum[0];
	for ( ; ; ) {
		if (high <= PACKER_Q2) Output(0);
		else if (low >= PACKER_Q2) {
			Output(1);  low -= PACKER_Q2;  high -= PACKER_Q2;
		} else if (low >= PACKER_Q1 && high <= PACKER_Q3) {
			shifts++;  low -= PACKER_Q1;  high -= PACKER_Q1;
		} else break;
		low += low;  high += high;
	}
}

void Packer::EncodeEnd(void)
{
	shifts++;
	if (low < PACKER_Q1) Output(0);  else Output(1);
	FlushBitBuffer();  /* flush bits remaining in buffer */
}

int Packer::BinarySearchSym(unsigned int x)
	/* 1      if x >= sym_cum[1],
	   N_CHAR if sym_cum[N_CHAR] > x,
	   i such that sym_cum[i - 1] > x >= sym_cum[i] otherwise */
{
	int i, j, k;
	
	i = 1;  j = PACKER_N_CHAR;
	while (i < j) {
		k = (i + j) / 2;
		if (sym_cum[k] > x) i = k + 1;  else j = k;
	}
	return i;
}

int Packer::BinarySearchPos(unsigned int x)
	/* 0 if x >= position_cum[1],
	   N - 1 if position_cum[N] > x,
	   i such that position_cum[i] > x >= position_cum[i + 1] otherwise */
{
	int i, j, k;
	
	i = 1;  j = PACKER_N;
	while (i < j) {
		k = (i + j) / 2;
		if (position_cum[k] > x) i = k + 1;  else j = k;
	}
	return i - 1;
}

void Packer::StartDecode(void)
{
	int i;

	for (i = 0; i < PACKER_M + 2; i++)
		value = 2 * value + GetBit();
}

int Packer::DecodeChar(void)
{
	int	 sym, ch;
	unsigned long int  range;
	
	range = high - low;
	sym = BinarySearchSym((unsigned int)
		(((value - low + 1) * sym_cum[0] - 1) / range));
	high = low + (range * sym_cum[sym - 1]) / sym_cum[0];
	low +=       (range * sym_cum[sym    ]) / sym_cum[0];
	for ( ; ; ) {
		if (low >= PACKER_Q2) {
			value -= PACKER_Q2;  low -= PACKER_Q2;  high -= PACKER_Q2;
		} else if (low >= PACKER_Q1 && high <= PACKER_Q3) {
			value -= PACKER_Q1;  low -= PACKER_Q1;  high -= PACKER_Q1;
		} else if (high > PACKER_Q2) break;
		low += low;  high += high;
		value = 2 * value + GetBit();
	}
	ch = sym_to_char[sym];
	UpdateModel(sym);
	return ch;
}

int Packer::DecodePosition(void)
{
	int position;
	unsigned long int  range;
	
	range = high - low;
	position = BinarySearchPos((unsigned int)
		(((value - low + 1) * position_cum[0] - 1) / range));
	high = low + (range * position_cum[position    ]) / position_cum[0];
	low +=       (range * position_cum[position + 1]) / position_cum[0];
	for ( ; ; ) {
		if (low >= PACKER_Q2) {
			value -= PACKER_Q2;  low -= PACKER_Q2;  high -= PACKER_Q2;
		} else if (low >= PACKER_Q1 && high <= PACKER_Q3) {
			value -= PACKER_Q1;  low -= PACKER_Q1;  high -= PACKER_Q1;
		} else if (high > PACKER_Q2) break;
		low += low;  high += high;
		value = 2 * value + GetBit();
	}
	return position;
}

/********** Encode and Decode **********/

void Packer::Encode(void)
{
	int  i, c, len, r, s, last_match_length;
	
buffer = 0;  mask = 128;

	fseek(infile, 0L, SEEK_END);
	textsize = ftell(infile);
	if (fwrite(&textsize, sizeof textsize, 1, outfile) < 1)
		Error("Write Error");  /* output size of text */
	codesize += sizeof textsize;
	if (textsize == 0) return;
	fseek( infile, 0L, SEEK_SET );
//	rewind(infile);	//since rewind is not supported on win32, we use fseek
	textsize = 0;
	StartModel();  InitTree();
	s = 0;  r = PACKER_N - PACKER_F;
	for (i = s; i < r; i++) text_buf[i] = ' ';
	for (len = 0; len < PACKER_F && (c = getc(infile)) != EOF; len++)
		text_buf[r + len] = c;
	textsize = len;
	for (i = 1; i <= PACKER_F; i++) InsertNode(r - i);
	InsertNode(r);
	do {
		if (match_length > len) match_length = len;
		if (match_length <= PACKER_THRESHOLD) {
			match_length = 1;  EncodeChar(text_buf[r]);
		} else {
			EncodeChar(255 - PACKER_THRESHOLD + match_length);
			EncodePosition(match_position - 1);
		}
		last_match_length = match_length;
		for (i = 0; i < last_match_length &&
				(c = getc(infile)) != EOF; i++) {
			DeleteNode(s);  text_buf[s] = c;
			if (s < PACKER_F - 1) text_buf[s + PACKER_N] = c;
			s = (s + 1) & (PACKER_N - 1);
			r = (r + 1) & (PACKER_N - 1);
			InsertNode(r);
		}
		if ((textsize += i) > printcount) {
			printf("%12ld\r", textsize);  printcount += 1024;
		}
		while (i++ < last_match_length) {
			DeleteNode(s);
			s = (s + 1) & (PACKER_N - 1);
			r = (r + 1) & (PACKER_N - 1);
			if (--len) InsertNode(r);
		}
	} while (len > 0);
	EncodeEnd();
	printf("In : %lu bytes\n", textsize);
	printf("Out: %lu bytes\n", codesize);
	printf("Out/In: %.3f\n", (double)codesize / textsize);
}



void Packer::PutBit(int bit)  /* Output one bit (bit = 0,1) */
{

	if (bit) buffer |= mask;
	if ((mask >>= 1) == 0) {
		if(outbuffer)
		{
			*outbuffer++ = buffer;
		}
		else
		{
			if (putc(buffer, outfile) == EOF) Error("Write Error");
		}
		buffer = 0;  mask = 128;  codesize++;
	}
}


void Packer::FlushBitBuffer(void)  /* Send remaining bits */
{
	int  i;
	
	for (i = 0; i < 7; i++) PutBit(0);
}

int Packer::GetBit(void)  /* Get one bit (0 or 1) */
{

	if ((mask >>= 1) == 0) {
		if(inbuffer)
		{
			buffer = *inbuffer++;
		}
		else
		{
			buffer = getc(infile);
		}
		mask = 128;
	}
	return ((buffer & mask) != 0);
}


void	Packer::DeCompress(void *SrcBuffer, void *DestBuffer, int BufferSize)
{
	int  i, j, k, r, c;
	unsigned long int  count;
	InitMembers();
	buffer = 0;
	mask = 0;

	inbuffer  = (unsigned char *) SrcBuffer;
	outbuffer = (unsigned char *) DestBuffer;

	textsize =  *((unsigned long int *)inbuffer);
	inbuffer+=4;
	if (textsize == 0) return;
	StartDecode();  StartModel();
	for (i = 0; i < PACKER_N - PACKER_F; i++) text_buf[i] = ' ';
	r = PACKER_N - PACKER_F;
	for (count = 0; count < textsize; ) {
		c = DecodeChar();
		if (c < 256) {
			/*putc(c, outfile);*/ *outbuffer++ = c;  text_buf[r++] = c;
			r &= (PACKER_N - 1);  count++;
		} else {
			i = (r - DecodePosition() - 1) & (PACKER_N - 1);
			j = c - 255 + PACKER_THRESHOLD;
			for (k = 0; k < j; k++) {
				c = text_buf[(i + k) & (PACKER_N - 1)];
				/*putc(c, outfile);*/ *outbuffer++ = c;  text_buf[r++] = c;
				r &= (PACKER_N - 1);  count++;
				if(count==BufferSize) break;
			}
		}
		if(count==BufferSize) break;
	}
}

void	Packer::DeCompress(char *SrcFileName, void *DestBuffer, int BufferSize)
{
	int  i, j, k, r, c;
	unsigned long int  count;
	InitMembers();
	buffer = 0;
	mask = 0;

	infile = fopen(SrcFileName, "rb");
	if(infile==0) return;


	inbuffer = 0;
	outbuffer = (unsigned char *) DestBuffer;

	if (fread(&textsize, sizeof textsize, 1, infile) < 1)
		Error("Read Error");  /* read size of text */
	if (textsize == 0) return;
	StartDecode();  StartModel();
	for (i = 0; i < PACKER_N - PACKER_F; i++) text_buf[i] = ' ';
	r = PACKER_N - PACKER_F;
	for (count = 0; count < textsize; ) {
		c = DecodeChar();
		if (c < 256) {
			/*putc(c, outfile);*/ *outbuffer++ = c;  text_buf[r++] = c;
			r &= (PACKER_N - 1);  count++;
		} else {
			i = (r - DecodePosition() - 1) & (PACKER_N - 1);
			j = c - 255 + PACKER_THRESHOLD;
			for (k = 0; k < j; k++) {
				c = text_buf[(i + k) & (PACKER_N - 1)];
				/*putc(c, outfile);*/ *outbuffer++ = c;  text_buf[r++] = c;
				r &= (PACKER_N - 1);  count++;
				if(count==BufferSize) break;
			}
		}
		if(count==BufferSize) break;
	}

	fclose(infile);
	infile = 0;
}


int	Packer::GetDecompressSize(void *SrcBuffer)
{
	InitMembers();
	inbuffer  = (unsigned char *) SrcBuffer;

	textsize =  *((unsigned long int *)inbuffer);
	return textsize;
}

int	Packer::GetDecompressSize(char *SrcFileName)
{
	InitMembers();
	infile = fopen(SrcFileName, "rb");
	if(infile==0) return -1;

	if (fread(&textsize, sizeof textsize, 1, infile) < 1)
		return -1;

	fclose(infile);
	return textsize;
}


int  Packer::Compress(void *SrcBuffer, char *DestFileName)
{
	int  i, c, len, r, s, last_match_length;

	InitMembers();
	buffer = 0;  mask = 128;

	outfile = fopen(DestFileName, "wb");
	if(outfile==0) return 0;
	infile = 0;


	inbuffer = (unsigned char *) SrcBuffer;
	outbuffer = 0;

	fseek(outfile, 0L, SEEK_END);
	textsize = ftell(outfile);
	if (fwrite(&textsize, sizeof textsize, 1, outfile) < 1)
	{
		fclose(outfile);
		return 0;
	}
	codesize += sizeof textsize;
	if (textsize == 0) return 0;
	textsize = 0;
	StartModel();  InitTree();
	s = 0;  r = PACKER_N - PACKER_F;
	for (i = s; i < r; i++) text_buf[i] = ' ';
	for (len = 0; len < PACKER_F && (/*c = getc(infile)*/ c = *inbuffer++) != EOF; len++)
		text_buf[r + len] = c;
	textsize = len;
	for (i = 1; i <= PACKER_F; i++) InsertNode(r - i);
	InsertNode(r);
	do {
		if (match_length > len) match_length = len;
		if (match_length <= PACKER_THRESHOLD) {
			match_length = 1;  EncodeChar(text_buf[r]);
		} else {
			EncodeChar(255 - PACKER_THRESHOLD + match_length);
			EncodePosition(match_position - 1);
		}
		last_match_length = match_length;
		for (i = 0; i < last_match_length &&
				(/*c = getc(infile)*/ c = *inbuffer++ ) != EOF; i++) {
			DeleteNode(s);  text_buf[s] = c;
			if (s < PACKER_F - 1) text_buf[s + PACKER_N] = c;
			s = (s + 1) & (PACKER_N - 1);
			r = (r + 1) & (PACKER_N - 1);
			InsertNode(r);
		}
		while (i++ < last_match_length) {
			DeleteNode(s);
			s = (s + 1) & (PACKER_N - 1);
			r = (r + 1) & (PACKER_N - 1);
			if (--len) InsertNode(r);
		}
	} while (len > 0);
	EncodeEnd();
	return codesize;
}

int  Packer::Compress(void *SrcBuffer, void *DestBuffer, int BufferSize)
{
	int  i, c, len, r, s, last_match_length;

	InitMembers();
	buffer = 0;  mask = 128;

	outfile = 0;
	infile = 0;


	inbuffer = (unsigned char *) SrcBuffer;
	outbuffer = (unsigned char *) DestBuffer;

	textsize = BufferSize;
	*((unsigned int *)outbuffer) = textsize;
	outbuffer += 4;

	codesize += sizeof textsize;
	if (textsize == 0) return 0;
	textsize = 0;
	StartModel();  InitTree();
	s = 0;  r = PACKER_N - PACKER_F;
	for (i = s; i < r; i++) text_buf[i] = ' ';
	for (len = 0; len < PACKER_F && (/*c = getc(infile)*/ c = *inbuffer++) != EOF; len++)
		text_buf[r + len] = c;
	textsize = len;
	for (i = 1; i <= PACKER_F; i++) InsertNode(r - i);
	InsertNode(r);
	do {
		if (match_length > len) match_length = len;
		if (match_length <= PACKER_THRESHOLD) {
			match_length = 1;  EncodeChar(text_buf[r]);
		} else {
			EncodeChar(255 - PACKER_THRESHOLD + match_length);
			EncodePosition(match_position - 1);
		}
		last_match_length = match_length;
		for (i = 0; i < last_match_length &&
				(/*c = getc(infile)*/ c = *inbuffer++ ) != EOF; i++) {
			DeleteNode(s);  text_buf[s] = c;
			if (s < PACKER_F - 1) text_buf[s + PACKER_N] = c;
			s = (s + 1) & (PACKER_N - 1);
			r = (r + 1) & (PACKER_N - 1);
			InsertNode(r);
		}
		while (i++ < last_match_length) {
			DeleteNode(s);
			s = (s + 1) & (PACKER_N - 1);
			r = (r + 1) & (PACKER_N - 1);
			if (--len) InsertNode(r);
		}
	} while (len > 0);
	EncodeEnd();
	return codesize;
}

