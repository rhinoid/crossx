#ifndef _H_STORAGEVECTOR
#define _H_STORAGEVECTOR

#include "utils/debug.h"

//------- Storage classes

template<class T>
class StorageVector
{
public:
	StorageVector() : m_Threshold(20), m_NrOfItems(0), m_Storage(0) {}
	~StorageVector()
	{
		delete [] m_Storage;
	}

	
    StorageVector(StorageVector const &other)					// copy constructor
	{
		Copy(other);
	}


    StorageVector const &operator=(StorageVector const &other) // assignment constructor
	{
        if (this != &other)
        {
            delete [] m_Storage;
            Copy(other);
        }
        return *this;
	}

    void Copy(StorageVector const &other)
    {
        m_NrOfItems = other.m_NrOfItems;
        m_Threshold = other.m_Threshold;
        m_Storage = new T [m_Threshold];
		for(unsigned int i=0; i<m_NrOfItems; i++)
		{
			m_Storage[i] = other.m_Storage[i]; //use copy constructor if applicable forr the object
		}
    }

    T &Get(unsigned index) const
    {
#ifdef _DEBUG
        Boundary(index);
#endif
        return m_Storage[index];
    }

    T &operator[](unsigned index)
    {
        return Get(index);
    }

    T const &operator[](unsigned index) const
    {
        return Get(index);
    }

    void Boundary(unsigned index) const
    {
        if (index >= m_NrOfItems)
        {
			ASSERT(0);
        }
    }
	
	void    Add(T Item)
	{
		//exception with empty list
		if(m_Storage==0)
		{
			m_Storage = new T[m_Threshold];
		}
		m_Storage[m_NrOfItems] = Item;
		m_NrOfItems++;
		if(m_NrOfItems >= m_Threshold) //reached threshold? then increase storage
		{
			m_Threshold *= 2;
			T* NewStorage = new T[m_Threshold];
			for(unsigned int i=0; i<(m_Threshold/2); i++) //copy old items into new storage
			{
				NewStorage[i] = m_Storage[i];
			}
			delete [] m_Storage;
			m_Storage = NewStorage;
		}
	}

	int Size(void)
	{
		return m_NrOfItems;
	}

private:
	unsigned int	m_Threshold;
	unsigned int	m_NrOfItems;
	T              *m_Storage;
};


#endif
