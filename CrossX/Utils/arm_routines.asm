    AREA Arm_routines,CODE,READONLY
    EXPORT Mul64_8
    EXPORT Mul64_16
    EXPORT FDIV
    EXPORT FDIVS
    EXPORT SQRT
    EXPORT FDIVS16
    EXPORT FDIVS8
    EXPORT CLEARSCREEN
    EXPORT CLEARZBUFFER

Mul64_8   ;r0 has value1, r1 has value 2     outcome has to go into r0
    ;MOV r12, sp
    ;stmfd sp!, {r4-r11,lr} ;saving values not necessary.
    
    smull r2,r3,r0,r1 ;64 bit multiplication	
	mov r2, r2, lsr #8
    orr r0, r2, r3, LSL #24  ;32-8


    ;ldmfd sp!, {r4-r11,pc}
	MOV     PC, lr ;return from subroutine


Mul64_16   ;r0 has value1, r1 has value 2     outcome has to go into r0
    ;MOV r12, sp
    ;stmfd sp!, {r4-r11,lr} ;saving values not necessary.
    
    smull r2,r3,r0,r1 ;64 bit multiplication	
	mov r2, r2, lsr #16
    orr r0, r2, r3, LSL #16  ;32-8

    ;ldmfd sp!, {r4-r11,pc}
	MOV     PC, lr ;return from subroutine


FDIV
  cmp r1,#1
  beq uo
  mov r2,#0
  cmp r1,r0,lsr #15
  bhi l0_15
;16_31
  cmp r1,r0,lsr #23
  bhi l16_23
;24_31
  cmp r1,r0,lsr #27
  bhi l24_27
;28_31
  cmp r1,r0,lsr #29
  bhi l28_29
;30_31
  cmp r1,r0,lsr #30
  bhi u30
  b u31 
l28_29
  cmp r1,r0,lsr #28
  bhi u28
  b u29
  
l24_27
  cmp r1,r0,lsr #25
  bhi l24_25
;26_27
  cmp r1,r0,lsr #26
  bhi u26
  b u27  
l24_25
  cmp r1,r0,lsr #24
  bhi u24
  b u25  

l16_23
  cmp r1,r0,lsr #19
  bhi l16_19
;20_23
  cmp r1,r0,lsr #21
  bhi l20_21
;22_23
  cmp r1,r0,lsr #22
  bhi u22
  b u23
l20_21
  cmp r1,r0,lsr #20
  bhi u20
  b u21
  
l16_19
  cmp r1,r0,lsr #17
  bhi l16_17
;l18_19
  cmp r1,r0,lsr #18
  bhi u18
  b u19  
l16_17
  cmp r1,r0,lsr #16
  bhi u16
  b u17
  
l0_15
  cmp r1,r0,lsr #7
  bhi l0_7
;8_15
  cmp r1,r0,lsr #11
  bhi l8_11
;12_15
  cmp r1,r0,lsr #13
  bhi l12_13
;14_15
  cmp r1,r0,lsr #14
  bhi u14
  b u15  
l12_13
  cmp r1,r0,lsr #12
  bhi u12
  b u13
  
l8_11
  cmp r1,r0,lsr #9
  bhi l8_9
;10_11
  cmp r1,r0,lsr #10
  bhi u10
  b u11  
l8_9
  cmp r1,r0,lsr #8
  bhi u8
  b u9
  
l0_7
  cmp r1,r0,lsr #3
  bhi l0_3
;4_7
  cmp r1,r0,lsr #5
  bhi l4_5
;6_7
  cmp r1,r0,lsr #6
  bhi u6
  b u7
l4_5
  cmp r1,r0,lsr #4
  bhi u4
  b u5 
  
l0_3
  cmp r1,r0,lsr #1
  bhi l0_1
;2_3
  cmp r1,r0,lsr #2
  bhi u2
  b u3 
l0_1
  cmp r1,r0
  bhi u0
  b u1
  
u31
  cmp r0,r1,lsl #30
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #30
u30
  cmp r0,r1,lsl #29
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #29
u29
  cmp r0,r1,lsl #28
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #28
u28
  cmp r0,r1,lsl #27
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #27
u27
  cmp r0,r1,lsl #26
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #26
u26
  cmp r0,r1,lsl #25
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #25
u25
  cmp r0,r1,lsl #24
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #24
u24
  cmp r0,r1,lsl #23
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #23
u23
  cmp r0,r1,lsl #22
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #22
u22
  cmp r0,r1,lsl #21
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #21
u21
  cmp r0,r1,lsl #20
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #20
u20
  cmp r0,r1,lsl #19
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #19
u19
  cmp r0,r1,lsl #18
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #18
u18
  cmp r0,r1,lsl #17
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #17
u17
  cmp r0,r1,lsl #16
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #16
u16
  cmp r0,r1,lsl #15
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #15
u15
  cmp r0,r1,lsl #14
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #14
u14
  cmp r0,r1,lsl #13
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #13
u13
  cmp r0,r1,lsl #12
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #12
u12
  cmp r0,r1,lsl #11
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #11
u11
  cmp r0,r1,lsl #10
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #10
u10
  cmp r0,r1,lsl #9
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #9
u9
  cmp r0,r1,lsl #8
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #8
u8
  cmp r0,r1,lsl #7
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #7
u7
  cmp r0,r1,lsl #6
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #6
u6
  cmp r0,r1,lsl #5
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #5
u5
  cmp r0,r1,lsl #4
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #4
u4
  cmp r0,r1,lsl #3
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #3
u3
  cmp r0,r1,lsl #2
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #2
u2
  cmp r0,r1,lsl #1
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #1
u1
  cmp r0,r1,lsl #0
  adc r2,r2,r2
  subcs r0,r0,r1,lsl #0
u0
  
  mov r1,r0
  mov r0,r2
  mov     PC, lr ;return from subroutine
  
uo
  mov r1,#0
  mov     PC, lr ;return from subroutine
  


FDIVS
  ands r3,r1,#&80000000
  rsbmi r1,r1,#0
  eor r3,r3,r0,asr #1             ;bit 31 = quotsign, bit 30 = remsign
  cmp r0,#0
  rsbmi r0,r0,#0
  
  mov r12,lr
  bl FDIV
  
  tst r3,#&80000000
  rsbne r0,r0,#0
  tst r3,#&40000000
  rsbne r1,r1,#0
  
  mov     PC, r12 ;return from subroutine



; IN :  r0 32 bit unsigned integer
; OUT:  r1 = INT (SQRT (n))
; TMP:  r2
SQRT
	MOV    r2, #3 << 30
	MOV    r1, #1 << 30

	CMP    r0, r1, ROR #0
	SUBHS  r0, r0, r1, ROR #0
	ADC    r1, r2, r1, LSL #1

	CMP    r0, r1, ROR #2
	SUBHS  r0, r0, r1, ROR #2
	ADC    r1, r2, r1, LSL #1

	CMP    r0, r1, ROR #4
	SUBHS  r0, r0, r1, ROR #4
	ADC    r1, r2, r1, LSL #1

	CMP    r0, r1, ROR #6
	SUBHS  r0, r0, r1, ROR #6
	ADC    r1, r2, r1, LSL #1

	CMP    r0, r1, ROR #8
	SUBHS  r0, r0, r1, ROR #8
	ADC    r1, r2, r1, LSL #1

	CMP    r0, r1, ROR #10
	SUBHS  r0, r0, r1, ROR #10
	ADC    r1, r2, r1, LSL #1

	CMP    r0, r1, ROR #12
	SUBHS  r0, r0, r1, ROR #12
	ADC    r1, r2, r1, LSL #1

	CMP    r0, r1, ROR #14
	SUBHS  r0, r0, r1, ROR #14
	ADC    r1, r2, r1, LSL #1

	CMP    r0, r1, ROR #16
	SUBHS  r0, r0, r1, ROR #16
	ADC    r1, r2, r1, LSL #1

	CMP    r0, r1, ROR #18
	SUBHS  r0, r0, r1, ROR #18
	ADC    r1, r2, r1, LSL #1

	CMP    r0, r1, ROR #20
	SUBHS  r0, r0, r1, ROR #20
	ADC    r1, r2, r1, LSL #1

	CMP    r0, r1, ROR #22
	SUBHS  r0, r0, r1, ROR #22
	ADC    r1, r2, r1, LSL #1

	CMP    r0, r1, ROR #24
	SUBHS  r0, r0, r1, ROR #24
	ADC    r1, r2, r1, LSL #1

	CMP    r0, r1, ROR #26
	SUBHS  r0, r0, r1, ROR #26
	ADC    r1, r2, r1, LSL #1

	CMP    r0, r1, ROR #28
	SUBHS  r0, r0, r1, ROR #28
	ADC    r1, r2, r1, LSL #1

	CMP    r0, r1, ROR #30
	SUBHS  r0, r0, r1, ROR #30
	ADC    r1, r2, r1, LSL #1

	BIC    r1, r1, #3 << 30  ; for rounding add: CMP r0, r1   ADC r1, #1
	MOV    r0, r1    ;end result will be in r0

	MOV     PC, lr ;return from subroutine




;// fdivs16
;//
;// long results=idiv16(long numerator,long denominator);
;// assumes numerator, denominator are 16:16
;// results is 16:16

;// register definitions

numerator   RN 0
denominator RN 1
guess       RN 2
t0          RN 0
t1          RN 1
t2          RN 3
n0          RN 6
n1          RN 7
sign        RN 4


FDIVS16
    stmdb     sp!, {r4 - r7, lr}        ;// save r4-r7, return address

    movs sign,denominator               ;// copy denominator to sign updating flags, N flag well be set if negative
    rsbmi denominator, denominator,#0   ;// make it 2s complment

    ;//we need a good guess to start the Newton-Raphson interation
    ;//the logic that follows is from a suggestion by   David Seal (dseal@acorn.co.uk) found at:
    ;//http://www.poppyfields.net/acorn/tech/division.shtml (toward the bottom)

    mov guess,#3                        ;// initial guess, two LSB

    DCD 0xE16F3F11 ;//    clz t2,denominator                  ;// find most significant bit
    sub t2,t2,#1                        ;// we shift the guess up by one less than the number of leading zeros, so dec the count
    mov guess,guess,lsl t2              ;// shift guess up

    ;//now we have our guess for the modified Newton-Raphson iteration
    ;//the following interative convergence algorithm is from:
    ;//Cavanagh, J. 1984. Digital Computer Arithmetic. McGraw-Hill. Page 284.

    smull t2,n1,guess,numerator
    smull t1,n0,guess,denominator
    mov t2,t2,lsr #16                   ;// shift low 32 bits of results down 16
    orr t2,t2,n1,lsl #16                ;// shift high 32 bits up 16 and or in with low, equiv to  c=(long)(((__int64)a*(__int64)b)>>16);
                                        ;// t2=guess*numerator
    mov t1,t1,lsr #16
    orr t1,t1,n0,lsl #16                ;// t1=guess*denominator
	rsb t0,t1,#0x20000

    smull t1,n0,t0,t1                   ;// repeat above
    smull t2,n1,t0,t2
    mov t1,t1,lsr #16
    orr t1,t1,n0,lsl #16
	rsb t0,t1,#0x20000
    mov t2,t2,lsr #16
    orr t2,t2,n1,lsl #16

    smull t1,n0,t0,t1                   ;// repeat above
    smull t2,n1,t0,t2
    mov t1,t1,lsr #16
    orr t1,t1,n0,lsl #16
	rsb t0,t1,#0x20000
    mov t2,t2,lsr #16
    orr t2,t2,n1,lsl #16

    smull t0,n0,t0,t2                   ;// last step
    mov t0,t0,lsr #16
    orr t0,t0,n0,lsl #16

    movs sign,sign                      ;// copy sign whilst updating flags, N flags well be set if negative
	rsbmi t0, t0,#0                     ;// 2s complement


    ldmia     sp!, {r4 - r7, pc}        ;// restore r4-r7 and return


;// fdivs16
;//
;// long results=idiv16(long numerator,long denominator);
;// assumes numerator, denominator are 16:16
;// results is 16:16

;// register definitions

numerator   RN 0
denominator RN 1
guess       RN 2
t0          RN 0
t1          RN 1
t2          RN 3
n0          RN 6
n1          RN 7
sign        RN 4


FDIVS8
    stmdb     sp!, {r4 - r7, lr}        ;// save r4-r7, return address

    movs sign,denominator               ;// copy denominator to sign updating flags, N flag well be set if negative
    rsbmi denominator, denominator,#0   ;// make it 2s complment

    ;//we need a good guess to start the Newton-Raphson interation
    ;//the logic that follows is from a suggestion by   David Seal (dseal@acorn.co.uk) found at:
    ;//http://www.poppyfields.net/acorn/tech/division.shtml (toward the bottom)

    mov guess,#3                        ;// initial guess, two LSB

    DCD 0xE16F3F11 ;//    clz t2,denominator                  ;// find most significant bit
;    sub t2,t2,#8                        ;// we shift the guess up by one less than the number of leading zeros, so dec the count
    add t2,t2,#2                        ;// we shift the guess up by one less than the number of leading zeros, so dec the count
    mov guess,guess,lsl t2              ;// shift guess up

    ;//now we have our guess for the modified Newton-Raphson iteration
    ;//the following interative convergence algorithm is from:
    ;//Cavanagh, J. 1984. Digital Computer Arithmetic. McGraw-Hill. Page 284.

    smull t2,n1,guess,numerator
    smull t1,n0,guess,denominator
    mov t2,t2,lsr #8                   ;// shift low 32 bits of results down 16
    orr t2,t2,n1,lsl #24                ;// shift high 32 bits up 16 and or in with low, equiv to  c=(long)(((__int64)a*(__int64)b)>>16);
                                        ;// t2=guess*numerator
    mov t1,t1,lsr #8
    orr t1,t1,n0,lsl #24                ;// t1=guess*denominator
	rsb t0,t1,#0x200

    smull t1,n0,t0,t1                   ;// repeat above
    smull t2,n1,t0,t2
    mov t1,t1,lsr #8
    orr t1,t1,n0,lsl #24
	rsb t0,t1,#0x200
    mov t2,t2,lsr #8
    orr t2,t2,n1,lsl #24

    smull t1,n0,t0,t1                   ;// repeat above
    smull t2,n1,t0,t2
    mov t1,t1,lsr #8
    orr t1,t1,n0,lsl #24
	rsb t0,t1,#0x200
    mov t2,t2,lsr #8
    orr t2,t2,n1,lsl #24

    smull t0,n0,t0,t2                   ;// last step
    mov t0,t0,lsr #8
    orr t0,t0,n0,lsl #24

    movs sign,sign                      ;// copy sign whilst updating flags, N flags well be set if negative
	rsbmi t0, t0,#0                     ;// 2s complement


    ldmia     sp!, {r4 - r7, pc}        ;// restore r4-r7 and return






; simple, unoptimised CLS routine, hard-coded to a 16bpp 320x240 screen.

        ;   Takes address of screen in R0
        ;   colour in R1

CLEARSCREEN
        stmfd   sp!, {r0-r12,r14}
        add     r1,r1,r1,lsl #16    ; colour into both pixels please
        mov     r2,r1
        mov     r3,r1
        mov     r4,r1
        mov     r5,r1
        mov     r6,r1
        mov     r7,r1
        mov     r8,r1
        mov     r9,r1
        mov     r10,r1
        mov     r11,r1
        mov     r12,r1

        mov r14,#320
clear_screen_arm_inner
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.

        subs    r14,r14,#1
        bne     clear_screen_arm_inner

        ldmfd   sp!, {r0-r12,pc}


 

; simple, unoptimised CLS routine, hard-coded to a 16bpp 320x240 screen.

        ;   Takes address of screen in R0
        ;   colour in R1

CLEARZBUFFER
        stmfd   sp!, {r0-r12,r14}
        mov     r1,#0x7fffffff
        mov     r2,r1
        mov     r3,r1
        mov     r4,r1
        mov     r5,r1
        mov     r6,r1
        mov     r7,r1
        mov     r8,r1
        mov     r9,r1
        mov     r10,r1
        mov     r11,r1
        mov     r12,r1

        mov r14,#640
clear_zbuffer_arm_inner
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.
        stmia   r0!,{r1-r12}    ; store 24 pixels in one shot.

        subs    r14,r14,#1
        bne     clear_zbuffer_arm_inner

        ldmfd   sp!, {r0-r12,pc}


 

    END
 