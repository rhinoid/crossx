#include "stdlib.h"
#include <string.h>

const char *stdlib_strcpy(char *outString1, const char *inString1)
{
	//untill we find a platform which doesn't support this, this solution will do
	strcpy(outString1, inString1);
	return outString1;
}

int stdlib_strlen(const char *inString1)
{
	//untill we find a platform which doesn't support this, this solution will do
	return strlen(inString1);
}

int stdlib_strcmp(const char *inString1, const char *inString2)
{
	//untill we find a platform which doesn't support this, this solution will do
	return strcmp(inString1, inString2);
}

int stdlib_strncmp (const char* inString1, const char* inString2, int inLength)
{
	return strncmp(inString1, inString2, inLength);
}

int stdlib_strnicmp (const char* inString1, const char* inString2, int inLength)
{
 while ((*inString1) || (*inString2)) {
	char theChar1		= *inString1;
	char theChar2		= *inString2;

	if (inLength-- > 0) {
		if ((theChar1 >= 'A') && (theChar1 <= 'Z')) {
			theChar1   += 'a' - 'A';
		}

		if ((theChar2 >= 'A') && (theChar2 <= 'Z')) {
			theChar2   += 'a' - 'A';
		}

		if (theChar1 == theChar2) {
			inString1++;
			inString2++;
		}
		else {
			if (theChar1 < theChar2) {
				return -1;
			}
			else {
				return +1;
			}
		}
	}
	else {
		return 0;
	}
 }
 return 0;
}

int stdlib_stricmp (const char* inString1, const char* inString2)
{
	return stdlib_strnicmp (inString1, inString2, 0x7fffff);
}



void stdlib_memcpy(void *dest, const void *src, int count )
{
	//untill we find a platform which doesn't support this, this solution will do
	memcpy(dest, src, count);
}

char *stdlib_strupr(char *inString1)
{
	char *Ptr = inString1;
	while(*Ptr!=0)
	{
		char c;
		c = *Ptr;
		if(c >= 'a' && c <= 'z')
		{
			c -= 'A'-'a';
			*Ptr = c;
		}

		Ptr++;
	}
	return inString1;
}
