

void stdlib_memcpy(void *dest, const void *src, int count );



const char *stdlib_strcpy(char *outString1, const char *inString1);
int stdlib_strlen(const char *inString1);
int stdlib_strcmp(const char *inString1, const char *inString2);
int stdlib_stricmp(const char*src, const char *dst);
int stdlib_strnicmp (const char* inString1, const char* inString2, int inLength);
int stdlib_strncmp (const char* inString1, const char* inString2, int inLength);
char *stdlib_strupr(char *inString1);

