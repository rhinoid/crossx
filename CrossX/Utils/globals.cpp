#include "globals.h"
#include "2d/surfacecache.h"
#include "3d/math.h"


#if defined _WIN32_WCE
#include "Winbase.h"
#endif


#if !defined(__SYMBIAN32__) || defined(SYMBIAN70)
#undef GLOBALS
Globals* GLOBALS;
#endif

void CalcFullPath();

void Globals::Init()
{
#if !defined(__SYMBIAN32__) || defined(SYMBIAN70)
	GLOBALS = new Globals();
#else
	Globals* d = new Globals();
	Dll::SetTls( d );
#endif

	GLOBALS->m_FullPath[0] = 0;
	CalcFullPath();
	InitMath();

	GLOBALS->g_nullrep_.size = 0;
	GLOBALS->g_nullrep_.capacity = 0; 
	GLOBALS->g_nullrep_.str[0] = 0;

}


XString MakeFullPath(XString Path)
{
	if(Path.length()>1)
	{
		if(Path.at(1)!=':' && Path.at(0)!='\\' && Path.at(0)!='/')   // not an absolute path?
		{
			XString NewPath = GLOBALS->m_FullPath;
			NewPath += Path;
			return NewPath;
		}
	}
	return Path;
}


#if defined WIN32 && !defined _WIN32_WCE
void CalcFullPath( )
{
	strcpy(GLOBALS->m_FullPath, "assets\\");
}
#endif

#if defined _WIN32_WCE
//-------------------------------------------------------------------------
/*!	Converts the given filename to its fullpath using unicode
*///-----------------------------------------------------------------------
unsigned short  s_WFileName[512];
unsigned short  s_WTemp[512];
char            s_Cchar[512];

void CalcFullPath()
{
	mbstowcs(s_WTemp, "", 512);
	GetModuleFileNameW( 0, s_WFileName, _MAX_PATH );
	int pos = wcslen( s_WFileName );
	if (pos) while (--pos) if (s_WFileName[pos] == '\\') break;
	wcscpy( s_WFileName + pos + 1, s_WTemp );

	wcstombs(GLOBALS->m_FullPath, s_WFileName, 512);
}
#endif

#ifdef __SYMBIAN32__

#include <coecntrl.h>
#include <coecobs.h>
#include <eiklabel.h>
#include <eikspane.h>
#include <eikapp.h>
#include <eikdoc.h>
#include <e32std.h>
void CalcFullPath()
{

#ifdef SYMBIAN70
//	strcpy( GLOBALS->m_FullPath, "\\private\\A000017F\\" );   //@@@
	strcpy( GLOBALS->m_FullPath, "\\private\\E0001001\\" );   //@@@

#else

	char m_LocalPath[512];
	TFileName appname = CEikonEnv::Static()->EikAppUi()->Application()->AppFullName();
	int pos = 0;
	const unsigned short* p = appname.Ptr();
	while (*p) m_LocalPath[pos++] = *p++;
	m_LocalPath[pos] = 0;
	char* tp = strstr( m_LocalPath, ".app" );
	while (*tp != '\\') *tp-- = 0;

	strcpy( GLOBALS->m_FullPath, m_LocalPath );
#endif
}

#endif

