#ifndef _H_GLOBALS
#define _H_GLOBALS

#include "utils/types.h"

#ifdef __SYMBIAN32__

#ifdef NULL    //to remove compile warnings in gnu
#undef NULL
#endif
#include <e32std.h>

#endif


class Launcher3D;
class TexManager;
class CSurfaceCache;

struct Rep
{
	unsigned int size, capacity;
	char str[1];
};


struct Globals
{
public:
	Globals() : m_TexManager(0), m_SurfaceCache(0), m_Fixed16SinTab(0), m_Fixed8SinTab(0) { }

	static void Init();		// NEEDS TO BE CALLED FIRST THING IN THE PROGRAM

//room for certain stuff which needs to be accessible everywhere
	TexManager   *m_TexManager;
	CSurfaceCache *m_SurfaceCache;

	int16      *m_Fixed16SinTab;	//precalced sin tables fixedpoint (used by fixed16)
	int16      *m_Fixed8SinTab;		//precalced sin tables fixedpoint (used by fixed8)

	// User defined space
	void       *m_MainClass;        //Storage for pointer to the main class
	void       *m_UserData;			//Pointer to user defined storage

	//constants omdat symbian zuigt
	int			PI;
	int			TWO_PI;
	int			HALF_PI;
	char		m_FullPath[512];   // path preappend part to make fullpath

	int			g_Matrix4Ident[16];
	Rep			g_nullrep_;;  //needed for tinyxml and xstring
};



#if !defined(__SYMBIAN32__) || defined(SYMBIAN70)
	extern Globals* GLOBALS;
#else
	#define GLOBALS ((Globals*)Dll::Tls())

#endif

class XString;

XString MakeFullPath(XString Path);


#endif
