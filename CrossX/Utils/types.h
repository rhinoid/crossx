#ifndef _TYPES_H
#define _TYPES_H


typedef unsigned char BYTE;
typedef unsigned char uint8;
typedef unsigned short uint16;
typedef unsigned int uint32;
typedef signed char int8;
typedef signed short int16;
typedef signed int int32;

#if defined(__SYMBIAN32__) && !defined (__WINS__) 
typedef unsigned long long uint64;
typedef signed long long int64;
#else
typedef unsigned __int64 uint64;
typedef signed __int64 int64;
#endif


#endif
