// TestGame.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "resource.h"

#define MAX_LOADSTRING 100

#include "../../crossx.h"

// Global Variables:
HWND gHWnd;
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];								// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];								// The title bar text

// Foward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK	About(HWND, UINT, WPARAM, LPARAM);
void				BlitBackbufToWindow(void);
CROSSX_KEY			MapKeyToGame(int VirtKey);

CSurface16 gBackbuffer;
CGame   *gGame;


#ifdef _WIN32_WCE
int WINAPI WinMain(	HINSTANCE hInstance,
					HINSTANCE hPrevInstance,
					LPTSTR    lpCmdLine,
					int       nCmdShow)
#else
int APIENTRY WinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPSTR     lpCmdLine,
                     int       nCmdShow)
#endif
{
 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_TESTGAME, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Create backbuffer
#ifdef CROSSX_SYMBIAN60
	int Width = 176, Height = 208;
#endif
#ifdef CROSSX_SYMBIANUIQ
	int Width = 208, Height = 320;
#endif
#ifdef CROSSX_POCKETPC
	int Width = 240, Height = 320;
#endif
	Width = 176;
	Height = 208;
	gBackbuffer.Create(Width, Height);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow)) 
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, (LPCTSTR)IDC_TESTGAME);

	gGame = new CROSSX_GAMECLASS;
	gGame->CRSX_SetBackbuf(&gBackbuffer);
	gGame->Init();						//initialize game
	
	// Main message loop, transformed to support idle-time processing:
	int PrevTime = GetTickCount();
	while( 1 )
    {
		if( PeekMessage( &msg, NULL, 0, 0, PM_NOREMOVE ) )
		{
			if( !GetMessage( &msg, NULL, 0, 0 ) )
				return (int)msg.wParam;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
        else
		{
			int CurTime = GetTickCount();
			gGame->Tick(CurTime-PrevTime);				//Call game
			PrevTime = CurTime;
			BlitBackbufToWindow();
		}
	}
	
	return msg.wParam;
}


typedef struct tagBITMAPINFORHINO {
    BITMAPINFOHEADER    bmiHeader;
    RGBQUAD             bmiColors[3];
} BITMAPINFORHINO;

void BlitBackbufToWindow(void)
{
    HDC             hdc;          // HDC of the DIB
    HBITMAP         hbm;          // HBITMAP of the DIB   
    HBITMAP         hbmOld;       // old HBITMAP from the hdc
    unsigned short *pSrcBits;     // pointer to DIB pixel array

    // fill in the BITMAPINFO structure
    BITMAPINFORHINO bmi;
    ZeroMemory(&bmi, sizeof(BITMAPINFORHINO));
    bmi.bmiHeader.biSize            =   sizeof(BITMAPINFOHEADER);
    bmi.bmiHeader.biWidth           =   gBackbuffer.GetWidth();
    bmi.bmiHeader.biHeight          =   -gBackbuffer.GetHeight();
    bmi.bmiHeader.biPlanes          =   1;
    bmi.bmiHeader.biBitCount        =   16;
    bmi.bmiHeader.biCompression     =   BI_BITFIELDS;
	bmi.bmiHeader.biClrUsed         =   3;
	int *maskcolors;
	maskcolors = (int *) bmi.bmiColors;
	*maskcolors++ = 0xf800;
	*maskcolors++ = 0x07E0;
	*maskcolors++ = 0x001F;
//    bmi.bmiHeader.biCompression     =   BI_RGB;
    bmi.bmiHeader.biXPelsPerMeter   =   72;
    bmi.bmiHeader.biYPelsPerMeter   =   72;

    // Get hdc of screen for CreateDIBSection and create the DIB
    HDC hdcScreen = GetDC(gHWnd);
    hbm = CreateDIBSection(
                hdcScreen, (BITMAPINFO *)&bmi, DIB_RGB_COLORS, 
                (void**)&pSrcBits, NULL, 0);

    // Create and select the bitmap into a DC
    hdc = CreateCompatibleDC(hdcScreen);        
    hbmOld = (HBITMAP)SelectObject(hdc, hbm);

    // Copy the backbuffer into the DIB
	int x,y;
	uint16 *Source, *Dest;
	Source = (uint16 *)gBackbuffer.GetBuffer();
	Dest = pSrcBits;
	for(y=0; y<gBackbuffer.GetHeight(); y++)
	{
		for(x=0; x<gBackbuffer.GetWidth(); x++)
		{
			*Dest++ = *Source++;	//copy bitmap
		}
	}

    // all GDI functions must be flushed before we can use the DIB
#ifndef _WIN32_WCE
    GdiFlush();
#endif

	//blit the image to the screen
    BitBlt(hdcScreen, 0, 0, gBackbuffer.GetWidth(), gBackbuffer.GetHeight(), hdc, 0, 0, SRCCOPY);

    ReleaseDC(NULL, hdcScreen);

    SelectObject(hdc, hbmOld);          
    DeleteObject(hbm);
    DeleteDC(hdc);
}


//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
//  COMMENTS:
//
//    This function and its usage is only necessary if you want this code
//    to be compatible with Win32 systems prior to the 'RegisterClassEx'
//    function that was added to Windows 95. It is important to call this function
//    so that the application will get 'well formed' small icons associated
//    with it.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{

#ifdef _WIN32_WCE
	WNDCLASS	wc;

    wc.style			= CS_HREDRAW | CS_VREDRAW;
    wc.lpfnWndProc		= (WNDPROC) WndProc;
    wc.cbClsExtra		= 0;
    wc.cbWndExtra		= 0;
    wc.hInstance		= hInstance;
    wc.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_SMALL));
    wc.hCursor			= 0;
    wc.hbrBackground	= (HBRUSH) GetStockObject(WHITE_BRUSH);
    wc.lpszMenuName		= 0;
    wc.lpszClassName	= szWindowClass;
	return RegisterClass(&wc);
#else
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX); 

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= (WNDPROC)WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, (LPCTSTR)IDI_TESTGAME);
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= 0;
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SMALL);
	return RegisterClassEx(&wcex);
#endif

}

//
//   FUNCTION: InitInstance(HANDLE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{


   hInst = hInstance; // Store instance handle in our global variable

#ifdef _WIN32_WCE
	gHWnd = CreateWindow(szWindowClass, szTitle, WS_VISIBLE,
		CW_USEDEFAULT, CW_USEDEFAULT, gBackbuffer.GetWidth(), gBackbuffer.GetHeight(), NULL, NULL, hInstance, NULL);
#else
	//account for the windows borders (not CE version)
	int OffsetX, OffsetY;
	OffsetX = GetSystemMetrics(SM_CXDRAG);
	OffsetY = GetSystemMetrics(SM_CYCAPTION);
	OffsetY += GetSystemMetrics(SM_CYDRAG);
	OffsetY += GetSystemMetrics(SM_CYDRAG);
	OffsetX += GetSystemMetrics(SM_CXDRAG);
	gHWnd = CreateWindow( szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, gBackbuffer.GetWidth()+OffsetX, gBackbuffer.GetHeight()+OffsetY, NULL, NULL, hInstance, NULL);
#endif

   if (!gHWnd)
   {
      return FALSE;
   }

   ShowWindow(gHWnd, nCmdShow);
   UpdateWindow(gHWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, unsigned, WORD, LONG)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	CROSSX_KEY key;

	switch (message) 
	{
		case WM_COMMAND:
			wmId    = LOWORD(wParam); 
			wmEvent = HIWORD(wParam); 
			// Parse the menu selections:
			switch (wmId)
			{
				case IDM_ABOUT:
				   DialogBox(hInst, (LPCTSTR)IDD_ABOUTBOX, hWnd, (DLGPROC)About);
				   break;
				case IDM_EXIT:
				   DestroyWindow(hWnd);
				   break;
				default:
				   return DefWindowProc(hWnd, message, wParam, lParam);
			}
			break;
		case WM_KEYDOWN:
			key = MapKeyToGame(wParam);
			if(key!=-1)gGame->HandleKeyDown(key);
			break;
		case WM_KEYUP:
			key = MapKeyToGame(wParam);
			if(key!=-1)gGame->HandleKeyUp(key);
			break;
		case WM_LBUTTONDOWN:
			{
				int x,y;
				x = LOWORD(lParam);
				y = HIWORD(lParam);
	 			gGame->HandleMouseDown(x, y);
			}
			break;
		case WM_MOUSEMOVE:
			{
				int x,y;
				x = LOWORD(lParam);
				y = HIWORD(lParam);
	 			gGame->HandleMouseMove(x, y);
			}
			break;
		case WM_LBUTTONUP:
			{
				int x,y;
				x = LOWORD(lParam);
				y = HIWORD(lParam);
	 			gGame->HandleMouseUp(x, y);
			}
			break;
		case WM_PAINT:
			hdc = BeginPaint(hWnd, &ps);
			EndPaint(hWnd, &ps);
			break;
		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
   }
   return 0;
}

// Mesage handler for about box.
LRESULT CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
		case WM_INITDIALOG:
				return TRUE;

		case WM_COMMAND:
			if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
			{
				EndDialog(hDlg, LOWORD(wParam));
				return TRUE;
			}
			break;
	}
    return FALSE;
}


//translates windows keys to CrossX keymappings
CROSSX_KEY MapKeyToGame(int VirtKey)
{
	//0-9 from 'phone keypad'
	if(VirtKey>='0'&&VirtKey<='9') return (CROSSX_KEY)VirtKey;

	// map fire/select button
	if(VirtKey==VK_SPACE) return CROSSX_KEY_FIRE;

	//map left softkey on insert button of windows
	if(VirtKey==VK_INSERT) return CROSSX_KEY_LEFTMENU;

	//map right softkey on del button of windows
	if(VirtKey==VK_DELETE) return CROSSX_KEY_RIGHTMENU;

	//left
	if(VirtKey==VK_LEFT) return CROSSX_KEY_LEFT;
	//right
	if(VirtKey==VK_RIGHT) return CROSSX_KEY_RIGHT;
	//up
	if(VirtKey==VK_UP) return CROSSX_KEY_UP;
	//ldown
	if(VirtKey==VK_DOWN) return CROSSX_KEY_DOWN;

	return CROSSX_UNKNOWN;
}


