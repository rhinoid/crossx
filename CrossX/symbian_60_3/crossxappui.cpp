/*
* ==============================================================================
*  Name        : helloworldbasicappui.cpp
*  Part of     : Helloworldbasic
*  Interface   : 
*  Description : 
*  Version     : 
*
*  Copyright (c) 2005-2006 Nokia Corporation.
*  This material, including documentation and any related 
*  computer programs, is protected by copyright controlled by 
*  Nokia Corporation.
* ==============================================================================
*/

// INCLUDE FILES
#include <avkon.hrh>
#include <aknnotewrappers.h>
#include <stringloader.h>
#include <CrossX.rsg>
#include <f32file.h>
#include <s32file.h>

#include "CrossX.pan"
#include "CrossXAppUi.h"
#include "CrossXAppView.h"
#include "CrossX.hrh"


// ============================ MEMBER FUNCTIONS ===============================


// -----------------------------------------------------------------------------
// CHelloWorldBasicAppUi::ConstructL()
// Symbian 2nd phase constructor can leave.
// -----------------------------------------------------------------------------
//
void CHelloWorldBasicAppUi::ConstructL()
    {
    // Initialise app UI with standard value.
    BaseConstructL(CAknAppUi::EAknEnableSkin);

    // Create view object
    iAppView = CHelloWorldBasicAppView::NewL( ClientRect() );

    
    }
// -----------------------------------------------------------------------------
// CHelloWorldBasicAppUi::CHelloWorldBasicAppUi()
// C++ default constructor can NOT contain any code, that might leave.
// -----------------------------------------------------------------------------
//
CHelloWorldBasicAppUi::CHelloWorldBasicAppUi()
    {
    // No implementation required
    }

// -----------------------------------------------------------------------------
// CHelloWorldBasicAppUi::~CHelloWorldBasicAppUi()
// Destructor.
// -----------------------------------------------------------------------------
//
CHelloWorldBasicAppUi::~CHelloWorldBasicAppUi()
    {
    if ( iAppView )
        {
        delete iAppView;
        iAppView = NULL;
        }

    }


void CHelloWorldBasicAppUi::HandleForegroundEventL(TBool aForeground)
{
	if (iAppView)
	{
		if(aForeground==false)
		{
			iAppView->AbortNow(RDirectScreenAccess::ETerminateRegion);
		}
		if(aForeground==true)
		{
			/*
			 // Construct en empty TApaTask object
			 // giving it a reference to the Window Server session
			 TApaTask task(iEikonEnv->WsSession( ));

			 // Initialise the object with the window group id of 
			 // our application (so that it represent our app)
			 task.SetWgId(CEikonEnv::Static()->RootWin().Identifier());

			 // Request window server to bring our application
			 // to foreground
			 task.BringToForeground(); 
//			 task.SetPriority (350) ;  //TProcessPriority::EPriorityForeground
*/
			 iAppView->Restart(RDirectScreenAccess::ETerminateRegion);

		}
	}
	CAknAppUi::HandleForegroundEventL(aForeground);
}



// ----------------------------------------------------
// CSymbianAppUi::HandleKeyEventL(
//     const TKeyEvent& aKeyEvent,TEventCode /*aType*/)
// ?implementation_description
// ----------------------------------------------------
//
TKeyResponse CHelloWorldBasicAppUi::HandleKeyEventL(const TKeyEvent& Event/*aKeyEvent*/,TEventCode Code/*aType*/)
{
	switch(Code)
	{
	case EEventKeyDown:
		if(Event.iScanCode == EStdKeyHash)	// hash key is fast exit
		{
			Exit();
			return EKeyWasConsumed;
		}

		if(iAppView->HandleKeyDown((TStdScanCode) Event.iScanCode))
		{
			return EKeyWasConsumed;
		}
		break;

	case EEventKeyUp:
		if(iAppView->HandleKeyUp((TStdScanCode)Event.iScanCode))
		{
			return EKeyWasConsumed;
		}
		break;

	case EEventKey:
		if(iAppView->HandleKey((TStdScanCode)Event.iScanCode))
		{
			return EKeyWasConsumed;
		}
		break;
	}
    return EKeyWasNotConsumed;
}

// -----------------------------------------------------------------------------
// CHelloWorldBasicAppUi::HandleCommandL()
// Takes care of command handling.
// -----------------------------------------------------------------------------
//
void CHelloWorldBasicAppUi::HandleCommandL( TInt aCommand )
    {
    switch( aCommand )
        {
        case EEikCmdExit:
        case EAknSoftkeyExit:
            Exit();
            break;

        default:
            break;
        }
    }
// -----------------------------------------------------------------------------
//  Called by the framework when the application status pane
//  size is changed.  Passes the new client rectangle to the
//  AppView
// -----------------------------------------------------------------------------
//
void CHelloWorldBasicAppUi::HandleStatusPaneSizeChange()
{
	iAppView->SetRect( ClientRect() );
	
} 

// End of File

