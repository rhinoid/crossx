/*
* ==============================================================================
*  Name        : helloworldbasicappview.cpp
*  Part of     : Helloworldbasic
*  Interface   : 
*  Description : 
*  Version     : 
*
*  Copyright (c) 2005-2006 Nokia Corporation.
*  This material, including documentation and any related 
*  computer programs, is protected by copyright controlled by 
*  Nokia Corporation.
* ==============================================================================
*/

// INCLUDE FILES
#include <coemain.h>
#include "CrossXAppView.h"
#include <w32std.h>
#include <e32std.h>


#include <coecntrl.h>
#include <coecobs.h>
//	#include "SymbianApp.h"
#include <eiklabel.h>
#include <eikspane.h>
#include <avkon.hrh>
#include <akndoc.h>
#include <aknapp.h>
#include <eikapp.h>
#include <eikdoc.h>
#include <e32std.h>
#include <bautils.h>
#include <utf.h>
#include <coeccntx.h>
#include <coemain.h>
#include <aknappui.h>
#include <aknsoundsystem.h>


#define HEAPSIZE (1024*1024*4)

// ============================ MEMBER FUNCTIONS ===============================

// -----------------------------------------------------------------------------
// CHelloWorldBasicAppView::NewL()
// Two-phased constructor.
// -----------------------------------------------------------------------------
//
CHelloWorldBasicAppView* CHelloWorldBasicAppView::NewL( const TRect& aRect )
    {
    CHelloWorldBasicAppView* self = CHelloWorldBasicAppView::NewLC( aRect );
    CleanupStack::Pop( self );
    return self;
    }

// -----------------------------------------------------------------------------
// CHelloWorldBasicAppView::NewLC()
// Two-phased constructor.
// -----------------------------------------------------------------------------
//
CHelloWorldBasicAppView* CHelloWorldBasicAppView::NewLC( const TRect& aRect )
    {
    CHelloWorldBasicAppView* self = new ( ELeave ) CHelloWorldBasicAppView;
    CleanupStack::PushL( self );
    self->ConstructL( aRect );
    return self;
    }

// -----------------------------------------------------------------------------
// CHelloWorldBasicAppView::ConstructL()
// Symbian 2nd phase constructor can leave.
// -----------------------------------------------------------------------------
//
void CHelloWorldBasicAppView::ConstructL( const TRect& aRect )
    {
    // Create a window for this application view
    CreateWindowL();

    // Set the windows size
    SetRect( aRect );
	SetExtentToWholeScreen();

    // Activate the window, which makes it ready to be drawn
    ActivateL();

	// these values should be filled out by some kind of system query
	CWindowGc& gc = SystemGc();
	CWsScreenDevice *pWDevice=(CWsScreenDevice *)gc.Device();
	TPixelsAndRotation pix;
	pWDevice->GetDefaultScreenSizeAndRotation(pix);
	m_Width=pix.iPixelSize.iWidth;
	m_Height=pix.iPixelSize.iHeight;
	m_Game = 0;
	
//	m_Width = 176*2;
//	m_Height = 208*2;
	m_ExitFlg = 0;
	m_PixelFormat = EColor4K;

	m_OrgBuffer = new unsigned short [176*208];


	TTime currentTime;
	currentTime.HomeTime();
	m_LastTick = currentTime.Int64();


	iAvkonAppUi->KeySounds()->PushContextL(R_AVKON_SILENT_SKEY_LIST);


	// Initialise DSA
	m_DrawingAllowed = true;
	iDirectScreenAccess = CDirectScreenAccess::NewL(iEikonEnv->WsSession(), *iEikonEnv->ScreenDevice(), Window(), *this);
	iDirectScreenAccess->StartL();

	iDSBitmap = CDirectScreenBitmap::NewL();
	TRect MyRect = TRect(0, 0, m_Width, m_Height);
	iDSBitmap->Create(MyRect, CDirectScreenBitmap::EDoubleBuffer);

	// start timer
	iIdleTimer = CIdle::NewL(CActive::EPriorityIdle);
	iIdleTimer->Start(TCallBack(CHelloWorldBasicAppView::Period,this));


	//toggle the backlight on
	m_LightDelay = 0;
	m_InitFlag = 1;

    }

void CHelloWorldBasicAppView::Restart(RDirectScreenAccess::TTerminationReasons aReason)
{
	m_DrawingAllowed = true;
}

void CHelloWorldBasicAppView::AbortNow(RDirectScreenAccess::TTerminationReasons aReason)
{
	if(m_Game) m_Game->m_FocusWasLost = true;
	m_DrawingAllowed = false;
}


// -----------------------------------------------------------------------------
// CHelloWorldBasicAppView::CHelloWorldBasicAppView()
// C++ default constructor can NOT contain any code, that might leave.
// -----------------------------------------------------------------------------
//
CHelloWorldBasicAppView::CHelloWorldBasicAppView()
    {
    // No implementation required
    }


// -----------------------------------------------------------------------------
// CHelloWorldBasicAppView::~CHelloWorldBasicAppView()
// Destructor.
// -----------------------------------------------------------------------------
//
CHelloWorldBasicAppView::~CHelloWorldBasicAppView()
    {
    // No implementation required
	delete iIdleTimer;
	iIdleTimer = NULL;
    }


// -----------------------------------------------------------------------------
// CHelloWorldBasicAppView::Draw()
// Draws the display.
// -----------------------------------------------------------------------------
//
void CHelloWorldBasicAppView::Draw( const TRect& /*aRect*/ ) const
    {
	/*
    // Get the standard graphics context
    CWindowGc& gc = SystemGc();

    // Gets the control's extent
    TRect drawRect( Rect());

    // Clears the screen
    gc.Clear( drawRect );

	gc.BitBlt(TPoint(0,0), &m_OffscreenBitmap);
	*/
  	}

// -----------------------------------------------------------------------------
// CHelloWorldBasicAppView::SizeChanged()
// Called by framework when the view size is changed.
// -----------------------------------------------------------------------------
//
void CHelloWorldBasicAppView::SizeChanged()
    {  
    DrawNow();
    }

// This function is called by the periodic timer
TInt CHelloWorldBasicAppView::Period(TAny *aPtr)
{
    //returning a value of TRUE indicates the callback should be done again
	return ((CHelloWorldBasicAppView*)aPtr)->Tick();
}

// non static variant of the timer function
TInt CHelloWorldBasicAppView::Tick()
{
	TInt RetVal=1;

	TTime nowtime;
	nowtime.UniversalTime();
	TTimeIntervalMicroSeconds tti;
	tti = nowtime.MicroSecondsFrom(m_TimeMark);
	m_TimeMark = nowtime;
	int millis = (tti.Int64()&0xffffffff)  / 1000;
	if (millis >= 1 && millis < 50)
	{
		int wait = 50-millis;
		User::After(TTimeIntervalMicroSeconds32(wait*1000));
	}	
	
	
	// make sure the backlight stays on
	m_LightDelay++;
	m_LightDelay&=63;
	if(m_LightDelay==0)
	{
		User::ResetInactivityTime(); 
	}


	//start direct screen access
	
	if(m_DrawingAllowed)
	{
	iDirectScreenAccess->ScreenDevice()->Update();

	TAcceleratedBitmapInfo bitmapInfo;
	iDSBitmap->BeginUpdate(bitmapInfo);        

	unsigned int *BackBuffer;
	BackBuffer = (unsigned int *)bitmapInfo.iAddress;



	// do the stuff	
	if(m_InitFlag==0)
	{
	//		if(m_LightDelay&1)
			{
		// Calculate elapsed time from last timer event in microseconds
				if(!m_Game->Tick(millis)) m_ExitFlg = 1;

				//stretch screen to phonescreen
				int x,y;
				int sy;
				sy = 0;
				int dtx = (176*256) / m_Width;
				int dty = (208*256) / m_Height;
				unsigned short *src;
				unsigned int *dst;
				src = m_OrgBuffer;
				dst = BackBuffer;
				for(y=0; y<m_Height; y++)
				{
					unsigned short *lsrc;
					lsrc = src + ((sy>>8)*176);

					int sx=0;
					for(x=0; x<m_Width; x++)
					{
						unsigned short Pixel;
						int r,g,b;
						Pixel = lsrc[sx>>8];  //convert from 16 bit (0-4-4-4) to 32 bit (0-8-8-8)
						b = Pixel&0xf;
						g = Pixel&0xf0;
						r = Pixel&0xf00;
						*dst++  = (r<<12) + (g<<8) + (b<<4);
		
						sx += dtx;
					}
					sy += dty;
				}

	}
		}
	else
	{
			//clear screen
			int y,x;
			unsigned int *ptr = BackBuffer;
			for(y=0; y<m_Height; y++)
			{
				for(x=0; x<m_Width; x++)
				{
					*ptr++ = 0;
				}
			}

			
		RHeap *heap = UserHeap::ChunkHeap(NULL,HEAPSIZE,HEAPSIZE);   //make sure we can allocate more memory
		User::SwitchHeap(heap);

			m_Game = new CGame();
			m_Game->_Init(m_OrgBuffer, 176, 208);	//Link the game to the backsurface
		m_Game->Init();						//initialize game
		m_InitFlag = 0;
	}

	//end screen access

	iDsb_active.Cancel();
	iDSBitmap->EndUpdate(iDsb_active.iStatus);
	iDsb_active.SetActive();
	iEikonEnv->WsSession().Flush();
	}

	// draw the graphics on screen
//	CWindowGc& gc = SystemGc();
//	gc.Activate(*DrawableWindow());
//	gc.BitBlt(TPoint(0,0), &m_OffscreenBitmap);
//	gc.Deactivate();

	if(m_ExitFlg)
	{
		User::Exit(0);
	}
	return 1;
}





bool CHelloWorldBasicAppView::HandleKey(TStdScanCode KeyCode)
{
	return false;
}


bool CHelloWorldBasicAppView::HandleKeyDown(TStdScanCode KeyCode)
{
	if(m_Game) m_Game->HandleKeyDown(KeyCode);
	return false;
}

bool CHelloWorldBasicAppView::HandleKeyUp(TStdScanCode KeyCode)
{
	if(m_Game) m_Game->HandleKeyUp(KeyCode);
	return false;
}


// End of File

