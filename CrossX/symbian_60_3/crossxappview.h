/*
* ==============================================================================
*  Name        : helloworldbasicappview.h
*  Part of     : Helloworldbasic
*  Interface   : 
*  Description : 
*  Version     : 
*
*  Copyright (c) 2005-2006 Nokia Corporation.
*  This material, including documentation and any related 
*  computer programs, is protected by copyright controlled by 
*  Nokia Corporation.
* ==============================================================================
*/

#ifndef __HELLOWORLDBASICAPPVIEW_H__
#define __HELLOWORLDBASICAPPVIEW_H__

// INCLUDES
#include <coecntrl.h>
#include <W32STD.H>
#include <cdsb.h>

#include "crossx.h"



class C_dsb_active: public CActive{
public:
   C_dsb_active():
      CActive(CActive::EPriorityHigh)
   {
      CActiveScheduler::Add(this);
   }
   virtual void RunL(){ }
   virtual void DoCancel(){ }
   void SetActive(){ CActive::SetActive(); }
};

// CLASS DECLARATION
class CHelloWorldBasicAppView : public CCoeControl, public MDirectScreenAccess
    {
    public: // New methods

        /**
        * NewL.
        * Two-phased constructor.
        * Create a CHelloWorldBasicAppView object, which will draw itself to aRect.
        * @param aRect The rectangle this view will be drawn to.
        * @return a pointer to the created instance of CHelloWorldBasicAppView.
        */
        static CHelloWorldBasicAppView* NewL( const TRect& aRect );

        /**
        * NewLC.
        * Two-phased constructor.
        * Create a CHelloWorldBasicAppView object, which will draw itself
        * to aRect.
        * @param aRect Rectangle this view will be drawn to.
        * @return A pointer to the created instance of CHelloWorldBasicAppView.
        */
        static CHelloWorldBasicAppView* NewLC( const TRect& aRect );

        /**
        * ~CHelloWorldBasicAppView
        * Virtual Destructor.
        */
        virtual ~CHelloWorldBasicAppView();

    public:  // Functions from base classes

        /**
        * From CCoeControl, Draw
        * Draw this CHelloWorldBasicAppView to the screen.
        * @param aRect the rectangle of this view that needs updating
        */
        void Draw( const TRect& aRect ) const;

        /**
        * From CoeControl, SizeChanged.
        * Called by framework when the view size is changed.
        */
        virtual void SizeChanged();


		//SYMBIAN FRAMEWORK ADDITIONS
		bool Init(void);

		bool HandleKey(TStdScanCode KeyCode);
		bool HandleKeyUp(TStdScanCode KeyCode);
		bool HandleKeyDown(TStdScanCode KeyCode);
        

		TInt Tick(void);
		static TInt Period(TAny* aPtr);


		int			m_Width;			// width of offscreen bitmap
		int			m_Height;			// height of offscreen bitmap
		int			m_ExitFlg;
		int			m_LightDelay;		// delay counter to make sure we keep the backlight on


		CGame      *m_Game;
		unsigned short *m_OrgBuffer;    //org buffer where game runs on

		CFbsBitmap m_OffscreenBitmap;	// offscreen bitmap where we'll draw on
		unsigned short *m_PixelBuffer;  // the pixelbuffer belonging to the bitmal

		TDisplayMode m_PixelFormat;		// pixelformat of offscreen bitmap
		CPeriodic* iPeriodicTimer;		// timer
		TInt64	m_LastTick;				// How much time did the frame draw take?
		int     m_InitFlag;

		
		CIdle    * iIdleTimer;
		TTime      m_TimeMark;
		
// Direct Screen Access
		CDirectScreenAccess* iDirectScreenAccess;
		CDirectScreenBitmap* iDSBitmap;
		C_dsb_active iDsb_active;
		int		m_DrawingAllowed;

// Implement MDirectScreenAccess
		void Restart(RDirectScreenAccess::TTerminationReasons aReason);
		void AbortNow(RDirectScreenAccess::TTerminationReasons aReason);


    private: // Constructors

        /**
        * ConstructL
        * 2nd phase constructor.
        * Perform the second phase construction of a
        * CHelloWorldBasicAppView object.
        * @param aRect The rectangle this view will be drawn to.
        */
        void ConstructL(const TRect& aRect);

        /**
        * CHelloWorldBasicAppView.
        * C++ default constructor.
        */
        CHelloWorldBasicAppView();

    };

#endif // __HELLOWORLDBASICAPPVIEW_H__

// End of File

