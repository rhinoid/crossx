This is a Cross Platform Library I wrote to aid creating games and apps for Mobile devices.
The platform it supports (most not relevant anymore) are: Symbian 60, Symbian 60 3rd, Symbian UIQ, Windows Mobile, PocketPC and Windows. The Windows build was primarily used for development and ease of debugging.

Among its features are:

* 3d subpixel Rendering/Rasterizing with fixed point math. (Does not rely on OpenGL ES)
* Own Audio mixer with support for 16 channel synth audio
* Layout engine to layout widgets
* Own set of custom widgets with highlights, select states, dropdownboxes, sliders, etc

If anyone feels brave, with just adding a few base files, any other platform could be supported as well. I would not recommend it though since there are so many libraries out there nowadays with better support.